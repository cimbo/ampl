# SETS

set V ordered;

set V_reduced := {i in V : ord(i) > 1};

param n := card(V);

set E := {i in V, j in V: ord(i) <> ord(j)};

# PARAMETERS


# VARIABLES


# OBJECTIVE FUNCTION


# CONSTRAINTS

