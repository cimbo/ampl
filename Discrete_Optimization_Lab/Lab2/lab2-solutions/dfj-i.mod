#dfj-i.mod

set V ordered;
param n := card {V};

set POWERSET := 0 .. (2**n - 1);

set S {k in POWERSET} := {i in V: (k div 2**(ord(i)-1)) mod 2 = 1};

set E := {i in V, j in V: ord(i) <> ord(j)};

#add parameters declarations

#add variables declarations

#add model