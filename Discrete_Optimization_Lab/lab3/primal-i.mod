#primal.mod

param m;
param n;
set I := 1..m;
set J := 1..n;

param c{I,J};
param w{I,J};
param b{J};

param nc >= 0;
set CUTS := 1..nc;

set C{CUTS} within I;
param J_bar{CUTS} symbolic within J;

#Variables

#Objective function

#Constraints
