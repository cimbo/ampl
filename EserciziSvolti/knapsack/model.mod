# Set of items
param n > 0;
set I := 1..n;

# Profit
param p{I};

# Weigths
param w{I};

# Knapsack capacity
param B >= 0;

#--------------------------------------------------
#                 Knapsack problem
#--------------------------------------------------

# Binary knasack variables
var x {I} binary;#>= 0, <= 1;

# Obj function
maximize Profit:
	sum {i in I} p[i]*x[i];

subject to Capacity:
	sum {i in I} w[i]*x[i] <= B;