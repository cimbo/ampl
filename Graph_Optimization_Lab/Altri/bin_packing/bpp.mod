
# ----------------------------------------
# Bin Packing - Descriptive Formulation
# ----------------------------------------

param m integer;
set ITEMS := 1..m;

param n integer;
set BINS := 1..n;

param d{ITEMS};
param b;

var y{BINS} binary;   
var x{ITEMS,BINS} binary;
# var x{CLIENTS,FACILITIES} >= 0;

minimize Number:
  sum {j in BINS} y[j];   

subj to Packed {i in ITEMS}:
   sum {j in BINS} x[i,j] = 1;

subj to Used {i in ITEMS, j in BINS}:
  x[i,j] <= y[j];

subj to Capacity {j in BINS}:
   sum {i in ITEMS} d[i] * x[i,j] <= b * y[j];
