
# ----------------------------------------
# Bin Packing - Column Generation
# ----------------------------------------

param m;
set ITEMS := 1..m;
param d{ITEMS};
param b;

set canS := 1..(2^m)-1;
set setcanS{S in canS} := {i in ITEMS : (S div 2^(i-1)) mod 2 == 1};

param nS; 
set calS := 1..nS;
set setS{calS};

var x{S in calS} binary;

minimize Number:
  sum{S in calS} x[S];   

subj to Packed{i in ITEMS}:
  sum {S in calS : i in setS[S]} x[S] = 1;

