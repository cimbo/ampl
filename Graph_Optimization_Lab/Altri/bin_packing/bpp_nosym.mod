
# ----------------------------------------
# Bin Packing - Descriptive Formulation
# ----------------------------------------

param m integer;
set ITEMS := 1..m;

param n integer;
set BINS := 1..n;

param d{ITEMS};
param b;

var y{BINS} binary;   
var x{ITEMS,BINS} binary;

minimize Number:
  sum {j in BINS} y[j];   

subj to Packed {i in ITEMS}:
   sum {j in BINS} x[i,j] = 1;

subj to Used {i in ITEMS, j in BINS}:
  x[i,j] <= y[j];

subj to Capacity {j in BINS}:
   sum {i in ITEMS} d[i] * x[i,j] <= b * y[j];

subj to A1: x[1,1] = 1;
subj to A2: x[4,2] = 1;
subj to A3: x[7,3] = 1;
subj to A4: x[8,4] = 1;
subj to A5: x[9,5] = 1;
subj to A6: x[10,6] = 1;
subj to A7: x[12,7] = 1;
subj to A8: x[14,8] = 1;
subj to A9: x[15,9] = 1;

