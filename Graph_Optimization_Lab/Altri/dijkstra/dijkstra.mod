# AMPL model for the Minimum Cost Network Flow Problem
#
# By default, this model assumes that b[i] = 0, c[i,j] = 0,
# l[i,j] = 0 and u[i,j] = Infinity.
#
# Parameters not specified in the data file will get their default values.


set N;				# nodes in the network
set A within {N, N}; 	# arcs in the network

param b {N} default 0;		# supply/demand for node i
param c {A} default 0;		# cost of one of flow on arc(i,j)
param l {A} default 0;               # lower bound on flow on arc(i,j)
param u {A} default Infinity;	# upper bound on flow on arc(i,j)
var x {A};				# flow on arc (i,j)


minimize cost: sum{(i,j) in A} c[i,j] * x[i,j];

# Flow Out(i) - Flow In(i) = b(i)
subject to flow_balance {i in N}:
sum{j in N: (i,j) in A} x[i,j] - sum{j in N: (j,i) in A} x[j,i] = b[i];

subject to capacity {(i,j) in A}: l[i,j] <= x[i,j] <= u[i,j];