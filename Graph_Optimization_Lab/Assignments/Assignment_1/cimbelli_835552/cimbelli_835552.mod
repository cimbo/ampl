# Number of nodes
param n;
set N := 1..n;


set canS := 1..(2^n)-1;
set setcanS{S in canS} := {i in N : (S div 2^(i-1)) mod 2 == 1};
###############################################
param n_ss;
set S := 1..n_ss;
set Subset {S};

###############################################


#Varianles
var l{S} >= 0;

#Objective
minimize number_subsets:
  sum{s in S} l[s];   

#subj to item_in_a_subset{i in N}:
 # sum {s in S : i in Subset[s]} l[s] >= 1;
  
subj to item_in_a_subset{i in N}:
  sum {s in S : i in Subset[s]} l[s] = 1;