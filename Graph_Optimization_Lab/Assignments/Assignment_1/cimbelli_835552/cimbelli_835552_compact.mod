# Number of nodes
param n;
set N := 1..n;

param b;

param w{N};

#Varianles
var x{N,N} >= 0 , <= 1;
var y{N} >= 0 , <= 1;

#objective

minimize bin_number: sum{j in N} y[j];

s.t. assignment_c{i in N}: sum{j in N} x[i,j] = 1;

s.t. budget_c{j in N}: sum{i in N} w[i]*x[i,j] <= b*y[j];
