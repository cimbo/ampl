#————————---------
# PRICING PROBLEM
#-----------------


#param
param pi{N};
param w{N};
param b;


#variables
var u{N} binary;

#Objective

maximize knapsack: sum{i in N} pi[i] * u[i];

s.t. capacity_c: sum{i in N} w[i] * u[i] <= b;