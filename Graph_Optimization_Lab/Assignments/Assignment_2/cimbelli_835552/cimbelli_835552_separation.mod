#valore x*[i,j] nel primale
param x_star{V,V};

var z{V} binary;
var y{(i,j) in E: x_star[i,j] > 0} binary;

minimize separationObjective:
        sum{(i,j) in E: x_star[i,j] > 0} x_star[i,j]*y[i,j];
        
        
subject to 2b{(i,j) in E: x_star[i,j] > 0}:
        z[i] - z[j] <= y[i,j];
        
subject to 3b{(i,j) in E: x_star[i,j] > 0}:
		z[j] - z[i] <= y[i,j];
		
subject to 4b: sum{i in V} z[i] >= 1;

subject to 5b: sum{i in V} z[i] <= (card(V) - 1);