set V ordered;

set E := {i in V, j in V: ord(i) > ord(j)};

param nc >=0 integer;

set Cut{1..nc} within V;

#parameters
param c{E}, >=0;

#variables
var x{E}, >=0, <=1;

#objective function
minimize cost: sum{(i,j) in E} c[i,j] * x[i,j];
        
        
# constraints
subject to degree{i in V}:
        sum{(i,j) in E} x[i,j] + sum{(j,i) in E} x[j,i] = 2;

subject to cuts{e in 1..nc}:        
		sum{i in Cut[e], j in V diff Cut[e]: (i,j) in E} x[i,j] +
        sum{i in Cut[e], j in V diff Cut[e]: (j,i) in E} x[j,i] >= 2;