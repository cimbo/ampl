# Number of jobs
param n;
set J := 1..n;
#Helper set
set L := setof{i in J,j in J: i <> j} (i,j);

#Parameters
param p{J};
param d{J};
param M := 2 * sum{j in J}p[j];

#Varianles
var C{J} integer >= 0;
var T{J} integer >= 0;
var x{L} binary;

#Objective
minimize tardiness: sum{j in J} T[j];   

#Constraints

subject to c_1{(i,j) in L}: x[i,j] + x[j,i] = 1;

subject to c_2{(i,j) in L}: M*(1 - x[i,j]) + C[j] - C[i] >= p[j];

subject to c_3{j in J}: T[j] >= C[j] - d[j];

subject to c_4{j in J}: C[j] >= p[j];



