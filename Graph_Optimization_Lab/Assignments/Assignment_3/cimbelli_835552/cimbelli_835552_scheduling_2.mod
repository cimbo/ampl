# Number of jobs
param n;
set J := 1..n;
#Helper set
set L := setof{i in J,j in J: i <> j} (i,j);

#Parameters
param p{J};
param d{J};

#Varianles
var T{J} integer >= 0;
var x{L} binary;

#Objective
minimize tardiness: sum{j in J} T[j];  
 
#Constraints

subject to c_1{j in J}: sum{i in J: i<>j} x[i,j] = 1;

subject to c_2{i in J}: sum{j in J: i<>j} x[i,j] = 1;

subject to c_3{j in J}:
 T[j] >= sum{k in 1..j, i in J: i<>k} p[i]*x[i,k] - sum{i in 1..n: i<>j} d[i]*x[i,j];
