# transportation model

param n > 0;              # Number of source nodes
set S := 1..n;            # Set of source nodes
param m > 0; # number of demand nodes
set D := 1..m; # set of demand nodes


# cost
param cost{S,D} >= 0;

# source capacity
param p{S};

# demand amount
param q{D};

var x{S,D} >= 0 ;



minimize transportation_cost: sum{ i in S, j in D} cost[i,j]*x[i,j];

subject to balance_source{i in S}:
	sum{ j in D} x[i,j] = p[i];

subject to balance_demand{j in D}:
	sum{i in S} x[i,j] = q[j];

