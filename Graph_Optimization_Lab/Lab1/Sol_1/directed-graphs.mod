#Shortest Path Problem 
#Model file

set N; #Nodes
set A within N cross N; #Arcs

param c{A};	#Arc cost
param s symbolic in N default 1; #surce node

var x{A} >=0; #1 if arc (i,j) is used

minimize cost:
	sum{(i,j) in A} c[i,j]* x[i,j];
	
subject to flow{i in N: i <> s}:
	sum {j in N: i <> j and (j,i) in A} x[j,i] - sum {j in N: i <> j and (i,j) in A} x[i,j] = 1;

subject to flow_source:
	sum {j in N: s <> j and (j,s) in A} x[j,s] - sum {j in N: s <> j and (s,j) in A} x[s,j] = 1 - card(N);