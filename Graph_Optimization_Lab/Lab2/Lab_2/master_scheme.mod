# Number of nodes
param n; 
set N := 1..n;
set A within N cross N;

param c {A};              # Arc cost
param u {A} default 4;  # Arc capacity

# Number and set of demands 
param n_d; 
set K := 1..n_d;

param s {K} within N; # Source
param t {K} within N; # Destination
param d {K}; # Flow demand


# Number and set of paths
param n_p; 
set P := 1..n_p;
set Path {P} within A;
set P_K {K} within P default {}; # set of demands serving demand k

param or  {P} within N;  # Origin
param dest  {P} within N;  # Destination
param cp {P};  # Cost


#-----------------
# MASTER PROBELM
#-----------------
