#————————
# PRICING PROBLEM
#-----------------
param source symbolic in N;
param target symbolic in N;

param b {i in N} := if i = target then 1 else ( if i = source then - 1 else 0 );

# Pricing subproblem cost vector
param g {A} default 1;


