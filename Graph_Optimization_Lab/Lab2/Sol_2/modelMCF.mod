# Number of nodes
param n; 
set N := 1..n;
set A within N cross N;

param c{A};              # Arc cost
param u{A} ;  # Arc capacitiy

# Number and set of demands 
param n_d; 
set K := 1..n_d;

param s {K} within N; # Source
param t {K} within N; # Destination
param d {K}; # Flow demand

#-----------------
# MODEL ARC FORMULATION
#-----------------
var x { A, K } >= 0;

minimize cost:
	sum {(i,j) in A, k in K} c[i,j]*x[i,j,k];

subject to flow_s{i in N, k in K: i == s[k]}:
	sum {(i,j) in A} x[i,j,k] - sum {(j,i) in A} x[j,i,k] = d[k];

subject to flow_t{i in N, k in K: i == t[k]}:
	sum {(i,j) in A} x[i,j,k] - sum {(j,i) in A} x[j,i,k] = -d[k];

subject to flow{i in N, k in K: i != s[k] and i != t[k]}:
	sum {(i,j) in A} x[i,j,k] - sum {(j,i) in A} x[j,i,k] = 0;

subject to capacity{(i,j) in A}:
	sum {k in K} x[i,j,k] <= u[i,j];
