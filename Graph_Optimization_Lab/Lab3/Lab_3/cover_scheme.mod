# Set of items
param n > 0;
set I := 1..n;

# Profit
param p{I} >= 0;

# Weigths
param w{I} >= 0;

# Knapsack capacity
param B >= 0;

# Number of new cover inequalities
param nc default 0;
# Set of indices for the cover inequalities
set C := 1..nc;
# Cover inequalities
set CI{C} within I;

#--------------------------------------------------
#                 Knapsack problem
#--------------------------------------------------

# Binary knasack variables
var x {I} binary;

# Obj function

# Capacity constraints

# Cover inequalities constraints


#--------------------------------------------------
#                 Separation problem
#--------------------------------------------------

# Fractional solution (parameter)


# Look for a violated inequality: binary variable 
# for item in the cover
var z {I} binary;

# minimize the violation


# Capacity constraint

