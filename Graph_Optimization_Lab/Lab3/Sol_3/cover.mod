# Set of items
param n > 0;
set I := 1..n;

# Profit
param p{I} >= 0;

# Weigths
param w{I} >= 0;

# Knapsack capacity
param B >= 0;

# Number of new cover inequalities
param nc default 0;
# Set of indices for the cover inequalities
set C := 1..nc;
# Cover inequalities
set CI{C} within I;

#--------------------------------------------------
#                 Knapsack problem
#--------------------------------------------------

# Binary knasack variables
var x {I} binary;#>= 0, <= 1;

# Obj function
maximize Profit:
	sum {i in I} p[i]*x[i];

subject to Capacity:
	sum {i in I} w[i]*x[i] <= B;

# Cover inequalities
subject to Cover {c in C}:
	sum {i in CI[c]} x[i] <= card(CI[c])-1;

#--------------------------------------------------
#                 Separation problem
#--------------------------------------------------

# Fractional solution
param x_star {I} >= 0, <= 1;

# Look for a violated inequality: binary variable 
# for item in the cover
var z {I} binary;

# Maximize the violation
minimize Violation: 
	sum {i in I} (1 - x_star[i])*z[i];


# Cover
subject to CoverCondition:
	sum {i in I} w[i]*z[i] >= B+1;
