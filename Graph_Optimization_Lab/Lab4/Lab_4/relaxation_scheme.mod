

# Set of items
param n > 0;
set I := 1..n;


# Size of each item
param w{I} default 1;

param c{I} default 1;

param q{I} default 1;

# capacities
param B1 default card(I);
param B2 default card(I);
param B3 default card(I);

# Score of each item
param p{I} default 1;

#lagrangian relaxation parameters
param u1 default 0;
param u2 default 0;
param u3 default 0;

param u1_old default 1;
param u2_old default 1;
param u3_old default 1;

param t default 1;


#surrogate relaxation parameters
param m1 default 1;
param m2 default 1;
param m3 default 1;

param m1_old default 1;
param m2_old default 1;
param m3_old default 1;

#VLSN parameters
param k default 0;
set selectedItems within I default {};