# Knapsack model

# Set of items
param n > 0;
set I := 1..n;
set selectedItems within I default {};

# Size of each item
param w{I} default 1;

param c{I} default 1;

param q{I} default 1;

# Overall Maximum size
param B1 default card(I);
param B2 default card(I);
param B3 default card(I);

# Score of each item
param p{I} default 1;

#lagrangian relaxation parameters
param u1 default 0;
param u2 default 0;
param u3 default 0;

param u1_old default 1;
param u2_old default 1;
param u3_old default 1;

param t default 1;

param k default 0;

#surrogate relaxation parameters
param m1 default 1;
param m2 default 1;
param m3 default 1;

param m1_old default 1;
param m2_old default 1;
param m3_old default 1;


#----------------------------------------
# Integer Linear Programming model
#----------------------------------------

var x{I} binary;

maximize OverallScore:
	sum {i in I} p[i]*x[i];

subject to Capacity:
	sum {i in I} w[i]*x[i] <= B1;

subject to Volume:
	sum {i in I} q[i]*x[i] <= B2;

subject to Budget:
	sum {i in I} c[i]*x[i] <= B3;

#----------------------------------------
# Very large scale neighborhood
#----------------------------------------

subject to neighSize:
	sum{i in selectedItems} (1-x[i]) + sum{i in I diff selectedItems}x[i] <= k; 

#----------------------------------------
# Lagrangian
#----------------------------------------


maximize Dual:
	sum {i in I} p[i]*x[i] + u1*(B1 - sum {i in I} w[i]*x[i]) + u2*(B2 - sum {i in I} q[i]*x[i]) + u3*(B3 - sum {i in I} c[i]*x[i]);

#----------------------------------------
# Surrogate relaxation
#----------------------------------------

subject to surrogate_capacity:
sum {i in I} m1*w[i]*x[i] + sum {i in I} m2*q[i]*x[i] + sum {i in I} m3*c[i]*x[i] <= m1*B1 + m2*B2 + m3*B3;
