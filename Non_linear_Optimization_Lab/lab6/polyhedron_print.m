function polyhedron_print(A, b)

  [m, n] = size(A);
  Ineq = A(:, 1 : n-m);
  rel = '';
  for i = 1 : m
    rel = [ rel '<' ];
  end
  t = extrpts(Ineq,rel,b);
  t = t(1:2,:);
  t = delcols(t);
  t1 = t(1,:);
  t2 = t(2,:);
  z = convhull(t1,t2);
  hold on
  patch(t1(z),t2(z),[0.9, 0.9, 0.5])

end % end of function