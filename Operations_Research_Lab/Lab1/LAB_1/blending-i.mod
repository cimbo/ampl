# SETS

set I;
set J;

# PARAMS

param c{I};
param b{I};
param r{J};
param q_max{I,J} default 1;
param q_min{I,J} default 0;

