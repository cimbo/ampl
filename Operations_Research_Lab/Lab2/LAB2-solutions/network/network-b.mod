# SETS
set V;
set E within {V,V};
set A within {V,V} :=
   E union setof{(i,j) in E} (j,i);
# PARAMS
param s symbolic in V;
param t symbolic in V;
param k{(i,j) in A union {(t,s)}} >= 0, default if (j,i) in E then k[j,i] else Infinity;
# VARS
var x{(i,j) in A union {(t,s)}} >=0, <= k[i,j];
# OBJECTIVE FUNCTION
maximize value: x[t,s];
# CONSTRAINTS
subject to balance{h in V}:
   sum{(h,j) in A union {(t,s)}} x[h,j] - sum{(i,h) in A union {(t,s)}} x[i,h] = 0;