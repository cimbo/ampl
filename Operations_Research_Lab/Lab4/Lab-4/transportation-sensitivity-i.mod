# SETS

set I;
set J;

# PARAMS

param b{I} >= 0;
param d{J} >= 0;
param c{I,J} >= 0;

