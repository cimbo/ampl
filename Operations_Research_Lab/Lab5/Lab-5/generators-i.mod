# SETS

set I; #generatori
 
# PARAMS

param d >=0; #fabisogno giornaliero
param d2 >=0; #fabisogno notturno
param f{I};	#costo di attivazione di giorno
param f2{I};	#costo di attivazione di notte
param c{I};		#costo per ora del generatore i
param k{I};		#prestazione half day
param k2{I};	#prestazione full day

var m{I} , integer >=0; #quanto produce i la mattina
var n{I} , integer >=0; #quanto produce i la sera
var x{I} , binary;
var y{I} , binary;


#objective function

minimize costs:
 sum{i in I}(m[i]*c[i] + x[i]*f[i]) + sum{i in I}(n[i]*c[i] + y[i]*f2[i]);


#Se sto producendo qualcosa la mattina allora sono attivo la mattina
subject to production1{i in I}: m[i] <= x[i]*d;

#Se sto producendo qualcosa la sera allora sono attivo la sera
subject to production2{i in I}: n[i] <= y[i]*d2;

#Vincolo produzione mattina
subject to domandaM: sum{i in I} m[i] >= d;

#Vincolo produzione alla sera
subject to domandaN: sum{i in I} n[i] >= d2;

#Se attivo solo la mattina e la sera produco il non il top
subject to nontop{i in I}: m[i] + n[i] <= 2*k[i]-k[i]*x[i]-k[i]*y[i] + k2[i];

#sera o mattina allora sto al top
subject to top{i in I}: m[i] <= k[i];

#sera o mattina allora sto al top
subject to top2{i in I}: n[i] <= k[i];




