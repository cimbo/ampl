# SETS

set I;	#Discipline

param T;	#minuti disponibili al giorno per match
param n;	#limite tra un match e l'altro della stessa disciplina
param c{I,I};	#conflitto tra due discipline
param m{I};	#numero di match totali per la discilplina
param w{I};	#grado di interesse per la disciplina
param t{I};	#durata di un match

param G;
set J := 1..G;	#giorni
set C := setof{i1 in I,i2 in I: c[i1,i2]=1 } (i1,i2);
# PARAMS



#var

var x{I,J} , binary; #disputare un match di i in day d
var lessi integer>=0; #interesse giorno scrauso

#Objective function

maximize profit:lessi;

#Constraints

#nello stesso giorno no clonflicting game

subject to ss{j in J,(i1,i2) in C}: (x[i2,j]+x[i1,j]) <= 1;

subject to minuti_day{j in J}: sum{i in I} (x[i,j]*t[i]) <= 180;

subject to all_match{i in I}: sum{j in J}x[i,j] = m[i];

subject to dd{j in J }: sum{i in I} (x[i,j]*w[i]) >= lessi;

#subject to {i1 in I, i2 in I, j in J}: x[i,j]*i-x[i2,j]*i2 <= 6; 

subject to ccccc{ i in I, j1 in J , j2 in J: j1-j2 >6 }: x[i,j1]+x[i,j2] <= 1;
