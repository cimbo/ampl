
from pyomo.environ import *
infinity = float('inf')

model = AbstractModel()


####################################
#SETS
####################################

#Type of Oil
model.I = Set()

#Type of gasoline
model.J = Set()

####################################
# PARAMS
####################################

model.c = Param(model.I)

model.b = Param(model.I)

model.r = Param(model.J)

model.q_max = Param(model.I, model.J, default=1.0)

model.q_min = Param(model.I, model.J, default=0.0)

####################################
# VARS
####################################

model.x = Var(model.I, model.J, within=NonNegativeReals)

model.y = Var(model.J, within=NonNegativeReals)

####################################
# OBJECTIVE FUNCTION
####################################

#Se non specifici il sense nell'objective function lui di default considera il minimizzare
def revenue(model):
    return (sum(model.r[j] * model.y[j] for j in model.J) - sum(model.c[i] * model.x[i,j] for i in model.I for j in model.J))
model.revenue = Objective(rule=revenue, sense=maximize)

####################################
# CONSTRAINTS
####################################

def availability_rule(model, i):
    return sum(model.x[i,j] for j in model.J) <= model.b[i]
model.availability_limit = Constraint(model.I, rule=availability_rule)


def conservation_rule(model, j):
    return sum(model.x[i,j] for i in model.I) == model.y[j]
model.conservation_limit = Constraint(model.J, rule=conservation_rule)

def max_qty_rule(model, i , j):
     return model.q_max[i,j] * model.y[j] >= model.x[i,j]
model.max_qty_limit = Constraint(model.I, model.J, rule=max_qty_rule)

def min_qty_rule(model, i, j):
    return model.x[i,j] >= model.q_min[i,j] * model.y[j]
model.min_qty_limit = Constraint(model.I, model.J, rule=min_qty_rule)
