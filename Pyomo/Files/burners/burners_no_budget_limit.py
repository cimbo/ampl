
from pyomo.environ import *

infinity = float('inf')

model = AbstractModel()

####################################
#SETS
####################################

model.m = Param(within=NonNegativeIntegers)

model.I = RangeSet(1, model.m)

model.n = Param(within=NonNegativeIntegers)

model.J = RangeSet(1, model.n)

####################################
# PARAMS
####################################

model.c = Param(model.J)
model.a = Param(model.I, model.J)
model.b = Param(model.J)
model.B = Param()

####################################
# VARS
####################################

model.x = Var(model.J, within=Binary)

####################################
# OBJECTIVE FUNCTION
####################################

def capacity(model):
    return  sum( model.b[j] * model.x[j] for j in model.J)
model.capacity = Objective(rule=capacity, sense=maximize)

####################################
# CONSTRAINTS
####################################


def packing(model, i):
    return sum( model.a[i,j] * model.x[j] for j in model.J ) <= 1
model.packing = Constraint(model.I, rule=packing)