# --------------------------------------------------------Header-------------------------------------------------------- #
from pyomo.environ import *
from pyomo.opt import SolverFactory
from pyomo.core.base import *
# Import dei modelli astratti che poi vengono utilizzati nel file principale #
#------- .mod Files (Di Ampl) -------#
from master_abstract import generateModel_master
from pricing_abstract import generateModel_pricing

# Import dei file di dati che poi vengono utilizzati nel file principale per creare i modelli concreti #
#------- .dat Files (Di Ampl) -------#
dat_file_name_master = 'master.dat'
dat_file_name_pricing = 'pricing.dat'

# -------------------------------------------------Creation data Set-------------------------------------------------- #

#Initial number of available paths
n = 6
n_p = 7
n_d = 4

P = [1,2,3,4,5,6,7]
N = [1,2,3,4,5,6]

def get_starting_path_set():

    set = {}

    set[1] = [(1,2),(2,4),(4,6)]
    set[2] = [(1,4)]
    set[3] = [(5,2),(2,4),(4,6)]
    set[4] = [(5,6)]
    set[5] = [(1,2),(2,3),(3,5)]
    set[6] = [(1,5)]
    set[7] = [(1,2),(2,4)]

    return set

Path = get_starting_path_set()

def get_starting_origin_param():
    origin = {}

    origin[1] = 1
    origin[2] = 1
    origin[3] = 5
    origin[4] = 5
    origin[5] = 1
    origin[6] = 1
    origin[7] = 1

    return  origin

origin = get_starting_origin_param()

def get_starting_dest_param():
    dest = {}

    dest[1] = 6
    dest[2] = 4
    dest[3] = 6
    dest[4] = 6
    dest[5] = 5
    dest[6] = 5
    dest[7] = 4

    return  dest

dest = get_starting_dest_param()


# ----------------------------------------------------Main---------------------------------------------------- #

abstract_model = generateModel_master(n, n_p, n_d, Path, origin, dest)

instance = abstract_model.create_instance(dat_file_name_master)

# Create a dual suffix component on the instance
# so the solver plugin will know which suffixes to collect
instance.dual = Suffix(direction=Suffix.IMPORT)
instance.slack = Suffix(direction=Suffix.IMPORT)
instance.rc = Suffix(direction=Suffix.IMPORT)

# -----Solver----- #
opt = SolverFactory("CPLEX")

results = opt.solve(instance)
results.write()

print '\n\n# ----------------------------------------------------------'


iter = 0
flag = 0


while flag == 0 or iter > 100:
    iter = iter + 1

    results = opt.solve(instance)

    instance.display()
    print instance.dual.values()
    print instance.rc.values()
    print instance.slack.values()

    print "Iter %d: Master %.3f\n\n" % (iter, instance.PathCost())
    print "Used paths: "

    for p in range(1, instance.n_p.value):
        if instance.x[p].value > 0:
            for (i, j) in instance.Path[p]:
                print "(%d,%d) " % (i, j)
                print "%f, " % instance.x[p].value
               # print " - rc %f" % instance.rc[instance.x[p]]
        else:
            print " NU ",
            for (i, j) in instance.Path[p]:
                print "(%d,%d) "% (i, j),
                print "%f" % instance.x[p].value,
               # print " - rc %f" % instance.rc[instance.x[p]]

    # Use the dual variables to set the cost function of the pricing subproblem
    g = {}
    for (i,j) in instance.A:
        if len(instance.ArcsInPath[i, j]) > 0:
            g[i,j] = instance.c[i,j] - instance.dual[instance.PathCapacity[i,j]]

    sigma = {}
    for k in instance.K:
        sigma[k] = instance.dual[instance.PathBalance[k]]

    flag = 0

    # Solve a pricing subproblem for each commodity
    for k in instance.K:
        source = instance.s[k]
        target = instance.t[k]

        abstract_model_pricing = generateModel_pricing(source, target, g, sigma)

        instance_pricing = abstract_model_pricing.create_instance(dat_file_name_pricing)

        results_pricing = opt.solve(instance_pricing)
        #results_pricing.write()

        # Check the reduced cost
        print "\nshortest path %d, sigma %d " % (instance_pricing.ShortestPath(), instance_pricing.sigma[k])

        if instance_pricing.sigma[k] > instance_pricing.ShortestPath():
            n_p = n_p + 1
            #Update set P
            P.append(n_p)

            z = []

            for (i,j) in instance_pricing.A:
                if instance_pricing.z[i,j] == 1:
                    z.append((i,j))

            Path[n_p] = z

            #instance.cp[instance.n_p] = cp --> In this version cp is calculated when I create an instance of the model
            #So no need like in ampl version to recompute from the run file

            #instance.ori[instance.n_p] = instance.s[k]
            origin[n_p] = instance.s[k]

            #instance.dest[instance.n_p] = instance.t[k]
            dest[n_p] = instance.t[k]

            #instance.P_K[k] = instance.P_K[k].append(instance.n_p)--> In this version P_K is calculated when I create an instance of the model
            #So no need like in ampl version to update it from the run file

            #Reconstruct the updated model
            abstract_model = generateModel_master(n, n_p, n_d, Path, origin, dest)
            instance = abstract_model.create_instance(dat_file_name_master)

            flag = 1

            #print "ReducedCost = %7.3f |" % instance_pricing.ShortestPath() - instance_pricing.sigma[k]

            for (i,j) in instance.Path[k]:
                print (i,j),

            print

    print "# ----------------------------------------------------------"

    results = opt.solve(instance)

    print "Master Obj = %7.3f\n" % float(instance.PathCost())

    print "Used paths:"

    print len(P)

    for p in P:
        if instance.x[p].value > 0:
            for (i,j) in Path[p]:
                print "(%d,%d) "% (i, j),
            print "%.2f" % instance.x[p].value

    print

    instance.x.display()