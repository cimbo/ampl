
####################################
#          MASTER PROBLEM
####################################


from pyomo.environ import *

infinity = float('inf')

#Genera il modello matematico
def generateModel_master(n, n_p, n_d, Path, origin, dest):

    model = AbstractModel()

    ####################################
    #SETS OF PATHS
    ####################################

    model.n = Param(within=PositiveIntegers, initialize=n)

    model.N = RangeSet(1, model.n)

    model.A = Set(within=model.N * model.N)

    # Number and set of demands
    model.n_d = Param(initialize=n_d)
    model.K = RangeSet(1, model.n_d)

    ####################################
    # PARAMS 1
    ####################################

    # Arc cost
    model.c = Param(model.A)

    # Arc capacitiy
    model.u = Param(model.A)

    # Source
    model.s = Param(model.K, within=model.N)

    # Destination
    model.t = Param(model.K, within=model.N)

    # Flow demand
    model.d = Param(model.K)

    ####################################
    #SETS OF PATHS
    ####################################

    #Number of columns
    model.n_p = Param(default=0, initialize=n_p)
    model.P = RangeSet(1, model.n_p)
    model.Path = Set(model.P, dimen=2, initialize=Path)

    # Origin
    model.origin = Param(model.P, initialize=origin)

    # Destination
    model.dest = Param(model.P, initialize=dest)

    # set of demands serving demand k

    def P_K_init(model, k):
        p_k = set()
        for p in model.P:
	        if model.s[k] == model.origin[p] and model.t[k] == model.dest[p]:
                    p_k.add(p)

        return p_k
    model.P_K = Set(model.K, within=model.P, initialize=P_K_init)

    ####################################
    #SETS FOR THE ARCHS (HELPER)
    ####################################


    def NodesOut_init(model, node):
        retval = []
        for (i,j) in model.A:
            if i == node:
                retval.append(j)
        return retval
    model.NodesOut = Set(model.N, initialize=NodesOut_init)

    def NodesIn_init(model, node):
        retval = []
        for (i,j) in model.A:
            if j == node:
                retval.append(i)
        return retval
    model.NodesIn = Set(model.N, initialize=NodesIn_init)

    def ArcsInPath_init(model, i, j):
        retval = []
        for p in model.P:
            if (i,j) in model.Path[p]:
                retval.append(p)
        return retval
    model.ArcsInPath = Set(model.A, initialize=ArcsInPath_init)


    ####################################
    # PARAMS 2
    ####################################

    # Cost

    def cp_init(model, p):
        retval = 0
        for (i,j) in model.Path[p]:
            retval = retval + model.c[i,j]
        return retval

    model.cp = Param(model.P, initialize=cp_init)

    ####################################
    # VARS
    ####################################

    model.x = Var(model.P, within=NonNegativeReals)

    ####################################
    # OBJECTIVE FUNCTION
    ####################################

    def PathCost(model):
        return  sum( model.cp[p] * model.x[p] for p in model.P)
    model.PathCost = Objective(rule=PathCost, sense=minimize)

    ####################################
    # CONSTRAINTS
    ####################################

    def PathBalance(model, k):
        if len(model.P_K[k]) > 0:
            return sum( model.x[p] for p in model.P_K[k]) == model.d[k]
        else:
            return Constraint.Skip
    model.PathBalance = Constraint(model.K, rule=PathBalance)

    def PathCapacity(model, i, j):
        if len(model.ArcsInPath[i,j]) > 0:
            return sum ( model.x[p] for p in model.ArcsInPath[i,j]) <= model.u[i,j]
        else:
            return Constraint.Skip
    model.PathCapacity = Constraint(model.A, rule=PathCapacity)

    return model