####################################
#          PRICING PROBLEM
####################################

from pyomo.environ import *

infinity = float('inf')

#Genera il modello matematico
def generateModel_pricing(source, target, g, sigma):

    model = AbstractModel()

    # SETS OF PATHS
    ####################################

    model.n = Param(within=PositiveIntegers)

    model.N = RangeSet(1, model.n)

    model.A = Set(within=model.N * model.N)

    # Number and set of demands
    model.n_d = Param()
    model.K = RangeSet(1, model.n_d)

    ####################################
    # PARAMS
    ####################################


    model.source = Param(within=model.N, initialize=source)

    model.target = Param(within=model.N, initialize=target)

    def param_b_init(model, i):
        if i == target:
            return 1
        else:
            if i == source:
                return - 1
            else:
                return 0
    model.b = Param(model.N, initialize=param_b_init)

    # Pricing subproblem cost vector
    model.g = Param(model.A, initialize=g, default=1)
    model.sigma = Param(model.K, initialize=sigma, default=0)

    def NodesOut_init(model, node):
        retval = []
        for (i, j) in model.A:
            if i == node:
                retval.append(j)
        return retval

    model.NodesOut = Set(model.N, initialize=NodesOut_init)

    def NodesIn_init(model, node):
        retval = []
        for (i, j) in model.A:
            if j == node:
                retval.append(i)
        return retval

    model.NodesIn = Set(model.N, initialize=NodesIn_init)

    ####################################
    # VARS
    ####################################

    model.z = Var(model.A, within=NonNegativeReals) #within=Binary

    ####################################
    # OBJECTIVE FUNCTION
    ####################################

    def ShortestPath(model):
        return sum(model.g[i,j] * model.z[i,j] for (i,j) in model.A)
    model.ShortestPath = Objective(rule=ShortestPath, sense=minimize)

    def NodeBalance(model, i):
        return sum(model.z[j,i] for j in model.NodesIn[i]) - sum(model.z[i,j] for j in model.NodesOut[i]) == model.b[i]
    model.NodeBalance = Constraint(model.N, rule=NodeBalance)

    return model