
from pyomo.environ import *

infinity = float('inf')

model = AbstractModel()

####################################
#SETS
####################################

model.V = Set()

model.T = Set(within = model.V)
model.E = Set(within= model.V * model.V)

def init_set_A(model):
    a = set()

    for (i,j) in model.E:
        a.add((i,j))
        a.add((j,i))

    return a
model.A = Set(within=model.V*model.V, initialize=init_set_A)


####################################
# PARAMS
####################################

model.s = Param()

model.c1 = Param(model.E)

def init_param_c(model, i, j):

    if (i,j) in model.E:
        return model.c1[i,j]
    else:
        return model.c1[j,i]

model.c = Param(model.A, initialize = init_param_c)

def init_param_b(model, h):

    if h == model.s.value:
        return len(model.T) - 1
    else:
        if h in model.T:
            return -1
        else:
            return 0

model.b = Param(model.V, initialize = init_param_b)

def NodesOut_init(model, node):
    retval = []
    for (i,j) in model.A:
        if i == node:
            retval.append(j)
    return retval
model.NodesOut = Set(model.V, initialize=NodesOut_init)

def NodesIn_init(model, node):
    retval = []
    for (i,j) in model.A:
        if j == node:
            retval.append(i)
    return retval
model.NodesIn = Set(model.V, initialize=NodesIn_init)

####################################
# VARS
####################################

model.x = Var(model.A, within=NonNegativeReals)


####################################
# OBJECTIVE FUNCTION
####################################

def cost(model):
    return  sum( model.c[i,j] * model.x[i,j] for (i,j) in model.A)
model.cost = Objective(rule=cost, sense=minimize)

####################################
# CONSTRAINTS
####################################

def balance_rule(model, h):
    return (sum( model.x[h,j] for j in model.NodesOut[h] ) - sum( model.x[i,h] for i in model.NodesOut[h])) == model.b[h]
model.balance_constraint = Constraint(model.V, rule=balance_rule)


print "Finita prima passata di pyomo"