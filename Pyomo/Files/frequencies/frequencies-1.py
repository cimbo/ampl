
from pyomo.environ import *
import math

infinity = float('inf')

model = AbstractModel()

####################################
#SETS
####################################

model.V = Set()

model.J = Set()

####################################
# PARAMS
####################################

model.coord1 = Param(model.V)
model.coord2 = Param(model.V)

def init_param_d(model, i, j):
    d1 = (model.coord1[i] - model.coord1[j])
    d2 = (model.coord2[i] - model.coord2[j])

    return math.sqrt( pow(d1, 2) + pow(d2, 2) )
model.d = Param(model.V, model.V, initialize=init_param_d)

model.delta = Param(within=NonNegativeReals)
model.gamma = Param(within=NonNegativeReals)

####################################
#          AUXILIARY SETS          #
####################################

def init_set_E(model):
    e = set()

    for i in model.V:
        for j in model.V:
            if i < j and (model.d[i,j] <= model.delta):
                e.add((i,j))

    return e

model.E = Set(dimen=2, initialize=init_set_E)

####################################
# VARS
####################################

model.x = Var(model.V, model.J, within=Binary)

model.y = Var(model.J, within=Binary)

####################################
# OBJECTIVE FUNCTION
####################################

def use(model):
    return  sum( model.y[j] for j in model.J)
model.use = Objective(rule=use, sense=minimize)

def assignment(model, i):
    return  sum( model.x[i,j] for j in model.J ) == 1
model.assignment = Constraint(model.V, rule=assignment)

def activation(model, i, j):
    return  model.x[i,j] <= model.y[j]
model.activation = Constraint(model.V, model.J, rule=activation)

def interference(model, i, j, k):
    return  model.x[i,k] + model.x[j,k] <= 1
model.interference = Constraint(model.E, model.J, rule=interference)
