from __future__ import division
from pyomo.environ import *

model = AbstractModel()

####################################
#SETS
####################################

model.I = Set()

model.n = Param(within=NonNegativeIntegers)

model.J = RangeSet(1, model.n)


####################################
# PARAMS
####################################


model.b = Param(model.J)
model.d = Param(model.I,model.J)
model.r = Param(model.I)
model.c = Param(model.I)
model.q = Param(model.I)
model.f = Param(model.I)
model.l = Param(model.I)
model.m = Param(model.I)
model.K = Param()

####################################
# VARS
####################################

model.x = Var(model.I, model.J, within=NonNegativeIntegers)

model.v = Var(model.I, model.J, within=NonNegativeReals)

model.z = Var(model.I, model.J | {0}, within=NonNegativeReals)

####################################
# OBJECTIVE FUNCTION
####################################

def revenue(model):
    return  sum( model.r[i] * model.v[i,j] - model.c[i] * model.x[i,j] - model.m[i] * model.z[i,j] for i in model.I for j in model.J)
model.revenue = Objective(rule=revenue, sense=maximize)

####################################
# CONSTRAINTS
####################################

def demand(model, i, j):
    return (0, model.v[i,j], model.d[i,j])
model.demand = Constraint(model.I, model.J, rule=demand)

def production(model, j):
   return sum( (model.x[i,j]/model.q[i]) for i in model.I) <= model.b[j]
model.production = Constraint(model.J, rule=production)

def balance(model, i, j):
   return model.z[i,j-1] + model.x[i,j] == model.z[i,j] + model.v[i,j]
model.balance = Constraint(model.I, model.J, rule=balance)

def capacity(model, j):
   return sum( model.z[i,j] for i in model.I) <= model.K
model.capacity = Constraint(model.J, rule=capacity)

def demand_init(model, i):
    return (0, model.z[i,0], 0)
model.demand_init = Constraint(model.I, rule=demand_init)