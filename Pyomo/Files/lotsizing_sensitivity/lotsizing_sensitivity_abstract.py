
from pyomo.environ import *

infinity = float('inf')

model = AbstractModel()

####################################
#SETS
####################################

model.J = Set()

####################################
# PARAMS
####################################

model.B = Param()
model.d = Param(model.J)
model.r = Param(model.J)
model.c = Param(model.J)
model.q = Param(model.J)

####################################
# VARS
####################################

model.x = Var(model.J, within=NonNegativeReals)

####################################
# OBJECTIVE FUNCTION
####################################

def revenue(model):
    return  sum( (model.r[j] - model.c[j]) * model.x[j] for j in model.J)
model.revenue = Objective(rule=revenue, sense=maximize)

####################################
# CONSTRAINTS
####################################

def demand(model, j):
    return model.x[j] <= model.d[j]
model.demand = Constraint(model.J, rule=demand)

def production(model):
    return sum( (model.x[j] / model.q[j]) for j in model.J) <= model.B
model.production = Constraint(rule=production)
