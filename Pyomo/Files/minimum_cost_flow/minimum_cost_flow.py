from pyomo.environ import *

model = AbstractModel()

####################################
#SETS
####################################

model.Nodes = Set()
model.Arcs = Set(within=model.Nodes*model.Nodes)

def NodesOut_init(model, node):
    retval = []
    for (i,j) in model.Arcs:
        if i == node:
            retval.append(j)
    return retval
model.NodesOut = Set(model.Nodes, initialize=NodesOut_init)

def NodesIn_init(model, node):
    retval = []
    for (i,j) in model.Arcs:
        if j == node:
            retval.append(i)
    return retval
model.NodesIn = Set(model.Nodes, initialize=NodesIn_init)



####################################
# PARAMS
####################################

model.FlowCost = Param(model.Arcs)

model.Demand = Param(model.Nodes)

model.Supply = Param(model.Nodes)



####################################
# VARS
####################################

model.Flow = Var(model.Arcs, domain=NonNegativeReals)



####################################
# OBJECTIVE FUNCTION
####################################

def Obj_rule(model):
    return summation(model.FlowCost, model.Flow)
model.Obj = Objective(rule=Obj_rule, sense=minimize)



####################################
# CONSTRAINTS
####################################

def FlowBalance_rule(model, node):
    return model.Supply[node] \
     + sum(model.Flow[i, node] for i in model.NodesIn[node]) \
     - model.Demand[node] \
     - sum(model.Flow[node, j] for j in model.NodesOut[node]) \
     == 0
model.FlowBalance = Constraint(model.Nodes, rule=FlowBalance_rule)