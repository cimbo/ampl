
from pyomo.environ import *

infinity = float('inf')

model = AbstractModel()

####################################
#SETS
####################################

model.V = Set()

model.E = Set(within= model.V * model.V)

def init_set_A(model):
    a = set()

    for (i,j) in model.E:
        a.add((i,j))
        a.add((j,i))

    return a
model.A = Set(within=model.V*model.V, initialize=init_set_A)

model.D = Set()

####################################
# PARAMS
####################################

model.s = Param(model.D, default=0);
model.t = Param(model.D, default=0);

model.k1 = Param(model.E)

def init_param_k(model, i, j):

    if (i,j) in model.E:
        return model.k1[i,j]
    else:
        return model.k1[j,i]

model.k = Param(model.A, initialize = init_param_k)

def NodesOut_init(model, node):
    retval = []
    for (i,j) in model.A:
        if i == node:
            retval.append(j)
    return retval
model.NodesOut = Set(model.V, initialize=NodesOut_init)

def NodesIn_init(model, node):
    retval = []
    for (i,j) in model.A:
        if j == node:
            retval.append(i)
    return retval
model.NodesIn = Set(model.V, initialize=NodesIn_init)

####################################
# VARS
####################################

model.x = Var(model.A * model.D, within=NonNegativeIntegers)

model.phi = Var(model.D, within=NonNegativeReals)

####################################
# OBJECTIVE FUNCTION
####################################

def value(model):
    return  sum(model.phi[d] for d in model.D)
model.value = Objective(rule=value, sense=maximize)

####################################
# CONSTRAINTS
####################################

def balance_s(model, d):
        return sum( model.x[model.s[d],j,d] for j in model.NodesOut[model.s[d]]) - sum( model.x[i,model.s[d],d] for i in model.NodesOut[model.s[d]]) == model.phi[d]
model.balance_s = Constraint(model.D, rule=balance_s)

def balance_t(model, d):
        return sum( model.x[model.t[d],j,d] for j in model.NodesOut[model.t[d]]) - sum( model.x[i,model.t[d],d] for i in model.NodesOut[model.t[d]]) == - model.phi[d]
model.balance_t = Constraint(model.D, rule=balance_t)

def balance(model, h, d):
    if h != model.s[d] and h != model.t[d]:
        return sum( model.x[h,j,d] for j in model.NodesOut[h]) - sum( model.x[i,h,d] for i in model.NodesOut[h]) == 0
    else:
        return Constraint.Skip
model.balance = Constraint(model.V, model.D, rule=balance)

def capacity(model, i, j):
    return sum( model.x[i,j,d] for d in model.D) <= model.k[i,j]
model.capacity = Constraint(model.A, rule=capacity)