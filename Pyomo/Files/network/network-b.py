
from pyomo.environ import *

infinity = float('inf')

model = AbstractModel()

####################################
#SETS
####################################

model.V = Set()

model.E = Set(within= model.V * model.V)

def init_set_A(model):
    a = set()

    for (i,j) in model.E:
        a.add((i,j))
        a.add((j,i))

    return a
model.A = Set(within=model.V*model.V, initialize=init_set_A)

model.S = Set()
model.T = Set()

def init_set_AE(model):
    ae = set()

    for (l,m) in model.A:
        ae.add((l,m))
    for i in model.T:
        for j in model.S:
            ae.add((i,j))

    return ae
model.AE = Set(dimen=2, initialize=init_set_AE)

####################################
# PARAMS
####################################

model.s = Param()
model.t = Param()

model.k1 = Param(model.E)

def init_param_k(model, i, j):

    if (i,j) in model.E:
        return model.k1[i,j]
    else:
        if (j,i) in model.E:
            return model.k1[j,i]
        else:
            return infinity

model.k = Param(model.AE, initialize = init_param_k)

def NodesOut_init(model, node):
    retval = []
    for (i,j) in model.AE:
        if i == node:
            retval.append(j)
    return retval
model.NodesOut = Set(model.V, initialize=NodesOut_init)

def NodesIn_init(model, node):
    retval = []
    for (i,j) in model.AE:
        if j == node:
            retval.append(i)
    return retval
model.NodesIn = Set(model.V, initialize=NodesIn_init)

def NodesInSpecial_init(model, node):
    retval = []
    for (i,j) in model.A:
        if j == node:
            retval.append(i)
    return retval
model.NodesInSpecial = Set(model.V, initialize=NodesInSpecial_init)

def NodesOutSpecial_init(model, node):
    retval = []
    for (i,j) in model.A:
        if i == node:
            retval.append(j)
    return retval
model.NodesOutSpecial = Set(model.V, initialize=NodesOutSpecial_init)

####################################
# VARS
####################################

model.x = Var(model.AE)

def x_rule(model, i, j):
    return (0, model.x[i,j] , model.k[i,j])

model.x_limit = Constraint(model.AE, rule=x_rule)

####################################
# OBJECTIVE FUNCTION
####################################

def value(model, t, s):
    return  model.x[t,s]
model.value = Objective(model.T, model.S, rule=value, sense=maximize)

####################################
# CONSTRAINTS
####################################

def balance(model, h):
    if h in model.V - model.T - model.S:
        return (sum( model.x[h,j] for j in model.NodesOut[h]) - sum( model.x[i,h] for i in model.NodesOut[h])) == 0
    else:
        if h in model.T:
            return (sum(model.x[h, j] for j in model.NodesOut[h]) - sum(model.x[i, h] for i in model.NodesOutSpecial[h])) == 0
        else:
            if h in model.S:
                return (sum(model.x[h, j] for j in model.NodesOut[h]) - sum(
                    model.x[i, h] for i in model.NodesIn[h])) == 0
model.balance = Constraint(model.V, rule=balance)


