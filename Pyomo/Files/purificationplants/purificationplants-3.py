
from pyomo.environ import *

infinity = float('inf')

model = AbstractModel()

####################################
#SETS
####################################

model.m = Param(within=NonNegativeIntegers)

model.I = RangeSet(1, model.m)

model.n = Param(within=NonNegativeIntegers)

model.J = RangeSet(1, model.n)

####################################
# PARAMS
####################################

model.c = Param(model.J)
model.a = Param(model.I, model.J)
model.b = Param(model.J)
model.Q = Param()

####################################
# VARS
####################################

model.x = Var(model.J, within=Binary)

####################################
# OBJECTIVE FUNCTION
####################################

def cost(model):
    return  sum( model.x[j] * model.c[j] for j in model.J)
model.cost = Objective(rule=cost, sense=minimize)

####################################
# CONSTRAINTS
####################################

def covering(model, i):
    return sum( model.a[i,j] * model.x[j] for j in model.J ) >= 1
model.covering = Constraint(model.I, rule=covering)

def quantity(model):
    return sum( model.b[j] * model.x[j] for j in model.J ) >= model.Q
model.quantity = Constraint(rule=quantity)
