#########################################
# Lagrangian Relaxation Knapsack Problem
#########################################


from pyomo.environ import *

infinity = float('inf')


#Genera il modello matematico
def generateModel_Lagrangian_Relaxation(u1, u2, u3):

    model = AbstractModel()

    ####################################
    # SET
    ####################################

    # Number of items
    model.n = Param(within=NonNegativeIntegers)

    # Set of items
    model.I = RangeSet(1, model.n)


    ####################################
    # PARAMS
    ####################################

    # Profit
    model.p = Param(model.I, within=NonNegativeReals, default=1)

    # Weigths
    model.w = Param(model.I, within=NonNegativeReals, default=1)

    # Costs
    model.c = Param(model.I, within=NonNegativeReals, default=1)

    # Volumes
    model.q = Param(model.I, within=NonNegativeReals, default=1)

    # Overall Maximum size
    model.B1 = Param()
    model.B2 = Param()
    model.B3 = Param()

    ####################################
    # LAGRANGIAN MULTIPLIERS
    ####################################

    model.u1 = Param(initialize=u1)

    model.u2 = Param(initialize=u2)

    model.u3 = Param(initialize=u3)

    ####################################
    # VARS
    ####################################

    model.x = Var(model.I, within=Binary)  # >= 0, <= 1;

    ####################################
    # OBJECTIVE FUNCTION
    ####################################

    def Dual(model):
        return sum(model.p[i] * model.x[i] for i in model.I) +\
               model.u1 * (model.B1 - sum(model.w[i] * model.x[i] for i in model.I) ) +\
               model.u2 * (model.B2 - sum(model.q[i] * model.x[i] for i in model.I)) +\
               model.u3 * (model.B3 - sum(model.c[i] * model.x[i] for i in model.I) )
    model.Dual = Objective(rule=Dual, sense=maximize)

    return model







