#########################################
# Surrogate Relaxation Knapsack Problem
#########################################


from pyomo.environ import *

infinity = float('inf')

#Genera il modello matematico
def generateModel_Surrogate_Relaxation(m1, m2, m3):

    model = AbstractModel()

    ####################################
    # SET
    ####################################

    # Number of items
    model.n = Param(within=NonNegativeIntegers)

    # Set of items
    model.I = RangeSet(1, model.n)


    ####################################
    # PARAMS
    ####################################

    # Profit
    model.p = Param(model.I, within=NonNegativeReals, default=1)

    # Weigths
    model.w = Param(model.I, within=NonNegativeReals, default=1)

    # Costs
    model.c = Param(model.I, within=NonNegativeReals, default=1)

    # Volumes
    model.q = Param(model.I, within=NonNegativeReals, default=1)

    # Overall Maximum size
    model.B1 = Param()
    model.B2 = Param()
    model.B3 = Param()

    ####################################
    # SURROGATE MULTIPLIERS
    ####################################

    model.m1 = Param(initialize=m1)

    model.m2 = Param(initialize=m2)

    model.m3 = Param(initialize=m3)

    ####################################
    # VARS
    ####################################

    model.x = Var(model.I, within=Binary)  # >= 0, <= 1;

    ####################################
    # OBJECTIVE FUNCTION
    ####################################

    def OverallScore(model):
        return sum(model.p[i] * model.x[i] for i in model.I)
    model.OverallScore = Objective(rule=OverallScore, sense=maximize)

    ####################################
    # CONSTRAINTS
    ####################################

    def surrogate_capacity(model):
        return sum(model.m1 * model.w[i] * model.x[i] for i in model.I) +\
               sum(model.m2 * model.q[i] * model.x[i] for i in model.I) +\
               sum(model.m3 * model.c[i] * model.x[i] for i in model.I) <=\
               model.m1 * model.B1 + model.m2 * model.B2 + model.m3 * model.B3
    model.surrogate_capacity = Constraint(rule=surrogate_capacity)

    return model