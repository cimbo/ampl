
from pyomo.environ import *

#def pyomo_create_model(options=None, model_options=None):

infinity = float('inf')

model = AbstractModel()

####################################
#SETS
####################################

model.n = Param(within=PositiveIntegers)

model.S = RangeSet(1, model.n)

model.m = Param(within=PositiveIntegers)

model.D = RangeSet(1, model.m)

####################################
#  PARAMS
####################################

# cost
model.cost = Param(model.S, model.D)

# source capacity
model.p = Param(model.S)

# demand amount
model.q = Param(model.D)

####################################
# VARS
####################################

model.x = Var(model.S, model.D, within=NonNegativeReals)

####################################
# OBJECTIVE FUNCTION
####################################

def transportation_cost(model):
    return  sum( model.x[i,j] * model.cost[i,j] for i in model.S for j in model.D )
model.transportation_cost = Objective(rule=transportation_cost, sense=minimize)

####################################
# CONSTRAINTS
####################################

def balance_source(model, i):
    return sum( model.x[i,j] for j in model.D ) == model.p[i]
model.balance_source = Constraint(model.S, rule=balance_source)

def balance_demand(model, j):
       return sum( model.x[i,j] for i in model.S ) == model.q[j]
model.balance_demand = Constraint(model.D, rule=balance_demand)

#return model