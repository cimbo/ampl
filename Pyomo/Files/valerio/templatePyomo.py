# coding=utf-8

#Il "coding=utf-8" qui sopra serviva per evitare un problema generale di Python ma non mi ricordo più di cosa si tratasse, cmq c'è su internet


#from __future__ import division

#Imports necessari per pyomo
from pyomo.environ import *
from pyomo.opt import SolverFactory



#---FUNZIONI AUSILIARIE---
def calcolaTreni():
    t={1,2,3,4}
    return t

def calcolaPosizioni():
    p={1,2,3,4}
    return p

def calcolaDistanze():
 d={}
 for j in range(10):
  for i in range(10):
   d[i,j]=2
 return d

#Genera il modello matematico
def generateModel(treni,posizioni,matDistanze):

 #--creo il modello astratto
 model=AbstractModel()

 #--Sets
 model.T = Set(initialize=treni)
 model.P = Set(initialize=posizioni)

 #--Parameters
 def S_ruleD(model,p1,p2):
   return matDistanze[p1,p2]
 model.d     = Param(model.P,model.P,initialize=S_ruleD)

 #--Variables
 model.delta = Var(model.T, within=NonNegativeReals, bounds=(0,1200))
 model.alfa  = Var(within=NonNegativeReals, bounds=(0,1200))

#--Objective
 def obj_expression(model):
  return model.alfa
 model.OBJ = Objective(rule=obj_expression,sense=minimize)

#--Constraints

 #Alfa deve essere la distanza piu grande tra tutte le distanze
 def con1_rule(model,t):
  return model.alfa >= model.delta[t]
 model.con1=Constraint(model.T,rule=con1_rule)

 #Calcolo delle distanze
 #def con3_rule(model,i,h,j,k):
 #  return model.delta[int(i)] >= model.d[int(h),int(k)]*model.y[i,h,j,k]
 #model.con3=Constraint(model.V,rule=con3_rule)

 #Stampo a schermo il modello (solo per vedere quello che genera)
 model.pprint()
 return model

#
def createSolver(solver):
 solver_io = 'lp'  # Uses the LP file interface
 stream_solver = False  # True prints solver output to screen
 keepfiles = False  # True prints intermediate file names (.nl,.sol,...)

 opt = SolverFactory(solver, solver_io=solver_io)

 if opt is None:
    print("")
    print("ERROR: Unable to create solver plugin for %s "\
          "using the %s interface" % (solver, solver_io))
    print("")

 return opt
# --------------------------------------------------------MAIN--------------------------------------------------------

#---------Create a solver
opt = createSolver("CPLEX")
#opt.options["threads"] = 6

#---PARAMETRI---
treni=calcolaTreni()
pos=calcolaPosizioni()
mat=calcolaDistanze()

#--------Create the model

model= generateModel(treni,pos,mat)

#--------Create the instance
instance=model.create_instance(report_timing=True)

#--------Solve
instance.preprocess()
results=opt.solve(instance,keepfiles=True,tee=True)
instance.solutions.load_from(results)

#--------Print of the solutions
print "OBJ: "+str(instance.alfa.value)


for i in treni:
 print "delta" + str(i) +"==> "+ str(instance.delta[i].value)

raw_input ("premere Invio per uscire")


