
####################################
#         Separation Problem
####################################


from pyomo.environ import *

infinity = float('inf')

#Genera il modello matematico
def generateModel_Separation(I, x_star, w, B):

    model = AbstractModel()

    ####################################
    # SETS OF PATHS
    ####################################

    # Set of items
    model.I = Set(dimen=1, initialize=I)

    ####################################
    # PARAMS
    ####################################

    # Fractional solution
    model.x_star = Param(model.I, initialize=x_star)

    # Weigths
    model.w = Param(model.I, initialize=w)

    # Knapsack capacity
    model.B = Param(initialize=B)

    ####################################
    # VARS
    ####################################
    # Look for a violated inequality: binary variable
    # for item in the cover
    model.z = Var(model.I, within=Binary)

    ####################################
    # OBJECTIVE FUNCTION
    ####################################

    # Maximize the violation
    def Violation(model):
        return sum( (1 - model.x_star[i]) * model.z[i] for i in model.I )
    model.Violation = Objective(rule=Violation, sense=minimize)

    ####################################
    # CONSTRAINTS
    ####################################

    def CoverCondition(model):
        return sum( model.w[i] * model.z[i] for i in model.I) >= model.B + 1
    model.CoverCondition = Constraint(rule=CoverCondition)

    return model

