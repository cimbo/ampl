from pyomo.environ import *

#def pyomo_create_model(options=None, model_options=None):

infinity = float('inf')

model = AbstractModel()

####################################
#SETS
####################################

# Nodes
model.N = Set()

# Arcs
model.A = Set(within=model.N * model.N)

####################################
#  PARAMS
####################################

# Arc cost
model.c = Param(model.A)

# surce node
model.s = Param(within=model.N, default=1)

def NodesOut_init(model, node):
    retval = []
    for (i,j) in model.A:
        if i == node:
            retval.append(j)
    return retval
model.NodesOut = Set(model.N, initialize=NodesOut_init)

def NodesIn_init(model, node):
    retval = []
    for (i,j) in model.A:
        if j == node:
            retval.append(i)
    return retval
model.NodesIn = Set(model.N, initialize=NodesIn_init)

####################################
# VARS
####################################

# 1 if arc (i,j) is used
model.x = Var(model.A, within=NonNegativeReals)


####################################
# OBJECTIVE FUNCTION
####################################

def cost(model):
    return  sum( model.x[i,j] * model.c[i,j] for (i,j) in model.A )
model.cost = Objective(rule=cost, sense=minimize)

####################################
# CONSTRAINTS
####################################

def flow(model, i):
    if i != model.s.value:
        return sum(model.x[j, i] for j in model.NodesIn[i]) - sum(model.x[i, j] for j in model.NodesOut[i]) == 1
    else:
        return Constraint.Skip
model.flow = Constraint(model.N, rule=flow)


def flow_source(model):
    return sum(model.x[j, model.s.value] for j in model.NodesIn[model.s.value]) - sum(model.x[model.s.value, j] for j in model.NodesOut[model.s.value]) == 1 - len(model.N)
model.flow_source = Constraint(rule=flow_source)
