# --------------------------------------------------------Header-------------------------------------------------------- #
from pyomo.environ import *
from pyomo.opt import SolverFactory
# Import dei modelli astratti che poi vengono utilizzati nel file principale #
#------- .mod Files (Di Ampl) -------#
from shortest_path_abstract import model

# Import dei file di dati che poi vengono utilizzati nel file principale per creare i modelli concreti #
#------- .dat Files (Di Ampl) -------#
dat_file_name = 'directed_graphs.dat'

# ----------------------------------------------------Main---------------------------------------------------- #

abstract_model = model

instance = abstract_model.create_instance(dat_file_name)

# ---------------------------------------------------Solver---------------------------------------------------- #
opt = SolverFactory("CPLEX")
results = opt.solve(instance)

#current node
t = 0

#Total cost of the solution found
totCost = 0

#cost of the path from s to t
pathCost = 0

# Nodes on the shortest path from s to t
NSP = []

for j in instance.N:
    if j != instance.s.value:

        pathCost = 0
        t = j
        NSP = [t]

        while t != instance.s.value:
            for i in instance.N:
                if (i,t) in instance.A:
                    if instance.x[i,t].value > 0:
                        totCost = totCost + instance.c[i, t]
                        pathCost = pathCost + instance.c[i, t]
                        NSP.append(i)
                        t = i
                        break

        print "The shortest path from %d to %d has cost %d:" % (instance.s.value, j, pathCost)
        for k in range(1, len(NSP) ):
            print "(%d, %d) " % (NSP[len(NSP) - k], NSP[len(NSP) - k - 1]),
            # printf "Cost = %d\n",c[member(card(NSP) - j,NSP),member(card(NSP) - j - 1,NSP)];

        print

print "Total cost of the solution found %d\n" % totCost
