# --------------------------------------------------------Header-------------------------------------------------------- #
from __future__ import division
from pyomo.environ import *
from pyomo.opt import SolverFactory
from pyomo.core.base import *
# Import dei modelli astratti che poi vengono utilizzati nel file principale #
#------- .mod Files (Di Ampl) -------#
from knapsack_cover_abstract import generateModel_knapsack_cover
from knapsack_cover_relaxed_abstract import generateModel_knapsack_cover_relaxed
from separation_abstract import generateModel_Separation
from knapsack_abstract import model

# Import dei file di dati che poi vengono utilizzati nel file principale per creare i modelli concreti #
#------- .dat Files (Di Ampl) -------#
dat_file_name_knapsack_cover_relaxed = 'KP_1.dat'
dat_file_name_pricing = 'pricing.dat'

# -------------------------------------------------Creation data Set-------------------------------------------------- #

nc = 0

CI = {}

def get_z_i(instance_separation):
    z_set = []
    for i in instance_separation.I:
        if instance_separation.z[i].value == 1:
            z_set.append(i)
    return z_set

def get_x_star_from_instance_knapsack_cover_relaxed(instance):
    x_param = {}
    for i in instance.I:
        x_param[i] = instance.x[i].value
    return x_param

# ----------------------------------------------------Main---------------------------------------------------- #

abstract_model_knapsack_cover_relaxed = generateModel_knapsack_cover_relaxed(nc, CI)

instance_knapsack_cover_relaxed = abstract_model_knapsack_cover_relaxed.create_instance(dat_file_name_knapsack_cover_relaxed)

# -----Solver----- #
opt = SolverFactory("CPLEX")

initialSol = 0

print "\n---------------- Initial Linear Solution -------------------\n";

results = opt.solve(instance_knapsack_cover_relaxed)
results.write()

initialSol = instance_knapsack_cover_relaxed.Profit()

print initialSol

iter = 0

while iter == 0 or instance_separation.Violation() < 1:


    print "---------------- Separation Iter: %d -------------------\n" % iter
    iter = iter + 1

    abstract_model_knapsack_cover_relaxed = generateModel_knapsack_cover_relaxed(nc, CI)

    instance_knapsack_cover_relaxed = abstract_model_knapsack_cover_relaxed.create_instance(dat_file_name_knapsack_cover_relaxed)

    #solve KnapsackRelax
    results = opt.solve(instance_knapsack_cover_relaxed)

    #instance_knapsack_cover_relaxed.x.display()
    print "Profit: %f" % instance_knapsack_cover_relaxed.Profit()


    # Prepare the objective function of the separation problem
    x_star = get_x_star_from_instance_knapsack_cover_relaxed(instance_knapsack_cover_relaxed)
    I = instance_knapsack_cover_relaxed.I.values()
    w = instance_knapsack_cover_relaxed.w
    B = instance_knapsack_cover_relaxed.B.value

    #solve Separation
    abstract_model_separation = generateModel_Separation(I, x_star, w, B)
    instance_separation = abstract_model_separation.create_instance()

    results = opt.solve(instance_separation)

    print "Value Separation=%.3f" % instance_separation.Violation()

    if instance_separation.Violation() < 1:
        nc = nc + 1
        CI[nc] = get_z_i(instance_separation)


print "\n---------------- Final Linear Solution -------------------\n"

print "Final linear solution: %f" % instance_knapsack_cover_relaxed.Profit()
#print instance_knapsack_cover_relaxed.x.display()

print "\nNumber of cover inequalities considered: %d" % nc

print "\n---------------- Integer Optimal Solution -------------------\n"
print "\nInteger optimal solution: ",

abstract_model_knapsack = model

instance_knapsack = abstract_model_knapsack.create_instance(dat_file_name_knapsack_cover_relaxed)

results = opt.solve(instance_knapsack)
print instance_knapsack.Profit()
#print instance_knapsack.x.display()

print "\n---------------- Greedy Solution -------------------\n"

ratio = {}

# I created a data structure like this (ratio_val, index_item)
for i in instance_knapsack.I:
    ratio[i] = ((instance_knapsack.p[i] / instance_knapsack.w[i]), i)

# I ordered the data structure of the ratio Profit/weight from the highest to the lowest
sorted_ratio = sorted(ratio.values(), reverse=True)

#To display the items
#for i in instance_knapsack.I:
#    print i, sorted_ratio[i-1]


remaining_capacity = instance_knapsack.B
selected_items = []
profit = 0

while remaining_capacity > 0 and len(sorted_ratio) > 0:
    #If I can take the best one at this step k I take it and I update the remaning capacity
    if remaining_capacity - instance_knapsack.w[sorted_ratio[0][1]] >= 0:
        selected_items.append(sorted_ratio[0][1])
        profit = profit + instance_knapsack.p[sorted_ratio[0][1]]
        remaining_capacity = remaining_capacity - instance_knapsack.w[sorted_ratio[0][1]]
        del sorted_ratio[0]
    else:
        #If I can't take it because I don't have enough space I just discard the item from the first position
        del sorted_ratio[0]


print "Selected items: ", selected_items
print "Greedy Profit: ", profit
print "Remaning capacity: ", remaining_capacity

print "\n\n---------------- Comparison -------------------\n"
print "Greedy      %g \t\t Opt gap %g" % (profit, instance_knapsack.Profit() - profit)
print "Rel initial %g \t\t Opt gap %g" % (initialSol, instance_knapsack.Profit() - initialSol)
print "Rel final   %g \t Opt gap %g" % (instance_knapsack_cover_relaxed.Profit(), instance_knapsack.Profit() - instance_knapsack_cover_relaxed.Profit())
print "Integer     %d" % instance_knapsack.Profit()