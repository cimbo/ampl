
####################################
#        Knapsack Problem
####################################


from pyomo.environ import *

infinity = float('inf')


model = AbstractModel()

####################################
# SETS OF PATHS
####################################

# Number of items
model.n = Param(within=PositiveIntegers)

# Set of items
model.I = RangeSet(1, model.n)


####################################
# PARAMS
####################################

# Profit
model.p = Param(model.I, within=NonNegativeReals)


# Weigths
model.w = Param(model.I, within=NonNegativeReals)

# Knapsack capacity
model.B = Param(within=NonNegativeReals)

####################################
# VARS
####################################

model.x = Var(model.I, within=Binary) # >= 0, <= 1;

####################################
# OBJECTIVE FUNCTION
####################################

def Profit(model):
    return sum(model.p[i] * model.x[i] for i in model.I)
model.Profit = Objective(rule=Profit, sense=maximize)

####################################
# CONSTRAINTS
####################################

def Capacity(model):
    return sum( model.x[i] * model.w[i] for i in model.I) <= model.B
model.Capacity = Constraint(rule=Capacity)
