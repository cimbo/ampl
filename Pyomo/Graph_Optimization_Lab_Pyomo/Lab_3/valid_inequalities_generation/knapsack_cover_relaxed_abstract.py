
####################################
#    Relaxed Knapsack Cover Problem
####################################


from pyomo.environ import *

infinity = float('inf')

#Genera il modello matematico
def generateModel_knapsack_cover_relaxed(nc, CI):

    model = AbstractModel()

    ####################################
    # SETS OF PATHS
    ####################################

    # Number of items
    model.n = Param(within=PositiveIntegers)

    # Set of items
    model.I = RangeSet(1, model.n)

    # Number of new cover inequalities
    model.nc = Param(within=NonNegativeIntegers, initialize=nc, default=0)

    # Set of indices for the cover inequalities
    model.C = RangeSet(1, model.nc)

    # Cover inequalities
    model.CI = Set(model.C, initialize=CI)

    ####################################
    # PARAMS
    ####################################

    # Profit
    model.p = Param(model.I, within=NonNegativeReals)


    # Weigths
    model.w = Param(model.I, within=NonNegativeReals)

    # Knapsack capacity
    model.B = Param(within=NonNegativeReals)

    ####################################
    # VARS
    ####################################

    # 0 <= x <= 1
    model.x = Var(model.I, within=NonNegativeReals)

    def x_limit(model, i):
        return (0, model.x[i], 1)
    model.x_limit = Constraint(model.I, rule=x_limit)


    ####################################
    # OBJECTIVE FUNCTION
    ####################################

    def Profit(model):
        return sum(model.p[i] * model.x[i] for i in model.I)

    model.Profit = Objective(rule=Profit, sense=maximize)

    ####################################
    # CONSTRAINTS
    ####################################

    def Capacity(model):
        return sum( model.x[i] * model.w[i] for i in model.I) <= model.B
    model.Capacity = Constraint(rule=Capacity)

    def Cover(model, c):
        return sum( model.x[i] for i in model.CI[c]) <= len(model.CI[c]) - 1
    model.Cover = Constraint(model.C, rule=Cover)

    return model