####################################
#       VLS Knapsack Problem
####################################


from pyomo.environ import *

infinity = float('inf')


model = AbstractModel()

#Genera il modello matematico
def generateModel_VLS(selectedItems, k):

    ####################################
    # SET
    ####################################

    # Number of items
    model.n = Param(within=NonNegativeIntegers)

    # Set of items
    model.I = RangeSet(1, model.n)

    model.selectedItems = Set(initialize=selectedItems)

    ####################################
    # PARAMS
    ####################################

    # Profit
    model.p = Param(model.I, within=NonNegativeReals, default=1)

    # Weigths
    model.w = Param(model.I, within=NonNegativeReals, default=1)

    # Costs
    model.c = Param(model.I, within=NonNegativeReals, default=1)

    # Volumes
    model.q = Param(model.I, within=NonNegativeReals, default=1)

    model.k = Param(initialize=k)

    # Overall Maximum size
    model.B1 = Param()
    model.B2 = Param()
    model.B3 = Param()

    ####################################
    # VARS
    ####################################

    model.x = Var(model.I, within=Binary)  # >= 0, <= 1;

    ####################################
    # OBJECTIVE FUNCTION
    ####################################

    def OverallScore(model):
        return sum(model.p[i] * model.x[i] for i in model.I)

    model.OverallScore = Objective(rule=OverallScore, sense=maximize)

    ####################################
    # CONSTRAINTS
    ####################################

    def Capacity(model):
        return sum(model.x[i] * model.w[i] for i in model.I) <= model.B1

    model.Capacity = Constraint(rule=Capacity)

    def Volume(model):
        return sum(model.x[i] * model.q[i] for i in model.I) <= model.B2

    model.Volume = Constraint(rule=Volume)

    def Budget(model):
        return sum(model.x[i] * model.c[i] for i in model.I) <= model.B3

    model.Budget = Constraint(rule=Budget)

    def neighSize(model):
        return sum( (1 - model.x[i]) for i in selectedItems ) +\
               sum( model.x[i] for i in model.I - model.selectedItems) <= model.k
    model.neighSize = Constraint(rule=neighSize)

    return model
