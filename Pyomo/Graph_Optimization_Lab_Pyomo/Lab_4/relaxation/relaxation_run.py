# --------------------------------------------------------Header-------------------------------------------------------- #
from __future__ import division
from pyomo.environ import *
from pyomo.opt import SolverFactory
from pyomo.core.base import *
import math
# Import dei modelli astratti che poi vengono utilizzati nel file principale #
#------- .mod Files (Di Ampl) -------#
from ILP_abstract import model
from VLS_abstract import generateModel_VLS
from Lagrangian_Relaxation_abstract import generateModel_Lagrangian_Relaxation
from Surrogate_Relaxation_abstract import generateModel_Surrogate_Relaxation


# Import dei file di dati che poi vengono utilizzati nel file principale per creare i modelli concreti #
#------- .dat Files (Di Ampl) -------#
dat_file = 'exTest123.dat'


# -------------------------------------------------Creation data Set-------------------------------------------------- #

bestItems = []

selectedItems = []

#lagrangian relaxation parameters
u1 = 0
u2 = 0
u3 = 0

u1_old = 1
u2_old = 1
u3_old = 1

heur = 0
norma = 1

t = 1

k = 2

#surrogate relaxation parameters
m1 = 1
m2 = 1
m3 = 1

m1_old = 1
m2_old = 1
m3_old = 1

heur_surr = 0

ite_max = 11

# ----------------------------------------------Helper Functions---------------------------------------------- #

def get_max_value(instance, AvI):
    #As initial max I take the value of the first Item
    max_val = instance.p[AvI[0]]
    for i in AvI:
        if instance.p[i] > max_val:
            max_val = instance.p[i]
    return max_val

def get_best_items(instance, AvI):
    retval = []

    max_val = get_max_value(instance, AvI)

    for i in AvI:
        if instance.p[i] == max_val:
            retval.append(i)

    return retval

def get_relax(instance):
    retval = 0
    for i in instance.I:
        retval = retval + instance.p[i]
    return retval

# ----------------------------------------------------Main---------------------------------------------------- #

###################################################
# Integer Linear Programming model
###################################################

abstract_model_ILP_knapsack = model
instance_ILP_knapsack = abstract_model_ILP_knapsack.create_instance(dat_file)

###################################################
# Very large scale neighborhood
###################################################

abstract_model_VLS_knapsack = generateModel_VLS(selectedItems, k)
instance_VLS_knapsack = abstract_model_VLS_knapsack.create_instance(dat_file)

###################################################
# Lagrangian Relaxation
###################################################

abstract_model_Lagrangian_Relaxation_knapsack = generateModel_Lagrangian_Relaxation(u1, u2, u3)
instance_Lagrangian_Relaxation_knapsack = abstract_model_Lagrangian_Relaxation_knapsack.create_instance(dat_file)

###################################################
# Surrogate Relaxation
###################################################

abstract_model_Surrogate_Relaxation_knapsack = generateModel_Surrogate_Relaxation(m1, m2, m3)
instance_Surrogate_Relaxation_knapsack = abstract_model_Surrogate_Relaxation_knapsack.create_instance(dat_file)


# -----Solver----- #
opt = SolverFactory("CPLEX")

#insieme degli item da scegliere
AvI = instance_ILP_knapsack.I.values()

resB1 = instance_ILP_knapsack.B1.value
resB2 = instance_ILP_knapsack.B2.value
resB3 = instance_ILP_knapsack.B3.value

greedyValue = 0


print "\n---------------- Greedy Solution -------------------\n"

while len(AvI) > 0:
    bestItems = get_best_items(instance_ILP_knapsack, AvI)

    AvI = list( set(AvI) - set(bestItems))

    for i in bestItems:
        if resB1 >= instance_ILP_knapsack.w[i] and\
           resB2 >= instance_ILP_knapsack.q[i] and\
           resB3 >= instance_ILP_knapsack.c[i]:

            greedyValue = greedyValue + instance_ILP_knapsack.p[i]
            resB1 = resB1 - instance_ILP_knapsack.w[i]
            resB2 = resB2 - instance_ILP_knapsack.q[i]
            resB3 = resB3 - instance_ILP_knapsack.c[i]
            selectedItems.append(i)


print "Greedy value %d" % greedyValue

print "\n---------------- VLSN Solution -------------------\n"

bestLS = greedyValue
tmpValue = bestLS

selectedItems = []

results = opt.solve(instance_VLS_knapsack)

while instance_VLS_knapsack.OverallScore() > tmpValue:

    abstract_model_VLS_knapsack = generateModel_VLS(selectedItems, k)
    instance_VLS_knapsack = abstract_model_VLS_knapsack.create_instance(dat_file)
    # solve VLSN;
    results = opt.solve(instance_VLS_knapsack)

    if instance_VLS_knapsack.OverallScore() > bestLS:
        # soluzione migliorata
        selectedItems = []

        for i in instance_VLS_knapsack.I:
            if instance_VLS_knapsack.x[i].value == 1:
                selectedItems.append(i)

        bestLS = instance_VLS_knapsack.OverallScore()

print "Large Scale %d" % bestLS

print "\n---------------- Lagrangian Relaxation Solution -------------------\n"

relax = get_relax(instance_Lagrangian_Relaxation_knapsack)

print 'Relax %f\n' % relax

t = 1

for ii in range(1, ite_max):

    print 'Iteration %d' % ii

    abstract_model_Lagrangian_Relaxation_knapsack = generateModel_Lagrangian_Relaxation(u1, u2, u3)
    instance_Lagrangian_Relaxation_knapsack = abstract_model_Lagrangian_Relaxation_knapsack.create_instance(dat_file)

    #Solve Lagrangian Relaxation
    results = opt.solve(instance_Lagrangian_Relaxation_knapsack)
    #To short the name
    instance_LR = instance_Lagrangian_Relaxation_knapsack

    # Check for feasibility
    if sum( instance_LR.w[i] * instance_LR.x[i].value for i in instance_LR.I) <= instance_LR.B1.value and\
       sum( instance_LR.q[i] * instance_LR.x[i].value for i in instance_LR.I) <= instance_LR.B2.value and\
       sum( instance_LR.c[i] * instance_LR.x[i].value for i in instance_LR.I) <= instance_LR.B3.value:
            heur = max(heur, sum(instance_LR.p[i] * instance_LR.x[i].value for i in instance_LR.I))
            print "Lower Bound %.3f" % heur
    else:
        print "Lower Bound No Basis"

    t = 0.995 * t

    # Update lower bound
    relax = min(relax, instance_LR.Dual())

    # Update multipliers
    u1_old = u1
    u2_old = u2
    u3_old = u3

    t1 = instance_LR.B1.value - sum(instance_LR.w[i] * instance_LR.x[i].value for i in instance_LR.I)
    t2 = instance_LR.B2.value - sum(instance_LR.q[i] * instance_LR.x[i].value for i in instance_LR.I)
    t3 = instance_LR.B3.value - sum(instance_LR.c[i] * instance_LR.x[i].value for i in instance_LR.I)

    norma = math.sqrt( math.pow(t1,2) + math.pow(t2,2) + math.pow(t3,2))

    u1 = max(0, u1_old - t * (instance_LR.B1.value - sum(instance_LR.w[i] * instance_LR.x[i].value for i in instance_LR.I)) / norma)

    u2 = max(0, u2_old - t * (instance_LR.B2.value - sum(instance_LR.q[i] * instance_LR.x[i].value for i in instance_LR.I)) / norma)

    u3 = max(0, u3_old - t * (instance_LR.B3.value - sum(instance_LR.c[i] * instance_LR.x[i].value for i in instance_LR.I))/ norma)

    print "Upper Bound %.5f %.3f\n" % (relax, t)


print "\n---------------- Surrogate Relaxation Solution -------------------\n"

#t = 1
surr_relax = sum(instance_Surrogate_Relaxation_knapsack.p[i] for i in instance_Surrogate_Relaxation_knapsack.I)

m1 = 0.00001
m2 = 0.00001
m3 = 0.00001

print 'Surrogate Relaxation: %f' % surr_relax

for ii in range(1,ite_max):

    print 'Iteration %d' % ii

    abstract_model_Surrogate_Relaxation_knapsack = generateModel_Surrogate_Relaxation(m1, m2, m3)
    instance_Surrogate_Relaxation_knapsack = abstract_model_Surrogate_Relaxation_knapsack.create_instance(dat_file)
    instance_SR = instance_Surrogate_Relaxation_knapsack

    #Solve Surrogate Relaxation
    results = opt.solve(instance_SR)

    print 'Surrogate Relaxation Objective: %f' % instance_SR.OverallScore()

    # Check for feasibility
    if sum(instance_SR.w[i] * instance_SR.x[i].value for i in instance_SR.I) <= instance_SR.B1.value and \
       sum(instance_SR.q[i] * instance_SR.x[i].value for i in instance_SR.I) <= instance_SR.B2.value and \
       sum(instance_SR.c[i] * instance_SR.x[i].value for i in instance_SR.I) <= instance_SR.B3.value:
        heur_surr = max(heur_surr, sum(instance_SR.p[i] * instance_SR.x[i].value for i in instance_SR.I))
        print "Lower Bound %.3f" % heur_surr
    else:
        print "Lower Bound No Basis"

    # Update step size
    # if surr_relax < Dual + 0.01 then
    t = 0.995 * t

    # Update lower bound
    surr_relax = min(surr_relax, instance_SR.OverallScore())

    # Update multipliers
    m1_old = m1
    m2_old = m2
    m3_old = m3

    norma = 1

    m1 = max(0, m1_old - t * (min(0, instance_SR.B1.value - sum(instance_SR.w[i] * instance_SR.x[i].value for i in instance_SR.I))) / norma)
    m2 = max(0, m2_old - t * (min(0, instance_SR.B2.value - sum(instance_SR.q[i] * instance_SR.x[i].value for i in instance_SR.I))) / norma)
    m3 = max(0, m3_old - t * (min(0, instance_SR.B3.value - sum(instance_SR.c[i] * instance_SR.x[i].value for i in instance_SR.I))) / norma)

    #print'\nm1:= %f\nm2:=%f\nm3:=%f\n' % (m1, m2, m3)

    print "Upper Bound %.5f %.3f" % (surr_relax, t)

    if heur_surr == surr_relax:
        break


print "\n\nLB %5f UB %5f SurrLB %5f SurrUB %5f greedy %5f LS %5f\n\n" % (heur, relax, heur_surr, surr_relax, greedyValue, bestLS)

results = opt.solve(instance_ILP_knapsack)

print 'Objective Origina Problem: %d' % instance_ILP_knapsack.OverallScore()