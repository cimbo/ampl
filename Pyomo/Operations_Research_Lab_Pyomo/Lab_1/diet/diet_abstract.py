from pyomo.environ import *

infinity = float('inf')

model = AbstractModel()


####################################
#SETS
####################################

model.I = Set()

model.J = Set()

####################################
# PARAMS
####################################
model.c = Param(model.J)
model.q = Param(model.J)
model.b = Param(model.I)
model.a = Param(model.I, model.J)

# VARS
model.x = Var(model.J)

def x_rule(model, j):
    return (0, model.x[j] , model.q[j])
model.x_limit = Constraint(model.J, rule=x_rule)

####################################
# OBJECTIVE FUNCTION
####################################

#Se non specifici il sense nell'objective function lui di default considera il minimizzare
def cost(model):
    return  sum( model.c[j] * model.x[j] for j in model.J)
model.cost = Objective(rule=cost, sense=minimize)


# CONSTRAINTS

def nutrients_rule(model, i):
    return sum( model.a[i,j] * model.x[j] for j in model.J) >= model.b[i]
model.nutrients_limit = Constraint(model.I, rule=nutrients_rule)
