from pyomo.environ import *
from pyomo.opt import SolverFactory

infinity = float('inf')

model = AbstractModel()


####################################
#SETS
####################################

def I_Init(model):
    return range(1, 4)
model.I = Set(initialize=I_Init)



def J_Init(model):
    return range(1, 6)
model.J = Set(initialize=J_Init)


####################################
# PARAMS
####################################
model.t = Param(model.I, model.J)
model.p = Param(model.J)
model.q = Param(model.J)
model.T_min = Param(within=Reals, initialize=8)
model.T_max = Param(within=Reals, initialize=10)

# VARS
model.x = Var(model.I, model.J, within = NonNegativeIntegers )

####################################
# OBJECTIVE FUNCTION
####################################

#Se non specifici il sense nell'objective function lui di default considera il minimizzare
def revenue(model):
    return  sum( model.p[j] * model.x[i,j] for i in model.I for j in model.J)
model.revenue = Objective(rule=revenue, sense=maximize)

# CONSTRAINTS

def demand_rule(model, j):
    return (0, sum( model.x[i,j] for i in model.I) , model.q[j])
model.demand_limit = Constraint(model.J, rule=demand_rule)

def hours_rule(model, i):
    return (model.T_min, sum(model.t[i,j] * model.x[i,j]  for j in model.J) , model.T_max)
model.min_hours_limit = Constraint(model.I, rule=hours_rule)