# --------------------------------------------------------Header-------------------------------------------------------- #
from pyomo.environ import *
from pyomo.opt import SolverFactory
from pyomo.core.base import *
# Import dei modelli astratti che poi vengono utilizzati nel file principale #
#------- .mod Files (Di Ampl) -------#
from shoes_abstract import model

# ----------------------------------------------------Main---------------------------------------------------- #

# Import dei file di dati che poi vengono utilizzati nel file principale per creare i modelli concreti #
#------- .dat Files (Di Ampl) -------#
dat_file_name = 'shoes.dat'

abstract_model = model

instance = abstract_model.create_instance(dat_file_name)

# -----Solver----- #
opt = SolverFactory("CPLEX")

results = opt.solve(instance)
results.write()

print "# ----------------------------------------------------------"

instance.display()