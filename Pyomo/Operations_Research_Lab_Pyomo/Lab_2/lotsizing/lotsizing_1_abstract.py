from __future__ import division
from pyomo.environ import *

model1 = AbstractModel()

####################################
#SETS
####################################

model1.I = Set()

model1.n = Param(within=NonNegativeIntegers)

model1.J = RangeSet(1, model1.n)


####################################
# PARAMS
####################################


model1.b = Param(model1.J)
model1.d = Param(model1.I, model1.J)
model1.r = Param(model1.I)
model1.c = Param(model1.I)
model1.q = Param(model1.I)
model1.f = Param(model1.I)
model1.l = Param(model1.I)
model1.m = Param(model1.I)
model1.K = Param()

####################################
# VARS
####################################

model1.x = Var(model1.I, model1.J, within=NonNegativeIntegers)

model1.v = Var(model1.I, model1.J, within=NonNegativeReals)

model1.z = Var(model1.I, model1.J | {0}, within=NonNegativeReals)

####################################
# OBJECTIVE FUNCTION
####################################

def revenue(model):
    return  sum( model.r[i] * model.v[i,j] - model.c[i] * model.x[i,j] - model.m[i] * model.z[i,j] for i in model.I for j in model.J)
model1.revenue = Objective(rule=revenue, sense=maximize)

####################################
# CONSTRAINTS
####################################

def demand(model, i, j):
    return (0, model.v[i,j], model.d[i,j])
model1.demand = Constraint(model1.I, model1.J, rule=demand)

def production(model, j):
   return sum( (model.x[i,j]/model.q[i]) for i in model.I) <= model.b[j]
model1.production = Constraint(model1.J, rule=production)

def balance(model, i, j):
   return model.z[i,j-1] + model.x[i,j] == model.z[i,j] + model.v[i,j]
model1.balance = Constraint(model1.I, model1.J, rule=balance)

def capacity(model, j):
   return sum( model.z[i,j] for i in model.I) <= model.K
model1.capacity = Constraint(model1.J, rule=capacity)

def demand_init(model, i):
    return (0, model.z[i,0], 0)
model1.demand_init = Constraint(model1.I, rule=demand_init)