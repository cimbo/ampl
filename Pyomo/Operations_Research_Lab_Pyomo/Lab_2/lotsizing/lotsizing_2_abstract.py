from __future__ import division
from pyomo.environ import *

model2 = AbstractModel()

####################################
#SETS
####################################

model2.I = Set()

model2.n = Param(within=NonNegativeIntegers)

model2.J = RangeSet(1, model2.n)


####################################
# PARAMS
####################################


model2.b = Param(model2.J)
model2.d = Param(model2.I, model2.J)
model2.r = Param(model2.I)
model2.c = Param(model2.I)
model2.q = Param(model2.I)
model2.f = Param(model2.I)
model2.l = Param(model2.I)
model2.m = Param(model2.I)
model2.K = Param()

####################################
# VARS
####################################

model2.x = Var(model2.I, model2.J, within=NonNegativeIntegers)

model2.v = Var(model2.I, model2.J, within=NonNegativeReals)

model2.z = Var(model2.I, model2.J | {0}, within=NonNegativeReals)

model2.y = Var(model2.I, model2.J, within=Binary)

####################################
# OBJECTIVE FUNCTION
####################################

def revenue(model):
    return  sum( model.r[i] * model.v[i,j] - model.c[i] * model.x[i,j] - model.m[i] * model.z[i,j] - model.f[i] * model.y[i,j] for i in model.I for j in model.J)
model2.revenue = Objective(rule=revenue, sense=maximize)

####################################
# CONSTRAINTS
####################################

def demand(model, i, j):
    return (0, model.v[i,j], model.d[i,j])
model2.demand = Constraint(model2.I, model2.J, rule=demand)

def production(model, j):
   return sum( (model.x[i,j]/model.q[i]) for i in model.I) <= model.b[j]
model2.production = Constraint(model2.J, rule=production)

def balance(model, i, j):
   return model.z[i,j-1] + model.x[i,j] == model.z[i,j] + model.v[i,j]
model2.balance = Constraint(model2.I, model2.J, rule=balance)

def capacity(model, j):
   return sum( model.z[i,j] for i in model.I) <= model.K
model2.capacity = Constraint(model2.J, rule=capacity)

def demand_init(model, i):
    return (0, model.z[i,0], 0)
model2.demand_init = Constraint(model2.I, rule=demand_init)

# ADDITIONAL CONSTRAINTS

def activation(model, i, j):
   return model.x[i,j] <= model.b[j] * model.q[i] * model.y[i,j]
model2.activation = Constraint(model2.I, model2.J, rule=activation)

def lotsize(model, i, j):
   return model.x[i,j] >= model.l[i] * model.y[i,j]
model2.lotsize = Constraint(model2.I, model2.J, rule=lotsize)
