
from pyomo.environ import *

infinity = float('inf')

model = AbstractModel()

####################################
#SETS
####################################

model.V = Set()

model.E = Set(within= model.V * model.V)

def init_set_A(model):
    a = set()

    for (i,j) in model.E:
        a.add((i,j))
        a.add((j,i))

    return a
model.A = Set(within=model.V*model.V, initialize=init_set_A)

model.S = Set()
model.T = Set()

####################################
# PARAMS
####################################

model.s = Param()
model.t = Param()

model.k1 = Param(model.E)

def init_param_k(model, i, j):

    if (i,j) in model.E:
        return model.k1[i,j]
    else:
        return model.k1[j,i]

model.k = Param(model.A, initialize = init_param_k)

def NodesOut_init(model, node):
    retval = []
    for (i,j) in model.A:
        if i == node:
            retval.append(j)
    return retval
model.NodesOut = Set(model.V, initialize=NodesOut_init)

def NodesIn_init(model, node):
    retval = []
    for (i,j) in model.A:
        if j == node:
            retval.append(i)
    return retval
model.NodesIn = Set(model.V, initialize=NodesIn_init)

####################################
# VARS
####################################

model.x = Var(model.A)

def x_rule(model, i, j):
    return (0, model.x[i,j] , model.k[i,j])

model.x_limit = Constraint(model.A, rule=x_rule)

model.phi = Var(within=NonNegativeReals)

####################################
# OBJECTIVE FUNCTION
####################################

def value(model):
    return  model.phi
model.value = Objective(rule=value, sense=maximize)

####################################
# CONSTRAINTS
####################################

def balance_s(model, s):
    return (sum( model.x[s,j] for j in model.NodesOut[s]) - sum( model.x[i,s] for i in model.NodesOut[s])) == model.phi
model.balance_s = Constraint(model.S, rule=balance_s)

def balance_t(model, t):
    return (sum( model.x[t,j] for j in model.NodesOut[t]) - sum( model.x[i,t] for i in model.NodesOut[t])) == - model.phi
model.balance_t = Constraint(model.T, rule=balance_t)

def balance(model, h):
    return (sum( model.x[h,j] for j in model.NodesOut[h]) - sum( model.x[i,h] for i in model.NodesOut[h])) == 0
model.balance = Constraint(model.V - model.S - model.T, rule=balance)


