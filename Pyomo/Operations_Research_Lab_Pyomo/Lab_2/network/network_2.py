
from pyomo.environ import *

infinity = float('inf')

model2 = AbstractModel()

####################################
#SETS
####################################

model2.V = Set()

model2.E = Set(within=model2.V * model2.V)

def init_set_A(model):
    a = set()

    for (i,j) in model.E:
        a.add((i,j))
        a.add((j,i))

    return a
model2.A = Set(within=model2.V * model2.V, initialize=init_set_A)

model2.D = Set()

####################################
# PARAMS
####################################

model2.s = Param(model2.D, default=0);
model2.t = Param(model2.D, default=0);

model2.k1 = Param(model2.E)

def init_param_k(model, i, j):

    if (i,j) in model.E:
        return model.k1[i,j]
    else:
        return model.k1[j,i]

model2.k = Param(model2.A, initialize = init_param_k)

def NodesOut_init(model, node):
    retval = []
    for (i,j) in model.A:
        if i == node:
            retval.append(j)
    return retval
model2.NodesOut = Set(model2.V, initialize=NodesOut_init)

def NodesIn_init(model, node):
    retval = []
    for (i,j) in model.A:
        if j == node:
            retval.append(i)
    return retval
model2.NodesIn = Set(model2.V, initialize=NodesIn_init)

####################################
# VARS
####################################

model2.x = Var(model2.A * model2.D, within=NonNegativeIntegers)

model2.phi = Var(model2.D, within=NonNegativeReals)

####################################
# OBJECTIVE FUNCTION
####################################

def value(model):
    return  sum(model.phi[d] for d in model.D)
model2.value = Objective(rule=value, sense=maximize)

####################################
# CONSTRAINTS
####################################

def balance_s(model, d):
        return sum( model.x[model.s[d],j,d] for j in model.NodesOut[model.s[d]]) - sum( model.x[i,model.s[d],d] for i in model.NodesOut[model.s[d]]) == model.phi[d]
model2.balance_s = Constraint(model2.D, rule=balance_s)

def balance_t(model, d):
        return sum( model.x[model.t[d],j,d] for j in model.NodesOut[model.t[d]]) - sum( model.x[i,model.t[d],d] for i in model.NodesOut[model.t[d]]) == - model.phi[d]
model2.balance_t = Constraint(model2.D, rule=balance_t)

def balance(model, h, d):
    if h != model.s[d] and h != model.t[d]:
        return sum( model.x[h,j,d] for j in model.NodesOut[h]) - sum( model.x[i,h,d] for i in model.NodesOut[h]) == 0
    else:
        return Constraint.Skip
model2.balance = Constraint(model2.V, model2.D, rule=balance)

def capacity(model, i, j):
    return sum( model.x[i,j,d] for d in model.D) <= model.k[i,j]
model2.capacity = Constraint(model2.A, rule=capacity)