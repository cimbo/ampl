
from pyomo.environ import *

infinity = float('inf')

modelb = AbstractModel()

####################################
#SETS
####################################

modelb.V = Set()

modelb.E = Set(within=modelb.V * modelb.V)

def init_set_A(model):
    a = set()

    for (i,j) in model.E:
        a.add((i,j))
        a.add((j,i))

    return a
modelb.A = Set(within=modelb.V * modelb.V, initialize=init_set_A)

modelb.S = Set()
modelb.T = Set()

def init_set_AE(model):
    ae = set()

    for (l,m) in model.A:
        ae.add((l,m))
    for i in model.T:
        for j in model.S:
            ae.add((i,j))

    return ae
modelb.AE = Set(dimen=2, initialize=init_set_AE)

####################################
# PARAMS
####################################

modelb.s = Param()
modelb.t = Param()

modelb.k1 = Param(modelb.E)

def init_param_k(model, i, j):

    if (i,j) in model.E:
        return model.k1[i,j]
    else:
        if (j,i) in model.E:
            return model.k1[j,i]
        else:
            return infinity

modelb.k = Param(modelb.AE, initialize = init_param_k)

def NodesOut_init(model, node):
    retval = []
    for (i,j) in model.AE:
        if i == node:
            retval.append(j)
    return retval
modelb.NodesOut = Set(modelb.V, initialize=NodesOut_init)

def NodesIn_init(model, node):
    retval = []
    for (i,j) in model.AE:
        if j == node:
            retval.append(i)
    return retval
modelb.NodesIn = Set(modelb.V, initialize=NodesIn_init)

def NodesInSpecial_init(model, node):
    retval = []
    for (i,j) in model.A:
        if j == node:
            retval.append(i)
    return retval
modelb.NodesInSpecial = Set(modelb.V, initialize=NodesInSpecial_init)

def NodesOutSpecial_init(model, node):
    retval = []
    for (i,j) in model.A:
        if i == node:
            retval.append(j)
    return retval
modelb.NodesOutSpecial = Set(modelb.V, initialize=NodesOutSpecial_init)

####################################
# VARS
####################################

modelb.x = Var(modelb.AE)

def x_rule(model, i, j):
    return (0, model.x[i,j] , model.k[i,j])

modelb.x_limit = Constraint(modelb.AE, rule=x_rule)

####################################
# OBJECTIVE FUNCTION
####################################

def value(model, t, s):
    return  model.x[t,s]
modelb.value = Objective(modelb.T, modelb.S, rule=value, sense=maximize)

####################################
# CONSTRAINTS
####################################

def balance(model, h):
    if h in model.V - model.T - model.S:
        return (sum( model.x[h,j] for j in model.NodesOut[h]) - sum( model.x[i,h] for i in model.NodesOut[h])) == 0
    else:
        if h in model.T:
            return (sum(model.x[h, j] for j in model.NodesOut[h]) - sum(model.x[i, h] for i in model.NodesOutSpecial[h])) == 0
        else:
            if h in model.S:
                return (sum(model.x[h, j] for j in model.NodesOut[h]) - sum(
                    model.x[i, h] for i in model.NodesIn[h])) == 0
modelb.balance = Constraint(modelb.V, rule=balance)


