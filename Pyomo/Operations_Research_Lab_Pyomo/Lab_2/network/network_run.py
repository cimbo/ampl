# --------------------------------------------------------Header-------------------------------------------------------- #
from pyomo.environ import *
from pyomo.opt import SolverFactory
from pyomo.core.base import *
# Import dei modelli astratti che poi vengono utilizzati nel file principale #
#------- .mod Files (Di Ampl) -------#
from network import model
from network_b import modelb
from network_2 import model2
# ----------------------------------------------------Main---------------------------------------------------- #

# Import dei file di dati che poi vengono utilizzati nel file principale per creare i modelli concreti #
#------- .dat Files (Di Ampl) -------#
dat_file_name = 'network.dat'
dat_file_name_2 = 'network_2.dat'

print "\n\n# --------------------------Model 1--------------------------------\n\n"

abstract_model = model
instance = abstract_model.create_instance(dat_file_name)

# -----Solver----- #
opt = SolverFactory("CPLEX")

results = opt.solve(instance)
results.write()

print "# ----------------------------------------------------------"

instance.display()

print "\n\n# --------------------------Model b--------------------------------\n\n"

abstract_model_b = modelb
instance = abstract_model_b.create_instance(dat_file_name)

results = opt.solve(instance)
results.write()

print "# ----------------------------------------------------------"

instance.display()

print "\n\n# --------------------------Model 2--------------------------------\n\n"

abstract_model_2 = model2
instance = abstract_model_2.create_instance(dat_file_name_2)

results = opt.solve(instance)
results.write()

print "# ----------------------------------------------------------"

instance.display()