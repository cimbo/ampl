
from pyomo.environ import *

infinity = float('inf')

model_no_budget = AbstractModel()

####################################
#SETS
####################################

model_no_budget.m = Param(within=NonNegativeIntegers)

model_no_budget.I = RangeSet(1, model_no_budget.m)

model_no_budget.n = Param(within=NonNegativeIntegers)

model_no_budget.J = RangeSet(1, model_no_budget.n)

####################################
# PARAMS
####################################

model_no_budget.c = Param(model_no_budget.J)
model_no_budget.a = Param(model_no_budget.I, model_no_budget.J)
model_no_budget.b = Param(model_no_budget.J)
model_no_budget.B = Param()

####################################
# VARS
####################################

model_no_budget.x = Var(model_no_budget.J, within=Binary)

####################################
# OBJECTIVE FUNCTION
####################################

def capacity(model):
    return  sum( model.b[j] * model.x[j] for j in model.J)
model_no_budget.capacity = Objective(rule=capacity, sense=maximize)

####################################
# CONSTRAINTS
####################################


def packing(model, i):
    return sum( model.a[i,j] * model.x[j] for j in model.J ) <= 1
model_no_budget.packing = Constraint(model_no_budget.I, rule=packing)