# --------------------------------------------------------Header-------------------------------------------------------- #
from pyomo.environ import *
from pyomo.opt import SolverFactory
from pyomo.core.base import *
# Import dei modelli astratti che poi vengono utilizzati nel file principale #
#------- .mod Files (Di Ampl) -------#
from burners import model
from burners_no_budget_limit import model_no_budget
# ----------------------------------------------------Main---------------------------------------------------- #

# Import dei file di dati che poi vengono utilizzati nel file principale per creare i modelli concreti #
#------- .dat Files (Di Ampl) -------#
dat_file_name = 'burners.dat'

print "\n\n# --------------------------Model 1--------------------------------\n\n"

abstract_model = model
instance = abstract_model.create_instance(dat_file_name)

# -----Solver----- #
opt = SolverFactory("CPLEX")

results = opt.solve(instance)
results.write()

print "# ----------------------------------------------------------"

instance.display()

print "\n\n# --------------------------Model No Budget Limit--------------------------------\n\n"

abstract_model_no_budget_limit = model_no_budget
instance = abstract_model_no_budget_limit.create_instance(dat_file_name)

results = opt.solve(instance)
results.write()

print "# ----------------------------------------------------------"

instance.display()