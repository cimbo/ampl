
from pyomo.environ import *
import math

infinity = float('inf')

model1 = AbstractModel()

####################################
#SETS
####################################

model1.V = Set()

model1.J = Set()

####################################
# PARAMS
####################################

model1.coord1 = Param(model1.V)
model1.coord2 = Param(model1.V)

def init_param_d(model, i, j):
    d1 = (model.coord1[i] - model.coord1[j])
    d2 = (model.coord2[i] - model.coord2[j])

    return math.sqrt( pow(d1, 2) + pow(d2, 2) )
model1.d = Param(model1.V, model1.V, initialize=init_param_d)

model1.delta = Param(within=NonNegativeReals)
model1.gamma = Param(within=NonNegativeReals)

####################################
#          AUXILIARY SETS          #
####################################

def init_set_E(model):
    e = set()

    for i in model.V:
        for j in model.V:
            if i < j and (model.d[i,j] <= model.delta):
                e.add((i,j))

    return e

model1.E = Set(dimen=2, initialize=init_set_E)

####################################
# VARS
####################################

model1.x = Var(model1.V, model1.J, within=Binary)

model1.y = Var(model1.J, within=Binary)

####################################
# OBJECTIVE FUNCTION
####################################

def use(model):
    return  sum( model.y[j] for j in model.J)
model1.use = Objective(rule=use, sense=minimize)

def assignment(model, i):
    return  sum( model.x[i,j] for j in model.J ) == 1
model1.assignment = Constraint(model1.V, rule=assignment)

def activation(model, i, j):
    return  model.x[i,j] <= model.y[j]
model1.activation = Constraint(model1.V, model1.J, rule=activation)

def interference(model, i, j, k):
    return  model.x[i,k] + model.x[j,k] <= 1
model1.interference = Constraint(model1.E, model1.J, rule=interference)
