
from pyomo.environ import *
import math

infinity = float('inf')

model2 = AbstractModel()

####################################
#SETS
####################################

model2.V = Set()

model2.J = Set()

####################################
# PARAMS
####################################

model2.coord1 = Param(model2.V)
model2.coord2 = Param(model2.V)

def init_param_d(model, i, j):
    d1 = (model.coord1[i] - model.coord1[j])
    d2 = (model.coord2[i] - model.coord2[j])

    return math.sqrt( pow(d1, 2) + pow(d2, 2) )
model2.d = Param(model2.V, model2.V, initialize=init_param_d)

model2.delta = Param(within=NonNegativeReals)
model2.gamma = Param(within=NonNegativeReals)

####################################
#          AUXILIARY SETS          #
####################################

def init_set_E(model):
    e = set()

    for i in model.V:
        for j in model.V:
            if i < j and (model.d[i,j] <= model.delta):
                e.add((i,j))

    return e

model2.E = Set(dimen=2, initialize=init_set_E)

####################################
# VARS
####################################

model2.x = Var(model2.V, model2.J, within=Binary)

model2.y = Var(model2.J, within=Binary)

####################################
# OBJECTIVE FUNCTION
####################################

def use(model):
    return  sum( model.y[j] for j in model.J)
model2.use = Objective(rule=use, sense=minimize)

def assignment(model, i):
    return  sum( model.x[i,j] for j in model.J ) == 1
model2.assignment = Constraint(model2.V, rule=assignment)

def activation(model, i, j):
    return  model.x[i,j] <= model.y[j]
model2.activation = Constraint(model2.V, model2.J, rule=activation)

def interference2(model, i, j, k, z):
    if abs(k - z) <= model.gamma:
        return  model.x[i,k] + model.x[j,z] <= 1
    return Constraint.Skip
model2.interference2 = Constraint(model2.E, model2.J, model2.J, rule=interference2)
