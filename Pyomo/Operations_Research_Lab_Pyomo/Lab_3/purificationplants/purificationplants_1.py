
from pyomo.environ import *

infinity = float('inf')

model1 = AbstractModel()

####################################
#SETS
####################################

model1.m = Param(within=NonNegativeIntegers)

model1.I = RangeSet(1, model1.m)

model1.n = Param(within=NonNegativeIntegers)

model1.J = RangeSet(1, model1.n)

####################################
# PARAMS
####################################

model1.c = Param(model1.J)
model1.a = Param(model1.I, model1.J)
model1.b = Param(model1.J)
model1.Q = Param()

####################################
# VARS
####################################

model1.x = Var(model1.J, within=Binary)

####################################
# OBJECTIVE FUNCTION
####################################

def cost(model):
    return  sum( model.x[j] * model.c[j] for j in model.J)
model1.cost = Objective(rule=cost, sense=minimize)

####################################
# CONSTRAINTS
####################################

def covering(model, i):
    return sum( model.a[i,j] * model.x[j] for j in model.J ) >= 1
model1.covering = Constraint(model1.I, rule=covering)

