
from pyomo.environ import *

infinity = float('inf')

model2 = AbstractModel()

####################################
#SETS
####################################

model2.m = Param(within=NonNegativeIntegers)

model2.I = RangeSet(1, model2.m)

model2.n = Param(within=NonNegativeIntegers)

model2.J = RangeSet(1, model2.n)

####################################
# PARAMS
####################################

model2.c = Param(model2.J)
model2.a = Param(model2.I, model2.J)
model2.b = Param(model2.J)
model2.Q = Param()

####################################
# VARS
####################################

model2.x = Var(model2.J, within=Binary)

####################################
# OBJECTIVE FUNCTION
####################################

def cost(model):
    return  sum( model.x[j] * model.c[j] for j in model.J)
model2.cost = Objective(rule=cost, sense=minimize)

####################################
# CONSTRAINTS
####################################

def partitioning(model, i):
    return sum( model.a[i,j] * model.x[j] for j in model.J ) == 1
model2.partitioning = Constraint(model2.I, rule=partitioning)

