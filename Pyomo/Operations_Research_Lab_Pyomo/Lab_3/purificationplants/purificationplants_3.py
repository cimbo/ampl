
from pyomo.environ import *

infinity = float('inf')

model3 = AbstractModel()

####################################
#SETS
####################################

model3.m = Param(within=NonNegativeIntegers)

model3.I = RangeSet(1, model3.m)

model3.n = Param(within=NonNegativeIntegers)

model3.J = RangeSet(1, model3.n)

####################################
# PARAMS
####################################

model3.c = Param(model3.J)
model3.a = Param(model3.I, model3.J)
model3.b = Param(model3.J)
model3.Q = Param()

####################################
# VARS
####################################

model3.x = Var(model3.J, within=Binary)

####################################
# OBJECTIVE FUNCTION
####################################

def cost(model):
    return  sum( model.x[j] * model.c[j] for j in model.J)
model3.cost = Objective(rule=cost, sense=minimize)

####################################
# CONSTRAINTS
####################################

def covering(model, i):
    return sum( model.a[i,j] * model.x[j] for j in model.J ) >= 1
model3.covering = Constraint(model3.I, rule=covering)

def quantity(model):
    return sum( model.b[j] * model.x[j] for j in model.J ) >= model.Q
model3.quantity = Constraint(rule=quantity)
