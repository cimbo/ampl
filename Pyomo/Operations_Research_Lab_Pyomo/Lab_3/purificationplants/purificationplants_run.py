# --------------------------------------------------------Header-------------------------------------------------------- #
from pyomo.environ import *
from pyomo.opt import SolverFactory
from pyomo.core.base import *
# Import dei modelli astratti che poi vengono utilizzati nel file principale #
#------- .mod Files (Di Ampl) -------#
from purificationplants_1 import model1
from purificationplants_2 import model2
from purificationplants_3 import model3
# ----------------------------------------------------Main---------------------------------------------------- #

# Import dei file di dati che poi vengono utilizzati nel file principale per creare i modelli concreti #
#------- .dat Files (Di Ampl) -------#
dat_file_name = 'purificationplants.dat'

print "\n\n# --------------------------Model 1--------------------------------\n\n"

abstract_model_1 = model1
instance = abstract_model_1.create_instance(dat_file_name)

# -----Solver----- #
opt = SolverFactory("CPLEX")

results = opt.solve(instance)
results.write()

print "# ----------------------------------------------------------"

instance.display()

print "\n\n# --------------------------Model 2--------------------------------\n\n"

abstract_model_2 = model2
instance = abstract_model_2.create_instance(dat_file_name)

# -----Solver----- #
opt = SolverFactory("CPLEX")

results = opt.solve(instance)
results.write()

print "# ----------------------------------------------------------"

instance.display()

print "\n\n# --------------------------Model 3--------------------------------\n\n"

abstract_model_3 = model3
instance = abstract_model_3.create_instance(dat_file_name)

# -----Solver----- #
opt = SolverFactory("CPLEX")

results = opt.solve(instance)
results.write()

print "# ----------------------------------------------------------"

instance.display()

