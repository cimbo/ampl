# --------------------------------------------------------Header-------------------------------------------------------- #
from pyomo.environ import *
from pyomo.opt import SolverFactory
# Import dei modelli astratti che poi vengono utilizzati nel file principale #
#------- .mod Files (Di Ampl) -------#
from lotsizing_sensitivity_abstract import model

# Import dei file di dati che poi vengono utilizzati nel file principale per creare i modelli concreti #
#------- .dat Files (Di Ampl) -------#
dat_file_name = 'lotsizing_sensitivity.dat'

# ----------------------------------------------------Main---------------------------------------------------- #

abstract_model = model

instance = abstract_model.create_instance(dat_file_name)

# Create a dual suffix component on the instance
# so the solver plugin will know which suffixes to collect
instance.dual = Suffix(direction=Suffix.IMPORT)
instance.slack = Suffix(direction=Suffix.IMPORT)
instance.rc = Suffix(direction=Suffix.IMPORT)

# ---------------------------------------------------Solver---------------------------------------------------- #
opt = SolverFactory("CPLEX")
results = opt.solve(instance)
results.write()

print '\n# ----------------------------------------------------------'
print 'x[j]:='

for j in instance.J:
    print j , instance.x[j].value

print instance.dual.values()

#question 2
#The functions lslack() and uslack() return the upper and lower slacks, respectively, for a constraint.
print '\nProduction.current', (instance.B - instance.production.uslack())
print 'Production.dual', instance.dual[instance.production]
print
#question 3 4 5
for j in instance.J:
    print 'Production.current', (instance.d[j] - instance.demand[j].uslack())
    print 'Production.dual', instance.dual[instance.demand[j]]
    print

print instance.dual.values()
print instance.rc.values()

print 'x.rc [j] :='
for j in instance.J:
    print j# , instance.get(instance.x[j].rc)
