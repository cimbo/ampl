
from pyomo.environ import *

infinity = float('inf')

model = AbstractModel()

####################################
#SETS
####################################

#generators
model.I = Set()

####################################
# PARAMS
####################################

#day demand in MWh
model.d = Param(within=NonNegativeReals)

#night demand in MWh
model.d2 = Param(within=NonNegativeReals)

#cost per MWh for generator i in I
model.c = Param(model.I)

#day activation cost for generator i in I
model.f = Param(model.I)

#night activation cost for generator i in I
model.f2 = Param(model.I)

#half-day max production for generator i in I (half-day mode)
model.k = Param(model.I)

#half-day max production for generator i in I (full-day mode)
model.k2 = Param(model.I)

####################################
# VARS
####################################

#energy produced by generator i in I during the day
model.x = Var(model.I, within=NonNegativeReals)

#energy produced by generator i in I during the night
model.x2 = Var(model.I, within=NonNegativeReals)

#1 if generator i in I is active only during the day, 0 otherwise
model.y = Var(model.I, within=Binary)

#1 if generator i in I is active only during the night, 0 otherwise
model.y2 = Var(model.I, within=Binary)

#1 if generator i in I is active during both day and night, 0 otherwise
model.z = Var(model.I, within=Binary)

####################################
# OBJECTIVE FUNCTION
####################################

def cost(model):
    return  sum( model.c[i] * (model.x[i] + model.x2[i]) +
                 model.f[i] * (model.y[i] + model.z[i]) +
                 model.f2[i] * (model.y2[i] + model.z[i]) for i in model.I)
model.cost = Objective(rule=cost, sense=minimize)

####################################
# CONSTRAINTS
####################################

def demand_day(model):
    return sum( model.x[i] for i in model.I ) >= model.d
model.demand_day = Constraint(rule=demand_day)

def demand_night(model):
    return sum( model.x2[i] for i in model.I ) >= model.d2
model.demand_night = Constraint(rule=demand_night)

def max_production_day(model, i):
    return model.x[i] <= model.k[i] * model.y[i] + model.k2[i] * model.z[i]
model.max_production_day = Constraint(model.I, rule=max_production_day)

def max_production_night(model, i):
    return model.x2[i] <= model.k[i] * model.y2[i] + model.k2[i] * model.z[i]
model.max_production_night = Constraint(model.I, rule=max_production_night)

def activation(model, i):
    return model.y[i] + model.y2[i] + model.z[i] <= 1
model.activation = Constraint(model.I, rule=activation)
