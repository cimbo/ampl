
from pyomo.environ import *

infinity = float('inf')

model = AbstractModel()

####################################
#SETS
####################################

#disciplines
model.I = Set()

model.G = Param()
#days
model.J = RangeSet(1,model.G)

####################################
# PARAMS
####################################

#available time per day
model.T = Param()

#maximum number of days between first and last match
model.n = Param()

#1 if i-th and i2-th matches are in conflict, i, i2 in I
model.c = Param(model.I, model.I)

#number of matches for discipline i in I
model.m = Param(model.I)

#degree of interest for discipline i in I
model.w = Param(model.I)

#length of a match for discipline i in I
model.t = Param(model.I)

####################################
# VARS
####################################

#1 if a match of discipline i is played in day j,i in I, j in J
model.x = Var(model.I, model.J, within=Binary)

#interest of the less interesting day
model.eta = Var(within=NonNegativeReals)

####################################
# OBJECTIVE FUNCTION
####################################

def min_interest(model):
    return model.eta
model.min_interest = Objective(rule=min_interest, sense=maximize)

####################################
# CONSTRAINTS
####################################

def interest(model, j):
    return sum( model.x[i,j] * model.w[i] for i in model.I ) >= model.eta
model.interest = Constraint(model.J, rule=interest)

def conflicts(model, i, i2, j):
    if model.c[i,i2] == 1:
        return model.x[i,j] + model.x[i2,j] <= 1
    else:
        return Constraint.Skip
model.conflicts = Constraint(model.I, model.I, model.J, rule=conflicts)

def number(model, i):
    return sum( model.x[i,j] for j in model.J ) == model.m[i]
model.number = Constraint(model.I, rule=number)

def time(model, j):
    return sum( model.x[i,j] * model.t[i] for i in model.I ) <= model.T
model.time = Constraint(model.J, rule=time)

def limit(model, i, j, j2):
    if j - j2 > model.n:
        return model.x[i,j] + model.x[i,j2] <= 1
    else:
        return Constraint.Skip
model.limit = Constraint(model.I, model.J, model.J, rule=limit)
