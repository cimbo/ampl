####################################
#       VNoM 3-INDEX PROBLEM
####################################
from __future__ import division
from pyomo.environ import *

infinity = float('inf')

#Genera il modello matematico
def generateModel_VNoM_3index(n_param, k_set, r_param, L_set, vn_param, vl_param, Vf_set, Vfc_set, loc_param, qin_param, qout_param, fixed_center_param):

    model = AbstractModel()

    ####################################
    # PARAMS & SETS
    ####################################

    # Set of requests
    model.K = Set(initialize=k_set)

    model.n = Param(initialize=n_param)

    # Sets of physical nodes and arcs
    model.N = RangeSet(1, model.n)

    model.L = Set(within=model.N * model.N, initialize=L_set)

    #####################################
    # Physical nodes and links capacities
    #####################################

    # residual capacity
    def init_r(model, i, j):
        return r_param[i,j]
    model.r = Param(model.L, within=NonNegativeReals, initialize=init_r)

    ########################################
    # Sets of virtual nodes, one per request
    ########################################

    def init_vn(model, k):
        return vn_param[k]

    # number of nodes VN
    model.vn = Param(model.K, initialize=init_vn)

    # Virtual request traffic matrix (nominal)


    def VN_Init(model, kk):

        retval = []
        for jj in range(1, model.vn[kk] + 1):
            retval.append(jj)
        return retval

    # helper set
    model.VN = Set(model.K, initialize=VN_Init)

    def VNl_Init(model, kk):
        retval = []
        for jj in range(2, model.vn[kk] + 1):
            retval.append(jj)
        return retval

    # set of leaves
    model.VNl = Set(model.K, initialize=VNl_Init)

    # number of arcs of each VN(the same for all)
    model.D = 6

    #HELPER SET
    def KN_Init(model):

        dict = []

        for k in model.K:
            for l in model.VN[k]:
                dict.append((k,l))

        return dict


    model.KN = Set(dimen=2, initialize=KN_Init)

    def qin_init(model,k,l):
        return qin_param[k,l]
    model.qin = Param(model.KN, default=0, initialize=qin_init)

    def qout_init(model, k, l):
        return qout_param[k, l]
    model.qout = Param(model.KN, default=0, initialize=qout_init)

    # Location of leaves

    #IN THEORY WITHIN model.N . WHEN 0 means that the parameter is not defined
    def init_vl(model, k, l):
        return vl_param[k,l]
    model.vl = Param(model.KN, default=0, initialize=init_vl)

    # Set of nodes considered as leaves for different VNs

    def Vf_init(model,k):
        return Vf_set[k].value
    model.Vf = Set(model.K, within= model.N, initialize=Vf_init)

    #Only one element at the time
    def Vfc_init(model, k):
        return Vfc_set[k]
    model.Vfc = Set(model.K, initialize=Vfc_init)

    ########################################
    # param location
    ########################################

    # HELPER SET
    def LOC_Init(model):

        dict = []

        for k in model.K:
            for l in model.VNl[k]:
                for ip in model.Vf[k]:
                    dict.append((k, l, ip))

        return dict

    model.LOC = Set(dimen=3, initialize=LOC_Init)

    def loc_init(model,k,l,ip):
        return loc_param[k,l,ip]
    #kk in K, VNl[kk], ip in Vf[kk]
    model.loc = Param(model.LOC, default=0, initialize=loc_init)

    ####################################
    # VARS
    ####################################

    # HELPER SET
    def Y_Init(model):

        dict = []

        for k in model.K:
            for l in model.Vfc[k]:
                dict.append((k,l))
        return dict

    model.Y = Set(dimen=2, initialize=Y_Init)

    # One if a virtual ii from request rr is mapped onto physical node jj (within V_local)
    model.y = Var(model.Y, within=Binary)

    # Multicommodity flow for virtual request rr between virtual nodes ii and jj on physical arc (i,j)
    # Note: it's defined only for strictly positive demands

    # HELPER SET
    def Z_Init(model):

        dict = []

        for k in model.K:
            for l in model.VNl[k]:
                for (i,j) in model.L:
                    dict.append((k, l, i, j))

        return dict


    #{kk in K, jj in VNl[kk], (i,j) in L
    model.Z = Set(dimen=4, initialize=Z_Init)

    model.zin = Var(model.Z, within=NonNegativeReals) #considero (1,jj)

    model.zout = Var(model.Z, within=NonNegativeReals) # considero (jj,1)

    def prova(model):
        retval = []

        '''    print 'Set K'
        for k in model.K:
            print k

        print 'n'
        print model.n.value

        print 'Set N'
        for n in model.N:
            print n

        print 'Set L'
        for l in model.L:
            print l

        print 'a'
        for l in model.L:
            print model.a[l]

        print 'vn'
        for k in model.K:
            print model.vn[k]

        print 'Set VN'
        for k in model.K:
            for i in model.VN[k]:
                print i

        print 'Set VNl'
        for k in model.K:
            for i in model.VNl[k]:
                print i

        print 'qin'
        for v in model.KN:
            print model.qin[v]

        print 'qout'
        for v in model.KN:
            print model.qout[v]

        print 'vl'
        for v in model.KN:
            print model.vl[v]

        print 'Vf'
        for k in model.K:
            for i in model.Vf[k]:
                print i

        print 'Vfc'
        for k in model.K:
            print model.Vfc[k].value

        print 'Loc'
        for loc in model.LOC:
            print model.loc[loc]

        print 'Set Y'
        for k in model.K:
            for l in model.Vfc[k]:
                print l'''

        return retval

    # helper set
    model.prova = Set(initialize=prova)


    ####################################
    # OBJECTIVE FUNCTION
    ####################################

    def Price(model):
        return sum( model.qin[k,l] * sum( (1/model.r[i,j]) * model.zin[k,l,i,j] for (i,j) in model.L ) +
                    model.qout[k,l] * sum( (1/model.r[i,j]) * model.zout[k,l,i,j] for (i,j) in model.L)
                     for k in model.K for l in model.VNl[k])
    model.Price = Objective(rule=Price, sense=minimize)


    ####################################
    # CONSTRAINTS
    ####################################

    # ---------
    # EMBEDDING
    # ---------
    # A virtual node assigned to exactly a single physical node
    # (since each VN is accepted in this case)
    def mapping(model, k, l):
        return model.y[k,l]  <= fixed_center_param[k,l]
    model.mapping = Constraint(model.Y,rule=mapping)



    # Arc capacities constraint
    #def arc_capacities(model, i, j):
    #    return sum(model.qin[kk, im] * model.zin[kk, im, i, j] + model.qout[kk, im] * model.zout[kk, im, i, j] for kk in
    #               model.K for im in model.VNl[kk]) <= model.r[i, j]
    #model.arc_capacities = Constraint(model.L, rule=arc_capacities)

    # HELPER SET {kk in K, ip in Vfc[kk], im in VNl[kk]}
    def FLOW1_Init(model):

        dict = []

        for kk in model.K:
            for ip in model.Vfc[kk]:
                for im in model.VNl[kk]:
                    dict.append((kk, ip, im))
        return dict

    model.FLOW1 = Set(dimen=3, initialize=FLOW1_Init)

    # HELPER SET { kk in K, ip in Vfc[kk], im in VNl[kk] }
    def FLOW2_Init(model):

        dict = []

        for kk in model.K:
            for ip in model.Vf[kk]:
                for im in model.VNl[kk]:
                    dict.append((kk, ip, im))
        return dict

    model.FLOW2 = Set(dimen=3, initialize=FLOW2_Init)

    def LIn_init(model, node):
        retval = []
        for (i, j) in model.L:
            if j == node:
                retval.append(i)
        return retval

    model.LIn = Set(model.N, initialize=LIn_init)

    def LOut_init(model, node):
        retval = []
        for (i, j) in model.L:
            if i == node:
                retval.append(j)
        return retval

    model.LOut = Set(model.N, initialize=LOut_init)

    # centres
    def flows1a(model, kk, ip, im):
        return sum(model.zin[kk, im, ip, j] for j in model.LOut[ip]) - sum(
            model.zin[kk, im, j, ip] for j in model.LIn[ip]) == model.y[kk, ip]
    model.flows1a = Constraint(model.FLOW1, rule=flows1a)

    # leaves
    def flows2a(model, kk, ip, im):
        if model.loc[kk, im, ip] == 1:
            return sum(model.zin[kk, im, ip, j] for j in model.LOut[ip]) - sum(
                model.zin[kk, im, j, ip] for j in model.LIn[ip]) == -1
        else:
            return Constraint.Skip
    model.flows2a = Constraint(model.FLOW2, rule=flows2a)

    def flows3a(model, kk, ip, im):
        if model.loc[kk, im, ip] == 0:
            return sum(model.zin[kk, im, ip, j] for j in model.LOut[ip]) - sum(
                model.zin[kk, im, j, ip] for j in model.LIn[ip]) == 0
        else:
            return Constraint.Skip
    model.flows3a = Constraint(model.FLOW2, rule=flows3a)

    def flows1b(model, kk, ip, im):
        return sum(model.zout[kk, im, ip, j] for j in model.LOut[ip]) - sum(
            model.zout[kk, im, j, ip] for j in model.LIn[ip]) == -model.y[kk, ip]

    model.flows1b = Constraint(model.FLOW1, rule=flows1b)

    def flows2b(model, kk, ip, im):
        if model.loc[kk, im, ip] == 1:
            return sum(model.zout[kk, im, ip, j] for j in model.LOut[ip]) - sum(
                model.zout[kk, im, j, ip] for j in model.LIn[ip]) == 1
        else:
            return Constraint.Skip
    model.flows2b = Constraint(model.FLOW2, rule=flows2b)

    def flows3b(model, kk, ip, im):
        if model.loc[kk, im, ip] == 0:
            return sum(model.zout[kk, im, ip, j] for j in model.LOut[ip]) - sum(
                model.zout[kk, im, j, ip] for j in model.LIn[ip]) == 0
        else:
            return Constraint.Skip
    model.flows3b = Constraint(model.FLOW2, rule=flows3b)

    return model

