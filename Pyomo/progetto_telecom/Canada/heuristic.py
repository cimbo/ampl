# --------------------------------------------------------Header-------------------------------------------------------- #
from pyomo.environ import *
import random
import time
from pyomo.opt import SolverFactory, SolverStatus, TerminationCondition
from pyomo.core.base import *
# Import dei modelli astratti che poi vengono utilizzati nel file principale #
#------- .mod Files (Di Ampl) -------#
from VNoM import generateModel_VNoM_3index
from milp_3index import generateModel_milp_3index
from milp_3index_phase2 import generateModel_milp_3index_fixed_centers










######################################################################
# HELPER FUNCTIONS
######################################################################

def getKey(item):
    return item[1]

def build_VNs_ordered_vector(instance):
    v = []

    for k in instance.K:
        sum = 0
        for l in instance.VN[k]:
            sum = sum + instance.qin[k,l] + instance.qout[k,l]
        v.append((k,sum))

    v = sorted(v, key=getKey, reverse=True)

    return v

def create_fixed_center(k, center, Vfc):
    param = {}
    for l in Vfc[k]:
        if l == center:
            param[k,l] = 1
        else:
            param[k,l] = 0

    return param

def calculate_used_capacity(k, used_c, inst):

    used_c_param = {}

    for l in inst.L:
        val = sum(inst.qin[kk, im] * inst.zin[kk, im, l].value + inst.qout[kk, im] * inst.zout[kk, im, l].value for kk in
                                inst.K for im in inst.VNl[kk])

        used_c_param[l] = used_c[l] + val

    return used_c_param

def calculate_current_capacity(inst):

    a_param = {}

    for l in inst.L:
        a_param[l] = inst.a[l] + inst.b[l].value

    return a_param

def save_y_assignment_VNk(k, y_param, inst):

    for l in inst.Vfc[k]:
        if inst.y[k,l].value == 1:
            y_param[k,l] = 1

    return y_param

def init_y_assign_param():
    y = {}
    for i in instance.Y:
        y[i] = 0
    return y

def init_used_capacity(L):

    used_c = {}

    for l in L:
        used_c[l] = 0

    return used_c













# ----------------------------------------------------Main---------------------------------------------------- #

# Import dei file di dati che poi vengono utilizzati nel file principale per creare i modelli concreti #
#------- .dat Files (Di Ampl) -------#
#dat_file_name = 'CodiciTelecom/IstanzeAbilene/abilene_s_t_8.dat'
dat_file_name = 'CodiciTelecom/IstanzeFrance/france_s_t_29.dat'

#Tollerance or the greedy algorithm
alpha = 0.001

#Number Of Iteration Grasp
n_ite = 8

# -----Solver----- #
opt = SolverFactory("gurobi")
opt.options["mipgap"] = 0.01
opt.options["timelimit"] = 120


print "# -------------------------------------------------------------------------"

print "# Solving the exact model to find the optimal solution. We need this information for the Gap estimation"

start_time_exact = time.time()

abstract_model = generateModel_milp_3index()

instance = abstract_model.create_instance(dat_file_name)

#results = opt.solve(instance)

print 'Exact Optimal solution: '
#print instance.Price()

end_time_exact = float(time.time() - start_time_exact)
print("Time optimal solution: %s \n" % end_time_exact)

#####################################################################
# Parameters
#####################################################################

n = instance.n.value
a = instance.a
c = instance.c
L = instance.L
vn = instance.vn
vl = instance.vl
Vf = instance.Vf
loc = instance.loc
qin = instance.qin
qout = instance.qout

best = 0

opt = SolverFactory("gurobi")
opt.options["mipgap"] = 0.03

print "# -------------------------------------------------------------------------"
print "# Heuristic\n The idea is to fix the center in a greedy way and then, using the phase 2 of the milp, find a solution"
print "# -------------------------------------------------------------------------\n"

i = 0

while i <= n_ite:

    print "# -------------------------------------------------------------------------"
    print "# Iteration: %d" % (i + 1)
    print "# -------------------------------------------------------------------------\n"

    start_time = time.time()

    start_time_phase1 = time.time()

    used_capacity = init_used_capacity(L)

    print "# -------------------------------------------------------------------------"
    print "# Phase 1: Now fore each k in K choose the substrate node onto which the substrate center of Gk has to be mapped"
    print "# -------------------------------------------------------------------------\n"

    ordered_VNs_vector = build_VNs_ordered_vector(instance)
    y = init_y_assign_param()

    r = instance.a

    ################# RANDOM PART ################
    '''
    if i > 0:
        percentage = 0.10
        #Defaulf 0
        start_range = 4
        end_range = int(len(ordered_VNs_vector) - start_range * percentage)

        #Randomize The part of the vector selected [9,7,6,5,{1,2,3,4},0] for exmple i shuffle the ones within {}
        copy = ordered_VNs_vector[start_range:end_range]
        random.shuffle(copy)
        # overwrite the original
        ordered_VNs_vector[start_range:end_range] = copy
    '''

    while len(ordered_VNs_vector) > 0:

        k = ordered_VNs_vector[0][0]

        fixed_center = create_fixed_center(k, instance.Vfc[k], instance.Vfc)

        model = generateModel_VNoM_3index(n, {k}, a, used_capacity, c, L, vn, vl, Vf, instance.Vfc, loc, qin, qout, fixed_center)
        instance_center = model.create_instance()
        results = opt.solve(instance_center)

        used_capacity = calculate_used_capacity(k, used_capacity, instance_center)

        a = calculate_current_capacity(instance_center)

        y = save_y_assignment_VNk(k, y, instance_center)

        ordered_VNs_vector.remove(ordered_VNs_vector[0])

    end_time_phase1 = float(time.time() - start_time_phase1)

    print "# -------------------------------------------------------------------------"
    print "# Phase 2: Solve Partial linear relaxation with NON splittable flows "
    print "# -------------------------------------------------------------------------\n"

    start_time_phase2 = time.time()

    abstract_model_phase_2 = generateModel_milp_3index_fixed_centers(y)
    instance_final = abstract_model_phase_2.create_instance(dat_file_name)

    results = opt.solve(instance_final)
    print instance_final.Price()

    if best == 0:
        best = instance_final
    else:
        if best.Price() > instance_final.Price():
            best = instance_final

    n_ite = n_ite - 1

    end_time_phase2 = float(time.time() - start_time_phase2)

    end_time_heuristic = float(time.time() - start_time)

    print 'Iteration time: %f ' % end_time_heuristic

    i = i + 1

    ############## END ITERATION ################################

print '\n\n############## RESULTS ##############\n'

print 'Exact optimal solution'
print instance.Price()
print 'Exact execution time'
print end_time_exact

print '\nBest Solution found with heuristic'
print best.Price()
print '\nHeuristic execution time (Last iteration)'
print 'Phase1 Time: '
print end_time_phase1
print 'Phase2 Time: '
print end_time_phase2
print 'Phase1 + Phase2 Time: '
print end_time_heuristic

print '\nGap%'
print ((best.Price() * 100) / instance.Price()) - 100
