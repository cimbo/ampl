############################################################################
#       MILP 3-INDEX PROBLEM CENTER AS PARAMETERS AND FLOW VARIABLES BINARY
############################################################################
from __future__ import division
from pyomo.environ import *

infinity = float('inf')

#Genera il modello matematico
def generateModel_milp_3index_fixed_y_z_b(v_param, zzin_param, zzout_param, bb_param):

    model = AbstractModel()

    ####################################
    # PARAMS & SETS
    ####################################

    model.k = Param()

    # Set of requests
    model.K = RangeSet(1, model.k)

    model.n = Param()

    # Sets of physical nodes and arcs
    model.N = RangeSet(1, model.n)

    model.L = Set(within=model.N * model.N)

    model.Pk = Param(model.K)

    #####################################
    # Physical nodes and links capacities
    #####################################

    # capacities of arcs
    model.a = Param(model.L, within=NonNegativeReals)

    # capacities of nodes
    model.aa = Param(model.N, within=NonNegativeReals)

    # distance
    model.dist = Param(model.L, within=NonNegativeReals)

    # cost(=dist*1000)
    model.c = Param(model.L, within=NonNegativeReals)

    # activation cost
    model.cc = Param(model.N, within=NonNegativeReals)

    ########################################
    # Sets of virtual nodes, one per request
    ########################################

    # number of nodes
    model.vn = Param(model.K)

    # Virtual request traffic matrix (nominal)


    def VN_Init(model, kk):
        retval = []
        for jj in range(1, model.vn[kk] + 1):
            retval.append(jj)
        return retval

    # helper set
    model.VN = Set(model.K, initialize=VN_Init)

    def VNl_Init(model, kk):
        retval = []
        for jj in range(2, model.vn[kk] + 1):
            retval.append(jj)
        return retval

    # set of leaves
    model.VNl = Set(model.K, initialize=VNl_Init)

    # number of arcs of each VN(the same for all)
    model.D = 6

    #HELPER SET
    def KN_Init(model):

        dict = []

        for k in model.K:
            for l in model.VN[k]:
                dict.append((k,l))

        return dict


    model.KN = Set(dimen=2, initialize=KN_Init)

    model.qin = Param(model.KN, default=0)

    model.qout = Param(model.KN, default=0)

    # Location of leaves

    # Set of nodes considered as leaves for different VNs
    model.Vf = Set(model.K, within= model.N)

    def Vfc_init(model, kk):
        return model.N.difference(model.Vf[kk])

    model.Vfc = Set(model.K, initialize=Vfc_init)

    ########################################
    # param location
    ########################################

    # HELPER SET
    def LOC_Init(model):

        dict = []

        for k in model.K:
            for l in model.VNl[k]:
                for ip in model.Vf[k]:
                    dict.append((k, l, ip))

        return dict

    model.LOC = Set(dimen=3, initialize=LOC_Init)


    #kk in K, VNl[kk], ip in Vf[kk]
    model.loc = Param(model.LOC, default=0)

    ####################################
    # VARS
    ####################################

    # HELPER SET
    def Y_Init(model):

        dict = []

        for k in model.K:
            for l in model.Vfc[k]:
                dict.append((k, l))
        return dict

    model.Y = Set(dimen=2, initialize=Y_Init)

    ####################################
    # FURTHER PARAMETER
    ####################################

    # HELPER SET
    def Vl_Init(model):

        dict = []

        for k in model.K:
            for l in model.VN[k]:
                dict.append((k, l))

        return dict

    model.Vl = Set(dimen=2, initialize=Vl_Init)

    model.vl = Param(model.Vl , default=0)

    def v_kl_Init(model, k, l):
        return v_param[k,l]

    # One if a virtual ii from request rr is mapped onto physical node jj (within V_local)
    model.y = Param(model.Y, initialize=v_kl_Init)

    # One if at least one centre is located onto node i
    model.w = Var(model.N, within=Binary)

    # Multicommodity flow for virtual request rr between virtual nodes ii and jj on physical arc (i,j)
    # Note: it's defined only for strictly positive demands

    # HELPER SET
    def Z_Init(model):

        dict = []

        for k in model.K:
            for l in model.VNl[k]:
                for (i,j) in model.L:
                    dict.append((k, l, i, j))

        return dict


    #{kk in K, jj in VNl[kk], (i,j) in L
    model.Z = Set(dimen=4, initialize=Z_Init)

    def init_zzin_param(model,k,l,i,j):
        return zzin_param[k,l,i,j]
    model.zin = Param(model.Z, initialize=init_zzin_param) #considero (1,jj)

    def init_zzout_param(model,k,l,i,j):
        return zzout_param[k,l,i,j]
    model.zout = Param(model.Z, initialize=init_zzout_param) # considero (jj,1)

    # INVESTMENT

    # Maximum unitary price
    model.v  = Var(within=NonNegativeReals)

    # HELPER SET
    def P_Init(model):

        dict = []

        for k in model.K:
            for (i, j) in model.L:
                dict.append((k, i, j))

        return dict

    # kk in K, (i,j) in L
    model.P = Set(dimen=3, initialize=P_Init)

    # Price paid by VN kk to expand arc (i,j)
    model.p = Var(model.P, within=NonNegativeReals)

    # Price paid by VN kk to activate node i as a centre
    # var pc{kk in K, i in N}>=0;

    def init_b_param(model,i,j):
        return bb_param[i,j]
    # Capacity added to arc (i,j)
    model.b = Param(model.L, initialize=init_b_param)

    # HELPER SET
    def KNK_Init(model):

        dict = []

        for k in model.K:
            for l in model.VN[k]:
                dict.append((k,l))

        return dict

    # kk in K, (i,j) in L
    model.KNK = Set(dimen=2, initialize=KNK_Init)

    ####################################
    # OBJECTIVE FUNCTION
    ####################################

    def Price(model):
        return model.v
    model.Price = Objective(rule=Price, sense=minimize)

    def prova(model):

        return []

    # helper set
    model.prova = Set(initialize=prova)


    ####################################
    # CONSTRAINTS
    ####################################

    def massimo(model, kk):
        return model.v >= sum(model.p[kk,i,j] for (i,j) in model.L) / sum( model.qin[kk, jj] + model.qout[kk, jj] for jj in model.VNl[kk])
    model.massimo = Constraint(model.K, rule=massimo)

    # ----------
    # INVESTMENT
    # ----------

    # Relation between the investment and its cost
    def investment(model, i, j):
        return model.b[i, j] == sum( (model.p[kk, i, j] / model.c[i, j]) for kk in model.K)
    model.investment = Constraint(model.L, rule=investment)

    # Minimum price
    def minimumpricer(model, kk):
        return 0.1 * sum(model.qin[kk, jj] + model.qout[kk, jj] for jj in model.VNl[kk]) <= sum( model.p[kk, i, j] for (i,j) in model.L)
    model.minimumpricer = Constraint(model.K, rule=minimumpricer)

    return model






