# --------------------------------------------------------Header-------------------------------------------------------- #
from pyomo.environ import *
from pyomo.opt import SolverFactory
import time
from pyomo.core.base import *
# Import dei modelli astratti che poi vengono utilizzati nel file principale #
#------- .mod Files (Di Ampl) -------#
from milp_3index_phase1 import generateModel_milp_3index_relaxed
from milp_3index_phase2 import generateModel_milp_3index_fixed_centers


######################################################################
# HELPER FUNCTIONS
######################################################################

def build_v_parameter(instance):
    v = {}

    for k in instance.K:
        for l in instance.Vfc[k]:
            v[k,l] = instance.y[k,l].value
    return v

# ----------------------------------------------------Main---------------------------------------------------- #

# Import dei file di dati che poi vengono utilizzati nel file principale per creare i modelli concreti #
#------- .dat Files (Di Ampl) -------#
#dat_file_name = 'CodiciTelecom/IstanzeAbilene/abilene_s_t_15.dat'
dat_file_name = 'CodiciTelecom/IstanzeFrance/france_s_t_4.dat'

# -----Solver----- #
opt = SolverFactory("gurobi")
opt.options["mipgap"] = 0.03

print "# -------------------------------------------------------------------------"
print "# Algorithm 1 Heuristic Procedure: Min-Price-VNE-T-S for Star Telecom VNs "
print "# -------------------------------------------------------------------------"

print "# -------------------------------------------------------------------------"
print "# Phase 1: Solve Partial linear relaxation with splittable flows "
print "# -------------------------------------------------------------------------"

start_time = time.time()

abstract_model = generateModel_milp_3index_relaxed()

instance = abstract_model.create_instance(dat_file_name)

opt.options["mipgap"] = 0.03

results = opt.solve(instance)
results.write()


print "# -------------------------------------------------------------------------"
print "# Phase 2: Solve Partial linear relaxation with NON splittable flows "
print "# -------------------------------------------------------------------------"

v = build_v_parameter(instance)
abstract_model_phase_2 = generateModel_milp_3index_fixed_centers(v)

instance = abstract_model_phase_2.create_instance(dat_file_name)

results = opt.solve(instance)
results.write()
#instance.y.display()

print("--- %s seconds ---" % (time.time() - start_time))

print "# ----------------------------------------------------------"

#instance.display()

