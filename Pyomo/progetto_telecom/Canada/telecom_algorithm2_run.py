# --------------------------------------------------------Header-------------------------------------------------------- #
from __future__ import division
from pyomo.environ import *
import random
import time
from pyomo.opt import SolverFactory, SolverStatus, TerminationCondition
from pyomo.core.base import *
# Import dei modelli astratti che poi vengono utilizzati nel file principale #
#------- .mod Files (Di Ampl) -------#
from VNoM import generateModel_VNoM_3index
from milp_3index import generateModel_milp_3index
from milp_3index_phase2 import generateModel_milp_3index_fixed_centers
from milp_3index_y_z_b_fixed import generateModel_milp_3index_fixed_y_z_b

######################################################################
# HELPER FUNCTIONS
######################################################################

def getKey(item):
    return item[1]

def build_VNs_ordered_vector(instance):
    v = []

    for k in instance.K:
        sum = 0
        for l in instance.VN[k]:
            sum = sum + instance.qin[k,l] + instance.qout[k,l]
        v.append((k,sum))

    v = sorted(v, key=getKey, reverse=True)

    return v

def create_fixed_center(k, center, Vfc):
    param = {}
    for l in Vfc[k]:
        if l == center:
            param[k,l] = 1
        else:
            param[k,l] = 0

    return param

def calculate_used_capacity(k, used_c, inst):

    used_c_param = {}

    for l in inst.L:
        val = sum(inst.qin[kk, im] * inst.zin[kk, im, l].value + inst.qout[kk, im] * inst.zout[kk, im, l].value for kk in
                                inst.K for im in inst.VNl[kk])

        used_c_param[l] = used_c[l] + val

    return used_c_param


def calculate_current_capacity(inst):

    a_param = {}

    for l in inst.L:
        a_param[l] = inst.a[l] + inst.b[l].value

    return a_param

def calculate_added_capacity(inst, add_capacity_param):
    for l in inst.L:
        add_capacity_param[l] = add_capacity_param[l] + inst.b[l].value
    return add_capacity_param

def save_y_assignment_VNk(k, y_param, inst):

    for kk in k:
        for l in inst.Vfc[kk]:
            if inst.y[kk,l].value == 1:
                y_param[kk,l] = 1

    return y_param

def save_zin_assignment_VNk(kk, zin_param, inst):

    for k in kk:
        for l in inst.VNl[k]:
            for (i, j) in inst.L:
                if inst.zin[k,l,i,j].value == 1:
                    zin_param[k,l,i,j] = 1
    return zin_param

def save_zout_assignment_VNk(kk, zout_param, inst):
    for k in kk:
        for l in inst.VNl[k]:
            for (i, j) in inst.L:
                if inst.zout[k,l,i,j].value == 1:
                    zout_param[k,l,i,j] = 1
    return zout_param


def init_y_assign_param():
    y = {}
    for i in instance.Y:
        y[i] = 0
    return y

def init_zin_assign_param():
    zin = {}
    for z in instance.Z:
        zin[z] = 0
    return zin

def init_zout_assign_param():
    zout = {}
    for z in instance.Z:
        zout[z] = 0
    return zout

def choose_assignment(alpha, alpha_vector):
    v = []

    min = alpha_vector[0][1]

    for i in alpha_vector:
        if i[1] < min * (1 + alpha):
            v.append(i)

    return random.choice(v)

def init_used_capacity(L):

    used_c = {}

    for l in L:
        used_c[l] = 0

    return used_c

def init_added_capacity(L):

    added_c = {}

    for l in L:
        added_c[l] = 0

    return added_c


# ----------------------------------------------------Main---------------------------------------------------- #

# Import dei file di dati che poi vengono utilizzati nel file principale per creare i modelli concreti #
#------- .dat Files (Di Ampl) -------#
#dat_file_name = 'CodiciTelecom/IstanzeAbilene/abilene_s_t_8.dat'
dat_file_name = 'CodiciTelecom/IstanzeFrance/france_s_t_29.dat'

#Tollerance or the greedy algorithm
alpha = 0.001

#Number Of Iteration Grasp
n_ite = 1

# -----Solver----- #
opt = SolverFactory("gurobi")
opt.options["mipgap"] = 0.01
opt.options["timelimit"] = 130

print "# -------------------------------------------------------------------------"

print "# Solving the exact model to find the optimal solution. We need this information for the Gap estimation"

start_time_exact = time.time()

abstract_model = generateModel_milp_3index()

instance = abstract_model.create_instance(dat_file_name)

#results = opt.solve(instance)

print 'Exact Optimal solution: '
#print instance.Price()

end_time_exact = float(time.time() - start_time_exact)
print("--- %s seconds ---" % end_time_exact)

print "# -------------------------------------------------------------------------"
print "# Sort VNs in K in decreasing order according to their demands"

#####################################################################
# Parameters
#####################################################################

n = instance.n.value
a = instance.a
c = instance.c
L = instance.L
vn = instance.vn
vl = instance.vl
Vf = instance.Vf
loc = instance.loc
qin = instance.qin
qout = instance.qout

print "# --------------------------------------------------------------------------------------------------------------"
print "# Phase 1: Now fore each k in K choose the substrate node onto which the substrate center of Gk has to be mapped"
print "# --------------------------------------------------------------------------------------------------------------"

best = 0

opt = SolverFactory("gurobi")
opt.options["mipgap"] = 0.03

while n_ite > 0:

    start_time = time.time()

    start_time_phase1 = time.time()

    used_capacity = init_used_capacity(L)

    added_capacity = init_added_capacity(L)

    ordered_VNs_vector = build_VNs_ordered_vector(instance)
    y = init_y_assign_param()
    zin = init_zin_assign_param()
    zout = init_zout_assign_param()
    r = instance.a

    #random.shuffle(ordered_VNs_vector)

    range = int(len(ordered_VNs_vector) * 0.001)

    print 'Range'
    print range

    if range == 0:
        range = 1
    print range

    chunks = [ordered_VNs_vector[x:x + range] for x in xrange(0, len(ordered_VNs_vector), range)]

    print chunks

    while len(chunks) > 0:

        k = set()

        for i in chunks[0]:
            k |= {i[0]}

        model = generateModel_VNoM_3index(n, k, a, used_capacity, c, L, vn, vl, Vf, instance.Vfc, loc, qin, qout)
        instance_center = model.create_instance()
        results = opt.solve(instance_center)

        used_capacity = calculate_used_capacity(k, used_capacity, instance_center)

        a = calculate_current_capacity(instance_center)
        added_capacity = calculate_added_capacity(instance_center, added_capacity)

        y = save_y_assignment_VNk(k, y, instance_center)
        zin = save_zin_assignment_VNk(k, zin, instance_center)
        zout = save_zout_assignment_VNk(k, zout, instance_center)

        chunks.remove(chunks[0])

    end_time_phase1 = float(time.time() - start_time_phase1)

    print "# -------------------------------------------------------------------------"
    print "# Phase 2: Solve Partial linear relaxation with NON splittable flows "
    print "# -------------------------------------------------------------------------\n"

    start_time_phase2 = time.time()

    abstract_model_phase_2 = generateModel_milp_3index_fixed_centers(y)
    instance_final = abstract_model_phase_2.create_instance(dat_file_name)

    results = opt.solve(instance_final)
    print instance_final.Price()

    if best == 0:
        best = instance_final
    else:
        if best.Price() > instance_final.Price():
            best = instance_final

    n_ite = n_ite - 1

    end_time_phase2 = float(time.time() - start_time_phase2)

    end_time_heuristic = float(time.time() - start_time)

    print 'Iteration time: %f ' % end_time_heuristic

    ############## END ITERATION ################################

print '\n\n############## RESULTS ##############\n'

print 'Exact optimal solution'
#print instance.Price()
print 'Exact execution time'
#print end_time_exact

print '\nBest Solution found with heuristic'
print best.Price()
print '\nHeuristic execution time (Last iteration)'
print 'Phase1 Time: '
print end_time_phase1
print 'Phase2 Time: '
print end_time_phase2
print 'Phase1 + Phase2 Time: '
print end_time_heuristic

print '\nGap%'
print ((best.Price() * 100) / instance.Price()) - 100

'''if (results.solver.status == SolverStatus.ok) and (
            results.solver.termination_condition == TerminationCondition.optimal):
            # Do something when the solution in optimal and feasible
            print instance_center.Price.display()

        elif (results.solver.termination_condition == TerminationCondition.infeasible):
            # Do something when model in infeasible
            print 'Infeasible solution found!'
        else:
            # Something else is wrong
            print 'Solver Status: ', results.solver.status'''
