# --------------------------------------------------------Header-------------------------------------------------------- #
from pyomo.environ import *
import random
import time
from pyomo.opt import SolverFactory, SolverStatus, TerminationCondition
from pyomo.core.base import *
# Import dei modelli astratti che poi vengono utilizzati nel file principale #
#------- .mod Files (Di Ampl) -------#
from VNoM import generateModel_VNoM_3index
from milp_3index import generateModel_milp_3index
from milp_3index_phase2 import generateModel_milp_3index_fixed_centers

######################################################################
# HELPER FUNCTIONS
######################################################################

def getKey(item):
    return item[1]

def build_VNs_ordered_vector(instance):
    v = []

    for k in instance.K:
        sum = 0
        for l in instance.VN[k]:
            sum = sum + instance.qin[k,l] + instance.qout[k,l]
        v.append((k,sum))

    v = sorted(v, key=getKey, reverse=True)

    return v

def create_fixed_center(k, center, Vfc):
    param = {}
    for l in Vfc[k]:
        if l == center:
            param[k,l] = 1
        else:
            param[k,l] = 0

    return param

def calculate_residual_capacity(k, r, inst):

    r_param = {}

    for l in inst.L:
        val = r[l] - sum(inst.qin[kk, im] * inst.zin[kk, im, l].value + inst.qout[kk, im] * inst.zout[kk, im, l].value for kk in
                                inst.K for im in inst.VNl[kk])
        if val >= 0:
            r_param[l] = val
        else:
            r_param[l] = epsilon

    return r_param

def init_y_assign_param():
    y = {}
    for i in instance.Y:
        y[i] = 0
    return y

def choose_assignment(alpha, alpha_vector):
    v = []

    min = alpha_vector[0][1]

    for i in alpha_vector:
        if i[1] < min * (1 + alpha):
            v.append(i)

    return random.choice(v)


# ----------------------------------------------------Main---------------------------------------------------- #

# Import dei file di dati che poi vengono utilizzati nel file principale per creare i modelli concreti #
#------- .dat Files (Di Ampl) -------#
#dat_file_name = 'CodiciTelecom/IstanzeAbilene/abilene_s_t_15.dat'
dat_file_name = 'CodiciTelecom/IstanzeFrance/france_s_t_26.dat'

#Tollerance or the greedy algorithm
alpha = 0.001

#Coefficient cost residual capacity
epsilon = 0.1

#Number Of Iteration Grasp
n_ite = 1

# -----Solver----- #
opt = SolverFactory("gurobi")
opt.options["mipgap"] = 0.001
opt.options["timelimit"] = 150

print "# -------------------------------------------------------------------------"

print "# Solving the exact model to find the optimal solution. We need this information for the Gap estimation"

start_time_exact = time.time()

abstract_model = generateModel_milp_3index()

instance = abstract_model.create_instance(dat_file_name)

results = opt.solve(instance)

end_time_exact = float(time.time() - start_time_exact)
print("--- %s seconds ---" % end_time_exact)

print "# -------------------------------------------------------------------------"
print "# Sort VNs in K in decreasing order according to their demands"

#####################################################################
# Parameters
#####################################################################

n = instance.n.value
r = instance.a
L = instance.L
vn = instance.vn
vl = instance.vl
Vf = instance.Vf
loc = instance.loc
qin = instance.qin
qout = instance.qout

print "# -------------------------------------------------------------------------"
print "# Now fore each k in K choose the substrate node onto which the substrate center of Gk has to be mapped"

#y = init_y_assign_param()

best = 0


while n_ite > 0:

    start_time = time.time()

    ordered_VNs_vector = build_VNs_ordered_vector(instance)
    y = init_y_assign_param()
    r = instance.a

    while len(ordered_VNs_vector) > 0:

        k = ordered_VNs_vector[0][0]

        #Vector where I go choose the "best assignment"
        alpha_vector = []

        #Evaluate the cost of each possible center location
        for center in instance.Vfc[k]:

            fixed_center = create_fixed_center(k,center,instance.Vfc)

            model = generateModel_VNoM_3index(n, {k}, r, L, vn, vl, Vf, instance.Vfc, loc, qin, qout, fixed_center)
            instance_center = model.create_instance()
            results = opt.solve(instance_center)

            current_residual_capacity = calculate_residual_capacity(k, r, instance_center)

            # (k, center, objective_value, residual_capacity)
            alpha_vector.append((k, instance_center.Price(), center, current_residual_capacity))

            #All the tuples ordered by the value of the objective function
        alpha_vector = sorted(alpha_vector, key=getKey)

        #for i in alpha_vector:
        #print i

        #Here
        selected_tuple = choose_assignment(alpha, alpha_vector)

        #Save the assignmet
        y[k,selected_tuple[2]] = 1

        #Update of the residual capacity
        r = selected_tuple[3]
        #print selected_tuple[1]

        #print 'Demand done'

        ordered_VNs_vector.remove(ordered_VNs_vector[0])

    print "# -------------------------------------------------------------------------"
    print "# Phase 2: Solve Partial linear relaxation with NON splittable flows "
    print "# -------------------------------------------------------------------------"

    print ''

    abstract_model_phase_2 = generateModel_milp_3index_fixed_centers(y)
    instance_final = abstract_model_phase_2.create_instance(dat_file_name)

    results = opt.solve(instance_final)
    print instance_final.Price()

    if best == 0:
        best = instance_final
    else:
        if best.Price() > instance_final.Price():
            best = instance_final

    n_ite = n_ite - 1

    end_time_heuristic = float(time.time() - start_time)
    print("--- %s seconds ---" % end_time_heuristic)

    ############## END ITERATION ################################

print '\n\n############## RESULTS ##############\n'

print 'Exact optimal solution'
print instance.Price()
print 'Exact execution time'
print end_time_exact

print '\nBest Solution found with heuristic'
print best.Price()
print '\nHeuristic execution time (Last iteration)'
print end_time_heuristic

print '\nGap%'
print ((best.Price() * 100) / instance.Price()) - 100





'''if (results.solver.status == SolverStatus.ok) and (
            results.solver.termination_condition == TerminationCondition.optimal):
            # Do something when the solution in optimal and feasible
            print instance_center.Price.display()

        elif (results.solver.termination_condition == TerminationCondition.infeasible):
            # Do something when model in infeasible
            print 'Infeasible solution found!'
        else:
            # Something else is wrong
            print 'Solver Status: ', results.solver.status'''