# --------------------------------------------------------Header-------------------------------------------------------- #
from pyomo.environ import *
from pyomo.opt import SolverFactory
from pyomo.core.base import *
# Import dei modelli astratti che poi vengono utilizzati nel file principale #
#------- .mod Files (Di Ampl) -------#
from milp_3index import generateModel_milp_3index

# ----------------------------------------------------Main---------------------------------------------------- #

# Import dei file di dati che poi vengono utilizzati nel file principale per creare i modelli concreti #
#------- .dat Files (Di Ampl) -------#
dat_file_name = 'CodiciTelecom/IstanzeAbilene/abilene_s_t_10.dat'
#dat_file_name = 'CodiciTelecom/IstanzeFrance/france_s_t_24.dat'

abstract_model = generateModel_milp_3index()

instance = abstract_model.create_instance(dat_file_name)

# -----Solver----- #
opt = SolverFactory("gurobi")
opt.options["mipgap"] = 0.005
opt.options["timelimit"] = 150

#tee = True is to see the output of the solver
results = opt.solve(instance)
results.write()
instance.display()
#instance.y.display()
#instance.K.display()
#instance.VNl.display()
#instance.Vf.display()
#instance.VN.display()
#instance.vl.display()
#instance.Z.display()
#instance.FLOW2.display()
#instance.flows1a.display()
#instance.flows2a.display()
#instance.flows3a.display()
#instance.flows1b.display()
#instance.flows2b.display()
#instance.flows3b.display()

#instance.y.display()

print "# ----------------------------------------------------------"

#instance.zin.display()
