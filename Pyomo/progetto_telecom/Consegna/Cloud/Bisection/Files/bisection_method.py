# --------------------------------------------------------Header-------------------------------------------------------- #
from pyomo.opt import SolverFactory
import json
import time
# Import dei modelli astratti che poi vengono utilizzati nel file principale #
#------- .mod Files (Di Ampl) -------#
from bisection_model import generateModel_bisection

######################################################################
# SOLVER PREFERENCES
######################################################################

# -----Solver----- #
opt = SolverFactory("gurobi")
opt.options["mipgap"] = 0.005
#opt.options["timelimit"] = 150

######################################################################
# BISECTION PREFERENCES
######################################################################

number_of_center = 5


# ----------------------------------------------------Main---------------------------------------------------- #

def main(file):

    start_time = time.time()

    abstract_model = generateModel_bisection()

    instance = abstract_model.create_instance(file)

    execute_bisection(instance)

    # tee = True is to see the output of the solver
    results = opt.solve(instance)
    #results.write()

    optimum = instance.Price()

    end_time = time.time() - start_time

    return build_solutions_json(file, optimum, end_time)


def execute_bisection(instance):

    n1 = number_of_center
    n2 = 1
    v =


def build_solutions_json(file, optimum, time):
    data = {}

    data['file_name'] = file
    data['optimum'] = optimum
    data['exact_time'] = time

    json_data = json.dumps(data, indent=4, separators=(',\n', ': '))

    return json_data





