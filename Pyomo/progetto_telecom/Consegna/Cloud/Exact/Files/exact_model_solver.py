# --------------------------------------------------------Header-------------------------------------------------------- #
from pyomo.opt import SolverFactory
import json
import time
# Import dei modelli astratti che poi vengono utilizzati nel file principale #
#------- .mod Files (Di Ampl) -------#
from milp_3index import generateModel_milp_3index

######################################################################
# SOLVER PREFERENCES
######################################################################

# -----Solver----- #
opt = SolverFactory("gurobi")
opt.options["mipgap"] = 0.005
#opt.options["timelimit"] = 150

# ----------------------------------------------------Main---------------------------------------------------- #

def main(file):

    start_time = time.time()

    abstract_model = generateModel_milp_3index()

    instance = abstract_model.create_instance(file)

    # tee = True is to see the output of the solver
    results = opt.solve(instance)
    #results.write()

    optimum = instance.Price()

    end_time = time.time() - start_time

    return build_solutions_json(file, optimum, end_time)


def build_solutions_json(file, optimum, time):
    data = {}

    data['file_name'] = file
    data['optimum'] = optimum
    data['exact_time'] = time

    json_data = json.dumps(data, indent=4, separators=(',\n', ': '))

    return json_data





