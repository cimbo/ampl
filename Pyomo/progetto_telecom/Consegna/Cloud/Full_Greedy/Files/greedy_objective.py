####################################
#       Greedy Objective
####################################
from __future__ import division
from pyomo.environ import *

infinity = float('inf')

#Genera il modello matematico
def generateModel_greedy_objective(n_param, k_set, c_param, L_set, vn_param, vl_param, Vf_set, Vfc_set, qin_param, qout_param, b_param):

    model = AbstractModel()

    ####################################
    # PARAMS & SETS
    ####################################

    # Set of requests
    model.K = Set(initialize=k_set)

    model.n = Param(initialize=n_param)

    # Sets of physical nodes and arcs
    model.N = RangeSet(1, model.n)

    model.L = Set(within=model.N * model.N, initialize=L_set)

    # cost(=dist*1000)
    # residual capacity
    def init_c(model, i, j):
        return c_param[i, j]
    model.c = Param(model.L, within=NonNegativeReals, initialize=init_c)

    ########################################
    # Sets of virtual nodes, one per request
    ########################################

    def init_vn(model, k):
        return vn_param[k]
    # number of nodes VN
    model.vn = Param(model.K, initialize=init_vn)

    # Virtual request traffic matrix (nominal)


    def VN_Init(model, kk):

        retval = []
        for jj in range(1, model.vn[kk] + 1):
            retval.append(jj)
        return retval

    # helper set
    model.VN = Set(model.K, initialize=VN_Init)

    def VNl_Init(model, kk):
        retval = []
        for jj in range(2, model.vn[kk] + 1):
            retval.append(jj)
        return retval

    # set of leaves
    model.VNl = Set(model.K, initialize=VNl_Init)

    # number of arcs of each VN(the same for all)
    model.D = 6

    #HELPER SET
    def KN_Init(model):

        dict = []

        for k in model.K:
            for l in model.VN[k]:
                dict.append((k,l))

        return dict


    model.KN = Set(dimen=2, initialize=KN_Init)

    def qin_init(model,k,l):
        return qin_param[k,l]
    model.qin = Param(model.KN, default=0, initialize=qin_init)

    def qout_init(model, k, l):
        return qout_param[k, l]
    model.qout = Param(model.KN, default=0, initialize=qout_init)

    # Location of leaves

    #IN THEORY WITHIN model.N . WHEN 0 means that the parameter is not defined
    def init_vl(model, k, l):
        return vl_param[k,l]
    model.vl = Param(model.KN, default=0, initialize=init_vl)

    # Set of nodes considered as leaves for different VNs

    def Vf_init(model,k):
        return Vf_set[k].value
    model.Vf = Set(model.K, within= model.N, initialize=Vf_init)

    #Only one element at the time
    def Vfc_init(model, k):
        return Vfc_set[k]
    model.Vfc = Set(model.K, initialize=Vfc_init)

    ########################################
    # param location
    ########################################

    # HELPER SET
    def LOC_Init(model):

        dict = []

        for k in model.K:
            for l in model.VNl[k]:
                for ip in model.Vf[k]:
                    dict.append((k, l, ip))

        return dict

    model.LOC = Set(dimen=3, initialize=LOC_Init)

    # HELPER SET
    def P_Init(model):

        dict = []

        for k in model.K:
            for (i, j) in model.L:
                dict.append((k, i, j))

        return dict

    # kk in K, (i,j) in L
    model.P = Set(dimen=3, initialize=P_Init)

    # Price paid by VN kk to expand arc (i,j)
    model.p = Var(model.P, within=NonNegativeReals)

    # Price paid by VN kk to activate node i as a centre
    # var pc{kk in K, i in N}>=0;

    # Capacity added to arc (i,j)
    def init_b(model,i,j):
        return b_param[i,j]
    model.b = Param(model.L, initialize=init_b)

    # Maximum unitary price
    model.v = Var(within=NonNegativeReals)

    ####################################
    # OBJECTIVE FUNCTION
    ####################################

    def Price(model):
        return model.v

    model.Price = Objective(rule=Price, sense=minimize)

    ####################################
    # CONSTRAINTS
    ####################################

    def value_bounds(model, kk):
        return (model.v >= sum(model.p[kk, i, j] for (i, j) in model.L) / sum(
            model.qin[kk, jj] + model.qout[kk, jj] for jj in model.VNl[kk]))
    model.value_bounds = Constraint(model.K, rule=value_bounds)

    # Relation between the investment and its cost
    def investment(model, i, j):
        return model.b[i, j] == sum( (model.p[kk, i, j] / model.c[i, j]) for kk in model.K)
    model.investment = Constraint(model.L, rule=investment)

    # Minimum price
    def minimumpricer(model, kk):
        return 0.1 <= model.v
    model.minimumpricer = Constraint(model.K, rule=minimumpricer)

    return model

