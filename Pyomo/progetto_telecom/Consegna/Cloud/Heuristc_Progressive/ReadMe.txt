The pure Heuristic can work according two version:
1) Zin and Zout variables in {0,1}
2) Zin and Zout variables in [0,1]

If you want to try 1) or 2) just change the Zin and Zout domain into the VNoM.py file. This file represents the model used by the heuristic in phase 1.
An alternative model for heuristic's phase 1 is represented by VNoM_objective_model_v.py. In this version the objective funtion is slightly different. All the consideration made above about the Zin and Zout domani are still valid.

