# --------------------------------------------------------Header-------------------------------------------------------- #
from __future__ import division
import random
import time
from pyomo.opt import SolverFactory
from VNoM import generateModel_VNoM_3index
from milp_3index import generateModel_milp_3index
from greedy_objective import generateModel_greedy_objective
from milp_3index_phase2_local_search_z import generateModel_milp_3index_local_search_z
from local_branching_phase_1 import generate_local_branching_model_phase_1
from milp_3index_phase2 import generateModel_milp_3index_fixed_centers
from local_branching_phase_2 import generate_local_branching_model_phase_2
from pyomo.opt import SolverStatus, TerminationCondition
import json
from Dijkstra_shortest_path import *
import sys
from pyomo.environ import *
import copy
######################################################################
# PSEUDORANDOM SETTING
######################################################################
SEED = 14041992

#This line of code allows to reproduce in any moment the same pseudo random execution of the algorithm
random.seed(SEED)

######################################################################
# SOLVER PREFERENCES
######################################################################

#<---- DETERMINISTIC HEURISTIC ---->
opt_VNoM_phase_1 = SolverFactory("gurobi")
opt_VNoM_phase_1.options["mipgap"] = 0.03
#opt_VNoM_phase_1.options["timelimit"] = 150

#<---- PHASE 2 WARM START ---->
opt_phase_2_milp_warm_start = SolverFactory("gurobi")
opt_phase_2_milp_warm_start.options["mipgap"] = 0.5
#opt_phase_2_milp_warm_start.options["timelimit"] = 50

#<----- GREEDY OBJECTIVE ------>
opt_greedy_objective = SolverFactory("gurobi")
opt_greedy_objective.options["mipgap"] = 0.03
#opt_randomized_heuristic_phase2.options["timelimit"] = 50

#<----- LOCAL SEARCH ------>
opt_greedy_local_search = SolverFactory("gurobi")
opt_greedy_local_search.options["mipgap"] = 0.05
opt_greedy_local_search.options["timelimit"] = 50

######################################################################
# GLOBAL VARIABLES
######################################################################

gamma = 0.2


#Local Branching Phase 2
total_time_limit_phase_1 = 90
node_time_limit_phase_1 = 10

#k_swap == 2 please
k_swap_phase_1 = 2
# Actually we don't have a way to diversify
dv_max_phase_1 = 0
local_branching_mipgap_phase_1_first = 10000
local_branching_mipgap_phase_1 = 1


#Local Branching Phase 2
total_time_limit_phase_2 = 200
node_time_limit_phase_2 = 10

#k_swap == 2 please
k_swap_phase_2 = 300
# Actually we don't have a way to diversify
dv_max_phase_2 = 0
local_branching_mipgap_phase_2_first = 10000
local_branching_mipgap_phase_2 = 1


#ORDINAMENTO_FLUSSI = 0 politica tesi Laura
#ORDINAMENTO_FLUSSI = 1 politica idea mia con il ciclo for esterno
ORDINAMENTO_FLUSSI = 0

min_price = 0.1

#Instance of the exact model
global exact_optimum
global instance
global end_time_exact

#Deterministic Heuristic
global deterministic_price
global end_time_phase1_pure_heuristic
global end_time_phase2_pure_heuristic
global end_time_heuristic_full_greedy
global y
global zin
global zout


#Randomized Heuristic
global best
global end_time_phase1_randomized_heuristic
global end_time_phase2_randomized_heuristic
global end_time_randomized_heuristic
global dat_file_name


######################################################################
# CONSTANTS
######################################################################

#Number Of Iteration Grasp
n_ite = 0

BASE_PERCENTAGE = 0.10
PERCENTAGE_IMPROVEMENT = 0.05
FROM_LEAF_TO_CENTER = 0
FROM_CENTER_TO_LEAF = 1

'''
After a set of indices greater than MAX_INDICES_FOR_FULL_PERMUTATION the indicies vector is just shuffled. So that cold be the possibilities to try two or more time the same case.
When the size of the set of choosen indicies is less or equal to MAX_INDICES_FOR_FULL_PERMUTATION for sure will never happen the same case two times (due to the fact that i calculated all the possible permutations)
'''
MAX_INDICES_FOR_FULL_PERMUTATION = 8



# ----------------------------------------------------Main---------------------------------------------------- #




def main(file, optimum, exact_t):

    print file

    set_parameters(file, optimum, exact_t)

    generate_data_instance()

    # Mapping of the centers
    y = init_y_assign_param()
    zin = init_z()
    zout = init_z()

    ordered_VNs_vector = build_VNs_ordered_vector(instance)

    y = solve_VNoM(y, ordered_VNs_vector)

    result = LocBra_phase_1(k_swap_phase_1, total_time_limit_phase_1, node_time_limit_phase_1, dv_max_phase_1)

    y = result['y']
    zin = result['zin']
    zout = result['zout']

    #construct_a_feasible_solution(y, zin, zout)

    #LocBra_phase_2(y, k_swap_phase_2, total_time_limit_phase_2, node_time_limit_phase_2, dv_max_phase_2)

    return build_solutions_json()



######################################################################
# HELPER FUNCTIONS
######################################################################
def build_graph():
    global instance

    g = Graph()

    for i in instance.N:
        g.add_vertex(i)

    for l in instance.L:
        g.add_edge(l[0], l[1], float(1 / instance.a[l]))

    return g


def set_parameters(file, optimum, exact_t):
    global dat_file_name
    global exact_optimum
    global end_time_exact

    dat_file_name = file
    exact_optimum = optimum
    end_time_exact = exact_t

def generate_data_instance():

    global instance
    global dat_file_name

    abstract_model = generateModel_milp_3index()

    instance = abstract_model.create_instance(dat_file_name)


def solve_VNoM(y, ordered_VNs_vector):

    n = instance.n.value
    c = instance.c
    L = instance.L
    vn = instance.vn
    vl = instance.vl
    Vf = instance.Vf
    loc = instance.loc
    qin = instance.qin
    qout = instance.qout

    start_VNoM_time = time.time()

    used_capacity = init_used_capacity(L)
    a = instance.a

    ####################################################
    # VNoM
    ####################################################

    best_price_VNoM = None

    for j in range(0, len(ordered_VNs_vector)):
        k = ordered_VNs_vector[j][0]

        model = generateModel_VNoM_3index(n, {k}, a, used_capacity, c, L, vn, vl, Vf, instance.Vfc, loc, qin,
                                          qout)
        instance_center = model.create_instance()
        results = opt_VNoM_phase_1.solve(instance_center)

        used_capacity = calculate_used_capacity(k, used_capacity, instance_center)

        a = calculate_current_capacity(instance_center)

        y = save_y_assignment_VNk(k, y, instance_center)

        end_VNoM_time = float(time.time() - start_VNoM_time)

    # print "# Phase 2: Solve Partial linear relaxation with NON splittable flows "

    best_price_VNoM = calculate_price(a)

    print 'Minimum price v that we can reach with the milp greddy assignment in phase 1: ', best_price_VNoM
    print 'Time VNom: ', end_VNoM_time

    return y




def solve_VLiM(y, ordered_VNs_vector):####################################################
    # VLiM
    ####################################################

    start_VLiM_time = time.time()

    #Starting substrate network situation
    used_capacity = init_used_capacity(instance.L)
    a = dict(instance.a)
    zin = init_z()
    zout = init_z()


    ordered_VNs_flows_vector = build_ordered_VNs_flows_vector(ordered_VNs_vector)

    g = build_graph()

    for i in range(0,len(ordered_VNs_flows_vector)):

        #( k, [(1,l) | (l,1)] , [flow_demand] , logical_leaf ) #The center is alway 1 from the logical point of view!
        flow = ordered_VNs_flows_vector[i]

        from_ = get_physical_node_from(flow, y)
        to_ = get_physical_node_to(flow, y)

        '''
            Se la domanda di flusso in esame e ad esempio 200, il costo degli archi che se seleionati avrebbero meno del 20% di capacita residua, devono avere un costo che consideri gia una possibile espansione!
        '''

        g = calculate_arch_cost_to_satisfy_capacity_constraints(g, flow, used_capacity, a)


        dijkstra(g, g.get_vertex(from_))
        target = g.get_vertex(to_)
        path = [target.get_id()]
        shortest_path = shortest(target, path)

        s_path = path[::-1]

        #print 'The shortest path : %s' % (s_path)

        used_capacity_current_flow = flow[2]
        #Data la scelta effettuata salvo il valore delle capacita utilizzate
        used_capacity = update_used_capacity(used_capacity_current_flow, s_path, used_capacity)

        #Aggiorno lo stato fisico delle capacita delle reti
        a = update_arch_capacity(used_capacity, a, s_path)

        # Salvo le scelte dell'iterazione corrente.
        zin = save_zin_assignment_(flow, zin, build_archs_list(s_path))
        zout = save_zout_assignment_(flow, zout, build_archs_list(s_path))

        g = reset_label_for_dijkstra(g)

    averaged_price = calculate_price(a)

    print 'Averaged_price: ',averaged_price

    end_VLiM_time = float(time.time() - start_VLiM_time)

    print 'Time VLim: ', end_VLiM_time





def build_solutions_json():

    object = {'file_name': dat_file_name, 'results': []}

    object['results'].append({'Exact optimal solution':  exact_optimum })
    object['results'].append({'Exact execution time': end_time_exact })

    '''
    object['results'].append({'Solution found with the Deterministic heuristic': deterministic_price })
    object['results'].append({'Deterministic Heutistic Phase1 Time': end_time_phase1_milp_greedy})
    object['results'].append({'Deterministic Heutistic Phase2 Time': end_time_phase2_pure_heuristic })
    object['results'].append({'Deterministic Heutistic Total_Time = Phase1 + Phase2': end_time_heuristic_full_greedy})
    object['results'].append({'Deterministic Heutistic Gap from optimum': ((
                                                                               deterministic_price * 100) / exact_optimum) - 100 })

    object['results'].append({'Best Solution found with the Randomized Heuristic': best })
    object['results'].append({'Randomized Heuristic total execution time': end_time_randomized_heuristic })


    gap = ((best * 100) / exact_optimum) - 100

    object['results'].append({'Randomized Heutistic Gap from optimum': gap })
    '''
    json_data = json.dumps(object, indent=4, separators=(',\n', ': '))

    return json_data


def getKey(item):
    return item[1]


'''
This helper function given a model with the K Virtual Networks construct a vector with tuples (K, total_demand),
ordered for decreasing values of total_demand
'''


def build_VNs_ordered_vector(instance):
    v = []

    for k in instance.K:
        sum = 0
        for l in instance.VN[k]:
            sum = sum + instance.qin[k, l] + instance.qout[k, l]
        v.append((k, sum))

    v = sorted(v, key=getKey, reverse=True)

    return v


'''
This helper function update the amount of used capacity durning the first phase of the heuristic
'''


def calculate_used_capacity(k, used_c, inst):
    used_c_param = {}

    for l in inst.L:
        val = sum(
            inst.qin[kk, im] * inst.zin[kk, im, l].value + inst.qout[kk, im] * inst.zout[kk, im, l].value for kk in
            inst.K for im in inst.VNl[kk])

        used_c_param[l] = used_c[l] + val

    return used_c_param


'''
This helper function keeps updated the sum of the defaut capacity plus the one related to the expansion of the network
'''


def calculate_current_capacity(inst):
    a_param = {}

    for l in inst.L:
        a_param[l] = inst.a[l] + inst.b[l].value

    return a_param


'''
This helper function save the decisons related to where to put the center of a VN k established in the first phase of the heuristic.
This parameter will be passed in phase 2
'''


def save_y_assignment_VNk(k, y_param, inst):
    for l in inst.Vfc[k]:
        if inst.y[k, l].value == 1:
            y_param[k, l] = 1
    return y_param


'''
This helper function just initialize the parameter Y where to save the decision that will be taken in phase 1
'''


def init_y_assign_param():
    y = {}
    for i in instance.Y:
        y[i] = 0
    return y


'''
This helper function just initialize the parameter related the used capacity durning the first phase
'''


def init_used_capacity(L):
    used_c = {}

    for l in L:
        used_c[l] = 0

    return used_c


'''
The following 3 definitions are needed to create the set of permutations for the index to be shuffle (In the randomized part)
'''


class unique_element:
    def __init__(self, value, occurrences):
        self.value = value
        self.occurrences = occurrences


def perm_unique(elements):
    eset = set(elements)
    listunique = [unique_element(i, elements.count(i)) for i in eset]
    u = len(elements)
    return perm_unique_helper(listunique, [0] * u, u - 1)


def perm_unique_helper(listunique, result_list, d):
    if d < 0:
        yield tuple(result_list)
    else:
        for i in listunique:
            if i.occurrences > 0:
                result_list[d] = i.value
                i.occurrences -= 1
                for g in perm_unique_helper(listunique, result_list, d - 1):
                    yield g
                i.occurrences += 1


'''
This helper function retrives the tuples from the ordered vector according to the choosen indices
'''
def get_selected_tuples(vector, indices):
    array = []
    for i in indices:
        array.append(vector[i - 1])
    return array



'''
This helper function update the ordered vector with the current permutation to explore
'''


def perform_index_permutation(ordered_VNs_vector, list_indices, permutation):
    count = 0
    for i in list_indices:
        ordered_VNs_vector[i - 1] = permutation[count]
        count = count + 1

    return ordered_VNs_vector

def get_permutation_list(selected_elements, n_ite):
    copy = list(selected_elements)

    # If branch when is possible to calculate all the permutations
    if len(selected_elements) <= MAX_INDICES_FOR_FULL_PERMUTATION:
        return list(perm_unique(selected_elements))
    else:
        list_random_permutations = []
        for i in range(0, n_ite):
            random.shuffle(copy)
            list_random_permutations.append(tuple(copy))
        list_random_permutations.append(tuple(selected_elements))
        return list_random_permutations


def build_list_of_ranges(ordered_VNs_vector):
    list_of_ranges = []

    current_reference_index = 1
    current_reference = ordered_VNs_vector[0]
    # First element in
    current_index = 2

    while current_index <= len(ordered_VNs_vector):
        if ordered_VNs_vector[current_index - 1][1] * (
                1 + BASE_PERCENTAGE + PERCENTAGE_IMPROVEMENT * len(list_of_ranges)) >= current_reference[1]:
            current_index = current_index + 1
        else:
            list_of_ranges.append(range(current_reference_index, current_index))
            current_reference = ordered_VNs_vector[current_index - 1]
            current_reference_index = current_index
            current_index = current_index + 1

    list_of_ranges.append(range(current_reference_index, current_index))

    return list_of_ranges




###############################################################################
#
###############################################################################

def build_ordered_VNs_flows_vector(ordered_VNs_vector):
    #Criterio tesi laura
    if ORDINAMENTO_FLUSSI == 0:
        return build_ordered_VNs_flows_vector_MODE_0(ordered_VNs_vector)
    #criterio mio ciclo for esterno flussi shortest path
    elif ORDINAMENTO_FLUSSI == 1:
        return build_ordered_VNs_flows_vector_MODE_1()
    else:
        print 'ERRORE ALL\'INTERNO DI build_ordered_VNs_flows_vector()'
        sys.exit()

def build_ordered_VNs_flows_vector_MODE_0(ordered_VNs_vector):
    global instance

    retval = []

    for i in range(0, len(ordered_VNs_vector)):
        v = []
        k = ordered_VNs_vector[i][0]

        for l in instance.VNl[k]:
            v.append((k, FROM_LEAF_TO_CENTER, instance.qin[k, l], l))
            v.append((k, FROM_CENTER_TO_LEAF, instance.qout[k, l], l))

        v = sorted(v, key=getFlowKey, reverse=True)

        for j in range(0, len(v)):
            retval.append(v[j])

    return retval


def build_ordered_VNs_flows_vector_MODE_1():
    global instance

    v = []

    for k in instance.K:
        for l in instance.VNl[k]:
            v.append((k, FROM_LEAF_TO_CENTER, instance.qin[k, l], l))
            v.append((k, FROM_CENTER_TO_LEAF, instance.qout[k, l], l))

    v = sorted(v, key=getFlowKey, reverse=True)

    return v


def getFlowKey(item):
    return item[2]

def get_physical_node_from(flow, y):

    k = flow[0]

    if flow[1] == FROM_CENTER_TO_LEAF:
        for i in instance.Vfc[k]:
            if y[k,i] == 1:
                return i
        print 'WARNING!!!! IL centro associato alla domanda k: ', k ,'Non e stato assegnato!'
        sys.exit()
    elif flow[1] == FROM_LEAF_TO_CENTER:
         return instance.vl[k, flow[3]]

def get_physical_node_to(flow, y):

    k = flow[0]

    if flow[1] == FROM_CENTER_TO_LEAF:
        #The logical leaf
        return instance.vl[k, flow[3]]
    elif flow[1] == FROM_LEAF_TO_CENTER:
        for i in instance.Vfc[k]:
            if y[k, i] == 1:
                return i
        print 'WARNING!!!! IL centro associato alla domanda k: ', k, 'Non e stato assegnato!'
        sys.exit()

def calculate_arch_cost_to_satisfy_capacity_constraints(g, flow, used_capacity, arch_capacity):

    flow_demand = flow[2]

    for l in instance.L:
        if  arch_capacity[l] - used_capacity[l] - flow_demand < gamma * arch_capacity[l]:
            #Il costo e dato dall'inverso della capacita residua + il costo dell'espandione da pagare
            capacity_expansion = (used_capacity[l] + flow_demand)/gamma - (arch_capacity[l])
            c = (1/(arch_capacity[l] - used_capacity[l])) + capacity_expansion * instance.c[l]
        else:
            c = (1/(arch_capacity[l] - used_capacity[l]))

        g.get_vertex(l[0]).update_neighbor_weigth(g.get_vertex(l[1]), c)

    return g

def update_used_capacity(used_capacity_current_flow, s_path, used_capacity):

    archs_shortest_path = build_archs_list(s_path)

    #print archs_shortest_path

    for l in archs_shortest_path:
        used_capacity[l] = used_capacity[l] + used_capacity_current_flow

    return used_capacity

def build_archs_list(s_path):
    archs_shortest_path = []

    for i in range(1, len(s_path)):
        #print s_path[i - 1]
        #print s_path[i]
        archs_shortest_path.append((s_path[i - 1], s_path[i]))

    return archs_shortest_path



def update_arch_capacity(used_capacity, arch_capacity, s_path):

    archs_shortest_path = build_archs_list(s_path)

    for l in archs_shortest_path:
        if arch_capacity[l] - used_capacity[l] < gamma * arch_capacity[l]:
            arch_capacity[l] = used_capacity[l] / (1 - gamma)
            #print 'ATTENZIONE, INCREMENTO DI CAPACITA SULL\'ARCO: ', l
            #print arch_capacity[l] - instance.a[l]
            #print 'ATTENZIONE PER SIMMETRIA INCREMENTO CAPACITA SULL\'ARCO: ', (l[1],l[0])
            arch_capacity[(l[1],l[0])] = arch_capacity[l]
            #print arch_capacity[(l[1],l[0])] - instance.a[l]
            #print 'Costo pagato: ', (arch_capacity[l] - instance.a[l]) * instance.c[l] + (arch_capacity[(l[1],l[0])] - instance.a[(l[1],l[0])]) * instance.c[(l[1],l[0])]


    return arch_capacity

def init_z():
    zin = {}
    for k in instance.K:
        for l in instance.VNl[k]:
            for (i, j) in instance.L:
                zin[k,l,i,j] = 0
    return zin

def save_zin_assignment_(flow, zin, archs_list):

    k = flow[0]
    logical_leaf = flow[3]

    if flow[1] == FROM_LEAF_TO_CENTER:
        for arch in archs_list:
            zin[k, logical_leaf, arch[0], arch[1]] = 1

    return zin

def save_zout_assignment_(flow, zout, archs_list):

    k = flow[0]
    logical_leaf = flow[3]

    if flow[1] == FROM_CENTER_TO_LEAF:
        for arch in archs_list:
            zout[k, logical_leaf, arch[0], arch[1]] = 1

    return zout

def calculate_price(expanded_capacity):

    b = {}

    for l in instance.L:
        b[l] = expanded_capacity[l] - instance.a[l]

    model = generateModel_greedy_objective(instance.n, instance.K, instance.c, instance.L, instance.vn, instance.vl, instance.Vf, instance.Vfc, instance.qin, instance.qout, b)
    instance_greedy = model.create_instance()
    results = opt_greedy_objective.solve(instance_greedy)

    v = instance_greedy.Price()

    return v

def reset_label_for_dijkstra(g):

    for v in g.get_vertices():
        g.get_vertex(v).set_unvisited()
        g.get_vertex(v).set_distance(sys.maxint)
        g.get_vertex(v).reset_previous()

    return g

def build_set_S(z):
    s = set()

    for i in instance.Z:
        if z[i] == 1:
            s.add(i)
    return s


def build_set_NS(z):
    s = set()

    for i in instance.Z:
        if z[i] == 0:
            s.add(i)
    return s

def build_set_C(y):
    c = set()

    for i in instance.Y:
        if y[i] == 1:
            c.add(i)

    return c


def build_set_NC(y):
    nc = set()

    for i in instance.Y:
        if y[i] == 0:
            nc.add(i)

    return nc

def execute_local_search(zin, zout):
    set_S_zin = build_set_S(zin)
    set_Not_S_zin = build_set_NS(zin)

    set_S_zout = build_set_S(zout)
    set_Not_S_zout = build_set_NS(zout)

    start_time_local_search = time.time()

    model = generateModel_milp_3index_local_search_z(best, set_S_zin, set_Not_S_zin, set_S_zout, set_Not_S_zout, max_arch_swap_phase_1)
    instance_local_search = model.create_instance(dat_file_name)
    results = opt_greedy_local_search.solve(instance_local_search)

    end_time_local_search = time.time() - start_time_local_search

    print 'Local search with maximum: ', max_arch_swap_phase_1, ' result v: ', instance_local_search.Price()
    print 'Local search Time: ', end_time_local_search



#The methods work if a starting solution is passed into x_star
def LocBra_phase_1_starting_from_a_solution(k_swap, total_time_limit, node_time_limit, dv_max, x_star, ub, zin_star, zout_star):
    infinity = float('infinity')
    rhs = k_swap
    TL = node_time_limit
    bestUB = UB = ub
    opt = True
    #first = True
    first = False #if we want to start with a starting solution
    diversify = False
    dv = 0
    x_bar = x_star

    opt_local_branching = SolverFactory("gurobi")

    start_time = time.time()
    elapsed_time = time.time() - start_time

    print 'Beginning of the local branching procedure\n'

    constraints_stack_list = []

    ite = 0

    model = generate_local_branching_model_phase_1(UB)
    instance_local_branching = model.create_instance(dat_file_name)

    num_constraints = 0

    while elapsed_time <= total_time_limit and dv <= dv_max:

        if rhs < infinity:
            constraints_stack_list.append("delta(x, x_bar_" + str(ite) + ") <= rhs")
            instance_local_branching = add_constraint_phase_1(instance_local_branching, num_constraints, ('Left', x_bar, rhs))
            num_constraints = num_constraints + 1
            # Add the local br. constraint into delta(x, x_bar) <= rhs


        TL = min(TL, total_time_limit - elapsed_time) # Da decommentare nel caso in cui non si parta con una feasible solution

        opt_local_branching = set_solver_time_limit_phase_1(opt_local_branching, first, TL)

        print 'Lodi_solution: ', constraints_stack_list

        instance_local_branching = add_ub_constraint(instance_local_branching, UB)

        results = opt_local_branching.solve(instance_local_branching, load_solutions=False)

        TL = node_time_limit

        #1)
        if (results.solver.status == SolverStatus.ok) and (
                    results.solver.termination_condition == TerminationCondition.optimal) and not first: #Last condition to differentiate from a feasible solution
            instance.solutions.load_from(results)
            print 'Optimal Solution found!: ', instance_local_branching.Price()

            if instance_local_branching.Price() < bestUB:
                bestUB = instance_local_branching.Price()
                x_star = instance_local_branching.y
                zin_star = instance_local_branching.zin
                zout_star = instance_local_branching.zout


            if rhs >= infinity:
                return opt

            # Reverse the last local br. constraint into delta(x, x_bar) >= rhs + 1
            constraints_stack_list.pop()
            constraints_stack_list.append("delta(x, x_bar_" + str(ite) + ") >= rhs + 1")

            instance_local_branching = revert_last_constraint_phase_1(instance_local_branching, num_constraints, ('Right', x_bar, rhs + 1))

            diversify = first = False
            x_bar = x_star
            UB = instance_local_branching.Price()
            rhs = k_swap

        #2)
        elif (results.solver.termination_condition == TerminationCondition.infeasible):
            instance.solutions.load_from(results)
            print 'Proved to be infeasible!'

            if rhs >= infinity:
                return opt

            # Reverse the last local br. constraint into delta(x, x_bar) >= rhs + 1
            constraints_stack_list.pop()
            constraints_stack_list.append("delta(x, x_bar_" + str(ite) + ") >= rhs + 1")

            instance_local_branching = revert_last_constraint_phase_1(instance_local_branching, num_constraints, ('Right', x_bar, rhs + 1))

            if diversify:
                print 'Diversify'
                UB = TL = infinity
                dv = dv + 1
                first = True

            rhs = rhs + k_swap/2
            diversify = True


        elif ( results.solver.status == SolverStatus.aborted and
                    results.solver.termination_condition == TerminationCondition.maxTimeLimit) or \
                ( results.solver.status == SolverStatus.ok and
                    results.solver.termination_condition == TerminationCondition.optimal and first ):
            try:
                instance.solutions.load_from(results)
                #3 ) Quando il mipgap e diverso da None vuol dire che cerchiamo una feasible solution
                print 'Solution found but not sure that is optimal: ', instance_local_branching.Price()
                if rhs < infinity:
                    if first:
                        # Delete the last local br. constraint into delta(x, x_bar) <= rhs
                        constraints_stack_list.pop()

                        instance_local_branching = delete_last_constraint_phase_1(instance_local_branching, num_constraints)
                        num_constraints = num_constraints - 1
                    else:
                        # Reverse the last local br. constraint into delta(x, x_bar) >= 1
                        constraints_stack_list.pop()
                        constraints_stack_list.append("delta(x, x_bar_" + str(ite) + ") >= 1")

                        instance_local_branching = revert_last_constraint_phase_1(instance_local_branching, num_constraints,
                                                                                  ('Right', x_bar, 1))

                refine(instance_local_branching.y)

                if instance_local_branching.Price() < bestUB:
                    bestUB = instance_local_branching.Price()
                    x_star = instance_local_branching.y
                    zin_star = instance_local_branching.zin
                    zout_star = instance_local_branching.zout

                first = diversify = False
                x_bar = instance_local_branching.y
                UB = instance_local_branching.Price()
                rhs = k_swap

            except ValueError:

                #4 )
                print 'No_Feasible_solution_found!'
                if diversify:
                    print 'Diversify'
                    # Replace the last local br. constraint delta(x, x_bar) <= rhs with delta(x, x_bar) >= 1
                    constraints_stack_list.pop()
                    constraints_stack_list.append("delta(x, x_bar_" + str(ite) + ") >= 1")

                    instance_local_branching = revert_last_constraint_phase_1(instance_local_branching, num_constraints,
                                                                              ('Right', x_bar, 1))

                    UB = TL = infinity
                    dv = dv + 1
                    rhs = rhs + k_swap / 2
                    first = True
                else:
                    # Delete the last local br. constraint into delta(x, x_bar) <= rhs
                    constraints_stack_list.pop()

                    instance_local_branching = delete_last_constraint_phase_1(instance_local_branching, num_constraints)
                    num_constraints = num_constraints - 1
                    rhs = rhs - k_swap / 2

                diversify = True


        elapsed_time = time.time() - start_time
        print 'elapsed_time: ', elapsed_time, '\n'
        ite += 1

    ###################### END LOOP ######################

    best_price = bestUB

    #print_stats_z(instance_local_branching)

    TL = max(total_time_limit - elapsed_time, 0)
    print 'Out of the loop. Time remaning: ', TL

    first = False

    instance_local_branching = add_ub_constraint(instance_local_branching, bestUB)
    opt_local_branching.options["mipgap"] = 0.03
    opt_local_branching = set_solver_time_limit_phase_1(opt_local_branching, first, TL)

    results = opt_local_branching.solve(instance_local_branching, load_solutions=False)

    try:
        instance.solutions.load_from(results)
        # Every Feasible solution would improve the previous solution due to the UB value
        if (results.solver.status == SolverStatus.ok) and (
                    results.solver.termination_condition == TerminationCondition.optimal):
            best_price = instance_local_branching.Price()
            x_star = instance_local_branching.y
            zin_star = instance_local_branching.zin
            zout_star = instance_local_branching.zout
        opt = True
    except ValueError:
        opt = False

    print 'Can you certify the optimaility of the found solution?: ', opt
    print 'V: ', best_price

    return opt



































def LocBra_phase_1(k_swap, total_time_limit, node_time_limit, dv_max):
    infinity = float('infinity')
    rhs = bestUB = UB = TL = infinity
    opt = first = True
    diversify = False
    dv = 0
    x_star = x_bar = zin_star = zout_star =None

    opt_local_branching = SolverFactory("gurobi")

    start_time = time.time()
    elapsed_time = time.time() - start_time

    print 'Beginning of the local branching procedure\n'

    constraints_stack_list = []

    ite = 0

    model = generate_local_branching_model_phase_1(UB)
    instance_local_branching = model.create_instance(dat_file_name)

    num_constraints = 0

    while elapsed_time <= total_time_limit and dv <= dv_max:

        if rhs < infinity:
            constraints_stack_list.append("delta(x, x_bar_" + str(ite) + ") <= rhs")
            instance_local_branching = add_constraint_phase_1(instance_local_branching, num_constraints, ('Left', x_bar, rhs))
            num_constraints = num_constraints + 1
            # Add the local br. constraint into delta(x, x_bar) <= rhs


        TL = min(TL, total_time_limit - elapsed_time) # Da decommentare nel caso in cui non si parta con una feasible solution

        opt_local_branching = set_solver_time_limit_phase_1(opt_local_branching, first, TL)

        print 'Lodi_solution: ', constraints_stack_list

        instance_local_branching = add_ub_constraint(instance_local_branching, UB)

        results = opt_local_branching.solve(instance_local_branching, load_solutions=False)

        TL = node_time_limit

        #1)
        if (results.solver.status == SolverStatus.ok) and (
                    results.solver.termination_condition == TerminationCondition.optimal) and not first: #Last condition to differentiate from a feasible solution
            instance.solutions.load_from(results)
            print 'Optimal Solution found!: ', instance_local_branching.Price()

            if instance_local_branching.Price() < bestUB:
                bestUB = instance_local_branching.Price()
                x_star = instance_local_branching.y
                zin_star = instance_local_branching.zin
                zout_star = instance_local_branching.zout


            if rhs >= infinity:
                return opt

            # Reverse the last local br. constraint into delta(x, x_bar) >= rhs + 1
            constraints_stack_list.pop()
            constraints_stack_list.append("delta(x, x_bar_" + str(ite) + ") >= rhs + 1")

            instance_local_branching = revert_last_constraint_phase_1(instance_local_branching, num_constraints, ('Right', x_bar, rhs + 1))

            diversify = first = False
            x_bar = x_star
            UB = instance_local_branching.Price()
            rhs = k_swap

        #2)
        elif (results.solver.termination_condition == TerminationCondition.infeasible):
            instance.solutions.load_from(results)
            print 'Proved to be infeasible!'

            if rhs >= infinity:
                return opt

            # Reverse the last local br. constraint into delta(x, x_bar) >= rhs + 1
            constraints_stack_list.pop()
            constraints_stack_list.append("delta(x, x_bar_" + str(ite) + ") >= rhs + 1")

            instance_local_branching = revert_last_constraint_phase_1(instance_local_branching, num_constraints, ('Right', x_bar, rhs + 1))

            if diversify:
                print 'Diversify'
                UB = TL = infinity
                dv = dv + 1
                first = True

            rhs = rhs + k_swap/2
            diversify = True


        elif ( results.solver.status == SolverStatus.aborted and
                    results.solver.termination_condition == TerminationCondition.maxTimeLimit) or \
                ( results.solver.status == SolverStatus.ok and
                    results.solver.termination_condition == TerminationCondition.optimal and first ):
            try:
                instance.solutions.load_from(results)
                #3 ) Quando il mipgap e diverso da None vuol dire che cerchiamo una feasible solution
                print 'Solution found but not sure that is optimal: ', instance_local_branching.Price()

                if rhs < infinity:
                    if first:
                        # Delete the last local br. constraint into delta(x, x_bar) <= rhs
                        constraints_stack_list.pop()

                        instance_local_branching = delete_last_constraint_phase_1(instance_local_branching, num_constraints)
                        num_constraints = num_constraints - 1
                    else:
                        # Reverse the last local br. constraint into delta(x, x_bar) >= 1
                        constraints_stack_list.pop()
                        constraints_stack_list.append("delta(x, x_bar_" + str(ite) + ") >= 1")

                        instance_local_branching = revert_last_constraint_phase_1(instance_local_branching, num_constraints,
                                                                                  ('Right', x_bar, 1))

                refine(instance_local_branching.y)

                if instance_local_branching.Price() < bestUB:
                    bestUB = instance_local_branching.Price()
                    x_star = instance_local_branching.y
                    zin_star = instance_local_branching.zin
                    zout_star = instance_local_branching.zout

                first = diversify = False
                x_bar = instance_local_branching.y
                UB = instance_local_branching.Price()
                rhs = k_swap

            except ValueError:

                #4 )
                print 'No_Feasible_solution_found!'
                if diversify:
                    print 'Diversify'
                    # Replace the last local br. constraint delta(x, x_bar) <= rhs with delta(x, x_bar) >= 1
                    constraints_stack_list.pop()
                    constraints_stack_list.append("delta(x, x_bar_" + str(ite) + ") >= 1")

                    instance_local_branching = revert_last_constraint_phase_1(instance_local_branching, num_constraints,
                                                                              ('Right', x_bar, 1))

                    UB = TL = infinity
                    dv = dv + 1
                    rhs = rhs + k_swap / 2
                    first = True
                else:
                    # Delete the last local br. constraint into delta(x, x_bar) <= rhs
                    constraints_stack_list.pop()

                    instance_local_branching = delete_last_constraint_phase_1(instance_local_branching, num_constraints)
                    num_constraints = num_constraints - 1
                    rhs = rhs - k_swap / 2

                diversify = True


        elapsed_time = time.time() - start_time
        print 'elapsed_time: ', elapsed_time, '\n'
        ite += 1

    ###################### END LOOP ######################

    best_price = bestUB

    #print_stats_z(instance_local_branching)

    TL = max(total_time_limit - elapsed_time, 0)
    print 'Out of the loop. Time remaning: ', TL

    first = False

    instance_local_branching = add_ub_constraint(instance_local_branching, bestUB)
    opt_local_branching.options["mipgap"] = 0.03
    opt_local_branching = set_solver_time_limit_phase_1(opt_local_branching, first, TL)

    results = opt_local_branching.solve(instance_local_branching, load_solutions=False)

    try:
        instance.solutions.load_from(results)
        # Every Feasible solution would improve the previous solution due to the UB value
        if (results.solver.status == SolverStatus.ok) and (
                    results.solver.termination_condition == TerminationCondition.optimal):
            best_price = instance_local_branching.Price()
            x_star = instance_local_branching.y
            zin_star = instance_local_branching.zin
            zout_star = instance_local_branching.zout
        opt = True
    except ValueError:
        opt = False

    print 'Can you certify the optimaility of the found solution?: ', opt
    print 'V: ', best_price

    return {'y':x_star, 'zin':zin_star ,'zout':zout_star }


########################################## PHASE 1 ###########################################


def refine(y):
    #TODO
    return

def set_solver_time_limit_phase_1(opt_local_branching, first, TL):
    if first == True:
        opt_local_branching.options["mipgap"] = local_branching_mipgap_phase_1_first
        opt_local_branching.options["timelimit"] = TL
    else:
        opt_local_branching.options["timelimit"] = TL
        opt_local_branching.options["mipgap"] = local_branching_mipgap_phase_1


    return opt_local_branching


def add_constraint_phase_1(instance_local_branching, num_constraints, tuple):
    choice = tuple[0]
    x_bar = tuple[1]
    k = tuple[2]

    C = Set(dimen=2, initialize=build_set_C(x_bar))
    # NC = Set(dimen=2, initialize=build_set_C(x_bar)) # We don't use it in the asymmetric version

    instance_local_branching.add_component('C_' + str(num_constraints), C)

    if choice == 'Left':
        instance_local_branching.add_component('local_branching_constraint_' + str(num_constraints),
                                               Constraint(
                                                   expr=sum((1 - instance_local_branching.y[yy]) for yy in getattr(instance_local_branching, 'C_' + str(num_constraints))) >= k / 2))
    elif choice == 'Right':
        instance_local_branching.add_component('local_branching_constraint_' + str(num_constraints),
                                               Constraint(
                                                   expr=sum((1 - instance_local_branching.y[yy]) for yy in getattr(instance_local_branching, 'C_' + str(num_constraints))) >= k / 2))

    return instance_local_branching


def revert_last_constraint_phase_1(instance_local_branching, num_constraints, tuple):
    choice = tuple[0]
    x_bar = tuple[1]
    k = tuple[2]

    instance_local_branching.del_component('local_branching_constraint_' + str(num_constraints - 1))
    instance_local_branching.del_component('C_' + str(num_constraints - 1))

    C = Set(dimen=2, initialize=build_set_C(x_bar))
    # NC = Set(dimen=2, initialize=build_set_C(x_bar)) # We don't use it in the asymmetric version

    instance_local_branching.add_component('C_' + str(num_constraints - 1), C)

    if choice == 'Left':
        instance_local_branching.add_component('local_branching_constraint_' + str(num_constraints - 1),
                                               Constraint(
                                                   expr=sum((1 - instance_local_branching.y[yy]) for yy in getattr(instance_local_branching, 'C_' + str(num_constraints - 1))) >= k / 2))
    elif choice == 'Right':
        instance_local_branching.add_component('local_branching_constraint_' + str(num_constraints - 1),
                                               Constraint(
                                                   expr=sum((1 - instance_local_branching.y[yy]) for yy in getattr(instance_local_branching, 'C_' + str(num_constraints - 1))) >= k / 2))

    return instance_local_branching



def delete_last_constraint_phase_1(instance_local_branching, num_constraints):
    instance_local_branching.del_component('local_branching_constraint_' + str(num_constraints - 1))
    instance_local_branching.del_component('C_' + str(num_constraints - 1))

    return instance_local_branching

def add_ub_constraint(instance_local_branching, UB):
    try:
        instance_local_branching.add_component('ub', Constraint(expr= instance_local_branching.v <= 0.999 * UB))
        return instance_local_branching
    except RuntimeError, AttributeError:
        instance_local_branching.del_component('ub')
        instance_local_branching.add_component('ub', Constraint(expr=instance_local_branching.v <= 0.999 * UB))
        return instance_local_branching




def print_stats_z(instance_local_branching):
    tot = 0
    fract = 0

    for z in instance_local_branching.Z:
        tot += 1
        if instance_local_branching.zin[z].value != 0 and instance_local_branching.zin[z].value != 1 and instance_local_branching.zin[z].value != None:
            fract += 1
            print z , ': ', instance_local_branching.zin[z].value

    for z in instance_local_branching.Z:
        tot += 1
        if instance_local_branching.zout[z].value != 0 and instance_local_branching.zout[z].value != 1 and instance_local_branching.zout[z].value != None:
            print z, ': ', instance_local_branching.zout[z].value

    print 'Percentage fractional: ', float((fract * 100) / tot)


def construct_a_feasible_solution(y, zin, zout):
    warm_start_time = time.time()

    model_phase2_warm_start = generateModel_milp_3index_fixed_centers(y)
    instance_phase2_warm_start = model_phase2_warm_start.create_instance(dat_file_name)

    for z in instance_phase2_warm_start.Z:
        if zin[z] == 1:
            instance_phase2_warm_start.zin[z] = 1
        elif zin[z] == 0:
            instance_phase2_warm_start.zin[z] = 0
        if zout[z] == 1:
            instance_phase2_warm_start.zout[z] = 1
        elif zout[z] == 0:
            instance_phase2_warm_start.zout[z] = 0


    results = opt_phase_2_milp_warm_start.solve(instance_phase2_warm_start, warmstart=True)

    end_time_phase2_warm_start = time.time() - warm_start_time

    print 'Minimum v price after local branching in phase 1 and warm start in phase 2: ', instance_phase2_warm_start.Price()
    print 'Phase 2 warm start time: ', end_time_phase2_warm_start



########################################## PHASE 2 ###########################################

def set_solver_time_limit_phase_2(opt_local_branching, first, TL):
    if first == True:
        opt_local_branching.options["mipgap"] = local_branching_mipgap_phase_2_first
        opt_local_branching.options["timelimit"] = TL
    else:
        opt_local_branching.options["timelimit"] = TL
        opt_local_branching.options["mipgap"] = local_branching_mipgap_phase_2


    return opt_local_branching

def add_constraint_phase_2(instance_local_branching, num_constraints, tuple):
    choice = tuple[0]
    zin_bar = tuple[1]
    zout_bar = tuple[2]
    k = tuple[3]

    S_zin = Set(dimen=4, initialize=build_set_S(zin_bar))
    NS_zin = Set(dimen=4, initialize=build_set_NS(zin_bar))

    S_zout = Set(dimen=4, initialize=build_set_S(zout_bar))
    NS_zout = Set(dimen=4, initialize=build_set_NS(zout_bar))

    instance_local_branching.add_component('S_zin_' + str(num_constraints), S_zin)
    instance_local_branching.add_component('S_zout_' + str(num_constraints), S_zout)

    instance_local_branching.add_component('NS_zin_' + str(num_constraints), NS_zin)
    instance_local_branching.add_component('NS_zout_' + str(num_constraints), NS_zout)

    if choice == 'Left':
        instance_local_branching.add_component('local_branching_constraint_zin_' + str(num_constraints),
                        Constraint(
                                   expr=
                                   sum((1 - instance_local_branching.zin[z])
                                       for z in getattr(instance_local_branching,'S_zin_' + str(num_constraints))) + \
                                   sum(instance_local_branching.zin[z]
                                       for z in getattr(instance_local_branching,'NS_zin_' + str(num_constraints))) <= k))

        instance_local_branching.add_component('local_branching_constraint_zout_' + str(num_constraints),
                        Constraint(
                                   expr=
                                   sum((1 - instance_local_branching.zout[z])
                                       for z in getattr(instance_local_branching,'S_zout_' + str(num_constraints))) + \
                                   sum(instance_local_branching.zout[z]
                                       for z in getattr(instance_local_branching,'NS_zout_' + str(num_constraints))) <= k))

    elif choice == 'Right':
        instance_local_branching.add_component('local_branching_constraint_zin_' + str(num_constraints),
                        Constraint(
                                   expr=
                                   sum((1 - instance_local_branching.zin[z])
                                        for z in getattr(instance_local_branching, 'S_zin_' + str(num_constraints))) + \
                                   sum(instance_local_branching.zin[z]
                                        for z in getattr(instance_local_branching, 'NS_zin_' + str(num_constraints))) >= k))

        instance_local_branching.add_component('local_branching_constraint_zout_' + str(num_constraints),
                        Constraint(
                                   expr=
                                   sum((1 - instance_local_branching.zout[z])
                                        for z in getattr(instance_local_branching, 'S_zout_' + str(num_constraints))) + \
                                   sum(instance_local_branching.zout[z]
                                       for z in getattr(instance_local_branching,'NS_zout_' + str(num_constraints))) >= k))

    return instance_local_branching


def revert_last_constraint_phase_2(instance_local_branching, num_constraints, tuple):
    choice = tuple[0]
    zin_bar = tuple[1]
    zout_bar = tuple[2]
    k = tuple[3]

    instance_local_branching.del_component('local_branching_constraint_zin_' + str(num_constraints - 1))
    instance_local_branching.del_component('local_branching_constraint_zout_' + str(num_constraints - 1))
    instance_local_branching.del_component('S_zin_' + str(num_constraints - 1))
    instance_local_branching.del_component('S_zout_' + str(num_constraints - 1))
    instance_local_branching.del_component('NS_zin_' + str(num_constraints - 1))
    instance_local_branching.del_component('NS_zout_' + str(num_constraints - 1))

    S_zin = Set(dimen=4, initialize=build_set_S(zin_bar))
    NS_zin = Set(dimen=4, initialize=build_set_NS(zin_bar))

    S_zout = Set(dimen=4, initialize=build_set_S(zout_bar))
    NS_zout = Set(dimen=4, initialize=build_set_NS(zout_bar))

    instance_local_branching.add_component('S_zin_' + str(num_constraints - 1), S_zin)
    instance_local_branching.add_component('S_zout_' + str(num_constraints - 1), S_zout)

    instance_local_branching.add_component('NS_zin_' + str(num_constraints - 1), NS_zin)
    instance_local_branching.add_component('NS_zout_' + str(num_constraints - 1), NS_zout)

    if choice == 'Left':
        instance_local_branching.add_component('local_branching_constraint_zin_' + str(num_constraints - 1),
                                               Constraint(
                                                   expr=
                                                   sum((1 - instance_local_branching.zin[z])
                                                       for z in getattr(instance_local_branching,
                                                                        'S_zin_' + str(num_constraints - 1))) + \
                                                   sum(instance_local_branching.zin[z]
                                                       for z in getattr(instance_local_branching,
                                                                        'NS_zin_' + str(num_constraints - 1))) <= k))

        instance_local_branching.add_component('local_branching_constraint_zout_' + str(num_constraints - 1),
                                               Constraint(
                                                   expr=
                                                   sum((1 - instance_local_branching.zout[z])
                                                       for z in getattr(instance_local_branching,
                                                                        'S_zout_' + str(num_constraints - 1))) + \
                                                   sum(instance_local_branching.zout[z]
                                                       for z in getattr(instance_local_branching,
                                                                        'NS_zout_' + str(num_constraints - 1))) <= k))

    elif choice == 'Right':
        instance_local_branching.add_component('local_branching_constraint_zin_' + str(num_constraints - 1),
                                               Constraint(
                                                   expr=
                                                   sum((1 - instance_local_branching.zin[z])
                                                       for z in getattr(instance_local_branching,
                                                                        'S_zin_' + str(num_constraints - 1))) + \
                                                   sum(instance_local_branching.zin[z]
                                                       for z in getattr(instance_local_branching,
                                                                        'NS_zin_' + str(num_constraints - 1))) >= k))

        instance_local_branching.add_component('local_branching_constraint_zout_' + str(num_constraints - 1),
                                               Constraint(
                                                   expr=
                                                   sum((1 - instance_local_branching.zout[z])
                                                       for z in getattr(instance_local_branching,
                                                                        'S_zout_' + str(num_constraints - 1))) + \
                                                   sum(instance_local_branching.zout[z]
                                                       for z in getattr(instance_local_branching,
                                                                        'NS_zout_' + str(num_constraints - 1))) >= k))

    return instance_local_branching


def delete_last_constraint_phase_2(instance_local_branching, num_constraints):
    instance_local_branching.del_component('local_branching_constraint_zin_' + str(num_constraints - 1))
    instance_local_branching.del_component('local_branching_constraint_zout_' + str(num_constraints - 1))
    instance_local_branching.del_component('S_zin_' + str(num_constraints - 1))
    instance_local_branching.del_component('S_zout_' + str(num_constraints - 1))
    instance_local_branching.del_component('NS_zin_' + str(num_constraints - 1))
    instance_local_branching.del_component('NS_zout_' + str(num_constraints - 1))

    return instance_local_branching




def LocBra_phase_2(y_phase_1, k_swap, total_time_limit, node_time_limit, dv_max):
    infinity = float('infinity')
    rhs = bestUB = UB = TL = infinity
    opt = first = True
    diversify = False
    dv = 0
    zin_bar = zout_bar = zin_star = zout_star =None

    opt_local_branching = SolverFactory("gurobi")

    start_time = time.time()
    elapsed_time = time.time() - start_time

    print 'Beginning of the local branching procedure\n'

    constraints_stack_list = []

    ite = 0

    model = generate_local_branching_model_phase_2(UB, y_phase_1)
    instance_local_branching = model.create_instance(dat_file_name)

    num_constraints = 0

    while elapsed_time <= total_time_limit and dv <= dv_max:

        if rhs < infinity:
            constraints_stack_list.append("delta(x, (zin_bar_" + str(ite) + ',zout_bar_' + str(ite) + ") ) <= rhs")
            instance_local_branching = add_constraint_phase_2(instance_local_branching, num_constraints, ('Left', zin_bar, zout_bar, rhs))
            num_constraints = num_constraints + 1
            # Add the local br. constraint into delta(x, x_bar) <= rhs


        TL = min(TL, total_time_limit - elapsed_time) # Da decommentare nel caso in cui non si parta con una feasible solution

        opt_local_branching = set_solver_time_limit_phase_2(opt_local_branching, first, TL)

        print 'Lodi_solution: ', constraints_stack_list

        instance_local_branching = add_ub_constraint(instance_local_branching, UB)

        results = opt_local_branching.solve(instance_local_branching, load_solutions=False)

        TL = node_time_limit

        #1)
        if (results.solver.status == SolverStatus.ok) and (
                    results.solver.termination_condition == TerminationCondition.optimal) and not first: #Last condition to differentiate from a feasible solution
            instance.solutions.load_from(results)
            print 'Optimal Solution found!: ', instance_local_branching.Price()

            if instance_local_branching.Price() < bestUB:
                bestUB = instance_local_branching.Price()
                zin_star = instance_local_branching.zin
                zout_star = instance_local_branching.zout


            if rhs >= infinity:
                return opt

            # Reverse the last local br. constraint into delta(x, x_bar) >= rhs + 1
            constraints_stack_list.pop()
            constraints_stack_list.append("delta(x, (zin_bar_" + str(ite) + ',zout_bar_' + str(ite) + ") ) >= rhs + 1")

            instance_local_branching = revert_last_constraint_phase_2(instance_local_branching, num_constraints, ('Right', zin_bar, zout_bar, rhs + 1))

            diversify = first = False
            zin_bar = zin_star
            zout_bar = zout_star
            UB = instance_local_branching.Price()
            rhs = k_swap

        #2)
        elif (results.solver.termination_condition == TerminationCondition.infeasible):
            instance.solutions.load_from(results)
            print 'Proved to be infeasible!'

            if rhs >= infinity:
                return opt

            # Reverse the last local br. constraint into delta(x, x_bar) >= rhs + 1
            constraints_stack_list.pop()
            constraints_stack_list.append("delta(x, (zin_bar_" + str(ite) + ',zout_bar_' + str(ite) + ") ) >= rhs + 1")

            instance_local_branching = revert_last_constraint_phase_2(instance_local_branching, num_constraints, ('Right', zin_bar, zout_bar, rhs + 1))

            if diversify:
                print 'Diversify'
                UB = TL = infinity
                dv = dv + 1
                first = True

            rhs = rhs + k_swap/2
            diversify = True


        elif ( results.solver.status == SolverStatus.aborted and
                    results.solver.termination_condition == TerminationCondition.maxTimeLimit) or \
                ( results.solver.status == SolverStatus.ok and
                    results.solver.termination_condition == TerminationCondition.optimal and first ):
            try:
                instance.solutions.load_from(results)
                #3 ) Quando il mipgap e diverso da None vuol dire che cerchiamo una feasible solution
                print 'Solution found but not sure that is optimal: ', instance_local_branching.Price()

                if rhs < infinity:
                    if first:
                        # Delete the last local br. constraint into delta(x, x_bar) <= rhs
                        constraints_stack_list.pop()

                        instance_local_branching = delete_last_constraint_phase_2(instance_local_branching, num_constraints)
                        num_constraints = num_constraints - 1
                    else:
                        # Reverse the last local br. constraint into delta(x, x_bar) >= 1
                        constraints_stack_list.pop()
                        constraints_stack_list.append("delta(x, (zin_bar_" + str(ite) + ',zout_bar_' + str(ite) + ") ) >= 1")

                        instance_local_branching = revert_last_constraint_phase_2(instance_local_branching, num_constraints,
                                                                                  ('Right', zin_bar, zout_bar, 1))

                #refine()

                if instance_local_branching.Price() < bestUB:
                    bestUB = instance_local_branching.Price()
                    zin_star = instance_local_branching.zin
                    zout_star = instance_local_branching.zout

                first = diversify = False
                zin_bar = instance_local_branching.zin
                zout_bar = instance_local_branching.zout
                UB = instance_local_branching.Price()
                rhs = k_swap

            except ValueError:

                #4 )
                print 'No_Feasible_solution_found!'
                if diversify:
                    print 'Diversify'
                    # Replace the last local br. constraint delta(x, x_bar) <= rhs with delta(x, x_bar) >= 1
                    constraints_stack_list.pop()
                    constraints_stack_list.append("delta(x, (zin_bar_" + str(ite) + ',zout_bar_' + str(ite) + ") ) >= 1")

                    instance_local_branching = revert_last_constraint_phase_2(instance_local_branching, num_constraints,
                                                                              ('Right', zin_bar, zout_bar, 1))

                    UB = TL = infinity
                    dv = dv + 1
                    rhs = rhs + k_swap / 2
                    first = True
                else:
                    # Delete the last local br. constraint into delta(x, x_bar) <= rhs
                    constraints_stack_list.pop()

                    instance_local_branching = delete_last_constraint_phase_2(instance_local_branching, num_constraints)
                    num_constraints = num_constraints - 1
                    rhs = rhs - k_swap / 2

                diversify = True


        elapsed_time = time.time() - start_time
        print 'elapsed_time: ', elapsed_time, '\n'
        ite += 1

    ###################### END LOOP ######################

    best_price = bestUB

    #print_stats_z(instance_local_branching)

    TL = max(total_time_limit - elapsed_time, 0)
    print 'Out of the loop. Time remaning: ', TL

    first = False

    instance_local_branching = add_ub_constraint(instance_local_branching, bestUB)
    opt_local_branching.options["mipgap"] = 0.03
    opt_local_branching = set_solver_time_limit_phase_2(opt_local_branching, first, TL)

    results = opt_local_branching.solve(instance_local_branching, load_solutions=False)

    try:
        instance.solutions.load_from(results)
        # Every Feasible solution would improve the previous solution due to the UB value
        if (results.solver.status == SolverStatus.ok) and (
                    results.solver.termination_condition == TerminationCondition.optimal):
            best_price = instance_local_branching.Price()
            zin_star = instance_local_branching.zin
            zout_star = instance_local_branching.zout
        opt = True
    except ValueError:
        opt = False

    print 'Can you certify the optimaility of the found solution?: ', opt
    print 'V: ', best_price

    for i in instance_local_branching.Z:
        if (instance_local_branching.zin[i].value > 0 and instance_local_branching.zin[i].value < 1):
            print 'Errore zin: ', instance_local_branching.zin[i].value
        if (instance_local_branching.zout[i].value > 0 and instance_local_branching.zout[i].value < 1):
            print 'Errore zout: ', instance_local_branching.zout[i].value


    return {'zin':zin_star ,'zout':zout_star }