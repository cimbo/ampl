############################################################################
#       MILP 3-INDEX PROBLEM CENTER AS PARAMETERS AND FLOW VARIABLES BINARY
############################################################################
from __future__ import division
from pyomo.environ import *

infinity = float('inf')

#Genera il modello matematico
def generateModel_milp_3index_local_search_z(v_to_improve, set_S_zin, set_Not_S_zin, set_S_zout, set_Not_S_zout, k):

    model = AbstractModel()

    ####################################
    # PARAMS & SETS
    ####################################

    model.k = Param()

    # Set of requests
    model.K = RangeSet(1, model.k)

    model.n = Param()

    # Sets of physical nodes and arcs
    model.N = RangeSet(1, model.n)

    model.L = Set(within=model.N * model.N)

    model.Pk = Param(model.K)

    #####################################
    # Physical nodes and links capacities
    #####################################

    # capacities of arcs
    model.a = Param(model.L, within=NonNegativeReals)

    # capacities of nodes
    model.aa = Param(model.N, within=NonNegativeReals)

    # distance
    model.dist = Param(model.L, within=NonNegativeReals)

    # cost(=dist*1000)
    model.c = Param(model.L, within=NonNegativeReals)

    # activation cost
    model.cc = Param(model.N, within=NonNegativeReals)

    ########################################
    # Sets of virtual nodes, one per request
    ########################################

    # number of nodes
    model.vn = Param(model.K)

    # Virtual request traffic matrix (nominal)


    def VN_Init(model, kk):
        retval = []
        for jj in range(1, model.vn[kk] + 1):
            retval.append(jj)
        return retval

    # helper set
    model.VN = Set(model.K, initialize=VN_Init)

    def VNl_Init(model, kk):
        retval = []
        for jj in range(2, model.vn[kk] + 1):
            retval.append(jj)
        return retval

    # set of leaves
    model.VNl = Set(model.K, initialize=VNl_Init)

    # number of arcs of each VN(the same for all)
    model.D = 6

    #HELPER SET
    def KN_Init(model):

        dict = []

        for k in model.K:
            for l in model.VNl[k]:
                dict.append((k,l))

        return dict


    model.KN = Set(dimen=2, initialize=KN_Init)

    model.qin = Param(model.KN, default=0)

    model.qout = Param(model.KN, default=0)

    # Location of leaves

    # Set of nodes considered as leaves for different VNs
    model.Vf = Set(model.K, within= model.N)

    def Vfc_init(model, kk):
        return model.N.difference(model.Vf[kk])

    model.Vfc = Set(model.K, initialize=Vfc_init)

    ########################################
    # param location
    ########################################

    # HELPER SET
    def LOC_Init(model):

        dict = []

        for k in model.K:
            for l in model.VNl[k]:
                for ip in model.Vf[k]:
                    dict.append((k, l, ip))

        return dict

    model.LOC = Set(dimen=3, initialize=LOC_Init)


    #kk in K, VNl[kk], ip in Vf[kk]
    model.loc = Param(model.LOC, default=0)

    ####################################
    # VARS
    ####################################

    # HELPER SET
    def Y_Init(model):

        dict = []

        for k in model.K:
            for l in model.Vfc[k]:
                dict.append((k, l))
        return dict

    model.Y = Set(dimen=2, initialize=Y_Init)

    ####################################
    # FURTHER PARAMETER
    ####################################

    # HELPER SET
    def Vl_Init(model):

        dict = []

        for k in model.K:
            for l in model.VN[k]:
                dict.append((k, l))

        return dict

    model.Vl = Set(dimen=2, initialize=Vl_Init)

    model.vl = Param(model.Vl , default=0)

    # One if a virtual ii from request rr is mapped onto physical node jj (within V_local)
    model.y = Var(model.Y, within=Binary)

    # One if at least one centre is located onto node i
    model.w = Var(model.N, within=Binary)

    # Multicommodity flow for virtual request rr between virtual nodes ii and jj on physical arc (i,j)
    # Note: it's defined only for strictly positive demands

    # HELPER SET
    def Z_Init(model):

        dict = []

        for k in model.K:
            for l in model.VNl[k]:
                for (i,j) in model.L:
                    dict.append((k, l, i, j))

        return dict


    #{kk in K, jj in VNl[kk], (i,j) in L
    model.Z = Set(dimen=4, initialize=Z_Init)

    model.zout = Var(model.Z, within=Binary, bounds=(0,1)) #considero (1,jj)

    model.zin = Var(model.Z, within=Binary, bounds=(0,1)) # considero (jj,1)

    # Set where zin[z] == 1
    model.S_zin = Set(dimen=4, initialize=set_S_zin)
    # Set where zin[z] == 0
    model.NS_zin = Set(dimen=4, initialize=set_Not_S_zin)
    # Set where zout[z] == 1
    model.S_zout = Set(dimen=4, initialize=set_S_zout)
    # Set where zout[z] == 0
    model.NS_zout = Set(dimen=4, initialize=set_Not_S_zout)

    # INVESTMENT

    # Maximum unitary price
    model.v  = Var(within=NonNegativeReals)

    # HELPER SET
    def P_Init(model):

        dict = []

        for k in model.K:
            for (i, j) in model.L:
                dict.append((k, i, j))

        return dict

    # kk in K, (i,j) in L
    model.P = Set(dimen=3, initialize=P_Init)

    # Price paid by VN kk to expand arc (i,j)
    model.p = Var(model.P, within=NonNegativeReals)

    # Price paid by VN kk to activate node i as a centre
    # var pc{kk in K, i in N}>=0;

    # Capacity added to arc (i,j)
    model.b = Var(model.L, within=NonNegativeReals)

    # HELPER SET
    def KNK_Init(model):

        dict = []

        for k in model.K:
            for l in model.VN[k]:
                dict.append((k,l))

        return dict

    # kk in K, (i,j) in L
    model.KNK = Set(dimen=2, initialize=KNK_Init)

    ####################################
    # OBJECTIVE FUNCTION
    ####################################

    def Price(model):
        return 0
    model.Price = Objective(rule=Price, sense=minimize)

    def prova(model):

        #for tuple in model.S_zin:
            #print tuple

        return []

    # helper set
    model.prova = Set(initialize=prova)


    ####################################
    # CONSTRAINTS
    ####################################

    def improve(model):
        return model.v <= v_to_improve * 0.95
    model.improve = Constraint(rule=improve)

    def massimo(model, kk):
        return model.v >= sum(model.p[kk,i,j] for (i,j) in model.L) / sum( model.qin[kk, jj] + model.qout[kk, jj] for jj in model.VNl[kk])
    model.massimo = Constraint(model.K, rule=massimo)

    # ---------
    # EMBEDDING
    # ---------
    # A virtual node assigned to exactly a single physical node
    # (since each VN is accepted in this case)

    # Arc capacities constraint
    def arc_capacities(model, i, j):
        return sum(model.qout[kk, im] * model.zout[kk, im, i, j] + model.qin[kk, im] * model.zin[kk, im, i, j] for kk in model.K for im in model.VNl[kk]) <= 0.8 * (model.a[i, j] + model.b[i, j])
    model.arc_capacities = Constraint(model.L, rule=arc_capacities)

    # HELPER SET {kk in K, ip in Vfc[kk], im in VNl[kk]}
    def FLOW1_Init(model):

        dict = []

        for kk in model.K:
            for ip in model.Vfc[kk]:
                for im in model.VNl[kk]:
                    dict.append((kk, ip, im))
        return dict

    model.FLOW1 = Set(dimen=3, initialize=FLOW1_Init)



    # HELPER SET { kk in K, ip in Vfc[kk], im in VNl[kk] }
    def FLOW2_Init(model):

        dict = []

        for kk in model.K:
            for ip in model.Vf[kk]:
                for im in model.VNl[kk]:
                    dict.append((kk, ip, im))
        return dict

    model.FLOW2 = Set(dimen=3, initialize=FLOW2_Init)

    def LIn_init(model, node):
        retval = []
        for (i, j) in model.L:
            if j == node:
                retval.append(i)
        return retval

    model.LIn = Set(model.N, initialize=LIn_init)

    def LOut_init(model, node):
        retval = []
        for (i, j) in model.L:
            if i == node:
                retval.append(j)
        return retval

    model.LOut = Set(model.N, initialize=LOut_init)

    # centres
    def flows1a(model, kk, ip, im):
        return sum(model.zout[kk, im, ip, j] for j in model.LOut[ip]) - sum(model.zout[kk, im, j, ip] for j in model.LIn[ip]) == model.y[kk, ip]
    model.flows1a = Constraint(model.FLOW1, rule=flows1a)

    # leaves
    def flows2a(model, kk, ip, im):
        if model.loc[kk,im,ip] == 1:
            return sum(model.zout[kk, im, ip, j] for j in model.LOut[ip]) - sum(model.zout[kk, im, j, ip] for j in model.LIn[ip]) == -1
        else:
            return Constraint.Skip
    model.flows2a = Constraint(model.FLOW2, rule=flows2a)

    def flows3a(model, kk, ip, im):
        if model.loc[kk, im, ip] == 0:
            return sum(model.zout[kk, im, ip, j] for j in model.LOut[ip]) - sum(
                model.zout[kk, im, j, ip] for j in model.LIn[ip]) == 0
        else:
            return Constraint.Skip
    model.flows3a = Constraint(model.FLOW2, rule=flows3a)

    def flows1b(model, kk, ip, im):
       return sum(model.zin[kk, im, ip, j] for j in model.LOut[ip]) - sum(model.zin[kk, im, j, ip] for j in model.LIn[ip]) == -model.y[kk, ip]
    model.flows1b = Constraint(model.FLOW1, rule=flows1b)

    def flows2b(model, kk, ip, im):
        if model.loc[kk, im, ip] == 1:
            return sum(model.zin[kk, im, ip, j] for j in model.LOut[ip]) - sum(model.zin[kk, im, j, ip] for j in model.LIn[ip]) == 1
        else:
            return Constraint.Skip
    model.flows2b = Constraint(model.FLOW2, rule=flows2b)

    def flows3b(model, kk, ip, im):
        if model.loc[kk, im, ip] == 0:
            return sum(model.zin[kk, im, ip, j] for j in model.LOut[ip]) - sum(model.zin[kk, im, j, ip] for j in model.LIn[ip]) == 0
        else:
            return Constraint.Skip
    model.flows3b = Constraint(model.FLOW2, rule=flows3b)

    # ----------
    # INVESTMENT
    # ----------

    # Relation between the investment and its cost
    def investment(model, i, j):
        return model.b[i, j] == sum( (model.p[kk, i, j] / model.c[i, j]) for kk in model.K)
    model.investment = Constraint(model.L, rule=investment)

    def simmetry(model, i, j):
        return model.b[i, j] == model.b[j, i]
    model.simmetry = Constraint(model.L, rule=simmetry)

    # Minimum price
    def minimumpricer(model, kk):
        return 0.1 <= model.v
    model.minimumpricer = Constraint(model.K, rule=minimumpricer)

    def arch_local_search(model):
        return sum((1 - model.zin[z]) for z in model.S_zin) + \
               sum((1 - model.zout[z]) for z in model.S_zout) + \
               sum(model.zin[z] for z in model.NS_zin) + \
               sum(model.zout[z] for z in model.NS_zout) <= k

    model.arch_local_search = Constraint(rule=arch_local_search)

    return model






