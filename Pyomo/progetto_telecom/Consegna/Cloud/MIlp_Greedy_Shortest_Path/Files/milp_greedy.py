# --------------------------------------------------------Header-------------------------------------------------------- #
from __future__ import division
import random
import time
from pyomo.opt import SolverFactory
from VNoM import generateModel_VNoM_3index
from milp_3index import generateModel_milp_3index
from greedy_objective import generateModel_greedy_objective
from milp_3index_phase2_local_search import generateModel_milp_3index_local_search
import json
from Dijkstra_shortest_path import *
import sys

######################################################################
# PSEUDORANDOM SETTING
######################################################################
SEED = 14041992

#This line of code allows to reproduce in any moment the same pseudo random execution of the algorithm
random.seed(SEED)

######################################################################
# SOLVER PREFERENCES
######################################################################

#<---- DETERMINISTIC HEURISTIC ---->
opt_deterministic_phase_1 = SolverFactory("gurobi")
opt_deterministic_phase_1.options["mipgap"] = 0.03
#opt_deterministic_phase_1.options["timelimit"] = 150

opt_deterministic_phase_2 = SolverFactory("gurobi")
opt_deterministic_phase_2.options["mipgap"] = 0.03
#opt_deterministic_phase_2.options["timelimit"] = 150

#<---- RANDOMIZED HEURISTIC ---->
opt_randomized_heuristic_phase1 = SolverFactory("gurobi")
opt_randomized_heuristic_phase1.options["mipgap"] = 0.10
#opt_randomized_heuristic_phase1.options["timelimit"] = 50

opt_randomized_heuristic_local_search = SolverFactory("gurobi")
opt_randomized_heuristic_local_search.options["mipgap"] = 0.03
opt_randomized_heuristic_local_search.options["timelimit"] = 50

#<----- GREEDY OBJECTIVE ------>
opt_greedy_objective = SolverFactory("gurobi")
opt_greedy_objective.options["mipgap"] = 0.03
#opt_randomized_heuristic_phase2.options["timelimit"] = 50

#<----- LOCAL SEARCH ------>
opt_greedy_local_search = SolverFactory("gurobi")
opt_greedy_local_search.options["mipgap"] = 0.05
opt_greedy_local_search.options["timelimit"] = 50

######################################################################
# GLOBAL VARIABLES
######################################################################

gamma = 0.2

max_arch_swap = 300
max_center_swap = 0

#ORDINAMENTO_FLUSSI = 0 politica tesi Laura
#ORDINAMENTO_FLUSSI = 1 politica idea mia con il ciclo for esterno
ORDINAMENTO_FLUSSI = 0

min_price = 0.1

#Instance of the exact model
global exact_optimum
global instance
global end_time_exact

#Deterministic Heuristic
global deterministic_price
global end_time_phase1_pure_heuristic
global end_time_phase2_pure_heuristic
global end_time_heuristic_full_greedy
global y
global zin
global zout


#Randomized Heuristic
global best
global end_time_phase1_randomized_heuristic
global end_time_phase2_randomized_heuristic
global end_time_randomized_heuristic
global dat_file_name


######################################################################
# CONSTANTS
######################################################################

#Number Of Iteration Grasp
n_ite = 20

BASE_PERCENTAGE = 0.10
PERCENTAGE_IMPROVEMENT = 0.05
FROM_LEAF_TO_CENTER = 0
FROM_CENTER_TO_LEAF = 1

'''
After a set of indices greater than MAX_INDICES_FOR_FULL_PERMUTATION the indicies vector is just shuffled. So that cold be the possibilities to try two or more time the same case.
When the size of the set of choosen indicies is less or equal to MAX_INDICES_FOR_FULL_PERMUTATION for sure will never happen the same case two times (due to the fact that i calculated all the possible permutations)
'''
MAX_INDICES_FOR_FULL_PERMUTATION = 8



# ----------------------------------------------------Main---------------------------------------------------- #




def main(file, optimum, exact_t):

    print file

    set_parameters(file, optimum, exact_t)

    generate_data_instance()

    solve_deterministic_heuristic()
    # Ranges are shuffled all togheter
    execute_randomized_progressive_grasp_heuristic()

    return build_solutions_json()



######################################################################
# HELPER FUNCTIONS
######################################################################
def build_graph():
    global instance

    g = Graph()

    for i in instance.N:
        g.add_vertex(i)

    for l in instance.L:
        g.add_edge(l[0], l[1], float(1 / instance.a[l]))

    return g


def set_parameters(file, optimum, exact_t):
    global dat_file_name
    global exact_optimum
    global end_time_exact

    dat_file_name = file
    exact_optimum = optimum
    end_time_exact = exact_t

def generate_data_instance():

    global instance
    global dat_file_name

    abstract_model = generateModel_milp_3index()

    instance = abstract_model.create_instance(dat_file_name)



def solve_deterministic_heuristic():

    # Parameters

    global deterministic_price
    global end_time_phase1_pure_heuristic
    global end_time_phase2_pure_heuristic
    global end_time_heuristic_full_greedy
    global y
    global best
    global zin
    global zout

    # Parameters

    n = instance.n.value
    c = instance.c
    L = instance.L
    vn = instance.vn
    vl = instance.vl
    Vf = instance.Vf
    loc = instance.loc
    qin = instance.qin
    qout = instance.qout

    #print "# #######################################################################################################################################"
    #print "# Pure Heuristic No Random: The idea is to fix the center in a greedy way and then, using the phase 2 of the milp, find a solution"
    #print "# #######################################################################################################################################\n"

    start_pure_heuristic_time = time.time()

    start_time_phase1_pure_heuristic = time.time()

    #print "# Phase 1: Now fore each k in K choose the substrate node onto which the substrate center of Gk has to be mapped"

    ordered_VNs_vector = build_VNs_ordered_vector(instance)
    y = init_y_assign_param()
    used_capacity = init_used_capacity(L)
    a = instance.a

    ####################################################
    # VNoM
    ####################################################

    for j in range(0, len(ordered_VNs_vector)):
        k = ordered_VNs_vector[j][0]

        model = generateModel_VNoM_3index(n, {k}, a, used_capacity, c, L, vn, vl, Vf, instance.Vfc, loc, qin,
                                          qout)
        instance_center = model.create_instance()
        results = opt_randomized_heuristic_phase1.solve(instance_center)

        used_capacity = calculate_used_capacity(k, used_capacity, instance_center)

        a = calculate_current_capacity(instance_center)

        y = save_y_assignment_VNk(k, y, instance_center)

        end_time_phase1_milp_greedy = float(time.time() - start_time_phase1_pure_heuristic)

    #print "# Phase 2: Solve Partial linear relaxation with NON splittable flows "

    ####################################################
    # VLiM
    ####################################################

    start_time_phase2_pure_heuristic = time.time()

    #Starting substrate network situation
    used_capacity = init_used_capacity(L)
    a = dict(instance.a)
    zin = init_z()
    zout = init_z()

    #print ordered_VNs_vector

    ordered_VNs_flows_vector = build_ordered_VNs_flows_vector(ordered_VNs_vector)

    #print ordered_VNs_flows_vector

    #random.shuffle(ordered_VNs_flows_vector)

    g = build_graph()

    for i in range(0,len(ordered_VNs_flows_vector)):

        #print 'Iteration number: ', i

        #( k, [(1,l) | (l,1)] , [flow_demand] , logical_leaf ) #The center is alway 1 from the logical point of view!
        flow = ordered_VNs_flows_vector[i]

        from_ = get_physical_node_from(flow, y)
        to_ = get_physical_node_to(flow, y)

        '''
            Se la domanda di flusso in esame e ad esempio 200, il costo degli archi che se seleionati avrebbero meno del 20% di capacita residua, devono avere un costo che consideri gia una possibile espansione!
        '''

        g = calculate_arch_cost_to_satisfy_capacity_constraints(g, flow, used_capacity, a)


        dijkstra(g, g.get_vertex(from_))
        target = g.get_vertex(to_)
        path = [target.get_id()]
        shortest_path = shortest(target, path)

        s_path = path[::-1]

        #print 'The shortest path : %s' % (s_path)

        used_capacity_current_flow = flow[2]
        #Data la scelta effettuata salvo il valore delle capacita utilizzate
        used_capacity = update_used_capacity(used_capacity_current_flow, s_path, used_capacity)

        #Aggiorno lo stato fisico delle capacita delle reti
        a = update_arch_capacity(used_capacity, a, s_path)

        # Salvo le scelte dell'iterazione corrente.
        zin = save_zin_assignment_(flow, zin, build_archs_list(s_path))
        zout = save_zout_assignment_(flow, zout, build_archs_list(s_path))

        g = reset_label_for_dijkstra(g)

    averaged_price = calculate_price(a)

    print 'Averaged_price: ',averaged_price

    for l in instance.L:
        print '\n', l , ': ', a[l] - 500

    best = averaged_price
    deterministic_price = averaged_price

    end_time_phase2_pure_heuristic = float(time.time() - start_time_phase2_pure_heuristic)

    end_time_heuristic_full_greedy = float(time.time() - start_time_phase1_pure_heuristic)

    print 'Tempo totale euristica greedy Deterministica: ', end_time_heuristic_full_greedy

    execute_local_search(zin, zout)


def execute_randomized_progressive_grasp_heuristic():

    global y
    global n_ite
    global best
    global end_time_phase1_randomized_heuristic
    global end_time_phase2_randomized_heuristic
    global end_time_randomized_heuristic
    global zin
    global zout

    # Parameters

    n = instance.n.value
    c = instance.c
    L = instance.L
    vn = instance.vn
    vl = instance.vl
    Vf = instance.Vf
    loc = instance.loc
    qin = instance.qin
    qout = instance.qout

    print "\n# #######################################################################################################################################"
    print 'Beginning of the randomized Part'
    print "# #######################################################################################################################################\n"

    ordered_VNs_vector = build_VNs_ordered_vector(instance)

    # In case the phase 1 gives us the same y choice is useless to procede with phase 2
    y_pure_heuristic = y

    ################# RANDOM PRE-SELECTION ################

    list_of_ranges = build_list_of_ranges(ordered_VNs_vector)

    #print list_of_ranges

    num_sub_ite = int(n_ite / len(list_of_ranges))

    best_ordered_vector = ordered_VNs_vector

    start_time_randomized_heuristic = time.time()

    best_zin = zin
    best_zout = zout

    for index in range(0, len(list_of_ranges)):

        #print "\n# #########################################################################"
        #print "# Range number: %d" % index
        #print "# #########################################################################"

        list_indices = list_of_ranges[index]

        #print 'List of used indices'
        #print list_indices

        # Randomize The part of the vector selected [9,7,6,5,{1,2,3,4},0] for exmple i shuffle the ones within {}
        selected_elements = get_selected_tuples(best_ordered_vector, list_indices)

        #print 'Selected_elements'
        #print selected_elements

        permutations_list = get_permutation_list(selected_elements, n_ite)
        # To remove the standard case with all decreasing demands
        permutations_list.remove(tuple(selected_elements))
        random.shuffle(permutations_list)

        for i in range(0, num_sub_ite):
            #print "\n# -------------------------------------------------------------------------"
            #print "# Iteration: %d" % (i + 1)
            #print "# -------------------------------------------------------------------------\n"

            start_time = time.time()

            start_time_phase1 = time.time()

            #print "# Phase 1"
            ordered_VNs_vector = list(best_ordered_vector)
            y = init_y_assign_param()
            used_capacity = init_used_capacity(instance.L)
            arch_capacity_plus_expansion = instance.a

            if len(permutations_list) == 0:
                print 'Skip to next Range'
                break

            tmp = permutations_list[0]

            permutations_list.remove(tmp)

            # overwrite the original
            ordered_VNs_vector = perform_index_permutation(ordered_VNs_vector, list_indices, tmp)

            ####################################################
            # VNoM
            ####################################################

            for j in range(0, len(ordered_VNs_vector)):
                k = ordered_VNs_vector[j][0]

                model = generateModel_VNoM_3index(n, {k}, arch_capacity_plus_expansion, used_capacity, c, L, vn, vl, Vf, instance.Vfc, loc, qin,
                                                  qout)
                instance_center = model.create_instance()
                results = opt_randomized_heuristic_phase1.solve(instance_center)

                used_capacity = calculate_used_capacity(k, used_capacity, instance_center)

                arch_capacity_plus_expansion = calculate_current_capacity(instance_center)

                y = save_y_assignment_VNk(k, y, instance_center)

                end_time_phase1_randomized_heuristic = float(time.time() - start_time_phase1)

            # If the phase 1 y assignment is equal to the phase 1 of the pure heuristic go on
            if y == y_pure_heuristic:
                print 'WARNING!:The phase 1 is the same of the pure heuristic, so I can skip to the next permutation. Going on here would be useless'
                i = (i + 1)
                continue

            #print "# Phase 2"

            ####################################################
            # VLiM
            ####################################################

            start_time_phase2 = time.time()

            # Starting substrate network situation
            used_capacity = init_used_capacity(L)
            arch_capacity_plus_expansion = dict(instance.a)
            zin = init_z()
            zout = init_z()

            ordered_VNs_flows_vector = build_ordered_VNs_flows_vector(ordered_VNs_vector)

            g = build_graph()

            for i in range(0, len(ordered_VNs_flows_vector)):
                #print 'Iteration number: ', i

                # ( k, [(1,l) | (l,1)] , [flow_demand] , logical_leaf ) #The center is alway 1 from the logical point of view!
                flow = ordered_VNs_flows_vector[i]

                from_ = get_physical_node_from(flow, y)
                to_ = get_physical_node_to(flow, y)

                '''
                    Se la domanda di flusso in esame e ad esempio 200, il costo degli archi che se seleionati avrebbero meno del 20% di capacita residua, devono avere un costo che consideri gia una possibile espansione!
                '''

                g = calculate_arch_cost_to_satisfy_capacity_constraints(g, flow, used_capacity, arch_capacity_plus_expansion)

                dijkstra(g, g.get_vertex(from_))
                target = g.get_vertex(to_)
                path = [target.get_id()]
                shortest_path = shortest(target, path)

                s_path = path[::-1]

                #print 'The shortest path : %s' % (s_path)

                used_capacity_current_flow = flow[2]
                # Data la scelta effettuata salvo il valore delle capacita utilizzate
                used_capacity = update_used_capacity(used_capacity_current_flow, s_path, used_capacity)

                # Aggiorno lo stato fisico delle capacita delle reti
                arch_capacity_plus_expansion = update_arch_capacity(used_capacity, arch_capacity_plus_expansion, s_path)

                # Salvo le scelte dell'iterazione corrente.
                zin = save_zin_assignment_(flow, zin, build_archs_list(s_path))
                zout = save_zout_assignment_(flow, zout, build_archs_list(s_path))

                g = reset_label_for_dijkstra(g)

                end_time_phase2_randomized_heuristic = float(time.time() - start_time_phase2)

            averaged_price = calculate_price(arch_capacity_plus_expansion)
            print averaged_price

            if best > averaged_price:
                best = averaged_price
                best_ordered_vector = ordered_VNs_vector
                best_zin = zin
                best_zout = zout
                print 'Objective improvement!'

            #print "\n# -------------------------------------------------------------------------"
            #print "# FINE Iteration: %d" % (i)
            #print "# -------------------------------------------------------------------------\n"



    end_time_randomized_heuristic = float(time.time() - start_time_randomized_heuristic)

    print 'Tempo totale euristica greedy Randomizzata: ', end_time_randomized_heuristic

    print 'Miglior soluzione trovata con l\'euristica randomizzata', best

    execute_local_search(best_zin, best_zout)


def build_solutions_json():

    object = {'file_name': dat_file_name, 'results': []}

    object['results'].append({'Exact optimal solution':  exact_optimum })
    object['results'].append({'Exact execution time': end_time_exact })

    object['results'].append({'Solution found with the Deterministic heuristic': deterministic_price })
    object['results'].append({'Deterministic Heutistic Phase1 Time': end_time_phase1_pure_heuristic})
    object['results'].append({'Deterministic Heutistic Phase2 Time': end_time_phase2_pure_heuristic })
    object['results'].append({'Deterministic Heutistic Total_Time = Phase1 + Phase2': end_time_heuristic_full_greedy})
    object['results'].append({'Deterministic Heutistic Gap from optimum': ((
                                                                               deterministic_price * 100) / exact_optimum) - 100 })

    object['results'].append({'Best Solution found with the Randomized Heuristic': best })
    object['results'].append({'Randomized Heuristic total execution time': end_time_randomized_heuristic })


    gap = ((best * 100) / exact_optimum) - 100

    object['results'].append({'Randomized Heutistic Gap from optimum': gap })

    json_data = json.dumps(object, indent=4, separators=(',\n', ': '))

    return json_data


def getKey(item):
    return item[1]


'''
This helper function given a model with the K Virtual Networks construct a vector with tuples (K, total_demand),
ordered for decreasing values of total_demand
'''


def build_VNs_ordered_vector(instance):
    v = []

    for k in instance.K:
        sum = 0
        for l in instance.VN[k]:
            sum = sum + instance.qin[k, l] + instance.qout[k, l]
        v.append((k, sum))

    v = sorted(v, key=getKey, reverse=True)

    return v


'''
This helper function update the amount of used capacity durning the first phase of the heuristic
'''


def calculate_used_capacity(k, used_c, inst):
    used_c_param = {}

    for l in inst.L:
        val = sum(
            inst.qin[kk, im] * inst.zin[kk, im, l].value + inst.qout[kk, im] * inst.zout[kk, im, l].value for kk in
            inst.K for im in inst.VNl[kk])

        used_c_param[l] = used_c[l] + val

    return used_c_param


'''
This helper function keeps updated the sum of the defaut capacity plus the one related to the expansion of the network
'''


def calculate_current_capacity(inst):
    a_param = {}

    for l in inst.L:
        a_param[l] = inst.a[l] + inst.b[l].value

    return a_param


'''
This helper function save the decisons related to where to put the center of a VN k established in the first phase of the heuristic.
This parameter will be passed in phase 2
'''


def save_y_assignment_VNk(k, y_param, inst):
    for l in inst.Vfc[k]:
        if inst.y[k, l].value == 1:
            y_param[k, l] = 1
    return y_param


'''
This helper function just initialize the parameter Y where to save the decision that will be taken in phase 1
'''


def init_y_assign_param():
    y = {}
    for i in instance.Y:
        y[i] = 0
    return y


'''
This helper function just initialize the parameter related the used capacity durning the first phase
'''


def init_used_capacity(L):
    used_c = {}

    for l in L:
        used_c[l] = 0

    return used_c


'''
The following 3 definitions are needed to create the set of permutations for the index to be shuffle (In the randomized part)
'''


class unique_element:
    def __init__(self, value, occurrences):
        self.value = value
        self.occurrences = occurrences


def perm_unique(elements):
    eset = set(elements)
    listunique = [unique_element(i, elements.count(i)) for i in eset]
    u = len(elements)
    return perm_unique_helper(listunique, [0] * u, u - 1)


def perm_unique_helper(listunique, result_list, d):
    if d < 0:
        yield tuple(result_list)
    else:
        for i in listunique:
            if i.occurrences > 0:
                result_list[d] = i.value
                i.occurrences -= 1
                for g in perm_unique_helper(listunique, result_list, d - 1):
                    yield g
                i.occurrences += 1


'''
This helper function retrives the tuples from the ordered vector according to the choosen indices
'''
def get_selected_tuples(vector, indices):
    array = []
    for i in indices:
        array.append(vector[i - 1])
    return array



'''
This helper function update the ordered vector with the current permutation to explore
'''


def perform_index_permutation(ordered_VNs_vector, list_indices, permutation):
    count = 0
    for i in list_indices:
        ordered_VNs_vector[i - 1] = permutation[count]
        count = count + 1

    return ordered_VNs_vector

def get_permutation_list(selected_elements, n_ite):
    copy = list(selected_elements)

    # If branch when is possible to calculate all the permutations
    if len(selected_elements) <= MAX_INDICES_FOR_FULL_PERMUTATION:
        return list(perm_unique(selected_elements))
    else:
        list_random_permutations = []
        for i in range(0, n_ite):
            random.shuffle(copy)
            list_random_permutations.append(tuple(copy))
        list_random_permutations.append(tuple(selected_elements))
        return list_random_permutations


def build_list_of_ranges(ordered_VNs_vector):
    list_of_ranges = []

    current_reference_index = 1
    current_reference = ordered_VNs_vector[0]
    # First element in
    current_index = 2

    while current_index <= len(ordered_VNs_vector):
        if ordered_VNs_vector[current_index - 1][1] * (
                1 + BASE_PERCENTAGE + PERCENTAGE_IMPROVEMENT * len(list_of_ranges)) >= current_reference[1]:
            current_index = current_index + 1
        else:
            list_of_ranges.append(range(current_reference_index, current_index))
            current_reference = ordered_VNs_vector[current_index - 1]
            current_reference_index = current_index
            current_index = current_index + 1

    list_of_ranges.append(range(current_reference_index, current_index))

    return list_of_ranges




###############################################################################
#
###############################################################################

def build_ordered_VNs_flows_vector(ordered_VNs_vector):
    #Criterio tesi laura
    if ORDINAMENTO_FLUSSI == 0:
        return build_ordered_VNs_flows_vector_MODE_0(ordered_VNs_vector)
    #criterio mio ciclo for esterno flussi shortest path
    elif ORDINAMENTO_FLUSSI == 1:
        return build_ordered_VNs_flows_vector_MODE_1()
    else:
        print 'ERRORE ALL\'INTERNO DI build_ordered_VNs_flows_vector()'
        sys.exit()

def build_ordered_VNs_flows_vector_MODE_0(ordered_VNs_vector):
    global instance

    retval = []

    for i in range(0, len(ordered_VNs_vector)):
        v = []
        k = ordered_VNs_vector[i][0]

        for l in instance.VNl[k]:
            v.append((k, FROM_LEAF_TO_CENTER, instance.qin[k, l], l))
            v.append((k, FROM_CENTER_TO_LEAF, instance.qout[k, l], l))

        v = sorted(v, key=getFlowKey, reverse=True)

        for j in range(0, len(v)):
            retval.append(v[j])

    return retval


def build_ordered_VNs_flows_vector_MODE_1():
    global instance

    v = []

    for k in instance.K:
        for l in instance.VNl[k]:
            v.append((k, FROM_LEAF_TO_CENTER, instance.qin[k, l], l))
            v.append((k, FROM_CENTER_TO_LEAF, instance.qout[k, l], l))

    v = sorted(v, key=getFlowKey, reverse=True)

    return v


def getFlowKey(item):
    return item[2]

def get_physical_node_from(flow, y):

    k = flow[0]

    if flow[1] == FROM_CENTER_TO_LEAF:
        for i in instance.Vfc[k]:
            if y[k,i] == 1:
                return i
        print 'WARNING!!!! IL centro associato alla domanda k: ', k ,'Non e stato assegnato!'
        sys.exit()
    elif flow[1] == FROM_LEAF_TO_CENTER:
         return instance.vl[k, flow[3]]

def get_physical_node_to(flow, y):

    k = flow[0]

    if flow[1] == FROM_CENTER_TO_LEAF:
        #The logical leaf
        return instance.vl[k, flow[3]]
    elif flow[1] == FROM_LEAF_TO_CENTER:
        for i in instance.Vfc[k]:
            if y[k, i] == 1:
                return i
        print 'WARNING!!!! IL centro associato alla domanda k: ', k, 'Non e stato assegnato!'
        sys.exit()

def calculate_arch_cost_to_satisfy_capacity_constraints(g, flow, used_capacity, arch_capacity):

    flow_demand = flow[2]

    for l in instance.L:
        if  arch_capacity[l] - used_capacity[l] - flow_demand < gamma * arch_capacity[l]:
            #Il costo e dato dall'inverso della capacita residua + il costo dell'espandione da pagare
            capacity_expansion = (used_capacity[l] + flow_demand)/gamma - (arch_capacity[l])
            c = (1/(arch_capacity[l] - used_capacity[l])) + capacity_expansion * instance.c[l]
        else:
            c = (1/(arch_capacity[l] - used_capacity[l]))

        g.get_vertex(l[0]).update_neighbor_weigth(g.get_vertex(l[1]), c)

    return g

def update_used_capacity(used_capacity_current_flow, s_path, used_capacity):

    archs_shortest_path = build_archs_list(s_path)

    #print archs_shortest_path

    for l in archs_shortest_path:
        used_capacity[l] = used_capacity[l] + used_capacity_current_flow

    return used_capacity

def build_archs_list(s_path):
    archs_shortest_path = []

    for i in range(1, len(s_path)):
        #print s_path[i - 1]
        #print s_path[i]
        archs_shortest_path.append((s_path[i - 1], s_path[i]))

    return archs_shortest_path



def update_arch_capacity(used_capacity, arch_capacity, s_path):

    archs_shortest_path = build_archs_list(s_path)

    for l in archs_shortest_path:
        if arch_capacity[l] - used_capacity[l] < gamma * arch_capacity[l]:
            arch_capacity[l] = used_capacity[l] / (1 - gamma)
            #print 'ATTENZIONE, INCREMENTO DI CAPACITA SULL\'ARCO: ', l
            #print arch_capacity[l] - instance.a[l]
            #print 'ATTENZIONE PER SIMMETRIA INCREMENTO CAPACITA SULL\'ARCO: ', (l[1],l[0])
            arch_capacity[(l[1],l[0])] = arch_capacity[l]
            #print arch_capacity[(l[1],l[0])] - instance.a[l]
            #print 'Costo pagato: ', (arch_capacity[l] - instance.a[l]) * instance.c[l] + (arch_capacity[(l[1],l[0])] - instance.a[(l[1],l[0])]) * instance.c[(l[1],l[0])]


    return arch_capacity

def init_z():
    zin = {}
    for k in instance.K:
        for l in instance.VNl[k]:
            for (i, j) in instance.L:
                zin[k,l,i,j] = 0
    return zin

def save_zin_assignment_(flow, zin, archs_list):

    k = flow[0]
    logical_leaf = flow[3]

    if flow[1] == FROM_LEAF_TO_CENTER:
        for arch in archs_list:
            zin[k, logical_leaf, arch[0], arch[1]] = 1

    return zin

def save_zout_assignment_(flow, zout, archs_list):

    k = flow[0]
    logical_leaf = flow[3]

    if flow[1] == FROM_CENTER_TO_LEAF:
        for arch in archs_list:
            zout[k, logical_leaf, arch[0], arch[1]] = 1

    return zout

def calculate_price(expanded_capacity):

    b = {}

    for l in instance.L:
        b[l] = expanded_capacity[l] - instance.a[l]

    model = generateModel_greedy_objective(instance.n, instance.K, instance.c, instance.L, instance.vn, instance.vl, instance.Vf, instance.Vfc, instance.qin, instance.qout, b)
    instance_greedy = model.create_instance()
    results = opt_greedy_objective.solve(instance_greedy)

    v = instance_greedy.Price()

    return v

def reset_label_for_dijkstra(g):

    for v in g.get_vertices():
        g.get_vertex(v).set_unvisited()
        g.get_vertex(v).set_distance(sys.maxint)
        g.get_vertex(v).reset_previous()

    return g

def build_set_S(z):
    s = set()

    for i in instance.Z:
        if z[i] == 1:
            s.add(i)
    return s


def build_set_NS(z):
    s = set()

    for i in instance.Z:
        if z[i] == 0:
            s.add(i)
    return s

def build_set_C(y):
    c = set()

    for i in instance.Y:
        if y[i] == 1:
            c.add(i)

    return c


def build_set_NC(y):
    nc = set()

    for i in instance.Y:
        if y[i] == 0:
            nc.add(i)

    return nc

def execute_local_search(zin, zout):
    set_S_zin = build_set_S(zin)
    set_Not_S_zin = build_set_NS(zin)

    set_S_zout = build_set_S(zout)
    set_Not_S_zout = build_set_NS(zout)

    set_C = build_set_C(y)

    set_NC = build_set_NC(y)

    start_time_local_search = time.time()

    model = generateModel_milp_3index_local_search(y, best, set_S_zin, set_Not_S_zin, set_S_zout, set_Not_S_zout,
                                                   max_arch_swap, set_C, set_NC, max_center_swap)
    instance_local_search = model.create_instance(dat_file_name)
    results = opt_greedy_local_search.solve(instance_local_search)

    end_time_local_search = time.time() - start_time_local_search

    print 'Local search with maximum: ', max_arch_swap, ' result v: ', instance_local_search.Price()
    print 'Local search Time: ', end_time_local_search