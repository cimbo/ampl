# --------------------------------------------------------Header-------------------------------------------------------- #
from pyomo.opt import SolverFactory
import time
import json
from pyomo.core.base import *
# Import dei modelli astratti che poi vengono utilizzati nel file principale #
#------- .mod Files (Di Ampl) -------#
from milp_3index_phase1 import generateModel_milp_3index_relaxed
from milp_3index_phase2 import generateModel_milp_3index_fixed_centers
from milp_3index_phase3_zin_zout import generateModel_milp_3index_fixed_centers_and_zin

# Import dei file di dati che poi vengono utilizzati nel file principale per creare i modelli concreti #
#------- .dat Files (Di Ampl) -------#
#dat_file_name = '../../../Data/Networks/IstanzeAbilene/abilene_s_t_15.dat'
#dat_file_name = '../../../Data/Networks/IstanzeFrance/france_s_t_29.dat'

global dat_file_name
global end_time_phase_1
global end_time_phase_2
global end_time_phase_3

######################################################################
# HELPER FUNCTIONS
######################################################################

def build_v_parameter(instance):
    v = {}

    for k in instance.K:
        for l in instance.Vfc[k]:
            v[k,l] = instance.y[k,l].value
    return v

# ----------------------------------------------------Main---------------------------------------------------- #
# -----Solver----- #
opt = SolverFactory("gurobi")
opt.options["mipgap"] = 0.1
#opt.options["timelimit"] = 100

def main(file):
    global dat_file_name

    dat_file_name = file

    print '\n\n'
    print file

    y =execute_phase_1()
    zin = execute_phase_2(y)
    final_instance = execute_phase_3(y, zin)

    return build_solutions_json(file, final_instance.Price(), end_time_phase_1, end_time_phase_2, end_time_phase_3)


def execute_phase_1():

    print "# -------------------------------------------------------------------------"
    print "# Phase 1: Solve Partial linear relaxation with splittable flows "
    print "# -------------------------------------------------------------------------"

    global end_time_phase_1

    start_time_phase_1 = time.time()

    abstract_model = generateModel_milp_3index_relaxed()

    instance = abstract_model.create_instance(dat_file_name)

    results = opt.solve(instance)

    end_time_phase_1 = time.time() - start_time_phase_1

    print 'V after phase 1: ', instance.Price()
    print 'Time Phase 1: ', end_time_phase_1

    return instance.y

def execute_phase_2(y):
    print "# -------------------------------------------------------------------------"
    print "# Phase 2: Solve Partial linear relaxation with NON splittable flows for zin variables "
    print "# -------------------------------------------------------------------------"

    global end_time_phase_2

    start_time_phase_2 = time.time()

    abstract_model_phase_2 = generateModel_milp_3index_fixed_centers(y)

    instance = abstract_model_phase_2.create_instance(dat_file_name)

    results = opt.solve(instance)

    end_time_phase_2 = time.time() - start_time_phase_2

    print 'V after phase 2: ', instance.Price()

    print 'Time Phase 2: ', end_time_phase_2

    return instance.zin

def execute_phase_3(y, zin):
    print "# -------------------------------------------------------------------------"
    print "# Phase 3: Solve Partial linear relaxation with NON splittable flows for zout variables"
    print "# -------------------------------------------------------------------------"

    global end_time_phase_3

    start_time_phase_3 = time.time()

    abstract_model_phase_3 = generateModel_milp_3index_fixed_centers_and_zin(y, zin)
    instance = abstract_model_phase_3.create_instance(dat_file_name)

    results = opt.solve(instance)

    end_time_phase_3 = time.time() - start_time_phase_3

    print 'V after phase 3: ', instance.Price()
    print 'Time Phase 3: ', end_time_phase_3

    return instance

print "# ----------------------------------------------------------\n"

def build_solutions_json(file, optimum, end_time_phase_1, end_time_phase_2, end_time_phase_3):
    data = {}

    data['file_name'] = file
    data['optimum'] = optimum
    data['Time Phase 1'] = end_time_phase_1
    data['Time Phase 2'] = end_time_phase_2
    data['Time Phase 3'] = end_time_phase_3


    json_data = json.dumps(data, indent=4, separators=(',\n', ': '))

    return json_data



