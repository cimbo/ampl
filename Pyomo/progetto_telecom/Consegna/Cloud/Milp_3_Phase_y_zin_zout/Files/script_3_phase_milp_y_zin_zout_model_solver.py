from milp_3_phase_y_zin_zout  import *
import json

# ----------------------------------------------------Main---------------------------------------------------- #

with open('../../Data/Json/Input/exact_data_files.json') as data_file:
    data = json.load(data_file)

output = {}

for network_name in data:

    output[network_name] = []

    for instance in data[network_name]:

        dat_file_name = instance['file_name']

        json_obj = main(dat_file_name)
        parsed = json.loads(json_obj)
        output[network_name].append({"file_name": parsed['file_name'] , "optimum": parsed['optimum'], 'Time Phase 1': parsed['Time Phase 1'],
                                     'Time Phase 2': parsed['Time Phase 2'], 'Time Phase 3': parsed['Time Phase 3']})


    # Encode JSON data
    with open('../../Data/Json/Output/Milp_3_phase_y_zin_zout_data_results.json', 'w') as f:
        json.dump(output, f, indent=4, sort_keys=True, separators=(',\n', ': '))

