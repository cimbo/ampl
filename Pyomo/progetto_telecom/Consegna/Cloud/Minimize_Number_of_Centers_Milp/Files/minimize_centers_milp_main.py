# --------------------------------------------------------Header-------------------------------------------------------- #
from pyomo.opt import SolverFactory
import json
import time
# Import dei modelli astratti che poi vengono utilizzati nel file principale #
#------- .mod Files (Di Ampl) -------#
from minimize_centers_model_splittable import generateModel_reduce_centers_phase_1
from milp_3index_phase2 import generateModel_milp_3index_fixed_centers
######################################################################
# SOLVER PREFERENCES
######################################################################

# -----Solver----- #
opt = SolverFactory("gurobi")
#opt.options["timelimit"] = 150

mipgap_phase_1 = 0.03
mipgap_phase_2 = 0.03

time_phase_1 = None
time_phase_2 = None
total_time = None

# ----------------------------------------------------Main---------------------------------------------------- #

def main(file, v_min):

    print file

    start_time = time.time()

    y = execute_minimize_centers_phase_1(file, v_min)

    final_instance = execute_phase_2(file, y)

    optimum = final_instance.Price()

    end_time = time.time() - start_time

    print 'Total_time: ', end_time

    number_of_centers = calculate_number_of_used_centers(final_instance)

    return build_solutions_json(file, optimum, time_phase_1, time_phase_2, end_time, number_of_centers)


def build_solutions_json(file, optimum, time_phase_1, time_phase_2, end_time, number_of_centers):
    data = {}

    data['file_name'] = file
    data['optimum'] = optimum
    data['time_center_minimization_after_phase_1'] = time_phase_1
    data['time_phase_2'] = time_phase_2
    data['total_time'] = end_time
    data['number_of_centers'] = number_of_centers

    json_data = json.dumps(data, indent=4, separators=(',\n', ': '))

    return json_data

def execute_minimize_centers_phase_1(dat_file_name, v_min):

    model = generateModel_reduce_centers_phase_1(v_min)
    instance = model.create_instance(dat_file_name)
    results = opt.solve(instance)

    print 'Number of centers: ', instance.Number_of_centers()

    return instance.y


def execute_phase_2(dat_file_name, y):

    start_time = time.time()

    abstract_model_phase_2 = generateModel_milp_3index_fixed_centers(y)

    instance_phase_2 = abstract_model_phase_2.create_instance(dat_file_name)

    results = opt.solve(instance_phase_2)## , tee=True)

    time_phase_2 = time.time() - start_time

    print 'V after phase 2: ', instance_phase_2.Price()
    print 'Time Phase 2: ', time_phase_2

    return instance_phase_2

def calculate_number_of_used_centers(instance):

    number_of_centers = []

    for (k,i) in instance.Y:
        if instance.y[k,i] == 1:
            number_of_centers.append(i)

    number_of_centers = len(set(number_of_centers))

    print 'number_of_centers: ', number_of_centers, '\n'
    return number_of_centers

def build_y_parameter(instance):
    v = {}

    for k in instance.K:
        for l in instance.Vfc[k]:
            v[k,l] = instance.y[k,l].value
    return v