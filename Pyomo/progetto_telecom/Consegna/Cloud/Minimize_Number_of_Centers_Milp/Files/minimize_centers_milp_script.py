from minimize_centers_milp_main  import *

# ----------------------------------------------------Main---------------------------------------------------- #

with open('../../Data/Json/Output/milp_2_phases_results.json') as data_file:
    data = json.load(data_file)

output = {}

for network_name in data:

    output[network_name] = []

    for instance in data[network_name]:

        dat_file_name = instance['file_name']
        v_min = instance['optimum']

        json_obj = main(dat_file_name, v_min)
        parsed = json.loads(json_obj)
        output[network_name].append({"file_name": parsed['file_name'] , "optimum": parsed['optimum'], 'time_center_minimization_after_phase_1': parsed['time_center_minimization_after_phase_1'],
                                     "time_phase_2": parsed['time_phase_2'], "total_time": parsed['total_time'], "number_of_centers": parsed['number_of_centers']})

    # Encode JSON data
    with open('../../Data/Json/Output/minimize_centers_milp_results.json', 'w') as f:
        json.dump(output, f, indent=4, sort_keys=True, separators=(',\n', ': '))

