from warm_start import *

# ----------------------------------------------------Main---------------------------------------------------- #

with open('../../Data/Json/Input/heuristic_data_files.json') as data_file:
    data = json.load(data_file)

output = {}

for network_name in data:

    output[network_name] = []

    for instance in data[network_name]:

        exact_optimum = instance['optimum']
        dat_file_name = instance['file_name']

        json_obj = main(dat_file_name, exact_optimum)
        parsed = json.loads(json_obj)

        output[network_name].append(parsed)


# Encode JSON data
with open('../../Data/Json/Output/warm_start_results.json', 'w') as f:
    json.dump(output, f, indent=4, sort_keys=True, separators=(',\n', ': '))

