

# --------------------------------------------------------------------------
# Parameters (classic)
# --------------------------------------------------------------------------
param k;
# Set of requests
set K := 1 .. k;
param Pk{K};

# Sets of physical nodes and arcs
set N;
param n default card(N);
set L within N cross N;

# Physical nodes and links capacities
param a{L} >= 0;#capacities of arcs
param aa{N} >= 0;#capacities of nodes
param dist{L}>=0;#distance
param c{L}>=0;#cost(=dist*1000)
param cc{N}; #activation cost


# Sets of virtual nodes, one per request
param vn{K}; #number of nodes
set VN{kk in K} default 1 .. vn[kk]; #set of nodes
set VNl{kk in K} default 2 .. vn[kk]; #set of leaves

#number of arcs of each VN(the same for all)
param D:=6;


# Virtual request traffic matrix (nominal)

param qin{kk in K, jj in VN[kk]} default 0;


param qout{kk in K, jj in VN[kk]} default 0;


#Location of leaves

param vl{kk in K, ii in VN[kk]} within N ;



#Set of nodes considered as leaves for different VNs

set Vf{kk in K} within N ;

set Vfc{kk in K}:= N diff Vf[kk];

#param location

param loc{kk in K, VNl[kk], ip in Vf[kk]} default 0;

# One if a virtual ii from request rr is mapped onto physical node jj (within V_local)
var y{kk in K, jj in Vfc[kk] } binary;

#One if at least one centre is located onto node i
var w{i in N} binary;

# Multicommodity flow for virtual request rr between virtual nodes ii and jj on physical arc (i,j)
# Note: it's defined only for strictly positive demands
var zin{kk in K, jj in VNl[kk], (i,j) in L} binary; #considero (1,jj)
var zout{kk in K, jj in VNl[kk], (i,j) in L} binary; #considero (jj,1)

#INVESTMENT

# Maximum unitary price
var v >=0;

# Price paid by VN kk to expand arc (i,j)
var p{ kk in K, (i,j) in L} >=0;

# Price paid by VN kk to activate node i as a centre
#var pc{kk in K, i in N}>=0;

# Capacity added to arc (i,j)
var b{ (i,j) in L} >=0;
# --------------------------------------------------------------------------
# Objective
# --------------------------------------------------------------------------

# Maximize the total number of accepted requests
minimize objective:
v;

# --------------------------------------------------------------------------
# Constraints
# --------------------------------------------------------------------------

# v is defined as the maximum unitary price

s.t.massimo {kk in K}:
v>= sum{(i,j) in L} p[kk,i,j]/(sum{jj in VNl[kk]} (qin[kk,jj]+qout[kk,jj]));


#---------
#EMBEDDING
#---------
# A virtual node assigned to exactly a single physical node
#(since each VN is accepted in this case)
s.t. mapping{kk in K}:
sum{ip in Vfc[kk]} y[kk,ip] = 1;


# Arc capacities constraint
s.t. arc_capacities{(i,j) in L}:
sum{kk in K} sum{im in VNl[kk]} (qin[kk,im]*zin[kk,im,i,j]+qout[kk,im]*zout[kk,im,i,j])<= 0.8*(a[i,j]+ b[i,j]);

#centres
s.t. flows1a {kk in K, ip in Vfc[kk], im in VNl[kk]}: 
sum{(ip,j) in L} zin[kk,im,ip,j] - sum{(j,ip) in L} zin[kk,im,j,ip] = y[kk,ip];
#leaves
s.t.flows2a {kk in K, ip in Vf[kk], im in VNl[kk]: loc[kk,im,ip]=1}:
sum{(ip,j) in L} zin[kk,im,ip,j] - sum{(j,ip) in L} zin[kk,im,j,ip] =- 1;

s.t.flows3a {kk in K, ip in Vf[kk], im in VNl[kk]: loc[kk,im,ip]=0}:
sum{(ip,j) in L} zin[kk,im,ip,j] - sum{(j,ip) in L} zin[kk,im,j,ip]= 0;

s.t. flows1b {kk in K, ip in Vfc[kk], im in VNl[kk]}: 
sum{(ip,j) in L} zout[kk,im,ip,j] - sum{(j,ip) in L} zout[kk,im,j,ip] = -y[kk,ip];

s.t.flows2b {kk in K, ip in Vf[kk], im in VNl[kk]: loc[kk,im,ip]=1}:
sum{(ip,j) in L} zout[kk,im,ip,j] - sum{(j,ip) in L} zout[kk,im,j,ip] = 1;

s.t.flows3b {kk in K, ip in Vf[kk], im in VNl[kk]: loc[kk,im,ip]=0}:
sum{(ip,j) in L} zout[kk,im,ip,j] - sum{(j,ip) in L} zout[kk,im,j,ip]= 0;

#----------
#INVESTMENT
#----------

# Relation between the investment and its cost
s.t. investment {(i,j) in L}:
b[i,j]=sum{kk in K} p[kk,i,j]/c[i,j];

s.t. simmetry {(i,j) in L}:
b[i,j]=b[j,i];

# Minimum price
s.t. minimumpricer {kk in K}:
0.1*sum{ jj in VNl[kk]} (qin[kk,jj]+qout[kk,jj])<=sum{(i,j) in L} p[kk,i,j];



data;

