# --------------------------------------------------------------------------
# Parameters
# --------------------------------------------------------------------------


## PHYSICAL NETWORK ##
----------------------
# Sets of physical nodes and arcs
set N;
param n default card(N);
set L within N cross N;

# Physical (nodes) and links capacities
param a{L} >= 0;#capacities of arcs
param aa{N} >= 0;#capacities of nodes
param dist{L}>=0;#distance
param c{L}>=0;#cost


## VIRTUAL NETWORKS ##
----------------------

# Set of requests
param k;
set K := 1 .. k;
param Pk{K}; #maximum price

# Sets of virtual nodes, one per request
param vn{K}; #number of nodes
set VN{kk in K} default 1 .. vn[kk]; #set of nodes

# Binary matrix: an[kk,ll,mm] = 1
#if nodes ll and mm in request kk are origin-destination pairs

param an{kk in K, VN[kk], VN[kk]} default 0, binary;

# Virtual request traffic matrix (nominal)
# is the request related to the arc (ll,mm) of VN kk
param q{kk in K, ii ll VN[kk], mm in VN[kk]: an[kk,ll,mm] == 1} default 0;



# --------------------------------------------------------------------------
# Variables
# --------------------------------------------------------------------------

#EMBEDDING#
-----------

# One if a virtual node ll from request kk is mapped onto physical node ii
var y{kk in K, ll in VN[kk], ii in N} binary;

# Multicommodity flow for virtual request kk between virtual nodes ll and mm on physical arc (ii,jj)

var z{kk in K, ll in VN[kk], mm in VN[kk], (i,j) in L: ll <> mm and an[kk,ll,mm] == 1 } binary;

#INVESTMENT#
------------

# Maximum unitary price
var v >=0;

# Price paid by VN kk to expand arc (ii,jj)
var p{ kk in K, (ii,jj) in L} >=0;

# Capacity added to arc (ii,jj)
var b{ (ii,jj) in L} >=0;

# --------------------------------------------------------------------------
# Objective
# --------------------------------------------------------------------------

# Maximize the total number of accepted requests
minimize objective:
v;

# --------------------------------------------------------------------------
# Constraints
# --------------------------------------------------------------------------

# v is defined as the maximum unitary price
s.t. massimo{ kk in K}:
v>= sum{(ii,jj) in L} p[kk,ii,jj]/(sum{ll in VN[kk], mm in VN[kk]: an[kk,ll,mm] == 1} q[kk,ll,mm]);


#EMBEDDING#
-----------

# A virtual node assigned to exactly a single physical node
#(since each VN is accepted in this case)
s.t. mapping{kk in K, ll in VN[kk]}:
sum{ii in N} y[kk,ll,ii] = 1;

# No co-location of two nodes of the same VN is allowed
s.t. nocolocation{kk in K, ii in N }:
sum{ ll in VN[kk]} y[kk, ll,ii]<=1;

# FLow balance constraints
s.t. flows{kk in K, ll in VN[kk], mm in VN[kk], ii in N: an[kk,ll,mm] == 1}:
sum{(ii,jj) in L} z[kk,ll,mm,ii,jj] - sum{(jj,ii) in L} z[kk,ll,mm,ii,jj] = y[kk,ll,ii]-y[kk,mm,ii];

#Arc capacity constraints
s.t. arc_capacities{(ii,jj) in L}:
sum{kk in K} sum{ll in VN[kk], mm in VN[kk]: an[kk,ll,mm] == 1} q[kk,ll,mm]*z[kk,ll,mm,ii,jj]<= 0.8*(a[ii,jj]+ b[ii,jj]);

#----------
#INVESTMENT
#----------

# Relation between the investment and its cost
s.t. investment {(ii,jj) in L}:
b[ii,jj]=sum{kk in K} p[kk,ii,jj]/c[ii,jj];


#Simmetria espansione
s.t. simmetry {(ii,jj) in L}:
b[ii,jj]=b[jj,ii];


# Minimum price
s.t. minimumprice {kk in K}:
0.1*sum{ll in VN[kk], mm in VN[kk]: an[kk,ll,mm] == 1} q[kk,ll,mm]<=sum{(ii,jj) in L} p[kk,ii,jj];





data;
