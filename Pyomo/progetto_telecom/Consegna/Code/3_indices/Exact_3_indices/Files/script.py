from main import *

# ----------------------------------------------------Main---------------------------------------------------- #

with open('../../Data/Json/Input/exact_data_files.json') as data_file:
    data = json.load(data_file)

output = {}

for network_name in data:

    output[network_name] = []

    for instance in data[network_name]:

        dat_file_name = instance['file_name']

        json_obj = main(dat_file_name)
        parsed = json.loads(json_obj)
        output[network_name].append({"file_name": parsed['file_name'] , "optimum": parsed['optimum'], 'number_of_centers': parsed['number_of_centers'], 'exact_time': parsed['exact_time']})

    # Encode JSON data
    with open('../../Data/Json/Input/heuristic_data_files.json', 'w') as f:
        json.dump(output, f, indent=4, sort_keys=True, separators=(',\n', ': '))

    # Encode JSON data
    with open('../../Data/Json/Output/Exact_data_results.json', 'w') as f:
        json.dump(output, f, indent=4, sort_keys=True, separators=(',\n', ': '))

