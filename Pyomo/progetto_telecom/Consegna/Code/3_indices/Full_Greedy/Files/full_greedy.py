# --------------------------------------------------------Header-------------------------------------------------------- #
from __future__ import division
import random
import time
from pyomo.opt import SolverFactory
from VNoM import generateModel_VNoM_3index
from min_price_vne_t_s_3index import generate_model_min_price_vne_t_s_3index
from calculate_v_objective import generate_model_v_objective
import json
from Dijkstra_shortest_path import *
import sys
import copy

######################################################################
# PSEUDORANDOM SETTING
######################################################################
SEED = 14041992

#This line of code allows to reproduce in any moment the same pseudo random execution of the algorithm
random.seed(SEED)

######################################################################
# SOLVER PREFERENCES
######################################################################

#<----- GREEDY OBJECTIVE ------>
opt_greedy_objective = SolverFactory("gurobi")
opt_greedy_objective.options["mipgap"] = 0.03
#opt_randomized_heuristic_phase2.options["timelimit"] = 50


######################################################################
# GLOBAL VARIABLES
######################################################################

gamma = 0.2

max_arch_swap = 300
max_center_swap = 0

#ORDINAMENTO_FLUSSI = 0 politica tesi Laura
#ORDINAMENTO_FLUSSI = 1 politica idea mia con il ciclo for esterno
ORDINAMENTO_FLUSSI = 0

min_price = 0.1

#Instance of the exact model
global exact_optimum
global instance
global end_time_exact


######################################################################
# CONSTANTS
######################################################################

#Number Of Iteration Grasp
n_ite = 0

BASE_PERCENTAGE = 0.10
PERCENTAGE_IMPROVEMENT = 0.05
FROM_LEAF_TO_CENTER = 0
FROM_CENTER_TO_LEAF = 1

'''
After a set of indices greater than MAX_INDICES_FOR_FULL_PERMUTATION the indicies vector is just shuffled. So that cold be the possibilities to try two or more time the same case.
When the size of the set of choosen indicies is less or equal to MAX_INDICES_FOR_FULL_PERMUTATION for sure will never happen the same case two times (due to the fact that i calculated all the possible permutations)
'''
MAX_INDICES_FOR_FULL_PERMUTATION = 8



# ----------------------------------------------------Main---------------------------------------------------- #




def main(file, optimum, exact_t):

    print file

    set_parameters(file, optimum, exact_t)

    generate_data_instance()

    results = solve_full_greedy_deterministic_heuristic()

    price_full_greedy = results[0]
    time_full_greedy = results[1]

    return build_solutions_json(price_full_greedy, time_full_greedy)



######################################################################
# HELPER FUNCTIONS
######################################################################
def build_graph():
    global instance

    g = Graph()

    for i in instance.N:
        g.add_vertex(i)

    for l in instance.L:
        g.add_edge(l[0], l[1], float(1 / instance.a[l]))

    return g


def set_parameters(file, optimum, exact_t):
    global dat_file_name
    global exact_optimum
    global end_time_exact

    dat_file_name = file
    exact_optimum = optimum
    end_time_exact = exact_t

def generate_data_instance():

    global instance
    global dat_file_name

    abstract_model = generate_model_min_price_vne_t_s_3index()

    instance = abstract_model.create_instance(dat_file_name)



def solve_full_greedy_deterministic_heuristic():

    #print "# #######################################################################################################################################"
    #print "# Pure Heuristic No Random: The idea is to fix the center in a greedy way and then, using the phase 2 of the milp, find a solution"
    #print "# #######################################################################################################################################\n"

    start_full_greedy_heuristic_time = time.time()

    ordered_VNs_vector = build_VNs_ordered_vector(instance)

    ####################################################
    # VNoM & VLim Full Greedy
    ####################################################

    used_capacity = init_used_capacity(instance.L)
    arch_capacity_plus_expansion = dict(instance.a)
    zin = init_z()
    zout = init_z()
    y = init_y_assign_param()

    for j in range(0, len(ordered_VNs_vector)):
        k = ordered_VNs_vector[j][0]

        print '###############################################################'
        print '###########################','VN',k ,'##############################'
        print '###############################################################\n'

        ordered_flows_vector_demand_k = build_ordered_flows_vector_demand_k(k)

        best_star_cost = sys.maxint
        best_y = None
        best_zin = None
        best_zout = None
        best_arch_capacity_plus_expansion = None
        best_used_capacity = None

        for possible_center in instance.Vfc[k]:

            #print '\n\n###############################################################'
            print '##########################', 'CENTER', possible_center, '###########################'
            #print '###############################################################'

            g_tmp = build_graph()
            zin_tmp = {}
            zout_tmp = {}
            arch_capacity_plus_expansion_tmp = copy.deepcopy(arch_capacity_plus_expansion)
            used_capacity_tmp = copy.deepcopy(used_capacity)

            for flow in ordered_flows_vector_demand_k:

                from_ = get_physical_node_from(flow, possible_center)
                to_ = get_physical_node_to(flow, possible_center)

                g_tmp = calculate_arch_cost_to_satisfy_capacity_constraints(g_tmp, flow, used_capacity_tmp, arch_capacity_plus_expansion_tmp)

                dijkstra(g_tmp, g_tmp.get_vertex(from_))
                target = g_tmp.get_vertex(to_)
                path = [target.get_id()]
                shortest_path = shortest(target, path)

                s_path = path[::-1]

                # print 'The shortest path : %s' % (s_path)

                used_capacity_current_flow = flow[2]
                # Data la scelta effettuata salvo il valore delle capacita utilizzate
                used_capacity_tmp = update_used_capacity(used_capacity_current_flow, s_path, used_capacity_tmp)

                # Aggiorno lo stato fisico delle capacita delle reti
                arch_capacity_plus_expansion_tmp = update_arch_capacity(used_capacity_tmp, arch_capacity_plus_expansion_tmp, s_path)

                # Salvo le scelte dell'iterazione corrente.
                zin_tmp = save_zin_assignment_(flow, zin_tmp, build_archs_list(s_path))
                zout_tmp = save_zout_assignment_(flow, zout_tmp, build_archs_list(s_path))

                g_tmp = reset_label_for_dijkstra(g_tmp)

            value_current_possible_center = calculate_start_cost_current_center(g_tmp, zin_tmp, zout_tmp)
            print 'Value of the current case: ', value_current_possible_center

            if value_current_possible_center < best_star_cost:
                best_star_cost = copy.deepcopy(value_current_possible_center)
                best_y = copy.deepcopy(possible_center)
                best_zin = copy.deepcopy(zin_tmp)
                best_zout = copy.deepcopy(zout_tmp)
                best_arch_capacity_plus_expansion = copy.deepcopy(arch_capacity_plus_expansion_tmp)
                best_used_capacity = copy.deepcopy(used_capacity_tmp)


        ############### Qui ho valutato tutti i possibili centri per k #################
        #print best_arch_capacity_plus_expansion

        arch_capacity_plus_expansion = copy.deepcopy(best_arch_capacity_plus_expansion)
        used_capacity = copy.deepcopy(best_used_capacity)
        y[k, best_y] = 1
        zin = merge_zin_with_best_k_choice(zin, best_zin)
        zout = merge_zout_with_best_k_choice(zout, best_zout)

        ############### Salvo decisioni al passo k e passo al passo k + 1 ###############

        print 'best_star_cost: ', best_star_cost

     #################### Qui una soluzione e stata costruita ###############################

    v = calculate_price(arch_capacity_plus_expansion)

    deterministic_price = v

    end_time_heuristic_full_greedy = float(time.time() - start_full_greedy_heuristic_time)

    print 'V: ', v
    print 'Execution_time: ', end_time_heuristic_full_greedy

    return (deterministic_price, end_time_heuristic_full_greedy)



def build_solutions_json(price_full_greedy, time_full_greedy):

    object = {'file_name': dat_file_name, 'results': []}

    object['results'].append({'Solution found with the Deterministic heuristic Full Greedy': price_full_greedy })
    object['results'].append({'Deterministic Heutistic Full Greedy Total_Time': time_full_greedy})
    object['results'].append({'Deterministic Heutistic Full Greedy Gap from optimum': ((
                                                                               price_full_greedy * 100) / exact_optimum) - 100 })

    json_data = json.dumps(object, indent=4, separators=(',\n', ': '))

    return json_data


def getKey(item):
    return item[1]


'''
This helper function given a model with the K Virtual Networks construct a vector with tuples (K, total_demand),
ordered for decreasing values of total_demand
'''


def build_VNs_ordered_vector(instance):
    v = []

    for k in instance.K:
        sum = 0
        for l in instance.VN[k]:
            sum = sum + instance.qin[k, l] + instance.qout[k, l]
        v.append((k, sum))

    v = sorted(v, key=getKey, reverse=True)

    return v


'''
This helper function update the amount of used capacity durning the first phase of the heuristic
'''


def calculate_used_capacity(k, used_c, inst):
    used_c_param = {}

    for l in inst.L:
        val = sum(
            inst.qin[kk, im] * inst.zin[kk, im, l].value + inst.qout[kk, im] * inst.zout[kk, im, l].value for kk in
            inst.K for im in inst.VNl[kk])

        used_c_param[l] = used_c[l] + val

    return used_c_param


'''
This helper function keeps updated the sum of the defaut capacity plus the one related to the expansion of the network
'''


def calculate_current_capacity(inst):
    a_param = {}

    for l in inst.L:
        a_param[l] = inst.a[l] + inst.b[l].value

    return a_param


'''
This helper function save the decisons related to where to put the center of a VN k established in the first phase of the heuristic.
This parameter will be passed in phase 2
'''


def save_y_assignment_VNk(k, y_param, inst):
    for l in inst.Vfc[k]:
        if inst.y[k, l].value == 1:
            y_param[k, l] = 1
    return y_param


'''
This helper function just initialize the parameter Y where to save the decision that will be taken in phase 1
'''


def init_y_assign_param():
    y = {}
    for i in instance.Y:
        y[i] = 0
    return y


'''
This helper function just initialize the parameter related the used capacity durning the first phase
'''


def init_used_capacity(L):
    used_c = {}

    for l in L:
        used_c[l] = 0

    return used_c


'''
The following 3 definitions are needed to create the set of permutations for the index to be shuffle (In the randomized part)
'''


class unique_element:
    def __init__(self, value, occurrences):
        self.value = value
        self.occurrences = occurrences


def perm_unique(elements):
    eset = set(elements)
    listunique = [unique_element(i, elements.count(i)) for i in eset]
    u = len(elements)
    return perm_unique_helper(listunique, [0] * u, u - 1)


def perm_unique_helper(listunique, result_list, d):
    if d < 0:
        yield tuple(result_list)
    else:
        for i in listunique:
            if i.occurrences > 0:
                result_list[d] = i.value
                i.occurrences -= 1
                for g in perm_unique_helper(listunique, result_list, d - 1):
                    yield g
                i.occurrences += 1


'''
This helper function retrives the tuples from the ordered vector according to the choosen indices
'''
def get_selected_tuples(vector, indices):
    array = []
    for i in indices:
        array.append(vector[i - 1])
    return array



'''
This helper function update the ordered vector with the current permutation to explore
'''


def perform_index_permutation(ordered_VNs_vector, list_indices, permutation):
    count = 0
    for i in list_indices:
        ordered_VNs_vector[i - 1] = permutation[count]
        count = count + 1

    return ordered_VNs_vector

def get_permutation_list(selected_elements, n_ite):
    copy = list(selected_elements)

    # If branch when is possible to calculate all the permutations
    if len(selected_elements) <= MAX_INDICES_FOR_FULL_PERMUTATION:
        return list(perm_unique(selected_elements))
    else:
        list_random_permutations = []
        for i in range(0, n_ite):
            random.shuffle(copy)
            list_random_permutations.append(tuple(copy))
        list_random_permutations.append(tuple(selected_elements))
        return list_random_permutations


def build_list_of_ranges(ordered_VNs_vector):
    list_of_ranges = []

    current_reference_index = 1
    current_reference = ordered_VNs_vector[0]
    # First element in
    current_index = 2

    while current_index <= len(ordered_VNs_vector):
        if ordered_VNs_vector[current_index - 1][1] * (
                1 + BASE_PERCENTAGE + PERCENTAGE_IMPROVEMENT * len(list_of_ranges)) >= current_reference[1]:
            current_index = current_index + 1
        else:
            list_of_ranges.append(range(current_reference_index, current_index))
            current_reference = ordered_VNs_vector[current_index - 1]
            current_reference_index = current_index
            current_index = current_index + 1

    list_of_ranges.append(range(current_reference_index, current_index))

    return list_of_ranges




###############################################################################
#
###############################################################################

def build_ordered_VNs_flows_vector(ordered_VNs_vector):
    #Criterio tesi laura
    if ORDINAMENTO_FLUSSI == 0:
        return build_ordered_VNs_flows_vector_MODE_0(ordered_VNs_vector)
    #criterio mio ciclo for esterno flussi shortest path
    elif ORDINAMENTO_FLUSSI == 1:
        return build_ordered_VNs_flows_vector_MODE_1()
    else:
        print 'ERRORE ALL\'INTERNO DI build_ordered_VNs_flows_vector()'
        sys.exit()

def build_ordered_VNs_flows_vector_MODE_0(ordered_VNs_vector):
    global instance

    retval = []

    for i in range(0, len(ordered_VNs_vector)):
        v = []
        k = ordered_VNs_vector[i][0]

        for l in instance.VNl[k]:
            v.append((k, FROM_LEAF_TO_CENTER, instance.qin[k, l], l))
            v.append((k, FROM_CENTER_TO_LEAF, instance.qout[k, l], l))

        v = sorted(v, key=getFlowKey, reverse=True)

        for j in range(0, len(v)):
            retval.append(v[j])

    return retval


def build_ordered_VNs_flows_vector_MODE_1():
    global instance

    v = []

    for k in instance.K:
        for l in instance.VNl[k]:
            v.append((k, FROM_LEAF_TO_CENTER, instance.qin[k, l], l))
            v.append((k, FROM_CENTER_TO_LEAF, instance.qout[k, l], l))

    v = sorted(v, key=getFlowKey, reverse=True)

    return v


def getFlowKey(item):
    return item[2]



def update_used_capacity(used_capacity_current_flow, s_path, used_capacity):

    archs_shortest_path = build_archs_list(s_path)

    #print archs_shortest_path

    for l in archs_shortest_path:
        used_capacity[l] = used_capacity[l] + used_capacity_current_flow

    return used_capacity

def build_archs_list(s_path):
    archs_shortest_path = []

    for i in range(1, len(s_path)):
        #print s_path[i - 1]
        #print s_path[i]
        archs_shortest_path.append((s_path[i - 1], s_path[i]))

    return archs_shortest_path



def update_arch_capacity(used_capacity, arch_capacity, s_path):

    archs_shortest_path = build_archs_list(s_path)

    for l in archs_shortest_path:
        if arch_capacity[l] - used_capacity[l] < gamma * arch_capacity[l]:
            arch_capacity[l] = used_capacity[l] / (1 - gamma)
            #print 'ATTENZIONE, INCREMENTO DI CAPACITA SULL\'ARCO: ', l
            #print arch_capacity[l] - instance.a[l]
            #print 'ATTENZIONE PER SIMMETRIA INCREMENTO CAPACITA SULL\'ARCO: ', (l[1],l[0])
            arch_capacity[(l[1],l[0])] = arch_capacity[l]
            #print arch_capacity[(l[1],l[0])] - instance.a[l]
            #print 'Costo pagato: ', (arch_capacity[l] - instance.a[l]) * instance.c[l] + (arch_capacity[(l[1],l[0])] - instance.a[(l[1],l[0])]) * instance.c[(l[1],l[0])]


    return arch_capacity

def init_z():
    zin = {}
    for k in instance.K:
        for l in instance.VNl[k]:
            for (i, j) in instance.L:
                zin[k,l,i,j] = 0
    return zin

def save_zin_assignment_(flow, zin, archs_list):

    k = flow[0]
    logical_leaf = flow[3]

    if flow[1] == FROM_LEAF_TO_CENTER:
        for arch in archs_list:
            zin[k, logical_leaf, arch[0], arch[1]] = 1

    return zin

def save_zout_assignment_(flow, zout, archs_list):

    k = flow[0]
    logical_leaf = flow[3]

    if flow[1] == FROM_CENTER_TO_LEAF:
        for arch in archs_list:
            zout[k, logical_leaf, arch[0], arch[1]] = 1

    return zout

def calculate_price(expanded_capacity):

    b = {}

    for l in instance.L:
        b[l] = expanded_capacity[l] - instance.a[l]

    model = generate_model_v_objective(instance.n, instance.K, instance.c, instance.L, instance.vn, instance.vl, instance.Vf, instance.Vfc, instance.qin, instance.qout, b)
    instance_greedy = model.create_instance()
    results = opt_greedy_objective.solve(instance_greedy)

    v = instance_greedy.Price()

    return v

def reset_label_for_dijkstra(g):

    for v in g.get_vertices():
        g.get_vertex(v).set_unvisited()
        g.get_vertex(v).set_distance(sys.maxint)
        g.get_vertex(v).reset_previous()

    return g



#########################################################
# FULL GREEDY
#########################################################

def build_ordered_flows_vector_demand_k(k):

    v = []

    for l in instance.VNl[k]:
        v.append((k, FROM_LEAF_TO_CENTER, instance.qin[k, l], l))
        v.append((k, FROM_CENTER_TO_LEAF, instance.qout[k, l], l))

    v = sorted(v, reverse=True)

    return v

def get_physical_node_from(flow, y):

    k = flow[0]

    if flow[1] == FROM_CENTER_TO_LEAF:
                return y
                print 'WARNING!!!! IL centro associato alla domanda k: ', k ,'Non e stato assegnato!'
                sys.exit()
    elif flow[1] == FROM_LEAF_TO_CENTER:
         return instance.vl[k, flow[3]]

def get_physical_node_to(flow, y):

    k = flow[0]

    if flow[1] == FROM_CENTER_TO_LEAF:
        #The logical leaf
        return instance.vl[k, flow[3]]
    elif flow[1] == FROM_LEAF_TO_CENTER:
                return y
                print 'WARNING!!!! IL centro associato alla domanda k: ', k, 'Non e stato assegnato!'
                sys.exit()

def calculate_arch_cost_to_satisfy_capacity_constraints(g, flow, used_capacity, arch_capacity):

    flow_demand = flow[2]

    for l in instance.L:
        if  arch_capacity[l] - used_capacity[l] - flow_demand < gamma * arch_capacity[l]:
            #Il costo e dato dall'inverso della capacita residua + il costo dell'espandione da pagare
            capacity_expansion = (used_capacity[l] + flow_demand)/gamma - (arch_capacity[l])
            c = (1/(arch_capacity[l] - used_capacity[l])) + capacity_expansion * instance.c[l]
        else:
            c = (1/(arch_capacity[l] - used_capacity[l]))

        g.get_vertex(l[0]).update_neighbor_weigth(g.get_vertex(l[1]), c)

    return g

def calculate_start_cost_current_center(g_tmp, zin_tmp, zout_tmp):
    cost = 0

    for z in zin_tmp:
        i = g_tmp.get_vertex(z[2])
        j = g_tmp.get_vertex(z[3])

        cost = cost + i.get_weight(j)

    for z in zout_tmp:
        i = g_tmp.get_vertex(z[2])
        j = g_tmp.get_vertex(z[3])

        cost = cost + i.get_weight(j)

    return cost

def merge_zin_with_best_k_choice(zin, best_zin):

    for z in best_zin:
        zin[z] = best_zin[z]

    return zin

def merge_zout_with_best_k_choice(zout, best_zout):
    for z in best_zout:
        zout[z] = best_zout[z]

    return zout



