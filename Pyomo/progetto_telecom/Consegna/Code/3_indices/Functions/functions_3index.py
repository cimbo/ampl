import time
from flows_local_search import generate_model_local_search_flows

def calculate_number_of_used_centers_3index(instance):
    """
        Args:
            instance (instance): The instance of which we want to calculate the number of used center.

        Returns:
            int: The return value. The number of used centers.
    """

    number_of_centers = []

    for (k,i) in instance.Y:
        if instance.y[k,i] == 1:
            number_of_centers.append(i)

    number_of_centers = len(set(number_of_centers))

    return number_of_centers

########################################################################################################################
# LOCAL SEARCH
########################################################################################################################

def execute_local_search(instance, best, y, zin, zout, max_arch_swap, file, solver):
    """
        Args:
            best (float): Best objective value found before the local search
            y (dict): The Mapping of the VNs's centers
            zin (dict): The mapping of the zin valriable before the local search
            zout (dict): The mapping of the zout valriable before the local search

        Returns:
            (price, zin, zout)
    """

    set_S_zin = build_set_S(instance, zin)
    set_Not_S_zin = build_set_NS(instance, zin)

    set_S_zout = build_set_S(instance, zout)
    set_Not_S_zout = build_set_NS(instance, zout)

    start_time_local_search = time.time()

    model = generate_model_local_search_flows(y, best, set_S_zin, set_Not_S_zin, set_S_zout, set_Not_S_zout,
                                                   max_arch_swap)
    instance_local_search = model.create_instance(file)
    results = solver.solve(instance_local_search)

    end_time_local_search = time.time() - start_time_local_search

    print 'Local search with maximum: ', max_arch_swap, ' result v: ', instance_local_search.Price()
    print 'Local search Time: ', end_time_local_search

    return (instance_local_search.Price(), instance_local_search.zin, instance_local_search.zout)

def build_set_S(instance, z):
    s = set()

    for i in instance.Z:
        if z[i] == 1:
            s.add(i)
    return s


def build_set_NS(instance, z):
    s = set()

    for i in instance.Z:
        if z[i] == 0:
            s.add(i)
    return s

def build_set_C(instance, y):
    c = set()

    for i in instance.Y:
        if y[i] == 1:
            c.add(i)

    return c


def build_set_NC(instance, y):
    nc = set()

    for i in instance.Y:
        if y[i] == 0:
            nc.add(i)

    return nc


########################################################################################################################
# LOCAL BRANCHING
########################################################################################################################
from pyomo.environ import *
from pyomo.opt import SolverStatus, TerminationCondition
from milp_3index_phase1 import generateModel_milp_3index_relaxed

OPTIMAL_SOLUTION = 1
INFEASIBLE = 2
FEASIBLE_SOLUTION = 3
NO_FEASIBLE_SOLUTION = 4



def calculate_case(instance, results):

    if results.solver.termination_condition == TerminationCondition.infeasible:
        return INFEASIBLE

    if (results.solver.status == SolverStatus.ok) and (
                results.solver.termination_condition == TerminationCondition.optimal):
        best_lb = float(results.problem._list[0]['Lower bound'])
        incumbent = float(results.problem._list[0]['Upper bound'])
        optimal = incumbent == best_lb
        instance.solutions.load_from(results)

        if optimal:
            return OPTIMAL_SOLUTION
        else:
            return FEASIBLE_SOLUTION

    if (results.solver.status == SolverStatus.aborted and
                    results.solver.termination_condition == TerminationCondition.maxTimeLimit):

        try:
            instance.solutions.load_from(results)
            return FEASIBLE_SOLUTION
        except ValueError:
            return NO_FEASIBLE_SOLUTION



#The methods work if a starting solution is passed into x_star
def LocBra_phase_1_starting_from_a_solution(dat_file_name, instance, k_swap, total_time_limit, node_time_limit, dv_max, x_star, ub, zin_star, zout_star,local_branching_mipgap_phase_1_first, local_branching_mipgap_phase_1):
    infinity = float('infinity')
    rhs = k_swap
    TL = node_time_limit
    bestUB = UB = ub
    opt = True
    #first = True
    first = False #if we want to start with a starting solution
    diversify = False
    dv = 0
    num_x_bar = 0

    x_bar = x_star
    num_x_bar = num_x_bar + 1

    opt_local_branching = SolverFactory("gurobi")

    start_time = time.time()
    elapsed_time = time.time() - start_time

    print 'Beginning of the local branching procedure\n'

    constraints_stack_list = []

    model = generateModel_milp_3index_relaxed()
    instance_local_branching = model.create_instance(dat_file_name)

    num_constraints = 0


    while elapsed_time <= total_time_limit and dv <= dv_max:

        if rhs < infinity:
            instance_local_branching = add_constraint_phase_1(instance_local_branching, num_constraints, ('Left', x_bar, rhs))
            num_constraints = num_constraints + 1
            constraints_stack_list.append("delta(x, x_bar_" + str(num_constraints) + ") <= rhs")
            # Add the local br. constraint into delta(x, x_bar) <= rhs


        TL = min(TL, total_time_limit - elapsed_time) # Da decommentare nel caso in cui non si parta con una feasible solution

        opt_local_branching = set_solver_time_limit_phase_1(opt_local_branching, first, TL, local_branching_mipgap_phase_1_first, local_branching_mipgap_phase_1)

        print 'Lodi_solution: ', constraints_stack_list

        instance_local_branching = add_ub_constraint(instance_local_branching, UB)

        results = opt_local_branching.solve(instance_local_branching, load_solutions=False, tee=True)

        TL = node_time_limit

        case = calculate_case(instance_local_branching, results)

        #1)
        if case == OPTIMAL_SOLUTION:
            print 'Optimal Solution found!: ', instance_local_branching.Price()

            if instance_local_branching.Price() < bestUB:
                bestUB = instance_local_branching.Price()
                x_star = instance_local_branching.y
                zin_star = instance_local_branching.zin
                zout_star = instance_local_branching.zout


            if rhs >= infinity:
                return {'y': x_star, 'zin': zin_star, 'zout': zout_star}

            # Reverse the last local br. constraint into delta(x, x_bar) >= rhs + 1
            instance_local_branching = revert_last_constraint_phase_1(instance_local_branching, num_constraints, ('Right', x_bar, rhs + 1))
            constraints_stack_list.pop()
            constraints_stack_list.append("delta(x, x_bar_" + str(num_constraints) + ") >= rhs + 1")

            diversify = first = False
            x_bar = x_star
            num_x_bar = num_x_bar + 1
            UB = instance_local_branching.Price()
            rhs = k_swap

        #2)
        elif case == INFEASIBLE:
            instance.solutions.load_from(results)
            print 'Proved to be infeasible!'

            if rhs >= infinity:
                return opt

            instance_local_branching = revert_last_constraint_phase_1(instance_local_branching, num_constraints, ('Right', x_bar, rhs + 1))
            # Reverse the last local br. constraint into delta(x, x_bar) >= rhs + 1
            constraints_stack_list.pop()
            constraints_stack_list.append("delta(x, x_bar_" + str(num_constraints) + ") >= rhs + 1")

            if diversify:
                print 'Diversify'
                UB = TL = infinity
                dv = dv + 1
                first = True

            rhs = rhs + k_swap/2
            diversify = True


        elif case == FEASIBLE_SOLUTION:
            #3 ) Quando il mipgap e diverso da None vuol dire che cerchiamo una feasible solution
            print 'Solution found but not sure that is optimal: ', instance_local_branching.Price()
            if rhs < infinity:
                if first:
                    # Delete the last local br. constraint into delta(x, x_bar) <= rhs
                    constraints_stack_list.pop()

                    instance_local_branching = delete_last_constraint_phase_1(instance_local_branching, num_constraints)
                    num_constraints = num_constraints - 1
                else:
                    instance_local_branching = revert_last_constraint_phase_1(instance_local_branching, num_constraints,
                                                                                  ('Right', x_bar, 1))
                    # Reverse the last local br. constraint into delta(x, x_bar) >= 1
                    constraints_stack_list.pop()
                    constraints_stack_list.append("delta(x, x_bar_" + str(num_constraints) + ") >= 1")

            refine(instance_local_branching.y)

            if instance_local_branching.Price() < bestUB:
                bestUB = instance_local_branching.Price()
                x_star = instance_local_branching.y
                zin_star = instance_local_branching.zin
                zout_star = instance_local_branching.zout

            first = diversify = False
            x_bar = instance_local_branching.y
            num_x_bar = num_x_bar + 1
            UB = instance_local_branching.Price()
            rhs = k_swap

        elif case == NO_FEASIBLE_SOLUTION:

            #4 )
            print 'No_Feasible_solution_found!'
            if diversify:
                print 'Diversify'

                instance_local_branching = revert_last_constraint_phase_1(instance_local_branching, num_constraints,
                                                                              ('Right', x_bar, 1))
                # Replace the last local br. constraint delta(x, x_bar) <= rhs with delta(x, x_bar) >= 1
                constraints_stack_list.pop()
                constraints_stack_list.append("delta(x, x_bar_" + str(num_constraints) + ") >= 1")

                UB = TL = infinity
                dv = dv + 1
                rhs = rhs + k_swap / 2
                first = True
            else:
                # Delete the last local br. constraint into delta(x, x_bar) <= rhs
                constraints_stack_list.pop()
                instance_local_branching = delete_last_constraint_phase_1(instance_local_branching, num_constraints)
                num_constraints = num_constraints - 1
                rhs = rhs - k_swap / 2
                print 'rhs = rhs - k_swap / 2'

            diversify = True
        else:
            print 'Nessuno dei casi e stato selezionato'


        elapsed_time = time.time() - start_time
        print 'elapsed_time: ', elapsed_time, '\n'

    ###################### END LOOP ######################

    best_price = bestUB

    #print_stats_z(instance_local_branching)

    TL = max(total_time_limit - elapsed_time, 0)
    print 'Out of the loop. Time remaning: ', TL

    first = False

    instance_local_branching = add_ub_constraint(instance_local_branching, bestUB)
    opt_local_branching.options["mipgap"] = 0.03
    opt_local_branching = set_solver_time_limit_phase_1(opt_local_branching, first, TL, local_branching_mipgap_phase_1_first, local_branching_mipgap_phase_1)

    results = opt_local_branching.solve(instance_local_branching, load_solutions=False)

    try:
        instance.solutions.load_from(results)
        # Every Feasible solution would improve the previous solution due to the UB value
        if (results.solver.status == SolverStatus.ok) and (
                    results.solver.termination_condition == TerminationCondition.optimal):
            best_price = instance_local_branching.Price()
            x_star = instance_local_branching.y
            zin_star = instance_local_branching.zin
            zout_star = instance_local_branching.zout
        opt = True
    except ValueError:
        opt = False

    print 'Can you certify the optimaility of the found solution?: ', opt
    print 'V: ', best_price

    return {'y':x_star, 'zin':zin_star ,'zout':zout_star }


##############################################################################################


def LocBra_phase_1(dat_file_name, instance, k_swap, total_time_limit_, node_time_limit, dv_max, x_star, zin_star, zout_star,local_branching_mipgap_phase_1_first, local_branching_mipgap_phase_1):
    infinity = float('infinity')
    rhs = bestUB = UB = TL = total_time_limit = infinity
    opt = first = True
    diversify = False
    dv = 0
    x_star = x_bar = zin_star = zout_star =None
    num_x_bar = 0

    opt_local_branching = SolverFactory("gurobi")

    start_time = time.time()
    elapsed_time = time.time() - start_time

    print 'Beginning of the local branching procedure\n'

    constraints_stack_list = []

    model = generateModel_milp_3index_relaxed()
    instance_local_branching = model.create_instance(dat_file_name)

    num_constraints = 0

    while elapsed_time <= total_time_limit and dv <= dv_max:

        if rhs < infinity:
            constraints_stack_list.append("delta(x, x_bar_" + str(num_constraints) + ") <= rhs")
            instance_local_branching = add_constraint_phase_1(instance_local_branching, num_constraints, ('Left', x_bar, rhs))
            num_constraints = num_constraints + 1
            # Add the local br. constraint into delta(x, x_bar) <= rhs


        TL = min(TL, total_time_limit - elapsed_time) # Da decommentare nel caso in cui non si parta con una feasible solution
        TL = max(TL, node_time_limit) # Se inizi un nodo almeno dedicagli del tempo plausibile (teoricamente non era presente)

        opt_local_branching = set_solver_time_limit_phase_1(opt_local_branching, first, TL, local_branching_mipgap_phase_1_first, local_branching_mipgap_phase_1)

        print 'Lodi_solution: ', constraints_stack_list

        instance_local_branching = add_ub_constraint(instance_local_branching, UB)

        results = opt_local_branching.solve(instance_local_branching, load_solutions=False, tee=True)

        TL = node_time_limit

        case = calculate_case(instance_local_branching, results)

        #1)
        if case == OPTIMAL_SOLUTION:
            print 'Optimal Solution found!: ', instance_local_branching.Price()

            if instance_local_branching.Price() < bestUB:
                bestUB = instance_local_branching.Price()
                x_star = instance_local_branching.y
                zin_star = instance_local_branching.zin
                zout_star = instance_local_branching.zout


            if rhs >= infinity:
                time_phase_1 = time.time() - start_time
                best_price = instance_local_branching.Price()
                return {'v':best_price, 'y':x_star, 'zin':zin_star ,'zout':zout_star, 'time': time_phase_1 }

            # Reverse the last local br. constraint into delta(x, x_bar) >= rhs + 1
            constraints_stack_list.pop()
            constraints_stack_list.append("delta(x, x_bar_" + str(num_constraints) + ") >= rhs + 1")

            instance_local_branching = revert_last_constraint_phase_1(instance_local_branching, num_constraints, ('Right', x_bar, rhs + 1))

            diversify = first = False
            x_bar = x_star
            num_x_bar = num_x_bar + 1
            UB = instance_local_branching.Price()
            rhs = k_swap

        #2)
        elif case == INFEASIBLE:
            print 'Proved to be infeasible!'

            if rhs >= infinity:
                return opt

            # Reverse the last local br. constraint into delta(x, x_bar) >= rhs + 1
            constraints_stack_list.pop()
            constraints_stack_list.append("delta(x, x_bar_" + str(num_constraints) + ") >= rhs + 1")

            instance_local_branching = revert_last_constraint_phase_1(instance_local_branching, num_constraints, ('Right', x_bar, rhs + 1))

            if diversify:
                print 'Diversify'
                UB = TL = infinity
                dv = dv + 1
                first = True

            rhs = rhs + k_swap/2
            diversify = True

        elif case == FEASIBLE_SOLUTION:
                print 'Solution found but not sure that is optimal: ', instance_local_branching.Price()

                if rhs < infinity:
                    if first:
                        # Delete the last local br. constraint into delta(x, x_bar) <= rhs
                        constraints_stack_list.pop()

                        instance_local_branching = delete_last_constraint_phase_1(instance_local_branching, num_constraints)
                        num_constraints = num_constraints - 1
                    else:
                        # Reverse the last local br. constraint into delta(x, x_bar) >= 1
                        constraints_stack_list.pop()
                        constraints_stack_list.append("delta(x, x_bar_" + str(num_constraints) + ") >= 1")

                        instance_local_branching = revert_last_constraint_phase_1(instance_local_branching, num_constraints,
                                                                                  ('Right', x_bar, 1))

                refine(instance_local_branching.y)

                if instance_local_branching.Price() < bestUB:
                    bestUB = instance_local_branching.Price()
                    x_star = instance_local_branching.y
                    zin_star = instance_local_branching.zin
                    zout_star = instance_local_branching.zout

                first = diversify = False
                x_bar = instance_local_branching.y
                num_x_bar = num_x_bar + 1
                UB = instance_local_branching.Price()
                rhs = k_swap

        elif case == NO_FEASIBLE_SOLUTION:
                #4 )
                print 'No_Feasible_solution_found!'
                if diversify:
                    print 'Diversify'
                    # Replace the last local br. constraint delta(x, x_bar) <= rhs with delta(x, x_bar) >= 1
                    constraints_stack_list.pop()
                    constraints_stack_list.append("delta(x, x_bar_" + str(num_constraints) + ") >= 1")

                    instance_local_branching = revert_last_constraint_phase_1(instance_local_branching, num_constraints,
                                                                              ('Right', x_bar, 1))

                    UB = TL = infinity
                    dv = dv + 1
                    rhs = rhs + k_swap / 2
                    first = True
                else:
                    # Delete the last local br. constraint into delta(x, x_bar) <= rhs
                    #constraints_stack_list.pop()

                    instance_local_branching = delete_last_constraint_phase_1(instance_local_branching, num_constraints)
                    num_constraints = num_constraints - 1
                    rhs = rhs - k_swap / 2
                    print 'rhs = rhs - k_swap / 2'


                diversify = True

        total_time_limit = total_time_limit_


        elapsed_time = time.time() - start_time
        print 'elapsed_time: ', elapsed_time, '\n'

    ###################### END LOOP ######################

    best_price = bestUB

    #print_stats_z(instance_local_branching)

    TL = max(total_time_limit - elapsed_time, 0)
    print 'Out of the loop. Time remaning: ', TL

    first = False

    instance_local_branching = add_ub_constraint(instance_local_branching, bestUB)
    opt_local_branching.options["mipgap"] = 0.03
    opt_local_branching = set_solver_time_limit_phase_1(opt_local_branching, first, TL, local_branching_mipgap_phase_1_first, local_branching_mipgap_phase_1)

    results = opt_local_branching.solve(instance_local_branching, load_solutions=False)

    case = calculate_case(instance_local_branching, results)

    if case == OPTIMAL_SOLUTION or case == FEASIBLE_SOLUTION:
            best_price = instance_local_branching.Price()
            x_star = instance_local_branching.y
            zin_star = instance_local_branching.zin
            zout_star = instance_local_branching.zout


    print 'Can you certify the optimaility of the found solution?: ', opt
    print 'V: ', best_price

    time_phase_1 = time.time() - start_time

    return {'v':best_price, 'y':x_star, 'zin':zin_star ,'zout':zout_star, 'time': time_phase_1 }

#################################################
# Helper functions local brancing
#################################################

def set_solver_time_limit_phase_1(opt_local_branching, first, TL, local_branching_mipgap_phase_1_first, local_branching_mipgap_phase_1):
    infinity = float('infinity')

    if first == True:
        opt_local_branching.options["mipgap"] = local_branching_mipgap_phase_1_first
    else:
        opt_local_branching.options["mipgap"] = local_branching_mipgap_phase_1

    if TL != infinity:
        opt_local_branching.options["timelimit"] = TL



    return opt_local_branching


def add_constraint_phase_1(instance_local_branching, num_constraints, tuple):
    choice = tuple[0]
    x_bar = tuple[1]
    k = tuple[2]

    C = Set(dimen=2, initialize=build_set_C(instance_local_branching, x_bar))
    # NC = Set(dimen=2, initialize=build_set_C(x_bar)) # We don't use it in the asymmetric version

    instance_local_branching.add_component('C_' + str(num_constraints), C)

    if choice == 'Left':
        instance_local_branching.add_component('local_branching_constraint_' + str(num_constraints),
                                               Constraint(
                                                   expr=sum((1 - instance_local_branching.y[yy]) for yy in getattr(instance_local_branching, 'C_' + str(num_constraints))) <= k / 2))
    else:
        print 'Error in add_constraint_phase_1'

    return instance_local_branching


def revert_last_constraint_phase_1(instance_local_branching, num_constraints, tuple):
    choice = tuple[0]
    x_bar = tuple[1]
    k = tuple[2]

    instance_local_branching.del_component('local_branching_constraint_' + str(num_constraints - 1))
    instance_local_branching.del_component('C_' + str(num_constraints - 1))

    C = Set(dimen=2, initialize=build_set_C(instance_local_branching, x_bar))
    # NC = Set(dimen=2, initialize=build_set_C(x_bar)) # We don't use it in the asymmetric version

    instance_local_branching.add_component('C_' + str(num_constraints - 1), C)

    if choice == 'Right':
        instance_local_branching.add_component('local_branching_constraint_' + str(num_constraints - 1),
                                               Constraint(
                                                   expr=sum((1 - instance_local_branching.y[yy]) for yy in getattr(instance_local_branching, 'C_' + str(num_constraints - 1))) >= k / 2))
    else:
        print 'Error in revert_last_constraint_phase_1'

    return instance_local_branching



def delete_last_constraint_phase_1(instance_local_branching, num_constraints):
    instance_local_branching.del_component('local_branching_constraint_' + str(num_constraints - 1))
    instance_local_branching.del_component('C_' + str(num_constraints - 1))

    return instance_local_branching

def add_ub_constraint(instance_local_branching, UB):
    try:
        instance_local_branching.add_component('ub', Constraint(expr= instance_local_branching.v <= UB * 0.999))
        return instance_local_branching
    except RuntimeError, AttributeError:
        instance_local_branching.del_component('ub')
        instance_local_branching.add_component('ub', Constraint(expr=instance_local_branching.v <= UB * 0.999))
        return instance_local_branching

def add_lb_constraint(instance_local_branching, LB):
    try:
        instance_local_branching.add_component('lb', Constraint(expr= instance_local_branching.v >= LB))
        return instance_local_branching
    except RuntimeError, AttributeError:
        instance_local_branching.del_component('lb')
        instance_local_branching.add_component('lb', Constraint(expr=instance_local_branching.v >= LB))
        return instance_local_branching

def refine(y):
    #TODO
    return



#########################################################################################################
# LOCAL SEARCH
#########################################################################################################

#The methods work if a starting solution is passed into x_star
def local_search_phase_1_starting_from_a_solution(dat_file_name, instance, k_swap, total_time_limit, node_time_limit, dv_max, x_star, ub, zin_star, zout_star,local_branching_mipgap_phase_1_first, local_branching_mipgap_phase_1):
    infinity = float('infinity')
    rhs = k_swap
    TL = node_time_limit
    bestUB = UB = ub
    opt = True
    #first = True
    first = False #if we want to start with a starting solution
    diversify = False
    dv = 0
    num_x_bar = 0

    x_bar = x_star
    num_x_bar = num_x_bar + 1

    opt_local_search = SolverFactory("gurobi")

    start_time = time.time()
    elapsed_time = time.time() - start_time

    print 'Beginning of the local search procedure\n'

    constraints_stack_list = []

    model = generateModel_milp_3index_relaxed()
    instance_local_search = model.create_instance(dat_file_name)

    num_constraints = 0


    while elapsed_time <= total_time_limit and dv <= dv_max:

        if rhs < infinity:
            instance_local_search = add_constraint_phase_1(instance_local_search, num_constraints, ('Left', x_bar, rhs))
            num_constraints = num_constraints + 1
            constraints_stack_list.append("delta(x, x_bar_" + str(num_constraints) + ") <= rhs")
            # Add the local br. constraint into delta(x, x_bar) <= rhs


        TL = min(TL, total_time_limit - elapsed_time) # Da decommentare nel caso in cui non si parta con una feasible solution

        opt_local_search = set_solver_time_limit_phase_1(opt_local_search, first, TL, local_branching_mipgap_phase_1_first, local_branching_mipgap_phase_1)

        print 'Lodi_solution: ', constraints_stack_list

        instance_local_search = add_ub_constraint(instance_local_search, UB)

        results = opt_local_search.solve(instance_local_search, load_solutions=False, tee=True)

        TL = node_time_limit

        case = calculate_case(instance_local_search, results)

        #1)
        if case == OPTIMAL_SOLUTION:
            print 'Optimal Solution found!: ', instance_local_search.Price()

            if instance_local_search.Price() < bestUB:
                bestUB = instance_local_search.Price()
                x_star = instance_local_search.y
                zin_star = instance_local_search.zin
                zout_star = instance_local_search.zout


            if rhs >= infinity:
                return {'y': x_star, 'zin': zin_star, 'zout': zout_star}

            # Reverse the last local br. constraint into delta(x, x_bar) >= rhs + 1
            instance_local_search = revert_last_constraint_phase_1(instance_local_search, num_constraints, ('Right', x_bar, rhs + 1))
            constraints_stack_list.pop()
            constraints_stack_list.append("delta(x, x_bar_" + str(num_constraints) + ") >= rhs + 1")

            diversify = first = False
            x_bar = x_star
            num_x_bar = num_x_bar + 1
            UB = instance_local_search.Price()
            rhs = k_swap

        #2)
        elif case == INFEASIBLE:
            instance.solutions.load_from(results)
            print 'Proved to be infeasible!'

            if rhs >= infinity:
                return opt

            instance_local_search = revert_last_constraint_phase_1(instance_local_search, num_constraints, ('Right', x_bar, rhs + 1))
            # Reverse the last local br. constraint into delta(x, x_bar) >= rhs + 1
            constraints_stack_list.pop()
            constraints_stack_list.append("delta(x, x_bar_" + str(num_constraints) + ") >= rhs + 1")

            if diversify:
                print 'Diversify'
                UB = TL = infinity
                dv = dv + 1
                first = True

            rhs = rhs + k_swap/2
            diversify = True


        elif case == FEASIBLE_SOLUTION:
            #3 ) Quando il mipgap e diverso da None vuol dire che cerchiamo una feasible solution
            print 'Solution found but not sure that is optimal: ', instance_local_search.Price()
            if rhs < infinity:
                if first:
                    # Delete the last local br. constraint into delta(x, x_bar) <= rhs
                    constraints_stack_list.pop()

                    instance_local_search = delete_last_constraint_phase_1(instance_local_search, num_constraints)
                    num_constraints = num_constraints - 1
                else:
                    instance_local_search = revert_last_constraint_phase_1(instance_local_search, num_constraints,
                                                                                  ('Right', x_bar, 1))
                    # Reverse the last local br. constraint into delta(x, x_bar) >= 1
                    constraints_stack_list.pop()
                    constraints_stack_list.append("delta(x, x_bar_" + str(num_constraints) + ") >= 1")

            refine(instance_local_search.y)

            if instance_local_search.Price() < bestUB:
                bestUB = instance_local_search.Price()
                x_star = instance_local_search.y
                zin_star = instance_local_search.zin
                zout_star = instance_local_search.zout

            first = diversify = False
            x_bar = instance_local_search.y
            num_x_bar = num_x_bar + 1
            UB = instance_local_search.Price()
            rhs = k_swap

        elif case == NO_FEASIBLE_SOLUTION:

            #4 )
            print 'No_Feasible_solution_found!'
            if diversify:
                print 'Diversify'

                instance_local_search = revert_last_constraint_phase_1(instance_local_search, num_constraints,
                                                                              ('Right', x_bar, 1))
                # Replace the last local br. constraint delta(x, x_bar) <= rhs with delta(x, x_bar) >= 1
                constraints_stack_list.pop()
                constraints_stack_list.append("delta(x, x_bar_" + str(num_constraints) + ") >= 1")

                UB = TL = infinity
                dv = dv + 1
                rhs = rhs + k_swap / 2
                first = True
            else:
                # Delete the last local br. constraint into delta(x, x_bar) <= rhs
                constraints_stack_list.pop()
                instance_local_search = delete_last_constraint_phase_1(instance_local_search, num_constraints)
                num_constraints = num_constraints - 1
                rhs = rhs - k_swap / 2
                print 'rhs = rhs - k_swap / 2'

            diversify = True
        else:
            print 'Nessuno dei casi e stato selezionato'


        elapsed_time = time.time() - start_time
        print 'elapsed_time: ', elapsed_time, '\n'

    ###################### END LOOP ######################

    best_price = bestUB

    #print_stats_z(instance_local_branching)

    TL = max(total_time_limit - elapsed_time, 0)
    print 'Out of the loop. Time remaning: ', TL

    first = False

    instance_local_search = add_ub_constraint(instance_local_search, bestUB)
    opt_local_search.options["mipgap"] = 0.03
    opt_local_search = set_solver_time_limit_phase_1(opt_local_search, first, TL, local_branching_mipgap_phase_1_first, local_branching_mipgap_phase_1)

    results = opt_local_search.solve(instance_local_search, load_solutions=False)

    try:
        instance.solutions.load_from(results)
        # Every Feasible solution would improve the previous solution due to the UB value
        if (results.solver.status == SolverStatus.ok) and (
                    results.solver.termination_condition == TerminationCondition.optimal):
            best_price = instance_local_search.Price()
            x_star = instance_local_search.y
            zin_star = instance_local_search.zin
            zout_star = instance_local_search.zout
        opt = True
    except ValueError:
        opt = False

    print 'Can you certify the optimaility of the found solution?: ', opt
    print 'V: ', best_price

    return {'y':x_star, 'zin':zin_star ,'zout':zout_star }


##############################################################################################


def local_search_phase_1(dat_file_name, instance, k_swap, total_time_limit_, node_time_limit, dv_max, x_star, zin_star, zout_star,local_branching_mipgap_phase_1_first, local_branching_mipgap_phase_1):
    infinity = float('infinity')
    rhs = bestUB = UB = TL = total_time_limit = infinity
    opt = first = True
    diversify = False
    dv = 0
    x_star = x_bar = zin_star = zout_star =None
    num_x_bar = 0

    opt_local_search = SolverFactory("gurobi")

    start_time = time.time()
    elapsed_time = time.time() - start_time

    print 'Beginning of the local search procedure\n'

    constraints_stack_list = []

    model = generateModel_milp_3index_relaxed()
    instance_local_search = model.create_instance(dat_file_name)

    num_constraints = 0

    while elapsed_time <= total_time_limit and dv <= dv_max:

        if rhs < infinity:
            constraints_stack_list.append("delta(x, x_bar_" + str(num_constraints) + ") <= rhs")
            instance_local_search = add_constraint_phase_1(instance_local_search, num_constraints, ('Left', x_bar, rhs))
            num_constraints = num_constraints + 1
            # Add the local br. constraint into delta(x, x_bar) <= rhs


        TL = min(TL, total_time_limit - elapsed_time) # Da decommentare nel caso in cui non si parta con una feasible solution
        #TL = max(TL, node_time_limit) # Se inizi un nodo almeno dedicagli del tempo plausibile (teoricamente non era presente)

        opt_local_search = set_solver_time_limit_phase_1(opt_local_search, first, TL, local_branching_mipgap_phase_1_first, local_branching_mipgap_phase_1)

        print 'Lodi_solution: ', constraints_stack_list

        instance_local_search = add_ub_constraint(instance_local_search, UB)

        start_last_execution_time = time.time()
        results = opt_local_search.solve(instance_local_search, load_solutions=False, tee=True)
        end_last_execution_time = time.time()

        TL = node_time_limit

        case = calculate_case(instance_local_search, results)

        #1)
        if case == OPTIMAL_SOLUTION:
            print 'Optimal Solution found!: ', instance_local_search.Price()

            if instance_local_search.Price() < bestUB:
                bestUB = instance_local_search.Price()
                x_star = instance_local_search.y
                zin_star = instance_local_search.zin
                zout_star = instance_local_search.zout


            if rhs >= infinity:
                time_phase_1 = time.time() - start_time
                best_price = instance_local_search.Price()
                return {'v':best_price, 'y':x_star, 'zin':zin_star ,'zout':zout_star, 'time': time_phase_1 }

            # Reverse the last local br. constraint into delta(x, x_bar) >= rhs + 1
            constraints_stack_list.pop()
            constraints_stack_list.append("delta(x, x_bar_" + str(num_constraints) + ") >= rhs + 1")

            instance_local_search = revert_last_constraint_phase_1(instance_local_search, num_constraints, ('Right', x_bar, rhs + 1))

            diversify = first = opt = False
            x_bar = x_star
            num_x_bar = num_x_bar + 1
            UB = instance_local_search.Price()
            rhs = k_swap

        #2)
        elif case == INFEASIBLE:
            print 'Proved to be infeasible!'

            if rhs >= infinity:
                return opt

            # Reverse the last local br. constraint into delta(x, x_bar) >= rhs + 1
            constraints_stack_list.pop()
            constraints_stack_list.append("delta(x, x_bar_" + str(num_constraints) + ") >= rhs + 1")

            instance_local_search = revert_last_constraint_phase_1(instance_local_search, num_constraints, ('Right', x_bar, rhs + 1))

            if diversify:
                print 'Diversify'
                UB = TL = infinity
                dv = dv + 1
                first = True

            rhs = rhs + k_swap/2
            diversify = True

        elif case == FEASIBLE_SOLUTION:
                print 'Solution found but not sure that is optimal: ', instance_local_search.Price()

                if rhs < infinity:
                    if first:
                        # Delete the last local br. constraint into delta(x, x_bar) <= rhs
                        constraints_stack_list.pop()

                        instance_local_search = delete_last_constraint_phase_1(instance_local_search, num_constraints)
                        num_constraints = num_constraints - 1
                    else:
                        #Questo era nel local branching
                        # Reverse the last local br. constraint into delta(x, x_bar) >= 1
                        #constraints_stack_list.pop()
                        #constraints_stack_list.append("delta(x, x_bar_" + str(num_constraints) + ") >= 1")

                        # Reverse the last local br. constraint into delta(x, x_bar) >= rhs + 1
                        constraints_stack_list.pop()
                        constraints_stack_list.append("delta(x, x_bar_" + str(num_constraints) + ") >= rhs + 1")

                        instance_local_search = revert_last_constraint_phase_1(instance_local_search, num_constraints,
                                                                                  ('Right', x_bar, 1))

                refine(instance_local_search.y)

                if instance_local_search.Price() < bestUB:
                    bestUB = instance_local_search.Price()
                    x_star = instance_local_search.y
                    zin_star = instance_local_search.zin
                    zout_star = instance_local_search.zout

                first = diversify = False
                x_bar = instance_local_search.y
                num_x_bar = num_x_bar + 1
                UB = instance_local_search.Price()
                rhs = k_swap

        elif case == NO_FEASIBLE_SOLUTION:
                #4 )
                print 'No_Feasible_solution_found!'
                if diversify:
                    print 'Diversify'
                    # Replace the last local br. constraint delta(x, x_bar) <= rhs with delta(x, x_bar) >= 1
                    constraints_stack_list.pop()
                    constraints_stack_list.append("delta(x, x_bar_" + str(num_constraints) + ") >= 1")

                    instance_local_search = revert_last_constraint_phase_1(instance_local_search, num_constraints,
                                                                              ('Right', x_bar, 1))

                    UB = TL = infinity
                    dv = dv + 1
                    rhs = rhs + k_swap / 2
                    first = True
                else:
                    # Delete the last local br. constraint into delta(x, x_bar) <= rhs
                    #constraints_stack_list.pop()

                    instance_local_search = delete_last_constraint_phase_1(instance_local_search, num_constraints)
                    num_constraints = num_constraints - 1
                    rhs = rhs - k_swap / 2
                    print 'rhs = rhs - k_swap / 2'


                diversify = True

        total_time_limit = total_time_limit_
        node_time_limit = node_time_limit * 1.3



        elapsed_time = time.time() - start_time
        print 'elapsed_time: ', elapsed_time, '\n'

    ###################### END LOOP ######################

    best_price = bestUB

    #print_stats_z(instance_local_branching)

    TL = max(total_time_limit - elapsed_time, 0)
    print 'Out of the loop. Time remaning: ', TL

    first = False

    instance_local_search = add_ub_constraint(instance_local_search, bestUB)
    opt_local_search.options["mipgap"] = 0.03
    opt_local_search = set_solver_time_limit_phase_1(opt_local_search, first, TL, local_branching_mipgap_phase_1_first, local_branching_mipgap_phase_1)

    results = opt_local_search.solve(instance_local_search, load_solutions=False)

    case = calculate_case(instance_local_search, results)

    if case == OPTIMAL_SOLUTION or case == FEASIBLE_SOLUTION:
            best_price = instance_local_search.Price()
            x_star = instance_local_search.y
            zin_star = instance_local_search.zin
            zout_star = instance_local_search.zout


    print 'Can you certify the optimaility of the found solution?: ', opt
    print 'V: ', best_price

    time_phase_1 = time.time() - start_time

    return {'v':best_price, 'y':x_star, 'zin':zin_star ,'zout':zout_star, 'time': time_phase_1 }


#########################################################################################################
# HOPS HEURISTIC FOR SECOND PHASE
#########################################################################################################

def count_Binary(instance):
    c = 0

    for z in instance.Z:
        if instance.zin[z].domain == Binary:
            c = c + 1
        if instance.zout[z].domain == Binary:
            c = c + 1
    return c


def init_last_out_of_centers_nodes_param(instance, centers_location):

    param = {}

    for k in instance.K:
        for l in instance.VNl[k]:
                param[k,l] = centers_location[k]

    return param

def init_last_out_of_leaves_nodes_param(instance):

    param = {}

    for k in instance.K:
        for l in instance.VNl[k]:
            param[k,l] = instance.vl[k,l]

    return param

def out_of_node(instance, kk, ll, n):

    val = []

    for (k,l,i,j) in instance.Z:
        if k == kk and ll == l and i == n:
            val.append(j)

    return val


def set_next_hops(instance, param_centers, param_leaves, centers_location):

    changed = False

    # Set Binary archs out fo last center->leafe node
    for k in instance.K:
        for l in instance.VNl[k]:

            last_node = param_centers[k,l]

            if last_node == instance.vl[k,l]:
                # Ho gia raggiunto la foglia
                continue
            else:
                for j in out_of_node(instance, k, l, last_node):
                    instance.zout[k, l, last_node ,j].domain = Binary
                    changed = True
                    print 'Colpevole zout: k: ',k,'l: ',l


    for kk in instance.K:
        for ll in instance.VNl[kk]:
            last_node = param_leaves[kk,ll]

            if last_node == centers_location[kk]:
                # Ho gia raggiunto il centro per questa foglia
                continue
            else:
                for jj in out_of_node(instance, kk, ll, last_node):
                    instance.zin[kk, ll, last_node, jj].domain = Binary
                    changed = True
                    print 'Colpevole zin: k: ', kk, 'l: ', ll

    return changed


def build_centers_location(instance):

    param = {}

    for k in instance.K:
        for i in instance.Vfc[k]:
            if instance.y[k, i] == 1:
                param[k] = i
                break

    return param

def update_last_out_of_centers_nodes_param(instance, param):

    for k in instance.K:
        for l in instance.VNl[k]:
            last_node = param[k, l]

            if last_node == instance.vl[k, l]:
                # Ho gia raggiunto la foglia
                continue
            else:
                for j in out_of_node(instance, k, l, last_node):
                    if instance.zout[k, l, last_node, j].value == 1 and instance.zout[k, l, last_node, j].domain == Binary:
                        instance.zout[k, l, last_node, j].fixed = True

                        #per ogni arco entrante nel current node k,l,i,current_node = 0 and fixed
                        for v in instance.N:
                            if (v,last_node) in instance.L:
                                instance.zout[k, l, v, last_node].value = 0
                                instance.zout[k, l, v, last_node].fixed = True

                        param[k, l] = j




def update_last_out_of_leaves_nodes_param(instance, param, centers_location):

    for k in instance.K:
        for l in instance.VNl[k]:
            last_node = param[k, l]

            if last_node == centers_location[k]:
                # Ho gia raggiunto il centro
                continue
            else:
                for j in out_of_node(instance, k, l, last_node):
                    if instance.zin[k, l, last_node, j].value == 1 and instance.zin[k, l, last_node, j].domain == Binary:
                        instance.zin[k, l, last_node, j].fixed = True


                        # per ogni arco entrante nel current node k,l,i,current_node = 0 and fixed
                        for v in instance.N:
                            if (v, last_node) in instance.L:
                                instance.zin[k, l, v, last_node].value = 0
                                instance.zin[k, l, v, last_node].fixed = True

                        param[k, l] = j





def execute_hops_2_phase_heuristic(instance, solver):

    centers_location = build_centers_location(instance)
    last_out_of_centers_nodes = init_last_out_of_centers_nodes_param(instance, centers_location)
    last_out_of_leaves_nodes = init_last_out_of_leaves_nodes_param(instance)

    changed = set_next_hops(instance, last_out_of_centers_nodes, last_out_of_leaves_nodes, centers_location)

    while changed:

        solver.solve(instance, tee=True)

        update_last_out_of_centers_nodes_param(instance, last_out_of_centers_nodes)
        update_last_out_of_leaves_nodes_param(instance, last_out_of_leaves_nodes, centers_location)

        for k in instance.K:
            print
            print 'K: ', k,
            for l in instance.VNl[k]:
                print
                print 'l: ', l
                print centers_location[k], '->', instance.vl[k, l]
                print 'center -> leaf: ', last_out_of_centers_nodes[k, l]
                for (i, j) in instance.L:
                    if instance.zout[k, l, i, j].value == 1 and instance.zout[k, l, i, j].domain == Binary:
                        print i, '->', j
                print 'leaf -> center: ', last_out_of_leaves_nodes[k, l]
                for (i, j) in instance.L:
                    if instance.zin[k, l, i, j].value == 1 and instance.zin[k, l, i, j].domain == Binary:
                        print i, '->', j

        changed = set_next_hops(instance, last_out_of_centers_nodes, last_out_of_leaves_nodes, centers_location)



        print 'Prezzo v: ', instance.Price()

        print 'bin_n: ',count_Binary(instance)

    for k in instance.K:
        print k,
        for l in instance.VNl[k]:
            print l,
            print centers_location[k], '->', instance.vl[k,l],': ',
            for (i,j) in instance.L:
                if instance.zout[k,l,i,j].value > 0:
                    print i,'->',j,
            print



    print 'Prezzo finale v: ', instance.Price()










def change_leaf_arch_domain(instance):

    changed = False

    for k in instance.K:
        for l in instance.VNl[k]:
            for (i, j) in instance.L:
                if i in instance.Vf[k]:
                    if instance.loc[k, l, i] == 1:
                        instance.zin[k, l, i, j].domain = Binary
                        changed = True
                if j in instance.Vf[k]:
                    if instance.loc[k, l, j] == 1:
                        instance.zout[k, l, i, j].domain = Binary
                        changed = True

    return changed



def change_center_arch_domain(inst):

    changed = False

    for k in inst.K:
        for l in inst.VNl[k]:
            for (i, j) in inst.L:
                if i in inst.Vfc[k] and inst.y[k, i] == 1:
                    inst.zout[k, l, i, j].domain = Binary
                    changed = True

                if j in inst.Vfc[k] and inst.y[k, j] == 1:
                    inst.zin[k, l, i, j].domain = Binary
                    changed = True

    return changed

def set_first_hop(instance):

    changed_center = change_center_arch_domain(instance)

    changed_leaf = change_leaf_arch_domain(instance)

    return changed_center or changed_leaf




def set_next_hops_______(instance):

    changed = False

    set_last_arch_out_from_centers = get_last_arch_out_from_centers(instance)

    set_last_arch_out_from_leaves = get_last_arch_out_from_leaves(instance)

    if len(set_last_arch_out_from_centers) > 0 or len(set_last_arch_out_from_leaves) > 0:
        changed = True

    print set_last_arch_out_from_centers
    print set_last_arch_out_from_leaves

    # Binary Fixing Zout
    for (k,l,i,j) in set_last_arch_out_from_centers:
        instance.zout[k,l,i,j].domain = Binary

    print

    # Binary Fixing Zin
    for (k,l,i,j) in set_last_arch_out_from_leaves:
        instance.zin[k,l,i,j].domain = Binary

    return changed





def get_last_arch_out_from_leaves(instance):

    set = []

    for k in instance.K:
        print k,
        for l in instance.VNl[k]:
            print l
            i = 0
            for n in instance.Vf[k]:
                if instance.loc[k,l,n] == 1:
                    i = n
                    break
            node = last_node_out_from_leaf(instance, i, k, i, l)
            if node > 0:
                for g in node_out(k, l, node, instance):
                    set.append((k, l, node, g))
        print

    return set


def get_last_arch_out_from_centers(instance):

    set = []

    for k in instance.K:
        print k,
        i = 0
        for n in instance.Vfc[k]:
            if instance.y[k, n] == 1:
                i = n
                break
        print i
        for l in instance.VNl[k]:
            print l
            node = last_node_out_from_center(instance, i, k, i, l)
            if node > 0:
                for g in node_out(k, l, node, instance):
                    set.append((k, l, node, g))
            print

    return set



def last_node_out_from_leaf(instance,n,k,i,l):

    print 'Foglia in'
    if n == i:
        print 'Caso base foglia'
        for j in node_out(k, l, n, instance):
            if instance.zin[k, l, n, j].value == 1 and instance.zin[k, l, n, j].domain == Binary:
                return last_node_out_from_leaf(instance, j, k, n, l)

    for h in instance.Vfc[k]:
        if n == h:
            print 'Sono Arrivato Nel centro!'
            return 0


    for j in node_out(k, l, n, instance):
        if instance.zin[k, l, n, j].value == 1 and instance.zin[k, l, n, j].domain == Binary:
            print 'Sono collegato ad un arco binario, vado avanti!'
            return last_node_out_from_leaf(instance, j, k, n, l)

    print 'Sono arrivato al nodo ultimo della catena'
    return n



def last_node_out_from_center(instance,n,k,i,l):

    if n == i:
        print 'Caso base centro'
        for j in node_out(k, l, n, instance):
            if instance.zout[k,l,n,j].value == 1 and instance.zout[k,l,n,j].domain == Binary:
                print j
                return last_node_out_from_center(instance,j,k,n,l)


    if (k,l,n) in instance.LOC and instance.loc[k,l,n] == 1:
        print 'Sono arrivato nella foglia'
        return 0


    for j in node_out(k, l, n, instance):
        if instance.zout[k, l, n, j].value == 1 and instance.zout[k, l, n, j].domain == Binary:
            #print instance.zout[k, l, n, j].domain
            print 'Sono collegato ad un arco binario, vado avanti!'
            return last_node_out_from_center(instance,j,k,n,l)

    print 'Sono arrivato al nodo ultimo della catena'
    return n




def node_out(k, l, n, instance):

    retval = []

    for (i,j) in instance.L:
        if i == n and (k,l,i,j) in instance.Z:
            retval.append(j)

    return retval


def fix_binary_variable(instance):

    for z in instance.Z:
        if instance.zin[z].domain == Binary:
            instance.zin[z].fixed = True

    for z in instance.Z:
        if instance.zout[z].domain == Binary:
            instance.zout[z].fixed = True