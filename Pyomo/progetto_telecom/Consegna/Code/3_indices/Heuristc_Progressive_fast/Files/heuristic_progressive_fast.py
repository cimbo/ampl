# --------------------------------------------------------Header-------------------------------------------------------- #
from __future__ import division
import random
import time
from pyomo.opt import SolverFactory
from VNoM import generateModel_VNoM_3index
from milp_3index import generateModel_milp_3index
from milp_3index_phase2 import generateModel_milp_3index_fixed_centers
from milp_3index_phase2_relaxed import generateModel_milp_3index_fixed_centers_relaxed
from functions_3index import *
import json

######################################################################
# PSEUDORANDOM SETTING
######################################################################
SEED = 14041992

#This line of code allows to reproduce in any moment the same pseudo random execution of the algorithm
random.seed(SEED)

######################################################################
# SOLVER PREFERENCES
######################################################################

#<---- DETERMINISTIC HEURISTIC ---->
opt_VNoM = SolverFactory("gurobi")
opt_VNoM.options["mipgap"] = 0.03
#opt_VNoM.options["timelimit"] = 150

opt_deterministic_phase_2 = SolverFactory("gurobi")
opt_deterministic_phase_2.options["mipgap"] = 0.03
opt_deterministic_phase_2.options["timelimit"] = 1000

opt_local_search = SolverFactory("gurobi")
opt_local_search.options["mipgap"] = 0.03

######################################################################
# GLOBAL VARIABLES
######################################################################

#Instance of the exact model
global exact_optimum
global instance
global end_time_exact


######################################################################
# CONSTANTS
######################################################################



# ----------------------------------------------------Main---------------------------------------------------- #

#Local Branching Phase 1
total_time_limit_phase_1 = 3000
node_time_limit_phase_1 = 600

#k_swap == 2 please
k_swap_phase_1 = 4
# Actually we don't have a way to diversify
dv_max_phase_1 = 0
local_branching_mipgap_phase_1_first = 1
local_branching_mipgap_phase_1 = 0.2


def main(file, optimum, exact_t):

    print '\n', file

    set_parameters(file, optimum, exact_t)

    generate_data_instance()

    results = solve_deterministic_heuristic()

    price = results[0]
    end_time_VNoM_deterministic_heuristic = results[1]
    end_time_exact_phase_2 = results[2]
    y = results[3]

    zin = None
    zout = None

    results = LocBra_phase_1(file, instance, k_swap_phase_1, total_time_limit_phase_1,
                                            node_time_limit_phase_1, dv_max_phase_1, y, price, zin, zout,
                                            local_branching_mipgap_phase_1_first, local_branching_mipgap_phase_1)

    y = results['y']

    execute_exact_phase_2(y, opt_deterministic_phase_2)

    return build_solutions_json(price, end_time_VNoM_deterministic_heuristic, end_time_exact_phase_2)





######################################################################
# HELPER FUNCTIONS
######################################################################

def set_parameters(file, optimum, exact_t):
    global dat_file_name
    global exact_optimum
    global end_time_exact

    dat_file_name = file
    exact_optimum = optimum
    end_time_exact = exact_t

def generate_data_instance():

    global instance
    global dat_file_name

    abstract_model = generateModel_milp_3index()

    instance = abstract_model.create_instance(dat_file_name)



def solve_deterministic_heuristic():

    start_time_deterministic_heuristic = time.time()

    print '\nDetermenistic heuristic'

    print "# Phase 1: Now fore each k in K choose the substrate node onto which the substrate center of Gk has to be mapped"

    ordered_VNs_vector = build_VNs_ordered_vector(instance)
    # (y, time_VNoM)
    results_VNoM = execute_VNoM(instance, ordered_VNs_vector, opt_VNoM)

    y = results_VNoM[0]
    end_time_VNoM_deterministic_heuristic = results_VNoM[1]

    print "# Phase 2: Solve Partial linear relaxation with NON splittable flows "

    results = execute_relaxed_phase_2(y, opt_deterministic_phase_2)

    price = results[0]
    end_time_exact_phase_2 = results[1]

    print 'Price Milp_Greedy + Exact Phase 2: ', price

    return (price, end_time_VNoM_deterministic_heuristic, end_time_exact_phase_2, y)


def build_solutions_json(price, end_time_VNoM_deterministic_heuristic, end_time_exact_phase_2):
    object = {'file_name': dat_file_name, 'results': []}

    object['results'].append({'Exact optimal solution':  exact_optimum })
    object['results'].append({'Exact execution time': end_time_exact })

    object['results'].append({'Solution found with the Deterministic heuristic': price })
    object['results'].append({'Deterministic Heutistic Phase1 Time': end_time_VNoM_deterministic_heuristic})
    object['results'].append({'Deterministic Heutistic Phase2 Time': end_time_exact_phase_2 })
    object['results'].append({'Deterministic Heutistic Total_Time = Phase1 + Phase2': end_time_VNoM_deterministic_heuristic + end_time_exact_phase_2})
    object['results'].append({'Deterministic Heutistic Gap from optimum': ((
                                                                               price * 100) / exact_optimum) - 100 })


    json_data = json.dumps(object, indent=4, separators=(',\n', ': '))

    return json_data


def getKey(item):
    return item[1]


'''
This helper function given a model with the K Virtual Networks construct a vector with tuples (K, total_demand),
ordered for decreasing values of total_demand
'''


def build_VNs_ordered_vector(instance):
    v = []

    for k in instance.K:
        sum = 0
        for l in instance.VN[k]:
            sum = sum + instance.qin[k, l] + instance.qout[k, l]
        v.append((k, sum))

    v = sorted(v, key=getKey, reverse=True)

    return v


'''
This helper function update the amount of used capacity durning the first phase of the heuristic
'''


def calculate_used_capacity(k, used_c, inst):
    used_c_param = {}

    for l in inst.L:
        val = sum(
            inst.qin[kk, im] * inst.zin[kk, im, l].value + inst.qout[kk, im] * inst.zout[kk, im, l].value for kk in
            inst.K for im in inst.VNl[kk])

        used_c_param[l] = used_c[l] + val

    return used_c_param


'''
This helper function keeps updated the sum of the defaut capacity plus the one related to the expansion of the network
'''


def calculate_current_capacity(inst):
    a_param = {}

    for l in inst.L:
        a_param[l] = inst.a[l] + inst.b[l].value

    return a_param


'''
This helper function save the decisons related to where to put the center of a VN k established in the first phase of the heuristic.
This parameter will be passed in phase 2
'''


def save_y_assignment_VNk(k, y_param, inst):
    for l in inst.Vfc[k]:
        if inst.y[k, l].value == 1:
            y_param[k, l] = 1
    return y_param


'''
This helper function just initialize the parameter Y where to save the decision that will be taken in phase 1
'''


def init_y_assign_param():
    y = {}
    for i in instance.Y:
        y[i] = 0
    return y


'''
This helper function just initialize the parameter related the used capacity durning the first phase
'''


def init_used_capacity(L):
    used_c = {}

    for l in L:
        used_c[l] = 0

    return used_c


########################################################################################################################

def execute_VNoM(instance, ordered_VNs_vector, solver):

    start_time_VNoM_deterministic_heuristic = time.time()

    y = init_y_assign_param()
    used_capacity = init_used_capacity(instance.L)
    arch_capacity_with_expansion = instance.a

    for j in range(0, len(ordered_VNs_vector)):
        k = ordered_VNs_vector[j][0]

        model = generateModel_VNoM_3index(instance.n, {k}, arch_capacity_with_expansion, used_capacity,
                                          instance.c, instance.L, instance.vn, instance.vl, instance.Vf,
                                          instance.Vfc, instance.loc, instance.qin, instance.qout)
        instance_center = model.create_instance()
        results = solver.solve(instance_center)

        used_capacity = calculate_used_capacity(k, used_capacity, instance_center)

        arch_capacity_with_expansion = calculate_current_capacity(instance_center)

        y = save_y_assignment_VNk(k, y, instance_center)

    end_time_VNoM_deterministic_heuristic = float(time.time() - start_time_VNoM_deterministic_heuristic)

    print 'VNoM time: ', end_time_VNoM_deterministic_heuristic

    return (y, end_time_VNoM_deterministic_heuristic)


def execute_exact_phase_2(y, solver):

    start_time_phase2_pure_heuristic = time.time()

    abstract_model_phase_2 = generateModel_milp_3index_fixed_centers(y)
    instance_final_pure_heuristic = abstract_model_phase_2.create_instance(dat_file_name)

    results = solver.solve(instance_final_pure_heuristic, tee=True)
    #print instance_final_pure_heuristic.Price()

    price = instance_final_pure_heuristic.Price()

    end_time_phase2_pure_heuristic = float(time.time() - start_time_phase2_pure_heuristic)

    print 'Final Price: ', price
    print 'Time exact phase 2: ', end_time_phase2_pure_heuristic

    return (price, end_time_phase2_pure_heuristic)

def execute_relaxed_phase_2(y, solver):

    start_time_phase2_pure_heuristic = time.time()

    abstract_model_phase_2 = generateModel_milp_3index_fixed_centers_relaxed(y)
    instance_final_pure_heuristic = abstract_model_phase_2.create_instance(dat_file_name)

    results = solver.solve(instance_final_pure_heuristic)
    #print instance_final_pure_heuristic.Price()

    price = instance_final_pure_heuristic.Price()

    end_time_phase2_pure_heuristic = float(time.time() - start_time_phase2_pure_heuristic)

    print 'Time exact phase 2: ', end_time_phase2_pure_heuristic
    print 'Price LB current mapping: ',price

    return (price, end_time_phase2_pure_heuristic)

