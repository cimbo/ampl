# --------------------------------------------------------Header-------------------------------------------------------- #
from __future__ import division
import random
import time
from pyomo.opt import SolverFactory
from VNoM import generateModel_VNoM_3index
from VNoM_objective_model_v import generateModel_VNoM_3index_v
from milp_3index import generateModel_milp_3index
from milp_3index_phase2 import generateModel_milp_3index_fixed_centers
import json
import math

######################################################################
# PSEUDORANDOM SETTING
######################################################################
SEED = 14041992

#This line of code allows to reproduce in any moment the same pseudo random execution of the algorithm
random.seed(SEED)

######################################################################
# SOLVER PREFERENCES
######################################################################

#<---- DETERMINISTIC HEURISTIC ---->
opt_VNoM = SolverFactory("gurobi")
opt_VNoM.options["mipgap"] = 0.03
#opt_VNoM.options["timelimit"] = 150

opt_deterministic_phase_2 = SolverFactory("gurobi")
opt_deterministic_phase_2.options["mipgap"] = 0.03
#opt_deterministic_phase_2.options["timelimit"] = 150

#<---- RANDOMIZED HEURISTIC ---->
opt_randomized_heuristic_VNoM_set = SolverFactory("gurobi")
opt_randomized_heuristic_VNoM_set.options["mipgap"] = 0.10
#opt_randomized_heuristic_VNoM.options["timelimit"] = 50

opt_randomized_heuristic_exact_phase2 = SolverFactory("gurobi")
opt_randomized_heuristic_exact_phase2.options["mipgap"] = 0.03
#opt_randomized_heuristic_exact_phase2.options["timelimit"] = 50

######################################################################
# GLOBAL VARIABLES
######################################################################

#Instance of the exact model
global exact_optimum
global instance
global end_time_exact


######################################################################
# CONSTANTS
######################################################################

#Number Of Iteration Grasp
n_ite = 0

BASE_PERCENTAGE = 0.5
PERCENTAGE_IMPROVEMENT = 0.05

'''
After a set of indices greater than MAX_INDICES_FOR_FULL_PERMUTATION the indicies vector is just shuffled. So that cold be the possibilities to try two or more time the same case.
When the size of the set of choosen indicies is less or equal to MAX_INDICES_FOR_FULL_PERMUTATION for sure will never happen the same case two times (due to the fact that i calculated all the possible permutations)
'''
MAX_INDICES_FOR_FULL_PERMUTATION = 8



# ----------------------------------------------------Main---------------------------------------------------- #




def main(file, optimum):
    print '\n', file

    set_parameters(file, optimum)

    generate_data_instance()

    results = solve_deterministic_heuristic()
    price = results[0]
    end_time_VNoM_deterministic_heuristic = results[1]
    end_time_exact_phase_2 = results[2]
    y = results[3]

    results = execute_randomized_heuristic(n_ite, y, price)

    price_randomized = results[0]
    end_time_randomized_heuristic = results[0]

    return build_solutions_json(price, end_time_VNoM_deterministic_heuristic, end_time_exact_phase_2, price_randomized,
                                end_time_randomized_heuristic)





######################################################################
# HELPER FUNCTIONS
######################################################################

def set_parameters(file, optimum):
    global dat_file_name
    global exact_optimum
    global end_time_exact

    dat_file_name = file
    exact_optimum = optimum

def generate_data_instance():

    global instance
    global dat_file_name

    abstract_model = generateModel_milp_3index()

    instance = abstract_model.create_instance(dat_file_name)



def solve_deterministic_heuristic():

    start_time_deterministic_heuristic = time.time()

    print '\nDetermenistic heuristic'

    print "# Phase 1: Now fore each k in K choose the substrate node onto which the substrate center of Gk has to be mapped"

    ordered_VNs_vector = build_VNs_ordered_vector(instance)
    # (y, time_VNoM)
    results_VNoM = execute_VNoM(instance, ordered_VNs_vector, opt_VNoM)

    y = results_VNoM[0]
    end_time_VNoM_deterministic_heuristic = results_VNoM[1]

    print "# Phase 2: Solve Partial linear relaxation with NON splittable flows "

    results = execute_exact_phase_2(y, opt_deterministic_phase_2)

    price = results[0]
    end_time_exact_phase_2 = results[1]

    print 'Price Milp_Greedy + Exact Phase 2: ', price

    return (price, end_time_VNoM_deterministic_heuristic, end_time_exact_phase_2, y)


def execute_randomized_heuristic(n_ite, y_pure_heuristic, price):

    #print "\n# #######################################################################################################################################"
    #print 'Beginning of the randomized Part'
    #print "# #######################################################################################################################################\n"

    ordered_VNs_vector = build_VNs_ordered_vector(instance)

    best = price

    ################# RANDOM PRE-SELECTION ################

    list_of_rang = build_list_of_ranges(ordered_VNs_vector)

    start_time_randomized_heuristic = time.time()

    i = 0

    while i < n_ite:

        # print "\n# -------------------------------------------------------------------------"
        # print "# Iteration: %d" % (i + 1)
        # print "# -------------------------------------------------------------------------\n"

        start_time_randomized_heuristic_ite = time.time()

        if i > 0:
            random.shuffle(list_of_rang)

        list_of_ranges = list(list_of_rang)

        # print "# Phase 1: Now fore each k in K choose the substrate node onto which the substrate center of Gk has to be mapped"

        ordered_VNs_vector = build_VNs_ordered_vector(instance)

        # overwrite the original
        list_sets_VNs = generate_sets_VNs_Milp(ordered_VNs_vector, list_of_ranges)

        # (y, time_VNoM)
        results_VNoM_set = execute_VNoM_with_sets(instance, list_sets_VNs, opt_randomized_heuristic_VNoM_set)
        y = results_VNoM_set[0]
        end_time_VNoM_deterministic_heuristic = results_VNoM_set[1]

        ###########################################################
        # CHECK IF RANDOM PHASE 1 IS EQUAL TO THE DETERMINISTIC ONE
        ###########################################################

        # If the phase 1 y assignment is equal to the phase 1 of the pure heuristic go on
        if y == y_pure_heuristic:
            # print 'WARNING!:The phase 1 is the same of the pure heuristic, so I can skip to the next permutation. Going on here would be useless'
            i = (i + 1)
            continue

        # print "# Phase 2: Solve Partial linear relaxation with NON splittable flows "

        results = execute_exact_phase_2(y, opt_randomized_heuristic_exact_phase2)

        price = results[0]
        end_time_exact_phase_2 = results[1]
        print "\n# Objective Iteration: %d" % (i + 1)
        print 'Price: ', price

        if best > price:
            best = price
            best_ordered_vector = ordered_VNs_vector
            # print best_ordered_vector
            print "\n# Objective Improvement!"
            # print price

        end_time_heuristic_ite = float(time.time() - start_time_randomized_heuristic_ite)

        print '\nIteration time: %f ' % end_time_heuristic_ite

        ############## END ITERATION ################################

    end_time_randomized_heuristic = time.time() - start_time_randomized_heuristic
    print 'Price Randomized Heuristic: ', best
    print 'Total Time Randomized Heuristic: ', end_time_randomized_heuristic

    return (best, end_time_randomized_heuristic)


def build_solutions_json(price, end_time_VNoM_deterministic_heuristic, end_time_exact_phase_2 , price_randomized, end_time_randomized_heuristic):
    object = {'file_name': dat_file_name, 'results': []}

    object['results'].append({'Exact optimal solution':  exact_optimum })

    object['results'].append({'Solution found with the Deterministic heuristic': price })
    object['results'].append({'Deterministic Heutistic Phase1 Time': end_time_VNoM_deterministic_heuristic})
    object['results'].append({'Deterministic Heutistic Phase2 Time': end_time_exact_phase_2 })
    object['results'].append({'Deterministic Heutistic Total_Time = Phase1 + Phase2': end_time_VNoM_deterministic_heuristic + end_time_exact_phase_2})
    object['results'].append({'Deterministic Heutistic Gap from optimum': ((
                                                                               price * 100) / exact_optimum) - 100 })

    object['results'].append({'Best Solution found with the Randomized Heuristic (Heuristic Progressive Grasp Milp)': price_randomized })
    object['results'].append({'Randomized Heuristic (Heuristic Progressive Grasp Milp) total execution time': end_time_randomized_heuristic })


    gap = ((price_randomized * 100) / exact_optimum) - 100

    object['results'].append({'Randomized Heutistic (Heuristic Progressive Grasp Milp) Gap from optimum': gap })

    json_data = json.dumps(object, indent=4, separators=(',\n', ': '))

    return json_data


def getKey(item):
    return item[1]


'''
This helper function given a model with the K Virtual Networks construct a vector with tuples (K, total_demand),
ordered for decreasing values of total_demand
'''


def build_VNs_ordered_vector(instance):
    v = []

    for k in instance.K:
        sum = 0
        for l in instance.VN[k]:
            sum = sum + instance.qin[k, l] + instance.qout[k, l]
        v.append((k, sum))

    v = sorted(v, key=getKey, reverse=True)

    return v


'''
This helper function update the amount of used capacity durning the first phase of the heuristic
'''


def calculate_used_capacity(k, used_c, inst):
    used_c_param = {}

    for l in inst.L:
        val = sum(
            inst.qin[kk, im] * inst.zin[kk, im, l].value + inst.qout[kk, im] * inst.zout[kk, im, l].value for kk in
            inst.K for im in inst.VNl[kk])

        used_c_param[l] = used_c[l] + val

    return used_c_param


'''
This helper function keeps updated the sum of the defaut capacity plus the one related to the expansion of the network
'''


def calculate_current_capacity(inst):
    a_param = {}

    for l in inst.L:
        a_param[l] = inst.a[l] + inst.b[l].value

    return a_param


'''
This helper function save the decisons related to where to put the center of a VN k established in the first phase of the heuristic.
This parameter will be passed in phase 2
'''


def save_y_assignment_VNk(k, y_param, inst):
    for l in inst.Vfc[k]:
        if inst.y[k, l].value == 1:
            y_param[k, l] = 1
    return y_param

def save_y_assignment_set_VNk(kk, y_param, inst):
    for k in kk:
        for l in inst.Vfc[k]:
            if inst.y[k, l].value == 1:
                y_param[k, l] = 1
    return y_param

'''
This helper function just initialize the parameter Y where to save the decision that will be taken in phase 1
'''


def init_y_assign_param():
    y = {}
    for i in instance.Y:
        y[i] = 0
    return y


'''
This helper function just initialize the parameter related the used capacity durning the first phase
'''


def init_used_capacity(L):
    used_c = {}

    for l in L:
        used_c[l] = 0

    return used_c


'''
The following 3 definitions are needed to create the set of permutations for the index to be shuffle (In the randomized part)
'''


class unique_element:
    def __init__(self, value, occurrences):
        self.value = value
        self.occurrences = occurrences


def perm_unique(elements):
    eset = set(elements)
    listunique = [unique_element(i, elements.count(i)) for i in eset]
    u = len(elements)
    return perm_unique_helper(listunique, [0] * u, u - 1)


def perm_unique_helper(listunique, result_list, d):
    if d < 0:
        yield tuple(result_list)
    else:
        for i in listunique:
            if i.occurrences > 0:
                result_list[d] = i.value
                i.occurrences -= 1
                for g in perm_unique_helper(listunique, result_list, d - 1):
                    yield g
                i.occurrences += 1


'''
This helper function retrives the tuples from the ordered vector according to the choosen indices
'''
def get_selected_tuples(vector, indices):
    array = []
    for i in indices:
        array.append(vector[i - 1])
    return array



'''
This helper function update the ordered vector with the current permutation to explore
'''


def generate_sets_VNs_Milp(ordered_VNs_vector, list_of_ranges):

    grup_k_VN_milp = []

    for range_ in list_of_ranges:
        random.shuffle(range_)

    for r in list_of_ranges:
        k = []
        for i in r:
            k.append(ordered_VNs_vector[i - 1][0])
        grup_k_VN_milp.append(set(k))

    return grup_k_VN_milp

def get_permutation_list(selected_elements, n_ite):
    copy = list(selected_elements)

    # If branch when is possible to calculate all the permutations
    if len(selected_elements) <= MAX_INDICES_FOR_FULL_PERMUTATION:
        return list(perm_unique(selected_elements))
    else:
        list_random_permutations = []
        for i in range(0, n_ite):
            random.shuffle(copy)
            list_random_permutations.append(tuple(copy))
        list_random_permutations.append(tuple(selected_elements))
        return list_random_permutations


def build_list_of_ranges(ordered_VNs_vector):
    list_of_ranges = range(0,len(ordered_VNs_vector))

    retval = chunkIt(list_of_ranges, int(math.ceil(len(list_of_ranges)*.25)))

    print retval

    return retval

'''
This helper function save the best solution found so far durning the heuristic's execution
'''
def check_if_best_solution_so_far(best, instance_final):
    if best == 0:
        best = instance_final
    else:
        if best.Price() > instance_final.Price():
            best = instance_final

    return best


def chunkIt(seq, num):
  avg = len(seq) / float(num)
  out = []
  last = 0.0

  while last < len(seq):
    out.append(seq[int(last):int(last + avg)])
    last += avg

  return out

def execute_VNoM(instance, ordered_VNs_vector, solver):

    start_time_VNoM_deterministic_heuristic = time.time()

    y = init_y_assign_param()
    used_capacity = init_used_capacity(instance.L)
    arch_capacity_with_expansion = instance.a

    for j in range(0, len(ordered_VNs_vector)):
        k = ordered_VNs_vector[j][0]

        model = generateModel_VNoM_3index(instance.n, {k}, arch_capacity_with_expansion, used_capacity,
                                          instance.c, instance.L, instance.vn, instance.vl, instance.Vf,
                                          instance.Vfc, instance.loc, instance.qin, instance.qout)
        instance_center = model.create_instance()
        results = solver.solve(instance_center)

        used_capacity = calculate_used_capacity(k, used_capacity, instance_center)

        arch_capacity_with_expansion = calculate_current_capacity(instance_center)

        y = save_y_assignment_VNk(k, y, instance_center)

    end_time_VNoM_deterministic_heuristic = float(time.time() - start_time_VNoM_deterministic_heuristic)

    return (y, end_time_VNoM_deterministic_heuristic)


def execute_exact_phase_2(y, solver):

    start_time_phase2_pure_heuristic = time.time()

    abstract_model_phase_2 = generateModel_milp_3index_fixed_centers(y)
    instance_final_pure_heuristic = abstract_model_phase_2.create_instance(dat_file_name)

    results = solver.solve(instance_final_pure_heuristic)
    #print instance_final_pure_heuristic.Price()

    price = instance_final_pure_heuristic.Price()

    end_time_phase2_pure_heuristic = float(time.time() - start_time_phase2_pure_heuristic)

    return (price, end_time_phase2_pure_heuristic)

def execute_VNoM_with_sets(instance, list_sets_VNs, solver):

    start_time_VNoM_deterministic_heuristic = time.time()

    y = init_y_assign_param()
    used_capacity = init_used_capacity(instance.L)
    arch_capacity_with_expansion = instance.a

    for k in list_sets_VNs:

        model = generateModel_VNoM_3index_v(instance.n, k, arch_capacity_with_expansion, used_capacity,
                                          instance.c, instance.L, instance.vn, instance.vl, instance.Vf,
                                          instance.Vfc, instance.loc, instance.qin, instance.qout)
        instance_center = model.create_instance()
        results = solver.solve(instance_center)

        used_capacity = calculate_used_capacity(k, used_capacity, instance_center)

        arch_capacity_with_expansion = calculate_current_capacity(instance_center)

        y = save_y_assignment_set_VNk(k, y, instance_center)

    end_time_VNoM_deterministic_heuristic = float(time.time() - start_time_VNoM_deterministic_heuristic)

    return (y, end_time_VNoM_deterministic_heuristic)