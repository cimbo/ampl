# --------------------------------------------------------Header-------------------------------------------------------- #
from __future__ import division
import random
import time
from pyomo.opt import SolverFactory
from milp_3index import generateModel_milp_3index
from milp_3index_phase1 import generateModel_milp_3index_relaxed
from calculate_v_objective import generate_model_v_objective
import json
from Dijkstra_shortest_path import *
import sys
from functions_3index import *

######################################################################
# PSEUDORANDOM SETTING
######################################################################
SEED = 14041992

#This line of code allows to reproduce in any moment the same pseudo random execution of the algorithm
random.seed(SEED)

######################################################################
# SOLVER PREFERENCES
######################################################################

#<---- DETERMINISTIC HEURISTIC ---->
opt_deterministic_phase_1 = SolverFactory("gurobi")
opt_deterministic_phase_1.options["mipgap"] = 0.1
#opt_deterministic_phase_1.options["timelimit"] = 150

#<----- GREEDY OBJECTIVE ------>
opt_greedy_objective = SolverFactory("gurobi")
opt_greedy_objective.options["mipgap"] = 0.03
#opt_randomized_heuristic_phase2.options["timelimit"] = 50

######################################################################
# GLOBAL VARIABLES
######################################################################

gamma = 0.2

max_arch_swap = 300
max_center_swap = 0

#ORDINAMENTO_FLUSSI = 0 politica tesi Laura
#ORDINAMENTO_FLUSSI = 1 politica idea mia con il ciclo for esterno
ORDINAMENTO_FLUSSI = 0

min_price = 0.1

#Instance of the exact model
global exact_optimum
global end_time_exact
global dat_file_name
global instance


######################################################################
# CONSTANTS
######################################################################

#Number Of Iteration Grasp
n_ite = 0

BASE_PERCENTAGE = 0.10
PERCENTAGE_IMPROVEMENT = 0.05
FROM_LEAF_TO_CENTER = 0
FROM_CENTER_TO_LEAF = 1

'''
After a set of indices greater than MAX_INDICES_FOR_FULL_PERMUTATION the indicies vector is just shuffled. So that cold be the possibilities to try two or more time the same case.
When the size of the set of choosen indicies is less or equal to MAX_INDICES_FOR_FULL_PERMUTATION for sure will never happen the same case two times (due to the fact that i calculated all the possible permutations)
'''
MAX_INDICES_FOR_FULL_PERMUTATION = 8



# ----------------------------------------------------Main---------------------------------------------------- #




def main(file, optimum, exact_t):

    print file

    set_parameters(file, optimum, exact_t)

    generate_data_instance()

    results = solve_deterministic_heuristic()

    best_deterministic = results[2]
    time_deterministic_heuristic_phase_1 = results[1]
    time_deterministic_heuristic_phase_2 = results[5]
    end_time_deterministic_exact_phase_1_VLiM = results[6]

    return build_solutions_json(best_deterministic, time_deterministic_heuristic_phase_1, time_deterministic_heuristic_phase_2, end_time_deterministic_exact_phase_1_VLiM)



######################################################################
# HELPER FUNCTIONS
######################################################################
def build_graph(instance):

    g = Graph()

    for i in instance.N:
        g.add_vertex(i)

    for l in instance.L:
        g.add_edge(l[0], l[1], float(1 / instance.a[l]))

    return g


def set_parameters(file, optimum, exact_t):
    global dat_file_name
    global exact_optimum
    global end_time_exact

    dat_file_name = file
    exact_optimum = optimum
    end_time_exact = exact_t

def generate_data_instance():

    global instance
    global dat_file_name

    abstract_model = generateModel_milp_3index()

    instance = abstract_model.create_instance(dat_file_name)



def solve_deterministic_heuristic():

    ####################################################
    # Phase 1 Exact
    ####################################################

    ordered_VNs_vector = build_VNs_ordered_vector(instance)

    ####################################################
    # Exact Phase 1
    ####################################################

    # (y, end_time_exact_phase_1)
    results_exact_phase_1 = execute_exact_phase_1()

    y = results_exact_phase_1[0]
    exact_phase_1 = results_exact_phase_1[1]

    ####################################################
    # VLiM
    ####################################################

    results_VLiM = execute_VLiM(y, instance, ordered_VNs_vector)

    price = results_VLiM[0]
    zin = results_VLiM[1]
    zout = results_VLiM[2]
    time_VLiM = results_VLiM[3]

    end_time_deterministic_exact_phase_1_VLiM = exact_phase_1 + time_VLiM

    print 'Tempo totale Fase 1 Esatta + VLiM (Dijsktra) Deterministica: ', end_time_deterministic_exact_phase_1_VLiM

    return (y, exact_phase_1, price, zin, zout, time_VLiM, end_time_deterministic_exact_phase_1_VLiM)




def build_solutions_json(deterministic_price, time_phase_1, time_phase_2, tot_time):

    object = {'file_name': dat_file_name, 'results': []}

    object['results'].append({'Solution found with the Deterministic heuristic': deterministic_price })
    object['results'].append({'Deterministic Heutistic Exact Phase1 Time': time_phase_1})
    object['results'].append({'Deterministic Heutistic Dijkstra Phase2 Time': time_phase_2 })
    object['results'].append({'Deterministic Heutistic (Exact Phase1 + Dijkstra) Total_Time = Phase1 + Phase2': tot_time})
    object['results'].append({'Deterministic Heutistic (Exact Phase1 + Dijkstra) Gap from optimum': ((
                                                                               deterministic_price * 100) / exact_optimum) - 100 })


    json_data = json.dumps(object, indent=4, separators=(',\n', ': '))

    return json_data


def getKey(item):
    return item[1]


'''
This helper function given a model with the K Virtual Networks construct a vector with tuples (K, total_demand),
ordered for decreasing values of total_demand
'''


def build_VNs_ordered_vector(instance):
    v = []

    for k in instance.K:
        sum = 0
        for l in instance.VN[k]:
            sum = sum + instance.qin[k, l] + instance.qout[k, l]
        v.append((k, sum))

    v = sorted(v, key=getKey, reverse=True)

    return v


'''
This helper function update the amount of used capacity durning the first phase of the heuristic
'''


def calculate_used_capacity(k, used_c, inst):
    used_c_param = {}

    for l in inst.L:
        val = sum(
            inst.qin[kk, im] * inst.zin[kk, im, l].value + inst.qout[kk, im] * inst.zout[kk, im, l].value for kk in
            inst.K for im in inst.VNl[kk])

        used_c_param[l] = used_c[l] + val

    return used_c_param


'''
This helper function keeps updated the sum of the defaut capacity plus the one related to the expansion of the network
'''


def calculate_current_capacity(inst):
    a_param = {}

    for l in inst.L:
        a_param[l] = inst.a[l] + inst.b[l].value

    return a_param


'''
This helper function save the decisons related to where to put the center of a VN k established in the first phase of the heuristic.
This parameter will be passed in phase 2
'''


def save_y_assignment_VNk(k, y_param, inst):
    for l in inst.Vfc[k]:
        if inst.y[k, l].value == 1:
            y_param[k, l] = 1
    return y_param


'''
This helper function just initialize the parameter Y where to save the decision that will be taken in phase 1
'''


def init_y_assign_param():
    y = {}
    for i in instance.Y:
        y[i] = 0
    return y


'''
This helper function just initialize the parameter related the used capacity durning the first phase
'''


def init_used_capacity(L):
    used_c = {}

    for l in L:
        used_c[l] = 0

    return used_c


'''
The following 3 definitions are needed to create the set of permutations for the index to be shuffle (In the randomized part)
'''


class unique_element:
    def __init__(self, value, occurrences):
        self.value = value
        self.occurrences = occurrences


def perm_unique(elements):
    eset = set(elements)
    listunique = [unique_element(i, elements.count(i)) for i in eset]
    u = len(elements)
    return perm_unique_helper(listunique, [0] * u, u - 1)


def perm_unique_helper(listunique, result_list, d):
    if d < 0:
        yield tuple(result_list)
    else:
        for i in listunique:
            if i.occurrences > 0:
                result_list[d] = i.value
                i.occurrences -= 1
                for g in perm_unique_helper(listunique, result_list, d - 1):
                    yield g
                i.occurrences += 1


'''
This helper function retrives the tuples from the ordered vector according to the choosen indices
'''
def get_selected_tuples(vector, indices):
    array = []
    for i in indices:
        array.append(vector[i - 1])
    return array



'''
This helper function update the ordered vector with the current permutation to explore
'''


def perform_index_permutation(ordered_VNs_vector, list_indices, permutation):
    count = 0
    for i in list_indices:
        ordered_VNs_vector[i - 1] = permutation[count]
        count = count + 1

    return ordered_VNs_vector

def get_permutation_list(selected_elements, n_ite):
    copy = list(selected_elements)

    # If branch when is possible to calculate all the permutations
    if len(selected_elements) <= MAX_INDICES_FOR_FULL_PERMUTATION:
        return list(perm_unique(selected_elements))
    else:
        list_random_permutations = []
        for i in range(0, n_ite):
            random.shuffle(copy)
            list_random_permutations.append(tuple(copy))
        list_random_permutations.append(tuple(selected_elements))
        return list_random_permutations


def build_list_of_ranges(ordered_VNs_vector):
    list_of_ranges = []

    current_reference_index = 1
    current_reference = ordered_VNs_vector[0]
    # First element in
    current_index = 2

    while current_index <= len(ordered_VNs_vector):
        if ordered_VNs_vector[current_index - 1][1] * (
                1 + BASE_PERCENTAGE + PERCENTAGE_IMPROVEMENT * len(list_of_ranges)) >= current_reference[1]:
            current_index = current_index + 1
        else:
            list_of_ranges.append(range(current_reference_index, current_index))
            current_reference = ordered_VNs_vector[current_index - 1]
            current_reference_index = current_index
            current_index = current_index + 1

    list_of_ranges.append(range(current_reference_index, current_index))

    return list_of_ranges




###############################################################################
#
###############################################################################

def build_ordered_VNs_flows_vector(ordered_VNs_vector):
    #Criterio tesi laura
    if ORDINAMENTO_FLUSSI == 0:
        return build_ordered_VNs_flows_vector_MODE_0(ordered_VNs_vector)
    #criterio mio ciclo for esterno flussi shortest path
    elif ORDINAMENTO_FLUSSI == 1:
        return build_ordered_VNs_flows_vector_MODE_1()
    else:
        print 'ERRORE ALL\'INTERNO DI build_ordered_VNs_flows_vector()'
        sys.exit()

def build_ordered_VNs_flows_vector_MODE_0(ordered_VNs_vector):
    global instance

    retval = []

    for i in range(0, len(ordered_VNs_vector)):
        v = []
        k = ordered_VNs_vector[i][0]

        for l in instance.VNl[k]:
            v.append((k, FROM_LEAF_TO_CENTER, instance.qin[k, l], l))
            v.append((k, FROM_CENTER_TO_LEAF, instance.qout[k, l], l))

        v = sorted(v, key=getFlowKey, reverse=True)

        for j in range(0, len(v)):
            retval.append(v[j])

    return retval


def build_ordered_VNs_flows_vector_MODE_1():
    global instance

    v = []

    for k in instance.K:
        for l in instance.VNl[k]:
            v.append((k, FROM_LEAF_TO_CENTER, instance.qin[k, l], l))
            v.append((k, FROM_CENTER_TO_LEAF, instance.qout[k, l], l))

    v = sorted(v, key=getFlowKey, reverse=True)

    return v


def getFlowKey(item):
    return item[2]

def get_physical_node_from(flow, y):

    k = flow[0]

    if flow[1] == FROM_CENTER_TO_LEAF:
        for i in instance.Vfc[k]:
            if y[k,i] == 1:
                return i
        print 'WARNING!!!! IL centro associato alla domanda k: ', k ,'Non e stato assegnato!'
        sys.exit()
    elif flow[1] == FROM_LEAF_TO_CENTER:
         return instance.vl[k, flow[3]]

def get_physical_node_to(flow, y):

    k = flow[0]

    if flow[1] == FROM_CENTER_TO_LEAF:
        #The logical leaf
        return instance.vl[k, flow[3]]
    elif flow[1] == FROM_LEAF_TO_CENTER:
        for i in instance.Vfc[k]:
            if y[k, i] == 1:
                return i
        print 'WARNING!!!! IL centro associato alla domanda k: ', k, 'Non e stato assegnato!'
        sys.exit()

def calculate_arch_cost_to_satisfy_capacity_constraints(g, flow, used_capacity, arch_capacity):

    flow_demand = flow[2]

    for l in instance.L:
        if  arch_capacity[l] - used_capacity[l] - flow_demand < gamma * arch_capacity[l]:
            #Il costo e dato dall'inverso della capacita residua + il costo dell'espandione da pagare
            capacity_expansion = (used_capacity[l] + flow_demand)/gamma - (arch_capacity[l])
            c = (1/(arch_capacity[l] - used_capacity[l])) + capacity_expansion * instance.c[l]
        else:
            c = (1/(arch_capacity[l] - used_capacity[l]))

        g.get_vertex(l[0]).update_neighbor_weigth(g.get_vertex(l[1]), c)

    return g

def update_used_capacity(used_capacity_current_flow, s_path, used_capacity):

    archs_shortest_path = build_archs_list(s_path)

    #print archs_shortest_path

    for l in archs_shortest_path:
        used_capacity[l] = used_capacity[l] + used_capacity_current_flow

    return used_capacity

def build_archs_list(s_path):
    archs_shortest_path = []

    for i in range(1, len(s_path)):
        #print s_path[i - 1]
        #print s_path[i]
        archs_shortest_path.append((s_path[i - 1], s_path[i]))

    return archs_shortest_path



def update_arch_capacity(used_capacity, arch_capacity, s_path):

    archs_shortest_path = build_archs_list(s_path)

    for l in archs_shortest_path:
        if arch_capacity[l] - used_capacity[l] < gamma * arch_capacity[l]:
            arch_capacity[l] = used_capacity[l] / (1 - gamma)
            #print 'ATTENZIONE, INCREMENTO DI CAPACITA SULL\'ARCO: ', l
            #print arch_capacity[l] - instance.a[l]
            #print 'ATTENZIONE PER SIMMETRIA INCREMENTO CAPACITA SULL\'ARCO: ', (l[1],l[0])
            arch_capacity[(l[1],l[0])] = arch_capacity[l]
            #print arch_capacity[(l[1],l[0])] - instance.a[l]
            #print 'Costo pagato: ', (arch_capacity[l] - instance.a[l]) * instance.c[l] + (arch_capacity[(l[1],l[0])] - instance.a[(l[1],l[0])]) * instance.c[(l[1],l[0])]


    return arch_capacity

def init_z():
    zin = {}
    for k in instance.K:
        for l in instance.VNl[k]:
            for (i, j) in instance.L:
                zin[k,l,i,j] = 0
    return zin

def save_zin_assignment_(flow, zin, archs_list):

    k = flow[0]
    logical_leaf = flow[3]

    if flow[1] == FROM_LEAF_TO_CENTER:
        for arch in archs_list:
            zin[k, logical_leaf, arch[0], arch[1]] = 1

    return zin

def save_zout_assignment_(flow, zout, archs_list):

    k = flow[0]
    logical_leaf = flow[3]

    if flow[1] == FROM_CENTER_TO_LEAF:
        for arch in archs_list:
            zout[k, logical_leaf, arch[0], arch[1]] = 1

    return zout

def calculate_price(expanded_capacity):

    b = {}

    for l in instance.L:
        b[l] = expanded_capacity[l] - instance.a[l]

    model = generate_model_v_objective(instance.n, instance.K, instance.c, instance.L, instance.vn, instance.vl, instance.Vf, instance.Vfc, instance.qin, instance.qout, b)
    instance_greedy = model.create_instance()
    results = opt_greedy_objective.solve(instance_greedy)

    v = instance_greedy.Price()

    return v

def reset_label_for_dijkstra(g):

    for v in g.get_vertices():
        g.get_vertex(v).set_unvisited()
        g.get_vertex(v).set_distance(sys.maxint)
        g.get_vertex(v).reset_previous()

    return g


def execute_exact_phase_1():

    start_time = time.time()

    abstract_model = generateModel_milp_3index_relaxed()

    instance = abstract_model.create_instance(dat_file_name)

    results = opt_deterministic_phase_1.solve(instance)

    end_time_exact_phase_1 = time.time() - start_time

    return (instance.y, end_time_exact_phase_1)



def execute_VLiM(y, instance, ordered_VNs_vector):

    start_time_VLiM_deterministic_heuristic = time.time()

    # Starting substrate network situation
    used_capacity = init_used_capacity(instance.L)
    arch_capacity_with_expansion = dict(instance.a)
    zin = init_z()
    zout = init_z()

    ordered_VNs_flows_vector = build_ordered_VNs_flows_vector(ordered_VNs_vector)

    g = build_graph(instance)

    for i in range(0, len(ordered_VNs_flows_vector)):
        # print 'Iteration number: ', i

        # ( k, [(1,l) | (l,1)] , [flow_demand] , logical_leaf ) #The center is alway 1 from the logical point of view!
        flow = ordered_VNs_flows_vector[i]

        from_ = get_physical_node_from(flow, y)
        to_ = get_physical_node_to(flow, y)

        '''
            Se la domanda di flusso in esame e ad esempio 200, il costo degli archi che se seleionati avrebbero meno del 20% di capacita residua, devono avere un costo che consideri gia una possibile espansione!
        '''

        g = calculate_arch_cost_to_satisfy_capacity_constraints(g, flow, used_capacity, arch_capacity_with_expansion)

        dijkstra(g, g.get_vertex(from_))
        target = g.get_vertex(to_)
        path = [target.get_id()]
        shortest_path = shortest(target, path)

        s_path = path[::-1]

        # print 'The shortest path : %s' % (s_path)

        used_capacity_current_flow = flow[2]
        # Data la scelta effettuata salvo il valore delle capacita utilizzate
        used_capacity = update_used_capacity(used_capacity_current_flow, s_path, used_capacity)

        # Aggiorno lo stato fisico delle capacita delle reti
        arch_capacity_with_expansion = update_arch_capacity(used_capacity, arch_capacity_with_expansion, s_path)

        # Salvo le scelte dell'iterazione corrente.
        zin = save_zin_assignment_(flow, zin, build_archs_list(s_path))
        zout = save_zout_assignment_(flow, zout, build_archs_list(s_path))

        g = reset_label_for_dijkstra(g)

    averaged_price = calculate_price(arch_capacity_with_expansion)

    print 'Averaged_price: ', averaged_price

    end_time_VLiM_deterministic_heuristic = float(time.time() - start_time_VLiM_deterministic_heuristic)

    return (averaged_price, zin, zout, end_time_VLiM_deterministic_heuristic)