# --------------------------------------------------------Header-------------------------------------------------------- #
from pyomo.opt import SolverFactory
import json
import time
import math
import re
import random
from functions_3index import *
from pyomo.environ import *
# Import dei modelli astratti che poi vengono utilizzati nel file principale #
#------- .mod Files (Di Ampl) -------#
from milp_3index import generateModel_milp_3index
from milp_3index_phase1 import generateModel_milp_3index_relaxed
from milp_3index_phase2 import generateModel_milp_3index_fixed_centers
from milp_3index_phase2_relaxed import generateModel_milp_3index_fixed_centers_relaxed
######################################################################
# SOLVER PREFERENCES
######################################################################

######################################################################
# PSEUDORANDOM SETTING
######################################################################
SEED = 14041992

#This line of code allows to reproduce in any moment the same pseudo random execution of the algorithm
random.seed(SEED)

######################################################################
# SOLVER PREFERENCES
######################################################################

opt_deterministic_phase_2 = SolverFactory("gurobi")
opt_deterministic_phase_2.options["mipgap"] = 0.03
#opt_deterministic_phase_2.options["timelimit"] = 1000

opt_local_search = SolverFactory("gurobi")
opt_local_search.options["mipgap"] = 0.05

######################################################################
# GLOBAL VARIABLES
######################################################################

#Instance of the exact model
global exact_optimum
global instance
global end_time_exact


######################################################################
# CONSTANTS
######################################################################



# ----------------------------------------------------Main---------------------------------------------------- #

#Local Branching Phase 1
global total_time_limit_phase_1  #Is going to be set in: time_estimation_calculator(file)
global node_time_limit_phase_1  #Is going to be set in: time_estimation_calculator(file)

#k_swap == 2 please
k_swap_percentage = 0.3333
global k_swap_phase_1
# Actually we don't have a way to diversify
dv_max_phase_1 = 20
local_branching_mipgap_phase_1_first = 1
local_branching_mipgap_phase_1 = 0.1


def main(file):

    print '\n', file

    set_parameters(file)

    configure_local_search(file)

    generate_data_instance()

    y = None
    zin = None
    zout = None

    results = local_search_phase_1(file, instance, k_swap_phase_1, 4000,
                                            1000, dv_max_phase_1, y, zin, zout,
                                            local_branching_mipgap_phase_1_first, local_branching_mipgap_phase_1)

    y = results['y']
    time_phase_1 = results['time']
    price_phase_1 = results['v']

    results = execute_exact_phase_2(y, opt_deterministic_phase_2)

    final_price = results['v']
    time_phase_2 = results['time']

    print 'Total Time: ', time_phase_1 + time_phase_2

    return build_solutions_json(final_price, time_phase_1, time_phase_2)





######################################################################
# HELPER FUNCTIONS
######################################################################

def set_parameters(file):
    global dat_file_name

    dat_file_name = file


def generate_data_instance():

    global instance
    global dat_file_name

    abstract_model = generateModel_milp_3index()

    instance = abstract_model.create_instance(dat_file_name)



def build_solutions_json(final_price, time_phase_1, time_phase_2):
    object = {'file_name': dat_file_name, 'results': []}

    object['results'].append({'Final price':  final_price })
    object['results'].append({'Time phase 1': time_phase_1})
    object['results'].append({'Time phase 2': time_phase_2})

    json_data = json.dumps(object, indent=4, separators=(',\n', ': '))

    return json_data


def getKey(item):
    return item[1]


'''
This helper function given a model with the K Virtual Networks construct a vector with tuples (K, total_demand),
ordered for decreasing values of total_demand
'''


def build_VNs_ordered_vector(instance):
    v = []

    for k in instance.K:
        sum = 0
        for l in instance.VN[k]:
            sum = sum + instance.qin[k, l] + instance.qout[k, l]
        v.append((k, sum))

    v = sorted(v, key=getKey, reverse=True)

    return v


'''
This helper function update the amount of used capacity durning the first phase of the heuristic
'''


def calculate_used_capacity(k, used_c, inst):
    used_c_param = {}

    for l in inst.L:
        val = sum(
            inst.qin[kk, im] * inst.zin[kk, im, l].value + inst.qout[kk, im] * inst.zout[kk, im, l].value for kk in
            inst.K for im in inst.VNl[kk])

        used_c_param[l] = used_c[l] + val

    return used_c_param


'''
This helper function keeps updated the sum of the defaut capacity plus the one related to the expansion of the network
'''


def calculate_current_capacity(inst):
    a_param = {}

    for l in inst.L:
        a_param[l] = inst.a[l] + inst.b[l].value

    return a_param


'''
This helper function save the decisons related to where to put the center of a VN k established in the first phase of the heuristic.
This parameter will be passed in phase 2
'''


def save_y_assignment_VNk(k, y_param, inst):
    for l in inst.Vfc[k]:
        if inst.y[k, l].value == 1:
            y_param[k, l] = 1
    return y_param


'''
This helper function just initialize the parameter Y where to save the decision that will be taken in phase 1
'''


def init_y_assign_param():
    y = {}
    for i in instance.Y:
        y[i] = 0
    return y


'''
This helper function just initialize the parameter related the used capacity durning the first phase
'''


def init_used_capacity(L):
    used_c = {}

    for l in L:
        used_c[l] = 0

    return used_c


########################################################################################################################

def execute_exact_phase_2(y, solver):

    start_time_phase2 = time.time()

    abstract_model_phase_2 = generateModel_milp_3index_fixed_centers(y)
    instance_final = abstract_model_phase_2.create_instance(dat_file_name)

    results = solver.solve(instance_final, tee=True)
    #print instance_final_pure_heuristic.Price()

    price = instance_final.Price()

    end_time_phase2 = float(time.time() - start_time_phase2)

    print 'Final Price: ', price
    print 'Time exact phase 2: ', end_time_phase2

    return {'v': price, 'time': end_time_phase2}

def execute_relaxed_phase_2(y, solver):

    start_time_phase2_pure_heuristic = time.time()

    abstract_model_phase_2 = generateModel_milp_3index_fixed_centers_relaxed(y)
    instance_final_pure_heuristic = abstract_model_phase_2.create_instance(dat_file_name)

    results = solver.solve(instance_final_pure_heuristic)
    #print instance_final_pure_heuristic.Price()

    price = instance_final_pure_heuristic.Price()

    end_time_phase2_pure_heuristic = float(time.time() - start_time_phase2_pure_heuristic)

    print 'Time exact phase 2: ', end_time_phase2_pure_heuristic
    print 'Price LB current mapping: ',price

    return (price, end_time_phase2_pure_heuristic)


def change_leaf_arch_domain(instance):
    for k in instance.K:
        for l in instance.VNl[k]:
            for (i, j) in instance.L:
                if i in instance.Vf[k]:
                    if instance.loc[k, l, i] == 1:
                        instance.zin[k, l, i, j].domain = Binary

    for k in instance.K:
        for l in instance.VNl[k]:
            for (i, j) in instance.L:
                if j in instance.Vf[k]:
                    if instance.loc[k, l, j] == 1:
                        instance.zout[k, l, i, j].domain = Binary


def change_arch_domain(inst):

    change_leaf_arch_domain(inst)

    for k in inst.K:
        for l in inst.VNl[k]:
            for (i, j) in inst.L:
                if i in inst.Vfc[k] and inst.y[k,i] == 1:
                    inst.zout[k,l,i,j].domain = Binary

                if j in inst.Vfc[k] and inst.y[k,j] == 1:
                    inst.zin[k,l,i,j].domain = Binary


def configure_local_search(file):

    time_estimation_calculator(file)

    set_k_swap(file)





def time_estimation_calculator(file):
    global total_time_limit_phase_1
    global node_time_limit_phase_1

    x = int(re.search(r'\d+', file).group())

    c = 1.20166

    f = c * math.exp(0.708242*(x - 4)) + 250

    total_time_limit_phase_1 = f

    node_time_limit_phase_1 = int(total_time_limit_phase_1)

    print total_time_limit_phase_1


def set_k_swap(file):
    global k_swap_phase_1

    vn_number = int(re.search(r'\d+', file).group())

    k_swap_phase_1 = math.ceil(vn_number * k_swap_percentage)
