# --------------------------------------------------------Header-------------------------------------------------------- #
from pyomo.opt import SolverFactory
import json
import time
from pyomo.environ import *
# Import dei modelli astratti che poi vengono utilizzati nel file principale #
#------- .mod Files (Di Ampl) -------#
from milp_3index_phase1 import generateModel_milp_3index_relaxed
from milp_3index_phase2_relaxed import generateModel_milp_3index_fixed_centers_relaxed
from milp_3index_phase2 import generateModel_milp_3index_fixed_centers
######################################################################
# SOLVER PREFERENCES
######################################################################

# -----Solver----- #
opt = SolverFactory("gurobi")
#opt.options["timelimit"] = 2000


mipgap_phase_1 = 0.05
mipgap_phase_2_partial_unsplittable = 0.05
mipgap_phase_2 = 0.03


time_phase_1 = None
time_phase_2 = None
total_time = None

# ----------------------------------------------------Main---------------------------------------------------- #

def main(file):

    print file

    start_time = time.time()

    y = execute_phase_1(file)

    partial_relaxed_instance = execute_phase_2_partial_unsplittable(file, y)

    final_instance = execute_phase_2(file, y, partial_relaxed_instance)

    optimum = final_instance.Price()

    end_time = time.time() - start_time

    print 'Total_time: ', end_time

    number_of_centers = calculate_number_of_used_centers(final_instance)

    return build_solutions_json(file, optimum, time_phase_1, time_phase_2, end_time, number_of_centers)


def build_solutions_json(file, optimum, time_phase_1, time_phase_2, end_time, number_of_centers):
    data = {}

    data['file_name'] = file
    data['optimum'] = optimum
    data['time_phase_1'] = time_phase_1
    data['time_phase_2'] = time_phase_2
    data['total_time'] = end_time
    data['number_of_centers'] = number_of_centers

    json_data = json.dumps(data, indent=4, separators=(',\n', ': '))

    return json_data

def execute_phase_1(dat_file_name):
    start_time = time.time()

    abstract_model = generateModel_milp_3index_relaxed()

    instance = abstract_model.create_instance(dat_file_name)

    opt.options["mipgap"] = mipgap_phase_1
    results = opt.solve(instance, tee=True)
    # results.write()

    time_phase_1 = time.time() - start_time

    print 'V after phase 1: ', instance.Price()
    print 'Time Phase 1: ', time_phase_1

    y = build_y_parameter(instance)

    return y

def execute_phase_2_partial_unsplittable(file, y):
    start_time = time.time()

    abstract_model_phase_2_partial_unsplittable = generateModel_milp_3index_fixed_centers_relaxed(y)

    instance_phase_2_partial_unsplittable = abstract_model_phase_2_partial_unsplittable.create_instance(file)

    opt.options["mipgap"] = mipgap_phase_2_partial_unsplittable

    change_arch_domain(instance_phase_2_partial_unsplittable)

    results = opt.solve(instance_phase_2_partial_unsplittable, tee=True)

    time_phase_2 = time.time() - start_time

    print 'V after phase 2 partial unsplittable: ', instance_phase_2_partial_unsplittable.Price()
    print 'Time Phase 2 partial unsplittable: ', time_phase_2

    return instance_phase_2_partial_unsplittable



def execute_phase_2(dat_file_name, y, partial_relaxed_instance):

    start_time = time.time()

    abstract_model_phase_2 = generateModel_milp_3index_fixed_centers(y)

    instance_phase_2 = abstract_model_phase_2.create_instance(dat_file_name)

    opt.options["mipgap"] = mipgap_phase_2

    preset_warm_start(partial_relaxed_instance, instance_phase_2)

    results = opt.solve(instance_phase_2, tee=True)

    time_phase_2 = time.time() - start_time

    print 'V after phase 2: ', instance_phase_2.Price()
    print 'Time Phase 2: ', time_phase_2

    return instance_phase_2

def preset_warm_start(inst_from_copy, inst_to_copy):
    for z in inst_from_copy.Z:
        if inst_from_copy.zin[z].domain == Binary:
            inst_to_copy.zin[z] = int(inst_from_copy.zin[z].value)
            inst_to_copy.zin[z].fixed = True
        if inst_from_copy.zout[z].domain == Binary:
            inst_to_copy.zout[z] = int(inst_from_copy.zout[z].value)
            inst_to_copy.zout[z].fixed = True





def calculate_number_of_used_centers(instance):

    number_of_centers = []

    for (k,i) in instance.Y:
        if instance.y[k,i] == 1:
            number_of_centers.append(i)

    number_of_centers = len(set(number_of_centers))

    print 'number_of_centers: ', number_of_centers, '\n'
    return number_of_centers

def build_y_parameter(instance):
    v = {}

    for k in instance.K:
        for l in instance.Vfc[k]:
            v[k,l] = instance.y[k,l].value
    return v


def change_leaf_arch_domain(instance):
    for k in instance.K:
        for l in instance.VNl[k]:
            for (i, j) in instance.L:
                if i in instance.Vf[k]:
                    if instance.loc[k, l, i] == 1:
                        instance.zin[k, l, i, j].domain = Binary

    for k in instance.K:
        for l in instance.VNl[k]:
            for (i, j) in instance.L:
                if j in instance.Vf[k]:
                    if instance.loc[k, l, j] == 1:
                        instance.zout[k, l, i, j].domain = Binary


def change_arch_domain(inst):

    change_leaf_arch_domain(inst)

    for k in inst.K:
        for l in inst.VNl[k]:
            for (i, j) in inst.L:
                if i in inst.Vfc[k] and inst.y[k,i] == 1:
                    inst.zout[k,l,i,j].domain = Binary

                if j in inst.Vfc[k] and inst.y[k,j] == 1:
                    inst.zin[k,l,i,j].domain = Binary