# --------------------------------------------------------Header-------------------------------------------------------- #
from pyomo.opt import SolverFactory
import json
import time
from functions_3index import *
# Import dei modelli astratti che poi vengono utilizzati nel file principale #
#------- .mod Files (Di Ampl) -------#
from milp_3index_phase1_relaxed import generateModel_milp_3index_centers_relaxed
from milp_3index_phase1_restricted_centers import generateModel_milp_3index_restricted_centers
from milp_3index_phase2 import generateModel_milp_3index_fixed_centers
######################################################################
# SOLVER PREFERENCES
######################################################################

# -----Solver----- #
opt = SolverFactory("gurobi")
#opt.options["timelimit"] = 2000


mipgap_phase_1_relaxed = 0.03
mipgap_phase_1_restricted = 0.05
mipgap_phase_1 = 0.05
mipgap_phase_2 = 0.03


time_phase_1 = None
time_phase_2 = None
total_time = None

# ----------------------------------------------------Main---------------------------------------------------- #

def main(file):

    print file

    start_time = time.time()

    results = execute_phase_1_relaxed(file)

    candidate_centers = results['candidate_centers']
    instance_candidate_centers = results['instance']

    print candidate_centers

    results = execute_phase_1_restricted_centers(file, candidate_centers, instance_candidate_centers)

    y = results['y']

    final_instance = execute_phase_2(file, y)

    optimum = final_instance.Price()

    end_time = time.time() - start_time

    print 'Total_time: ', end_time

    number_of_centers = calculate_number_of_used_centers(final_instance)

    print

    return build_solutions_json(file, optimum, time_phase_1, time_phase_2, end_time, number_of_centers)


def build_solutions_json(file, optimum, time_phase_1, time_phase_2, end_time, number_of_centers):
    data = {}

    data['file_name'] = file
    data['optimum'] = optimum
    data['time_phase_1'] = time_phase_1
    data['time_phase_2'] = time_phase_2
    data['total_time'] = end_time
    data['number_of_centers'] = number_of_centers

    json_data = json.dumps(data, indent=4, separators=(',\n', ': '))

    return json_data

def execute_phase_1_relaxed(dat_file_name):
    start_time = time.time()

    abstract_model = generateModel_milp_3index_centers_relaxed()

    instance = abstract_model.create_instance(dat_file_name)

    opt.options["mipgap"] = mipgap_phase_1_relaxed
    results = opt.solve(instance, tee=True)
    # results.write()

    time_phase_1 = time.time() - start_time

    print 'V after phase 1 Relaxed Centers: ', instance.Price()
    print 'Time Phase 1: ', time_phase_1

    y = build_y_parameter(instance)

    candidate_centers = build_candidate_centers_parameter(y, instance)

    return {'candidate_centers': candidate_centers, 'instance': instance}

def build_candidate_centers_parameter(y, instance):

    candidate_centers_parameter = {}

    for k in instance.K:
        lis = []
        for i in instance.Vfc[k]:
            if y[k, i] > 0:
                lis.append(i)
        candidate_centers_parameter[k] = lis

    return candidate_centers_parameter

def execute_phase_1_restricted_centers(dat_file_name, candidate_centers, inst):
    start_time = time.time()

    abstract_model_phase_1_restricted_centers = generateModel_milp_3index_restricted_centers(candidate_centers)

    instance_phase_1_restricted_centers = abstract_model_phase_1_restricted_centers.create_instance(dat_file_name)

    #add_lb_constraint(instance_phase_1_restricted_centers, 1.95)

    opt.options["mipgap"] = mipgap_phase_1_restricted
    opt.options["MIPFocus"] = 3
    results = opt.solve(instance_phase_1_restricted_centers, tee=True)
    #instance_phase_1_restricted_centers# .display()
    # results.write()

    time_phase_1 = time.time() - start_time

    print 'V after phase 1 Restricted Centers: ', instance_phase_1_restricted_centers.Price()
    print 'Time Phase 1: ', time_phase_1

    y = build_y_parameter_restricted_centers(instance_phase_1_restricted_centers, inst)

    return {'y': y, 'instance': instance_phase_1_restricted_centers }


def execute_phase_2(dat_file_name, y):

    start_time = time.time()

    abstract_model_phase_2 = generateModel_milp_3index_fixed_centers(y)

    instance_phase_2 = abstract_model_phase_2.create_instance(dat_file_name)

    opt.options["mipgap"] = mipgap_phase_2
    results = opt.solve(instance_phase_2, tee=True)

    time_phase_2 = time.time() - start_time

    print 'V after phase 2: ', instance_phase_2.Price()
    print 'Time Phase 2: ', time_phase_2

    return instance_phase_2



def calculate_number_of_used_centers(instance):

    number_of_centers = []

    for (k,i) in instance.Y:
        if instance.y[k,i] == 1:
            number_of_centers.append(i)

    number_of_centers = len(set(number_of_centers))

    print 'number_of_centers: ', number_of_centers, '\n'
    return number_of_centers

def build_y_parameter(instance):
    v = {}

    for k in instance.K:
        for l in instance.Vfc[k]:
            v[k,l] = instance.y[k,l].value
    return v

def build_y_parameter_restricted_centers(inst_restricted, instance):
    v = {}

    for k in instance.K:
        for i in instance.Vfc[k]:
            if i in inst_restricted.Vfc[k]:
                v[k, i] = inst_restricted.y[k, i].value
            else:
                v[k,i] = 0

    return v


