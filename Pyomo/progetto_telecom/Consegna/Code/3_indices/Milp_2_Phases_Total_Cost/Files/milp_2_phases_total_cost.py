# --------------------------------------------------------Header-------------------------------------------------------- #
from pyomo.opt import SolverFactory
import json
import time
# Import dei modelli astratti che poi vengono utilizzati nel file principale #
#------- .mod Files (Di Ampl) -------#
from milp_total_cost_phase_1_3index import generate_model_min_total_cost_vne_t_s_3index_phase_1
from milp_total_cost_phase_2_3index import generateModel_milp_3index_fixed_centers_total_cost
from calculate_prices import generate_model_calculate_prices
from calculate_v_objective import generate_model_v_objective
######################################################################
# SOLVER PREFERENCES
######################################################################

# -----Solver----- #
opt = SolverFactory("gurobi")
#opt.options["timelimit"] = 2000


mipgap_phase_1 = 0.01
mipgap_phase_2 = 0.01


time_phase_1 = None
time_phase_2 = None
total_time = None

# ----------------------------------------------------Main---------------------------------------------------- #

def main(file):

    print file

    start_time = time.time()

    y = execute_phase_1(file)

    final_instance = execute_phase_2(file, y)

    optimum = final_instance.Price()

    end_time = time.time() - start_time

    print 'Total_time: ', end_time

    number_of_centers = calculate_number_of_used_centers(final_instance)

    calculate_v_objective(final_instance)

    calculate_prices(final_instance)

    return build_solutions_json(file, optimum, time_phase_1, time_phase_2, end_time, number_of_centers)


def build_solutions_json(file, optimum, time_phase_1, time_phase_2, end_time, number_of_centers):
    data = {}

    data['file_name'] = file
    data['optimum'] = optimum
    data['time_phase_1'] = time_phase_1
    data['time_phase_2'] = time_phase_2
    data['total_time'] = end_time
    data['number_of_centers'] = number_of_centers

    json_data = json.dumps(data, indent=4, separators=(',\n', ': '))

    return json_data

def execute_phase_1(dat_file_name):
    start_time = time.time()

    abstract_model = generate_model_min_total_cost_vne_t_s_3index_phase_1()

    instance = abstract_model.create_instance(dat_file_name)

    opt.options["mipgap"] = mipgap_phase_1
    results = opt.solve(instance, tee=True)
    # results.write()

    time_phase_1 = time.time() - start_time

    print 'Network Total Cost after phase 1: ', instance.Price()
    print 'Time Phase 1: ', time_phase_1

    y = build_y_parameter(instance)

    return y

def execute_phase_2(dat_file_name, y):

    start_time = time.time()

    abstract_model_phase_2 = generateModel_milp_3index_fixed_centers_total_cost(y)

    instance_phase_2 = abstract_model_phase_2.create_instance(dat_file_name)

    opt.options["mipgap"] = mipgap_phase_2
    results = opt.solve(instance_phase_2, tee=True)

    time_phase_2 = time.time() - start_time

    print 'Network Total Cost after phase 2: ', instance_phase_2.Price()
    print 'Time Phase 2: ', time_phase_2

    return instance_phase_2

def calculate_v_objective(final_instance):
    model_v_objective = generate_model_v_objective(final_instance.n, final_instance.K,
                                                   final_instance.c, final_instance.L,
                                                   final_instance.vn, final_instance.vl,
                                                   final_instance.Vf, final_instance.Vfc,
                                                   final_instance.qout, final_instance.qin,
                                                   final_instance.b)

    instance_v_objective = model_v_objective.create_instance()
    opt = SolverFactory("gurobi")
    results = opt.solve(instance_v_objective, tee=True)

    print 'Min V price: ', instance_v_objective.Price()


def calculate_prices(inst):
    model = generate_model_calculate_prices(inst.n, inst.K, inst.a, inst.c, inst.b, inst.Price(), inst.L, inst.vn, inst.qout, inst.qin, inst.zin, inst.zout)
    instance_prices = model.create_instance()

    opt = SolverFactory("gurobi")
    results = opt.solve(instance_prices, tee=True)

    max_ = None
    for k in instance_prices.K:
        max_ = instance_prices.p[k].value
        break

    for k in instance_prices.K:
        max_ = max(instance_prices.p[k].value, max_)

    min_ = None
    for k in instance_prices.K:
        min_ = instance_prices.p[k].value
        break

    for k in instance_prices.K:
        min_ = min(instance_prices.p[k].value, min_)

    print 'Max Price: ', max_
    print 'Min Price: ', min_

    for k in instance_prices.K:
        n = 0
        n = instance_prices.p[k].value
        print 'K, ', k, ' price: ', n,
        cc = 0
        for l in instance_prices.VN[k]:
            cc = cc + instance_prices.qin[k, l] + instance_prices.qout[k, l]
        print 'Domanda: ', cc

    summ = 0
    for k in instance_prices.K:
        summ = summ + instance_prices.p[k].value

    print 'Prezzo finale: ', summ


    instance_prices.personal_price.display()
    instance_prices.remaning_discounted_price.display()
    for k in instance_prices.K:
        print sum( instance_prices.b[i,j] * instance_prices.sh[k,i,j] * instance_prices.c[i,j] for (i,j) in instance_prices.L )
        print '------------------'
        for (i,j) in instance_prices.L:
            print instance_prices.sh[k,i,j]




    return instance_prices

def calculate_number_of_used_centers(instance):

    number_of_centers = []

    for (k,i) in instance.Y:
        if instance.y[k,i] == 1:
            number_of_centers.append(i)

    number_of_centers = len(set(number_of_centers))

    print 'number_of_centers: ', number_of_centers, '\n'
    return number_of_centers

def build_y_parameter(instance):
    v = {}

    for k in instance.K:
        for l in instance.Vfc[k]:
            v[k,l] = instance.y[k,l].value
    return v