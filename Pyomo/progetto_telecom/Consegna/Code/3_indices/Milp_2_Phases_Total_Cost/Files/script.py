from milp_2_phases_total_cost  import *

# ----------------------------------------------------Main---------------------------------------------------- #

with open('../../Data/Json/Input/exact_data_files.json') as data_file:
    data = json.load(data_file)

output = {}

for network_name in data:

    output[network_name] = []

    for instance in data[network_name]:

        dat_file_name = instance['file_name']

        json_obj = main(dat_file_name)
        parsed = json.loads(json_obj)
        output[network_name].append(parsed)


    # Encode JSON data
    with open('../../Data/Json/Output/milp_2_phases_results.json', 'w') as f:
        json.dump(output, f, indent=4, sort_keys=True, separators=(',\n', ': '))

