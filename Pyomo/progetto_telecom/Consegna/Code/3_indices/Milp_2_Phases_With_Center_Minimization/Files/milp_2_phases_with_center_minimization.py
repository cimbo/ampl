# --------------------------------------------------------Header-------------------------------------------------------- #
from pyomo.opt import SolverFactory
import json
import time
from pyomo.environ import *
from pyomo.opt import SolverStatus, TerminationCondition
from milp_3index_phase1 import generateModel_milp_3index_relaxed
from minimize_centers_model_splittable import generateModel_reduce_centers_splittable
from oracle_centers_minimization_model import generateModel_oracle_centers_minimization
######################################################################
# SOLVER PREFERENCES
######################################################################

# -----Solver----- #
opt = SolverFactory("gurobi")
#opt.options["timelimit"] = 150

mipgap_phase_1 = 0.05
mipgap_splitable_centers_minimization = 0.03
mipgap_oracle_centers_minimization = 0.03


time_phase_1 = None
time_phase_2 = None
total_time = None

# ----------------------------------------------------Main---------------------------------------------------- #

def main(file):

    print file

    results = execute_2_phase_milp_with_centers_minimization_with_warm_starts(file)

    model_number_of_centers = results[0]
    n_ite = results[1]
    time_bisection_execution = results[2]
    objective_price = results[3]

    print

    return build_solutions_json(file, model_number_of_centers, n_ite, time_bisection_execution, objective_price)


def build_solutions_json(file, model_number_of_centers, n_ite, time_bisection_execution, objective_price):
    data = {}

    data['file_name'] = file
    data['number_of_centers'] = model_number_of_centers
    data['iteration'] = n_ite
    data['time_bisection_execution'] = time_bisection_execution
    data['final_v_reduced_centers'] = objective_price

    json_data = json.dumps(data, indent=4, separators=(',\n', ': '))

    return json_data

def execute_phase_1(dat_file_name):
    start_time = time.time()

    abstract_model = generateModel_milp_3index_relaxed()

    instance = abstract_model.create_instance(dat_file_name)

    opt.options["mipgap"] = mipgap_phase_1
    results = opt.solve(instance)#, tee=True)
    # results.write()

    number_of_centers = calculate_number_of_used_centers(instance)

    time_phase_1 = time.time() - start_time

    print 'V after phase 1: ', instance.Price()
    print 'number_of_centers: ', number_of_centers
    print 'Time Phase 1: ', time_phase_1

    y = build_y_parameter(instance)
    price = float(instance.Price())

    return (y, price)


def calculate_minimum_number_of_centers_splittable(file, v_min, y):

    start_time = time.time()

    model = generateModel_reduce_centers_splittable(v_min)
    instance = model.create_instance(file)

    instance = preset_warm_start(y, instance)

    opt.options["mipgap"] = mipgap_splitable_centers_minimization
    results = opt.solve(instance, warmstart=True, tee=True)

    end_time = time.time() - start_time

    lb_min_number_of_centers = int(round(instance.Number_of_centers()))

    print 'LB minimum number of centers: ', lb_min_number_of_centers
    print 'Time splittable centers minimization with warm start: ', end_time

    return (lb_min_number_of_centers, instance.y)


def calculate_number_of_used_centers(instance):

    number_of_centers = []

    for (k,i) in instance.Y:
        if instance.y[k,i] == 1:
            number_of_centers.append(i)

    number_of_centers = len(set(number_of_centers))

    print 'number_of_centers: ', number_of_centers, '\n'
    return number_of_centers

def build_y_parameter(instance):
    v = {}

    for k in instance.K:
        for l in instance.Vfc[k]:
            v[k,l] = instance.y[k,l].value
    return v

def preset_warm_start(y_relaxed_minimization, instance):

    for i in instance.Y:
        if y_relaxed_minimization[i] == 1:
            instance.y[i] = 1

    return instance


def execute_2_phase_milp_with_centers_minimization_with_warm_starts(file):
    
    start_time = time.time()

    #####################################################################
    # Phase 1 Milp
    #####################################################################
    
    results = execute_phase_1(file)

    y = results[0]
    v_min = results[1]

    #####################################################################
    # Minimize Number of center in the splittable case with a warm start
    #####################################################################
    results = calculate_minimum_number_of_centers_splittable(file, v_min, y)

    lb_min_number_of_centers = results[0]
    y_relaxed_minimization = results[1]

    #####################################################################
    # Look for a feasible solution with a warm start
    # We start looking for solutions that have:
    # number_of_centers = lower_bound_number_of_centers
    # If we find a solution it will be optimum
    # If not we we try:
    # number_of_centers = lower_bound_number_of_centers + 1
    # or
    # we can improve the mipgap for the objective
    #####################################################################

    opt.options["mipgap"] = mipgap_oracle_centers_minimization

    # Il meno uno per quando entri nel ciclo
    Sc = int(lb_min_number_of_centers - 1)

    model_number_of_centers = float('infinity')

    model_oracle_minimization = generateModel_oracle_centers_minimization(Sc)
    instance = model_oracle_minimization.create_instance(file)

    instance = preset_warm_start(y_relaxed_minimization, instance)

    n_ite = 0

    objective_price = v_min

    while Sc < model_number_of_centers:

        # Se non hai trovato qualcosa di buono vicino all'ottimo prova ad allontanarti
        Sc = Sc + 1

        instance.del_component(instance.Sc)
        instance.add_component('Sc', Constraint(expr=sum(instance.w[i] for i in instance.N) <= Sc))
        n_ite = n_ite + 1

        results = opt.solve(instance, tee=True, warmstart=True)
        if (results.solver.status == SolverStatus.ok) and (
                    results.solver.termination_condition == TerminationCondition.optimal):
            model_number_of_centers = calculate_model_number_of_centers(instance)
            objective_price = instance.Price()
        elif (results.solver.termination_condition == TerminationCondition.infeasible):
            # We impose infinity so that we go on with the loop
            model_number_of_centers = float('infinity')
            print 'Infeasible'
        else:
            # Something else is wrong
            print 'Error Bisection execution function'

    end_time = time.time() - start_time

    print 'Minimum number of centers: ', model_number_of_centers
    print 'Numer of iteration: ', n_ite
    print 'Remember that the v_min was: ', v_min
    print 'Final v with reduced centers: ', objective_price
    print 'Total execution time: ', end_time

    return (model_number_of_centers, n_ite, end_time, objective_price)


def calculate_model_number_of_centers(instance):

    count = 0

    for i in instance.N:
        if instance.w[i].value == 1:
            count = count + 1

    return int(count)