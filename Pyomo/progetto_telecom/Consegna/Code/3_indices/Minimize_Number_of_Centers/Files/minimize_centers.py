# --------------------------------------------------------Header-------------------------------------------------------- #
from __future__ import division
from pyomo.opt import SolverFactory
import json
import time
# Import dei modelli astratti che poi vengono utilizzati nel file principale #
#------- .mod Files (Di Ampl) -------#
from minimize_centers_model_unsplittable import generateModel_reduce_centers_unsplittable
######################################################################
# SOLVER PREFERENCES
######################################################################

# -----Solver----- #
opt = SolverFactory("gurobi")
opt.options["mipgap"] = 0.03

#mipgap_reduce = 0.03

time_reduce = None


# ----------------------------------------------------Main---------------------------------------------------- #

def main(file, v_min):

    print file

    start_time = time.time()

    results = execute_minimize_used_centers(file, v_min)

    time_reduce = time.time() - start_time

    print 'Total_time: ', time_reduce , '\n'

    return build_solutions_json(file, results, time_reduce)


# ----------------------------------------------------End---------------------------------------------------- #



def execute_minimize_used_centers(dat_file_name, v_min):

    model = generateModel_reduce_centers_unsplittable(v_min)
    instance = model.create_instance(dat_file_name)
    results = opt.solve(instance)#,tee=True)
    #results.write()

    number_of_centers = instance.Number_of_centers()
    number_of_centers_LB = results.problem._list[0]['Lower bound']
    print 'Number of centers: ', number_of_centers
    print 'Lower Bound: ' , number_of_centers_LB
    print 'Gap Computazionale %: ', ((number_of_centers / number_of_centers_LB) - 1) * 100


    return (number_of_centers,number_of_centers_LB)


def build_solutions_json(file, results, end_time):
    data = {}

    data['file_name'] = file
    data['number_of_centers'] = results[0]
    data['number_of_centers_LB'] = results[1]
    data['Gap_Computazionale_%'] = (((results[0] / results[1]) - 1) * 100 , 2)
    data['total_time'] = end_time

    json_data = json.dumps(data, indent=4, separators=(',\n', ': '))

    return json_data




