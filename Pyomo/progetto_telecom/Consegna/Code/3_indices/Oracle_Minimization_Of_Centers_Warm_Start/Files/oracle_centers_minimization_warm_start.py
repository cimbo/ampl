# --------------------------------------------------------Header-------------------------------------------------------- #
from __future__ import division
import json
import time
from pyomo.environ import *
from pyomo.opt import SolverStatus, TerminationCondition
from oracle_centers_minimization_model import generateModel_oracle_centers_minimization
from minimize_centers_model_splittable import generateModel_reduce_centers_splittable
######################################################################
# SOLVER PREFERENCES
######################################################################

# -----Solver----- #
opt = SolverFactory("gurobi")
opt.options["mipgap"] = 0.03
#opt.options["timelimit"] = 7200

time_reduce = None


# ----------------------------------------------------Main---------------------------------------------------- #

def main(file, v_min):

    print file

    results = execute_oracle_centers_minimization_warm_start(file, v_min)

    #results = (model_number_of_centers, n_ite, time_time_oracle_centers_minimization_warm_start_execution)

    model_number_of_centers = results[0]
    n_ite = results[1]
    time_time_oracle_centers_minimization_warm_start_execution = results[2]
    objective_price = results[3]

    print

    return build_solutions_json(file, model_number_of_centers, n_ite, time_time_oracle_centers_minimization_warm_start_execution, objective_price)


# ----------------------------------------------------End---------------------------------------------------- #

def execute_oracle_centers_minimization_warm_start(file, v_min):

    start_time_oracle_centers_minimization_warm_start = time.time()

    results = calculate_number_of_centers_splittable(file, v_min)

    lb_min_number_of_centers = results[0]
    y_relaxed_minimization = results[1]

    #Il meno uno per quando entri nel ciclo
    Sc = int(lb_min_number_of_centers - 1)

    model_number_of_centers = float('infinity')

    model_oracle_centers_minimization_warm_start = generateModel_oracle_centers_minimization(Sc)
    instance = model_oracle_centers_minimization_warm_start.create_instance(file)

    instance = preset_warm_start(y_relaxed_minimization, instance)

    n_ite = 0

    objective_price = v_min

    while Sc < model_number_of_centers:

        #Se non hai trovato qualcosa di buono vicino all'ottimo prova ad allontanarti
        Sc = Sc + 1

        instance.del_component(instance.Sc)
        instance.add_component('Sc', Constraint(expr=sum(instance.w[i] for i in instance.N) <= Sc))
        n_ite = n_ite + 1

        results = opt.solve(instance, tee=True, warmstart=True)
        if (results.solver.status == SolverStatus.ok) and (
            results.solver.termination_condition == TerminationCondition.optimal):
            model_number_of_centers = calculate_model_number_of_centers(instance)
            objective_price = instance.Price()
        elif (results.solver.termination_condition == TerminationCondition.infeasible):
                # We impose infinity so that we go on with the loop
                model_number_of_centers = float('infinity')
                print 'Infeasible'
        else:
            # Something else is wrong
            print 'Error Bisection execution function'


    end_execute_time_oracle_centers_minimization_warm_start = time.time() - start_time_oracle_centers_minimization_warm_start

    print 'Minimum number of centers: ', model_number_of_centers
    print 'Numer of iteration: ', n_ite
    print 'Remember that the v_min was: ', v_min
    print 'Final v with reduced centers: ', objective_price
    print 'Total time oracle centers minimization warm start execution: ', end_execute_time_oracle_centers_minimization_warm_start

    return (model_number_of_centers, n_ite, end_execute_time_oracle_centers_minimization_warm_start, objective_price)


def calculate_number_of_centers_splittable(file, v_min):

    start_time = time.time()

    model = generateModel_reduce_centers_splittable(v_min)
    instance = model.create_instance(file)
    results = opt.solve(instance)

    end_time = time.time() - start_time

    lb_min_number_of_centers = int(round(instance.Number_of_centers()))

    print 'LB minimum number of centers: ', lb_min_number_of_centers
    print 'Time: ', end_time

    return (lb_min_number_of_centers, instance.y)



def calculate_model_number_of_centers(instance):

    count = 0

    for i in instance.N:
        if instance.w[i].value == 1:
            count = count + 1

    return int(count)

def preset_warm_start(y_relaxed_minimization, instance):

    for i in instance.Y:
        if y_relaxed_minimization[i].value == 1:
            instance.y[i] = 1

    return instance


def build_solutions_json(file, model_number_of_centers, n_ite, time_oracle_centers_minimization_warm_start_execution, objective_price):
    data = {}

    data['file_name'] = file
    data['number_of_centers'] = model_number_of_centers
    data['iteration'] = n_ite
    data['time_oracle_centers_minimization_warm_start_execution'] = time_oracle_centers_minimization_warm_start_execution
    data['final_v_reduced_centers'] = objective_price

    json_data = json.dumps(data, indent=4, separators=(',\n', ': '))

    return json_data




