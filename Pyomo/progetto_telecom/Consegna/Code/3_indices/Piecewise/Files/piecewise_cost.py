# A simple example illustrating a piecewise
# representation of the function Z(X)
#
#          / c - 1 , 0 <= X <= 5
#  C(X) >=|
#          \  c ,  5 < X <= Inf
#
from __future__ import division
from pyomo.environ import *
infinity = float('inf')
# Define the function
# Just like in Pyomo constraint rules, a Pyomo model object
# must be the first argument for the function rule
def f(model,intervallo):
    print intervallo
    if intervallo <= model.limit:
        return intervallo
    else:
        return model.c


Topx = 1000 # range of x variables

model_cost = ConcreteModel()
model_cost.X = Var(bounds=(0, Topx))
model_cost.C = Var()
model_cost.c = Param(initialize=92500)
model_cost.gamma = Param(initialize=0.2)
model_cost.limit = Param(initialize=500 * (1 - model_cost.gamma))

PieceCnt = 100
bpts = []
for i in range(PieceCnt+1):
    bpts.append(float((i * model_cost.limit) / PieceCnt))
bpts.append(Topx)


# See documentation on Piecewise component by typing
# help(Piecewise) in a python terminal after importing coopr.pyomo
model_cost.con = Piecewise(model_cost.C, model_cost.X,  # range and domain variables
	                      pw_pts=bpts, pw_constr_type='EQ', f_rule=f)

# The default piecewise representation implemented by Piecewise is SOS2.
# Note, however, that no SOS2 variables will be generated since the
# check for convexity within Piecewise automatically simplifies the constraints
# when a lower bounding convex function is supplied. Adding 'force_pw=True'
# to the Piecewise argument list will cause the original piecewise constraints
# to be used even when simplifications can be applied.

model_cost.obj = Objective(expr=model_cost.C, sense=minimize)