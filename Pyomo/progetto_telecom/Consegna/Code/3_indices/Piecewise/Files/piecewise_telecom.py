#########################################
#    Piecewise Telecom
#########################################
from __future__ import division
from pyomo.environ import *
from math import *
infinity = float('inf')

#Genera il modello matematico
def generate_model_min_total_cost_vne_t_s_3index():

    model = AbstractModel()

    ####################################
    # PARAMS & SETS
    ####################################

    model.k = Param()

    # Set of requests
    model.K = RangeSet(1, model.k)

    model.n = Param()

    # Sets of physical nodes and arcs
    model.N = RangeSet(1, model.n)

    model.L = Set(within=model.N * model.N)

    model.Pk = Param(model.K)

    #####################################
    # Physical nodes and links capacities
    #####################################

    # capacities of arcs
    model.a = Param(model.L, within=NonNegativeReals)

    # capacities of nodes
    model.aa = Param(model.N, within=NonNegativeReals)

    # distance
    model.dist = Param(model.L, within=NonNegativeReals)

    # cost(=dist*1000)
    model.c = Param(model.L, within=NonNegativeReals)

    # activation cost
    model.cc = Param(model.N, within=NonNegativeReals)

    ########################################
    # Sets of virtual nodes, one per request
    ########################################

    # number of nodes
    model.vn = Param(model.K)

    # Virtual request traffic matrix (nominal)


    def VN_Init(model, kk):
        retval = []
        for jj in range(1, model.vn[kk] + 1):
            retval.append(jj)
        return retval

    # helper set
    model.VN = Set(model.K, initialize=VN_Init)

    def VNl_Init(model, kk):
        retval = []
        for jj in range(2, model.vn[kk] + 1):
            retval.append(jj)
        return retval

    # set of leaves
    model.VNl = Set(model.K, initialize=VNl_Init)

    # number of arcs of each VN(the same for all)
    model.D = 6

    #HELPER SET
    def KN_Init(model):

        dict = []

        for k in model.K:
            for l in model.VN[k]:
                dict.append((k,l))

        return dict


    model.KN = Set(dimen=2, initialize=KN_Init)

    model.qout = Param(model.KN, default=0)

    model.qin = Param(model.KN, default=0)

    # Location of leaves

    #IN THEORY WITHIN model.N . WHEN 0 means that the parameter is not defined
    model.vl = Param(model.KN, default=0)

    # Set of nodes considered as leaves for different VNs

    model.Vf = Set(model.K, within= model.N)

    def Vfc_init(model, kk):
        return model.N.difference(model.Vf[kk])

    model.Vfc = Set(model.K, initialize=Vfc_init)

    ########################################
    # param location
    ########################################

    # HELPER SET
    def LOC_Init(model):

        dict = []

        for k in model.K:
            for l in model.VNl[k]:
                for ip in model.Vf[k]:
                    dict.append((k, l, ip))

        return dict

    model.LOC = Set(dimen=3, initialize=LOC_Init)


    #kk in K, VNl[kk], ip in Vf[kk]
    model.loc = Param(model.LOC, default=0)

    ####################################
    # VARS
    ####################################

    # HELPER SET
    def Y_Init(model):

        dict = []

        for k in model.K:
            for l in model.Vfc[k]:
                dict.append((k,l))
        return dict

    model.Y = Set(dimen=2, initialize=Y_Init)

    # One if a virtual ii from request rr is mapped onto physical node jj (within V_local)
    model.y = Var(model.Y, within=Binary)

    # One if at least one centre is located onto node i
    model.w = Var(model.N, within=Binary)

    # Multicommodity flow for virtual request rr between virtual nodes ii and jj on physical arc (i,j)
    # Note: it's defined only for strictly positive demands

    # HELPER SET
    def Z_Init(model):

        dict = []

        for k in model.K:
            for l in model.VNl[k]:
                for (i,j) in model.L:
                    dict.append((k, l, i, j))

        return dict


    #{kk in K, jj in VNl[kk], (i,j) in L
    model.Z = Set(dimen=4, initialize=Z_Init)

    model.zout = Var(model.Z, within=NonNegativeReals, bounds=(0,1)) #considero (1,jj)

    model.zin = Var(model.Z, within=NonNegativeReals, bounds=(0,1)) # considero (jj,1)

    # Capacity added to arc (i,j)
    model.b = Var(model.L, within=NonNegativeReals)

    model.f = Var(model.L, within=NonNegativeReals)

    def f_c(model,i,j):
        return model.f[i,j] == sum(model.qout[kk, im] * model.zout[kk, im, i, j] + model.qin[kk, im] * model.zin[kk, im, i, j] for kk in model.K for im in model.VNl[kk])
    model.f_c = Constraint(model.L, rule=f_c)

    model.v = Var(model.L, within=NonNegativeReals)

    model.gamma = Param(initialize=0.2)

    model.limit = Param(initialize=500 * (1 - model.gamma))

    PieceCnt = 3

    def DistanzaTraDuePunti(x1, y1, x2, y2):
        dx = x2 - x1
        dy = y2 - y1
        DistQuadrata = dx ** 2 + dy ** 2
        Risultato = sqrt(DistQuadrata)
        return Risultato

    def init_list_bpts(model):

        list_bpts = {}

        for (i,j) in model.L:

            bpts = []

            lis = []
            for (h,m) in model.L:
                lis.append(model.c[h,m])

            min_ = min(lis)

            #print min_

            a = DistanzaTraDuePunti(0,0,400,model.c[i,j])

            sin_beta = float(min_/a)
            cos_beta = float(400/a)

            tan_b = float(sin_beta/cos_beta)

            #print 'Alpha: ', alpha

            for ii in range(PieceCnt):
                level = float((ii * model.limit) / PieceCnt)
                print level
                if ii ==0:
                    bpts.append((float((ii * model.limit) / PieceCnt) * tan_b,0))
                else:
                    m_old = bpts[-1][0]
                    b_old = bpts[-1][1]

                    m_new = float((ii * model.limit) / PieceCnt) * tan_b
                    bpts.append((m_new, (-m_new * level + (m_old * level + b_old) )))


            level = model.limit
            m_old = bpts[-1][0]
            b_old = bpts[-1][1]
            m_new = model.c[i,j]
            bpts.append((model.c[i,j], (-m_new * level + (m_old * level + b_old))))

            print (i,j),': ',bpts
            list_bpts[i,j] = bpts

        return list_bpts
    model.list_bpts = Param(model.L, initialize=init_list_bpts)

    def init_pw_c_set(model):
        dict = []
        for (i,j) in model.L:
            for l in range(PieceCnt + 1):
                dict.append((i,j,l))
        return dict

    model.pw_c_set = Set(dimen=3,initialize=init_pw_c_set)

    def pw_c(model,i,j,l):
        m = model.list_bpts[i,j][l][0]
        b = model.list_bpts[i,j][l][1]
        return model.v[i, j] >= m * model.f[i,j] + b
    model.pw_c = Constraint(model.pw_c_set, rule=pw_c)

    ###################################################
    # OBJECTIVE FUNCTION
    ###################################################

    def Price(model):
        return sum(model.v[i,j] for (i,j) in model.L)
    model.Price = Objective(rule=Price, sense=minimize)

    ####################################
    # CONSTRAINTS
    ####################################

    # ---------
    # EMBEDDING
    # ---------
    # A virtual node assigned to exactly a single physical node
    # (since each VN is accepted in this case)

    # Arc capacities constraint
    def arc_capacities(model, i, j):
        return sum(model.qout[kk, im] * model.zout[kk, im, i, j] + model.qin[kk, im] * model.zin[kk, im, i, j] for kk in model.K for im in model.VNl[kk]) <= 0.8 * (model.a[i, j] + model.b[i, j])
    model.arc_capacities = Constraint(model.L, rule=arc_capacities)

    # HELPER SET {kk in K, ip in Vfc[kk], im in VNl[kk]}
    def FLOW1_Init(model):

        dict = []

        for kk in model.K:
            for ip in model.Vfc[kk]:
                for im in model.VNl[kk]:
                    dict.append((kk, ip, im))
        return dict

    model.FLOW1 = Set(dimen=3, initialize=FLOW1_Init)



    # HELPER SET { kk in K, ip in Vfc[kk], im in VNl[kk] }
    def FLOW2_Init(model):

        dict = []

        for kk in model.K:
            for ip in model.Vf[kk]:
                for im in model.VNl[kk]:
                    dict.append((kk, ip, im))
        return dict

    model.FLOW2 = Set(dimen=3, initialize=FLOW2_Init)

    def LIn_init(model, node):
        retval = []
        for (i, j) in model.L:
            if j == node:
                retval.append(i)
        return retval

    model.LIn = Set(model.N, initialize=LIn_init)

    def LOut_init(model, node):
        retval = []
        for (i, j) in model.L:
            if i == node:
                retval.append(j)
        return retval

    model.LOut = Set(model.N, initialize=LOut_init)

    # centres
    def flows1a(model, kk, ip, im):
        return sum(model.zout[kk, im, ip, j] for j in model.LOut[ip]) - sum(model.zout[kk, im, j, ip] for j in model.LIn[ip]) == model.y[kk, ip]
    model.flows1a = Constraint(model.FLOW1, rule=flows1a)

    # leaves
    def flows2a(model, kk, ip, im):
        if model.loc[kk,im,ip] == 1:
            return sum(model.zout[kk, im, ip, j] for j in model.LOut[ip]) - sum(model.zout[kk, im, j, ip] for j in model.LIn[ip]) == -1
        else:
            return Constraint.Skip
    model.flows2a = Constraint(model.FLOW2, rule=flows2a)

    def flows3a(model, kk, ip, im):
        if model.loc[kk, im, ip] == 0:
            return sum(model.zout[kk, im, ip, j] for j in model.LOut[ip]) - sum(
                model.zout[kk, im, j, ip] for j in model.LIn[ip]) == 0
        else:
            return Constraint.Skip
    model.flows3a = Constraint(model.FLOW2, rule=flows3a)

    def flows1b(model, kk, ip, im):
       return sum(model.zin[kk, im, ip, j] for j in model.LOut[ip]) - sum(model.zin[kk, im, j, ip] for j in model.LIn[ip]) == -model.y[kk, ip]
    model.flows1b = Constraint(model.FLOW1, rule=flows1b)

    def flows2b(model, kk, ip, im):
        if model.loc[kk, im, ip] == 1:
            return sum(model.zin[kk, im, ip, j] for j in model.LOut[ip]) - sum(model.zin[kk, im, j, ip] for j in model.LIn[ip]) == 1
        else:
            return Constraint.Skip
    model.flows2b = Constraint(model.FLOW2, rule=flows2b)

    def flows3b(model, kk, ip, im):
        if model.loc[kk, im, ip] == 0:
            return sum(model.zin[kk, im, ip, j] for j in model.LOut[ip]) - sum(model.zin[kk, im, j, ip] for j in model.LIn[ip]) == 0
        else:
            return Constraint.Skip
    model.flows3b = Constraint(model.FLOW2, rule=flows3b)

    def simmetry(model, i, j):
        return model.b[i, j] == model.b[j, i]
    model.simmetry = Constraint(model.L, rule=simmetry)

    return model






