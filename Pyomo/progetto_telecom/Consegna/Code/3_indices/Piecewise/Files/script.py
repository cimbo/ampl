# --------------------------------------------------------Header-------------------------------------------------------- #
from pyomo.environ import *
from pyomo.opt import SolverFactory
from pyomo.core.base import *
# Import dei modelli astratti che poi vengono utilizzati nel file principale #
#------- .mod Files (Di Ampl) -------#
from piecewise_telecom import generate_model_min_total_cost_vne_t_s_3index
from milp_3index_phase2 import generateModel_milp_3index_fixed_centers
from calculate_v_objective import generate_model_v_objective

# ----------------------------------------------------Main---------------------------------------------------- #

# Import dei file di dati che poi vengono utilizzati nel file principale per creare i modelli concreti #
#------- .dat Files (Di Ampl) -------#

def main():
    dat_file_name = '../../Data/Networks/Germany/germany_s_t_m_14.dat'

    print dat_file_name
    print

    abstract_model = generate_model_min_total_cost_vne_t_s_3index()

    instance = abstract_model.create_instance(dat_file_name)

    # -----Solver----- #
    opt = SolverFactory("gurobi")
    opt.options["timelimit"] = 3100
    opt.options["mipgap"] = 1


    results = opt.solve(instance, tee=True)
    results.write()

    print "# ----------------------------------------------------------"

    #instance.f.display()
    #instance.v.display()

    tot_cost = 0
    for (i,j) in instance.L:
        tot_cost = tot_cost + instance.b[i,j].value * instance.c[i,j]

    print 'Total cost: ', tot_cost

    abstract_model = generateModel_milp_3index_fixed_centers(instance.y)
    instance = abstract_model.create_instance(dat_file_name)

    results = opt.solve(instance, tee=True)
    results.write()

    calculate_v_objective(instance)



    #instance.f_r.display()



def calculate_v_objective(final_instance):

    model_v_objective = generate_model_v_objective(final_instance.n, final_instance.K,
                                                   final_instance.c, final_instance.L,
                                                   final_instance.vn, final_instance.vl,
                                                   final_instance.Vf, final_instance.Vfc,
                                                   final_instance.qout, final_instance.qin,
                                                   final_instance.b)

    instance_v_objective = model_v_objective.create_instance()
    opt = SolverFactory("gurobi")
    results = opt.solve(instance_v_objective, tee=True)

    print 'Min V price: ', instance_v_objective.Price()


main()