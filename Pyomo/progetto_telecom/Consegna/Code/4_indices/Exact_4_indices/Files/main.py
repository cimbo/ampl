# --------------------------------------------------------Header-------------------------------------------------------- #
from pyomo.opt import SolverFactory
import json
import time
from min_price_vne_t_s_4index import generate_model_min_price_vne_t_s_4index
from functions_4index import calculate_number_of_used_centers_4index

######################################################################
# SOLVER PREFERENCES
######################################################################

# -----Solver----- #
opt = SolverFactory("gurobi")
opt.options["mipgap"] = 0.05
#opt.options["timelimit"] = 150

# ----------------------------------------------------Main---------------------------------------------------- #

def main(file):

    results = solve_exact_min_price_vne_t_s_4index_model(file)

    #print_results(file, results)

    return build_solutions_json(file, results)


def solve_exact_min_price_vne_t_s_4index_model(file):
    start_time = time.time()

    abstract_model = generate_model_min_price_vne_t_s_4index()
    instance = abstract_model.create_instance(file)

    for k in instance.K:
        array = []
        print 'k: ', k , ' : '
        for l in instance.VN[k]:
            for i in instance.N:
                if instance.y_fixed[k,l,i] == 1:
                    array.append(i)
        print set(array)

    # tee = True is to see the output of the solver
    #results = opt.solve(instance,tee=True)

    #end_time = time.time() - start_time

    #optimum = instance.Price()

    #number_of_centers = calculate_number_of_used_centers_4index(instance)

    return (0, 0, 0)
    #return (optimum , number_of_centers, end_time)


def print_results(file, results):
    print file
    print 'Price v: ', results[0]
    print 'Number of used centers: ', results[1]
    print 'Execution time: ', results[2] , '\n'



def build_solutions_json(file, results):
    data = {}

    data['file_name'] = file
    data['optimum'] = results[0]
    data['number_of_centers'] = results[1]
    data['exact_time'] = results[2]

    json_data = json.dumps(data, indent=4, separators=(',\n', ': '))

    return json_data







