import time
from flows_local_search import generate_model_local_search_flows

def calculate_number_of_used_centers_4index(instance):
    """
        Args:
            instance (instance): The instance of which we want to calculate the number of used center.

        Returns:
            int: The return value. The number of used centers.
    """

    number_of_centers = []

    for (k,l,i) in instance.Y:
        if l == 1 and instance.y[k,l,i] == 1:
            number_of_centers.append(i)

    number_of_centers = len(set(number_of_centers))

    return number_of_centers










########################################################################################################################
# LOCAL SEARCH
########################################################################################################################

def execute_local_search(instance, best, y, zin, zout, max_arch_swap, file, solver):
    """
        Args:
            best (float): Best objective value found before the local search
            y (dict): The Mapping of the VNs's centers
            zin (dict): The mapping of the zin valriable before the local search
            zout (dict): The mapping of the zout valriable before the local search

        Returns:
            (price, zin, zout)
    """

    set_S_zin = build_set_S(instance, zin)
    set_Not_S_zin = build_set_NS(instance, zin)

    set_S_zout = build_set_S(instance, zout)
    set_Not_S_zout = build_set_NS(instance, zout)

    start_time_local_search = time.time()

    model = generate_model_local_search_flows(y, best, set_S_zin, set_Not_S_zin, set_S_zout, set_Not_S_zout,
                                                   max_arch_swap)
    instance_local_search = model.create_instance(file)
    results = solver.solve(instance_local_search)

    end_time_local_search = time.time() - start_time_local_search

    print 'Local search with maximum: ', max_arch_swap, ' result v: ', instance_local_search.Price()
    print 'Local search Time: ', end_time_local_search

    return (instance_local_search.Price(), instance_local_search.zin, instance_local_search.zout)

def build_set_S(instance, z):
    s = set()

    for i in instance.Z:
        if z[i] == 1:
            s.add(i)
    return s


def build_set_NS(instance, z):
    s = set()

    for i in instance.Z:
        if z[i] == 0:
            s.add(i)
    return s

def build_set_C(instance, y):
    c = set()

    for i in instance.Y:
        if y[i] == 1:
            c.add(i)

    return c


def build_set_NC(instance, y):
    nc = set()

    for i in instance.Y:
        if y[i] == 0:
            nc.add(i)

    return nc
