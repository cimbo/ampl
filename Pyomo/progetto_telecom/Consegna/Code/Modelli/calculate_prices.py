####################################
#       Greedy Objective
####################################
from __future__ import division
from pyomo.environ import *

infinity = float('inf')

#Genera il modello matematico
def generate_model_calculate_prices(n_param, k_set, a_param, c_param, b_param, B, L_set, vn_param, qout_param, qin_param, zin_param, zout_param):

    model = AbstractModel()

    ####################################
    # PARAMS & SETS
    ####################################

    # Set of requests
    model.K = Set(initialize=k_set)

    model.n = Param(initialize=n_param)

    # Sets of physical nodes and arcs
    model.N = RangeSet(1, model.n)

    model.L = Set(within=model.N * model.N, initialize=L_set)

    # capacities of arcs
    def init_a(model, i, j):
        return a_param[i, j]
    model.a = Param(model.L, within=NonNegativeReals, initialize=init_a)

    #Total cost to pay
    model.B = Param(initialize=B)

    # cost(=dist*1000)
    # residual capacity
    def init_c(model, i, j):
        return c_param[i, j]
    model.c = Param(model.L, within=NonNegativeReals, initialize=init_c)

    def init_vn(model, k):
        return vn_param[k]
    # number of nodes VN
    model.vn = Param(model.K, initialize=init_vn)

    def VN_Init(model, kk):

        retval = []
        for jj in range(1, model.vn[kk] + 1):
            retval.append(jj)
        return retval

    # helper set
    model.VN = Set(model.K, initialize=VN_Init)

    def VNl_Init(model, kk):
        retval = []
        for jj in range(2, model.vn[kk] + 1):
            retval.append(jj)
        return retval

    # set of leaves
    model.VNl = Set(model.K, initialize=VNl_Init)

    #HELPER SET
    def KN_Init(model):

        dict = []

        for k in model.K:
            for l in model.VN[k]:
                dict.append((k,l))

        return dict


    model.KN = Set(dimen=2, initialize=KN_Init)

    def qout_init(model,k,l):
        return qout_param[k,l]
    model.qout = Param(model.KN, default=0, initialize=qout_init)

    def qin_init(model, k, l):
        return qin_param[k, l]
    model.qin = Param(model.KN, default=0, initialize=qin_init)

    # Price paid by VN kk to activate node i as a centre
    # var pc{kk in K, i in N}>=0;

    # Capacity added to arc (i,j)
    def init_b(model,i,j):
        return b_param[i,j]
    model.b = Param(model.L, initialize=init_b)

    # HELPER SET
    def Z_Init(model):

        dict = []

        for k in model.K:
            for l in model.VNl[k]:
                for (i,j) in model.L:
                    dict.append((k, l, i, j))

        return dict


    #{kk in K, jj in VNl[kk], (i,j) in L
    model.Z = Set(dimen=4, initialize=Z_Init)

    def init_zout(model, k, l, i, j):
        return zout_param[k, l, i, j]
    model.zout = Param(model.Z, initialize=init_zout) #considero (1,jj)

    def init_zin(model, k, l, i, j):
        return zin_param[k, l, i, j]
    model.zin = Param(model.Z, initialize=init_zin) # considero (jj,1)

    def init_re(model, i, j):
        if sum(model.qout[kk, im] * model.zout[kk, im, i, j] + model.qin[kk, im] * model.zin[kk, im, i, j] for kk in model.K for im in model.VNl[kk]) <= 0.8 * model.a[i, j]:
            return 0
        else:
            return 1
    #If that arch was expanded for a real demand and not for simmetry
    model.re = Param(model.L, initialize=init_re)

    def shareholding(model, k, i, j):
        if sum(model.qout[kk, im] * model.zout[kk, im, i, j] + model.qin[kk, im] * model.zin[kk, im, i, j] for kk in model.K for im in model.VNl[kk]) == 0:
            return 0
        else:
            return (sum(model.qout[k, im] * model.zout[k, im, i, j] + model.qin[k, im] * model.zin[k, im, i, j] for im in model.VNl[k]) /
                                   sum(model.qout[kk, im] * model.zout[kk, im, i, j] + model.qin[kk, im] * model.zin[kk, im, i, j] for kk in model.K for im in model.VNl[kk])) * model.re[i,j]
    model.sh = Param(model.K, model.L, initialize=shareholding)

    def init_total_demand(model):
        return sum(model.qin[k,l] + model.qout[k,l] for (k,l) in model.KN)
    model.total_demand = Param(initialize=init_total_demand)

    model.discount_percentage = Param(initialize=0.0000001)

    def discount_percentage_c(model):
        return (0.00000000000001, model.discount_percentage, 1)
    model.discount_percentage_c = Constraint(rule=discount_percentage_c)

    def init_d(model,k):
        return ( sum(model.qin[k,l] + model.qout[k,l] for l in model.VN[k]) / model.total_demand ) * model.discount_percentage
    model.d = Param(model.K, initialize=init_d)

    def init_tot_d(model):
        return sum(model.d[k] for k in model.K)
    model.tot_d = Param(initialize=init_tot_d)

    def init_tot_d2(model):
        return sum( (1 - (model.d[kk] / model.tot_d) ) for kk in model.K)
    model.tot_d2 = Param(initialize=init_tot_d2)

    def init_d2(model,k):
        return (1 - (model.d[k] / model.tot_d)) / model.tot_d2
    model.d2 = Param(model.K, initialize=init_d2)

    #Price to pay for each VN
    model.p = Var(model.K, within=NonNegativeReals)

    def personal_price(model, k):
        return (sum( model.b[i,j] * model.sh[k,i,j] * model.c[i,j] for (i,j) in model.L ) + (sum(model.b[i,j] * (1 - model.re[i,j]) * model.c[i,j] for (i,j) in model.L) / len(model.K)) ) * (1 - model.d[k])
    model.personal_price = Param(model.K, initialize=personal_price)

    def remaning_discounted_price(model, k):
        return (model.B - sum( model.personal_price[k] for k in model.K ) ) * model.d2[k]
    model.remaning_discounted_price = Param(model.K, initialize=remaning_discounted_price)

    model.base_price = Var(within=NonNegativeReals, bounds=(0, None))

    # Maximum unitary price
    #model.v = Var(within=NonNegativeReals)

    ####################################
    # OBJECTIVE FUNCTION
    ####################################

    def Price(model):
        return sum(model.p[k] for k in model.K)
    model.Price = Objective(rule=Price, sense=minimize)

    ####################################
    # CONSTRAINTS
    ####################################

    def cover_total_cost(model):
        return sum(model.p[k] for k in model.K)  >= model.B
    model.cover_total_cost = Constraint(rule=cover_total_cost)


    # Minimum price
    def price_structure(model, k):
        return model.p[k] >= model.personal_price[k] + model.remaning_discounted_price[k]
    model.price_structure = Constraint(model.K, rule=price_structure)


    return model

