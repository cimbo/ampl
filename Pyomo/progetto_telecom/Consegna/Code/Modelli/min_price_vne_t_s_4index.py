#########################################
#    MIN PRICE VNE T S 4-INDEX PROBLEM
#########################################
from __future__ import division
from pyomo.environ import *

infinity = float('inf')

#Genera il modello matematico
def generate_model_min_price_vne_t_s_4index():

    model = AbstractModel()

    ####################################
    # PARAMS & SETS
    ####################################

    model.k = Param()

    model.K = RangeSet(1, model.k)

    model.n = Param()

    # Sets of physical nodes and arcs
    model.N = RangeSet(1, model.n)

    model.L = Set(within=model.N * model.N)

    #####################################
    # Physical nodes and links capacities
    #####################################

    # capacities of arcs
    model.a = Param(model.L, within=NonNegativeReals)

    # capacities of nodes
    model.aa = Param(model.N, within=NonNegativeReals)

    # distance
    model.dist = Param(model.L, within=NonNegativeReals)

    # cost(=dist*1000)
    model.c = Param(model.L, within=NonNegativeReals)

    # activation cost
    model.cc = Param(model.N, within=NonNegativeReals)

    ########################################
    # Sets of virtual nodes, one per request
    ########################################

    # number of nodes
    model.vn = Param(model.K)

    def VN_Init(model, kk):
        retval = []
        for jj in range(1, model.vn[kk] + 1):
            retval.append(jj)
        return retval

    # helper set
    model.VN = Set(model.K, initialize=VN_Init)

    model.Pk = Param(model.K)

    def AN_init(model):

        dict = []

        for kk in model.K:
            for ll in model.VN[kk]:
                for mm in model.VN[kk]:
                    dict.append((kk, ll, mm))

        return dict

    model.AN = Set(dimen=3, initialize=AN_init)

    # Binary matrix: an[kk,ll,mm] = 1
    # if nodes ll and mm in request kk are origin-destination pairs
    model.an = Param(model.AN, default=0, within=Binary)

    def Q_init(model):

        dict = []

        for kk in model.K:
            for ll in model.VN[kk]:
                for mm in model.VN[kk]:
                    if model.an[kk, ll, mm] == 1:
                        dict.append((kk, ll, mm))

        return dict

    model.Q = Set(dimen=3, initialize=Q_init)

    model.q = Param(model.Q, default=0)

    ####################################
    # VARS
    ####################################

    # ----------
    # EMBEDDING
    # ----------

    # HELPER SET
    def Y_Init(model):

        dict = []

        for kk in model.K:
            for ll in model.VN[kk]:
                for ii in model.N:
                    dict.append((kk, ll, ii))
        return dict

    model.Y = Set(dimen=3, initialize=Y_Init)

    # One if a virtual node ll from request kk is mapped onto physical node ii
    model.y = Var(model.Y, within=Binary)

    model.y_fixed = Param(model.Y, default=0)

    # HELPER SET
    def Z_Init(model):

        dict = []

        for kk in model.K:
            for ll in model.VN[kk]:
                for mm in model.VN[kk]:
                    for (i, j) in model.L:
                        if ll != mm and model.an[kk, ll, mm] == 1:
                            dict.append((kk, ll, mm, i, j))

        return dict

    # {kk in K, jj in VNl[kk], (i,j) in L
    model.Z = Set(dimen=5, initialize=Z_Init)

    model.z = Var(model.Z, within=Binary)

    # ----------
    # INVESTMENT
    # ----------

    # Maximum unitary price
    model.v = Var(within=NonNegativeReals)

    # HELPER SET
    def P_Init(model):

        dict = []

        for k in model.K:
            for (i, j) in model.L:
                dict.append((k, i, j))

        return dict

    # kk in K, (i,j) in L
    model.P = Set(dimen=3, initialize=P_Init)

    # Price paid by VN kk to expand arc (i,j)
    model.p = Var(model.P, within=NonNegativeReals)

    # Capacity added to arc (i,j)
    model.b = Var(model.L, within=NonNegativeReals)


    ####################################
    # HELPER SET
    ####################################

    def massimo_set_init(model, kk):

        retval = []

        for ll in model.VN[kk]:
            for mm in model.VN[kk]:
                if model.an[kk, ll, mm] == 1:
                    retval.append((ll, mm))

        return retval

    model.massimo_set = Set(model.K, dimen=2, initialize=massimo_set_init)

    def mapping_set_Init(model):

        dict = []

        for kk in model.K:
            for ll in model.VN[kk]:
                dict.append((kk, ll))
        return dict

    model.mapping_set = Set(dimen=2, initialize=mapping_set_Init)

    # FLow balance constraints

    def NodesOut_init(model, node):
        retval = []
        for (i, j) in model.L:
            if i == node:
                retval.append(j)
        return retval

    model.NodesOut = Set(model.N, initialize=NodesOut_init)

    def NodesIn_init(model, node):
        retval = []
        for (i, j) in model.L:
            if j == node:
                retval.append(i)
        return retval

    model.NodesIn = Set(model.N, initialize=NodesIn_init)

    def flows_set_Init(model):

        retval = []

        for kk in model.K:
            for ll in model.VN[kk]:
                for mm in model.VN[kk]:
                        for ii in model.N:
                            if model.an[kk, ll, mm] == 1:
                                retval.append((kk,ll,mm,ii))

        return retval

    model.flows_set = Set(dimen=4, initialize=flows_set_Init)

    def arc_capacities_set_Init(model, kk):

        dict = []

        for ll in model.VN[kk]:
            for mm in model.VN[kk]:
                if model.an[kk, ll, mm] == 1:
                    dict.append((ll,mm))

        return dict

    model.arc_capacities_set = Set(model.K, dimen=2, initialize=arc_capacities_set_Init)


    def min_price_set_Init(model, kk):

        dict = []

        for ll in model.VN[kk]:
            for mm in model.VN[kk]:
                if model.an[kk, ll, mm] == 1:
                    dict.append((ll, mm))

        return dict

    model.min_price_set = Set(model.K, dimen=2, initialize=min_price_set_Init)


    ####################################
    # OBJECTIVE FUNCTION
    ####################################

    # Maximize the total number of accepted requests
    def Price(model):
        return model.v
    model.Price = Objective(rule=Price, sense=minimize)

    ####################################
    # CONSTRAINTS
    ####################################

    def massimo(model, kk):
        return model.v >= sum(model.p[kk, i, j] for (i, j) in model.L) / sum(
            model.q[kk, ll, mm] for (ll, mm) in model.massimo_set[kk])
    model.massimo = Constraint(model.K, rule=massimo)

    # ----------
    # EMBEDDING
    # ----------

    # A virtual node assigned to exactly a single physical node
    # (since each VN is accepted in this case)
    def mapping(model, kk, ll):
        return sum(model.y[kk,ll,ii] for ii in model.N) == 1
    model.mapping = Constraint(model.mapping_set, rule=mapping)

    # No co-location of two nodes of the same VN is allowed
    # s.t.nocolocation {kk in K, ii in N}: sum{ll in VN[kk]} y[kk, ll, ii] <= 1;
    def nocolocation(model, kk, ii):
        return sum(model.y[kk, ll, ii] for ll in model.VN[kk]) <= 1
    model.nocolocation = Constraint(model.K, model.N, rule=nocolocation)

    # ----------
    # leaves fixing
    # ----------

    def leaves_fixing(model,kk,ll,ii):
        if model.y_fixed[kk,ll,ii] == 1:
            return model.y[kk,ll,ii] == 1
        else:
            return Constraint.Skip
    model.leaves_fixing = Constraint(model.Y, rule=leaves_fixing)

    def flows(model,kk,ll,mm,ii):
        return sum( model.z[kk,ll,mm,ii,jj] for jj in model.NodesOut[ii]) - sum( model.z[kk,ll,mm,jj,ii] for jj in model.NodesIn[ii]) == (model.y[kk,ll,ii] - model.y[kk,mm,ii])
    model.flows = Constraint(model.flows_set, rule=flows)

    # Arc capacity constraints
    def arc_capacities(model, ii, jj):
        return sum( model.q[kk,ll,mm] * model.z[kk,ll,mm,ii,jj] for kk in model.K for (ll,mm) in model.arc_capacities_set[kk])<= 0.8 * (model.a[ii,jj]+ model.b[ii,jj])
    model.arc_capacities = Constraint(model.L, rule=arc_capacities)

    # ----------
    # INVESTMENT
    # ----------

    # Relation between the investment and its cost
    def investment(model, i, j):
        return model.b[i, j] == sum( (model.p[kk, i, j] / model.c[i, j]) for kk in model.K)
    model.investment = Constraint(model.L, rule=investment)

    def simmetry(model, i, j):
        return model.b[i, j] == model.b[j, i]
    model.simmetry = Constraint(model.L, rule=simmetry)

    # Minimum price
    def minimumpricer(model, kk):
        return 0.1 * sum(model.q[kk,ll,mm] for (ll,mm) in model.min_price_set[kk]) <= sum( model.p[kk, i, j] for (i,j) in model.L)
    model.minimumpricer = Constraint(model.K, rule=minimumpricer)

    return model






