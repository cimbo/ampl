\contentsline {section}{\numberline {1}Sommario}{2}{section.1}
\contentsline {section}{\numberline {2}Introduzione}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}Modello Matematico}{3}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Euristica Two-phase MILP-based}{6}{subsection.2.2}
\contentsline {section}{\numberline {3}Idea Euristica di base}{7}{section.3}
\contentsline {subsection}{\numberline {3.1}Fase 1 Greedy}{7}{subsection.3.1}
\contentsline {section}{\numberline {4}Versioni Randomizzate dell'Euristica di base}{7}{section.4}
\contentsline {subsection}{\numberline {4.1}Euristica Progressiva}{8}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Euristica Progressiva GRASP}{9}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Euristica Selezione manuale}{9}{subsection.4.3}
\contentsline {subsection}{\numberline {4.4}Euristica Selezione Randomizzata (Automatic Random)}{9}{subsection.4.4}
\contentsline {subsection}{\numberline {4.5}Euristica Pseudosmart}{10}{subsection.4.5}
\contentsline {subsection}{\numberline {4.6}Euristica Selezione Range}{11}{subsection.4.6}
\contentsline {section}{\numberline {5}Risultati}{11}{section.5}
\contentsline {subsection}{5.1\nobreakspace {}\nobreakspace {}Tabella Risultati Euristiche}{12}{section*.2}
