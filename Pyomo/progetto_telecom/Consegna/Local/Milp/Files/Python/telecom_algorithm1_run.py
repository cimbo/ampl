# --------------------------------------------------------Header-------------------------------------------------------- #
from pyomo.environ import *
from pyomo.opt import SolverFactory
import time
from pyomo.core.base import *
# Import dei modelli astratti che poi vengono utilizzati nel file principale #
#------- .mod Files (Di Ampl) -------#
from milp_3index_phase1 import generateModel_milp_3index_relaxed
from milp_3index_phase2 import generateModel_milp_3index_fixed_centers

# Import dei file di dati che poi vengono utilizzati nel file principale per creare i modelli concreti #
#------- .dat Files (Di Ampl) -------#
#dat_file_name = '../IstanzeAbilene/abilene_s_t_15.dat'
dat_file_name = '../IstanzeFrance/france_s_t_26.dat'


######################################################################
# HELPER FUNCTIONS
######################################################################

def build_v_parameter(instance):
    v = {}

    for k in instance.K:
        for l in instance.Vfc[k]:
            v[k,l] = instance.y[k,l].value
    return v

# ----------------------------------------------------Main---------------------------------------------------- #
# -----Solver----- #
opt = SolverFactory("gurobi")
opt.options["mipgap"] = 0.0001
#opt.options["timelimit"] = 100

print "# -------------------------------------------------------------------------"
print "# Algorithm 1 Heuristic Procedure: Min-Price-VNE-T-S for Star Telecom VNs "
print "# -------------------------------------------------------------------------"

print "# -------------------------------------------------------------------------"
print "# Phase 1: Solve Partial linear relaxation with splittable flows "
print "# -------------------------------------------------------------------------"

start_time = time.time()

abstract_model = generateModel_milp_3index_relaxed()

instance = abstract_model.create_instance(dat_file_name)

results = opt.solve(instance, tee=True)
#results.write()

print 'V after phase 1: ', instance.Price()
print 'Time Phase 1: ', (time.time() - start_time)

print "# -------------------------------------------------------------------------"
print "# Phase 2: Solve Partial linear relaxation with NON splittable flows "
print "# -------------------------------------------------------------------------"

start_time = time.time()

y = build_v_parameter(instance)
abstract_model_phase_2 = generateModel_milp_3index_fixed_centers(y)

instance_phase_2 = abstract_model_phase_2.create_instance(dat_file_name)

results = opt.solve(instance_phase_2, tee=True)
#results.write()
#instance.y.display()
print 'Objective Value: ', instance_phase_2.Price()
print 'Time Phase 2: ', (time.time() - start_time)
#instance_phase_2.b.display()
#instance.p.display()

print "# ----------------------------------------------------------"

costo_rete = 0

for l in instance.L:
    costo_rete = costo_rete + instance_phase_2.b[l].value * instance_phase_2.c[l]

print "Costo espansione rete Fase 2: ", costo_rete

#instance.display()

