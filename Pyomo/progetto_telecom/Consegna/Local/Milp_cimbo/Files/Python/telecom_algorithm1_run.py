# --------------------------------------------------------Header-------------------------------------------------------- #
from pyomo.environ import *
from pyomo.opt import SolverFactory
import time
from pyomo.core.base import *
# Import dei modelli astratti che poi vengono utilizzati nel file principale #
#------- .mod Files (Di Ampl) -------#
from milp_3index_phase1 import generateModel_milp_3index_relaxed_cimbo
from milp_3index_phase2 import generateModel_milp_3index_fixed_centers_cimbo
from greedy_objective import generateModel_greedy_objective

# Import dei file di dati che poi vengono utilizzati nel file principale per creare i modelli concreti #
#------- .dat Files (Di Ampl) -------#
#dat_file_name = '../IstanzeAbilene/abilene_s_t_15.dat'
dat_file_name = '../IstanzeFrance/france_s_t_26.dat'


######################################################################
# HELPER FUNCTIONS
######################################################################

def build_v_parameter(instance):
    v = {}

    for k in instance.K:
        for l in instance.Vfc[k]:
            v[k,l] = instance.y[k,l].value
    return v

# ----------------------------------------------------Main---------------------------------------------------- #
# -----Solver----- #
opt = SolverFactory("gurobi")
opt.options["mipgap"] = 0.00001
#opt.options["timelimit"] = 100

print "# -------------------------------------------------------------------------"
print "# Algorithm 1 Heuristic Procedure: Min-Price-VNE-T-S for Star Telecom VNs "
print "# -------------------------------------------------------------------------"

print "# -------------------------------------------------------------------------"
print "# Phase 1: Solve Partial linear relaxation with splittable flows "
print "# -------------------------------------------------------------------------"

start_time = time.time()

abstract_model = generateModel_milp_3index_relaxed_cimbo()

instance = abstract_model.create_instance(dat_file_name)

results = opt.solve(instance, tee=True)
#results.write()

print 'Costo espansione rete dopo phase 1: ', instance.Price()
print 'Time Phase 1: ', (time.time() - start_time)

print "# -------------------------------------------------------------------------"
print "# Phase 2: Solve Partial linear relaxation with NON splittable flows "
print "# -------------------------------------------------------------------------"

start_time = time.time()

y = build_v_parameter(instance)
abstract_model_phase_2 = generateModel_milp_3index_fixed_centers_cimbo(y)

instance = abstract_model_phase_2.create_instance(dat_file_name)

results = opt.solve(instance, tee=True)
#results.write()
#instance.y.display()
print 'Costo espansione rete dopo phase 2: ', instance.Price()
print 'Time Phase 2: ', (time.time() - start_time)
#instance.b.display()
#instance.p.display()

#print("--- %s seconds ---" % (time.time() - start_time))

print "# ----------------------------------------------------------"

model_greedy_objective = generateModel_greedy_objective(instance.n, instance.K, instance.c, instance.L, instance.vn, instance.vl, instance.Vf, instance.Vfc, instance.qin, instance.qout, instance.b)

instance_greedy_objective = model_greedy_objective.create_instance()

results = opt.solve(instance_greedy_objective)

print 'V: ', instance_greedy_objective.Price()

#instance.display()

