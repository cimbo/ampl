# --------------------------------------------------------Header-------------------------------------------------------- #
from __future__ import division
from pyomo.environ import *
import random
import time
from pyomo.opt import SolverFactory, SolverStatus, TerminationCondition
from pyomo.core.base import *
# Import dei modelli astratti che poi vengono utilizzati nel file principale #
#------- .mod Files (Di Ampl) -------#
from VNoM import generateModel_VNoM_3index
from milp_3index import generateModel_milp_3index
from milp_3index_phase2 import generateModel_milp_3index_fixed_centers


######################################################################
# HELPER FUNCTIONS
######################################################################

def getKey(item):
    return item[1]

def build_VNs_ordered_vector(instance):
    v = []

    for k in instance.K:
        sum = 0
        for l in instance.VN[k]:
            sum = sum + instance.qin[k,l] + instance.qout[k,l]
        v.append((k,sum))

    v = sorted(v, key=getKey, reverse=True)

    return v

def create_fixed_center(k, center, Vfc):
    param = {}
    for l in Vfc[k]:
        if l == center:
            param[k,l] = 1
        else:
            param[k,l] = 0

    return param

def calculate_used_capacity(k, used_c, inst):

    used_c_param = {}

    for l in inst.L:
        val = sum(inst.qin[kk, im] * inst.zin[kk, im, l].value + inst.qout[kk, im] * inst.zout[kk, im, l].value for kk in
                                inst.K for im in inst.VNl[kk])

        used_c_param[l] = used_c[l] + val

    return used_c_param

def calculate_current_capacity(inst):

    a_param = {}

    for l in inst.L:
        a_param[l] = inst.a[l] + inst.b[l].value

    return a_param

def save_y_assignment_VNk(k, y_param, inst):

    for l in inst.Vfc[k]:
        if inst.y[k,l].value == 1:
            y_param[k,l] = 1
    return y_param

def init_y_assign_param():
    y = {}
    for i in instance.Y:
        y[i] = 0
    return y

def init_used_capacity(L):

    used_c = {}

    for l in L:
        used_c[l] = 0

    return used_c

class unique_element:
    def __init__(self,value,occurrences):
        self.value = value
        self.occurrences = occurrences

def perm_unique(elements):
    eset=set(elements)
    listunique = [unique_element(i,elements.count(i)) for i in eset]
    u=len(elements)
    return perm_unique_helper(listunique,[0]*u,u-1)

def perm_unique_helper(listunique,result_list,d):
    if d < 0:
        yield tuple(result_list)
    else:
        for i in listunique:
            if i.occurrences > 0:
                result_list[d]=i.value
                i.occurrences-=1
                for g in  perm_unique_helper(listunique,result_list,d-1):
                    yield g
                i.occurrences+=1



# ----------------------------------------------------Main---------------------------------------------------- #

# Import dei file di dati che poi vengono utilizzati nel file principale per creare i modelli concreti #
#------- .dat Files (Di Ampl) -------#
#dat_file_name = 'CodiciTelecom/IstanzeAbilene/abilene_s_t_8.dat'
dat_file_name = 'CodiciTelecom/IstanzeFrance/france_s_t_4.dat'

# -----Solver----- #
opt = SolverFactory("gurobi")
opt.options["mipgap"] = 0.01
opt.options["timelimit"] = 150


print "# #######################################################################################################################################"
print "# Solving the exact model to find the optimal solution. We need this information for the Gap estimation"
print "# #######################################################################################################################################\n"

start_time_exact = time.time()

abstract_model = generateModel_milp_3index()

instance = abstract_model.create_instance(dat_file_name)

#results = opt.solve(instance)

print 'Exact Optimal solution: '
print instance.Price()



end_time_exact = float(time.time() - start_time_exact)
print("Time optimal solution: %s \n" % end_time_exact)

#####################################################################
# Parameters
#####################################################################

n = instance.n.value
a = instance.a
c = instance.c
L = instance.L
vn = instance.vn
vl = instance.vl
Vf = instance.Vf
loc = instance.loc
qin = instance.qin
qout = instance.qout

opt = SolverFactory("gurobi")
opt.options["mipgap"] = 0.03

print "# #######################################################################################################################################"
print "# Pure Heuristic No Random: The idea is to fix the center in a greedy way and then, using the phase 2 of the milp, find a solution"
print "# #######################################################################################################################################\n"

start_pure_heuristic_time = time.time()

start_time_phase1_pure_heuristic = time.time()

print "# Phase 1: Now fore each k in K choose the substrate node onto which the substrate center of Gk has to be mapped"

ordered_VNs_vector = build_VNs_ordered_vector(instance)
y = init_y_assign_param()
used_capacity = init_used_capacity(L)
a = instance.a

r = instance.a

while len(ordered_VNs_vector) > 0:

    k = ordered_VNs_vector[0][0]

    model = generateModel_VNoM_3index(n, {k}, a, used_capacity, c, L, vn, vl, Vf, instance.Vfc, loc, qin, qout)
    instance_center = model.create_instance()
    results = opt.solve(instance_center)

    used_capacity = calculate_used_capacity(k, used_capacity, instance_center)

    a = calculate_current_capacity(instance_center)

    y = save_y_assignment_VNk(k, y, instance_center)

    ordered_VNs_vector.remove(ordered_VNs_vector[0])

end_time_phase1_pure_heuristic = float(time.time() - start_time_phase1_pure_heuristic)

print "# Phase 2: Solve Partial linear relaxation with NON splittable flows "

start_time_phase2_pure_heuristic = time.time()

abstract_model_phase_2 = generateModel_milp_3index_fixed_centers(y)
instance_final_pure_heuristic = abstract_model_phase_2.create_instance(dat_file_name)

results = opt.solve(instance_final_pure_heuristic)
print instance_final_pure_heuristic.Price()

end_time_phase2_pure_heuristic = float(time.time() - start_time_phase2_pure_heuristic)

end_time_heuristic_full_greedy = float(time.time() - start_time_phase1_pure_heuristic)

print 'Iteration time: %f ' % end_time_heuristic_full_greedy


print "\n# #######################################################################################################################################"
print 'Beginning of the randomized Part'
print "# #######################################################################################################################################\n"

i = 0

#Number Of Iteration Grasp
n_ite = 2

ordered_VNs_vector = build_VNs_ordered_vector(instance)

best = 0

#In case the phase 1 gives us the same y choice is useless to procede with phase 2
y_pure_heuristic = y

################# RANDOM PRE-SELECTION ################

for ii in range(0,len(ordered_VNs_vector)):
    print (ii + 1),
    print ordered_VNs_vector[ii],
print 'Vector length: %d' % len(ordered_VNs_vector)

print 'Maximum range [1,Vector_length]'
start_range = input("Enter the beginning of the range: \n") - 1
end_range = input("Enter the end of the range: \n")

while start_range < 0 or end_range > len(ordered_VNs_vector):
    print '\nWARNING!: Please, insert a valid range\n'
    print 'Maximum range [1,Vector_length]'
    start_range = input("Enter the beginning of the range: \n") - 1
    end_range = input("Enter the end of the range: \n")

print 'Range: [%d,%d]' % (start_range + 1, end_range)

#Randomize The part of the vector selected [9,7,6,5,{1,2,3,4},0] for exmple i shuffle the ones within {}
copy = ordered_VNs_vector[start_range:end_range]
print copy

permutations_list = list(perm_unique(copy))
print len(permutations_list)
permutations_list.remove(tuple(copy))
random.shuffle(permutations_list)

start_time_randomized_heuristic = time.time()

while i < n_ite:

    print "\n# -------------------------------------------------------------------------"
    print "# Iteration: %d" % (i + 1)
    print "# -------------------------------------------------------------------------\n"

    start_time = time.time()

    start_time_phase1 = time.time()

    print "# Phase 1: Now fore each k in K choose the substrate node onto which the substrate center of Gk has to be mapped"

    ordered_VNs_vector = build_VNs_ordered_vector(instance)
    y = init_y_assign_param()
    used_capacity = init_used_capacity(L)
    a = instance.a

    r = instance.a

    if len(permutations_list) == 0:
        print 'No more available permutations. The Algorithm has to stop!'
        break

    tmp = permutations_list[0]

    permutations_list.remove(tmp)

    # overwrite the original
    ordered_VNs_vector[start_range:end_range] = tmp


    while len(ordered_VNs_vector) > 0:

        k = ordered_VNs_vector[0][0]

        model = generateModel_VNoM_3index(n, {k}, a, used_capacity, c, L, vn, vl, Vf, instance.Vfc, loc, qin, qout)
        instance_center = model.create_instance()
        results = opt.solve(instance_center)

        used_capacity = calculate_used_capacity(k, used_capacity, instance_center)

        a = calculate_current_capacity(instance_center)

        y = save_y_assignment_VNk(k, y, instance_center)

        ordered_VNs_vector.remove(ordered_VNs_vector[0])

    end_time_phase1 = float(time.time() - start_time_phase1)

    #If the phase 1 y assignment is equal to the phase 1 of the pure heuristic go on
    if y == y_pure_heuristic:
        print 'WARNING!:The phase 1 is the same of the pure heuristic, so I can skip to the next permutation. Going on here would be useless'
        i = (i + 1)
        n_ite = n_ite + 1
        continue

    print "# Phase 2: Solve Partial linear relaxation with NON splittable flows "

    start_time_phase2 = time.time()

    abstract_model_phase_2 = generateModel_milp_3index_fixed_centers(y)
    instance_final = abstract_model_phase_2.create_instance(dat_file_name)

    results = opt.solve(instance_final)
    print "\n# Objective Iteration: %d" % (i + 1)
    print instance_final.Price()

    if best == 0:
        best = instance_final
    else:
        if best.Price() > instance_final.Price():
            best = instance_final

    end_time_phase2 = float(time.time() - start_time_phase2)

    end_time_heuristic = float(time.time() - start_time)

    print '\nIteration time: %f ' % end_time_heuristic

    i = (i + 1)

    ############## END ITERATION ################################

end_time_randomized_heuristic = float(time.time() - start_time_randomized_heuristic)

print '\n\n############## RESULTS ##############\n'

#exact_optimum = instance.Price()
exact_optimum = 477.91

print 'Exact optimal solution'
print instance.Price()
print 'Exact execution time'
print end_time_exact

print '\nSolution found with the Pure heuristic'
print instance_final_pure_heuristic
print '\nPure Heuristic execution time'
print instance_final_pure_heuristic.Price()
print 'Phase1 Time: '
print end_time_phase1_pure_heuristic
print 'Phase2 Time: '
print end_time_phase2_pure_heuristic
print 'Phase1 + Phase2 Time: '
print end_time_heuristic_full_greedy
print '\nGap%'
print ((instance_final_pure_heuristic.Price() * 100) / exact_optimum) - 100


print '\nBest Solution found with the Randomized heuristic'
print instance_final_pure_heuristic
print '\nRandomized Heuristic total execution time'
print end_time_randomized_heuristic

print '\nGap%'
if best != 0:
    print ((best.Price() * 100) / exact_optimum) - 100
else:
    print 'No alternative solution in the randomized version was found'


