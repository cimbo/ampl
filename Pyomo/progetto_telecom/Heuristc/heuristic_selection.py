# --------------------------------------------------------Header-------------------------------------------------------- #
from __future__ import division
import random
import time
import sys
from pyomo.opt import SolverFactory
# Import dei modelli astratti che poi vengono utilizzati nel file principale #
#------- .mod Files (Di Ampl) -------#
from VNoM import generateModel_VNoM_3index
from milp_3index import generateModel_milp_3index
from milp_3index_phase2 import generateModel_milp_3index_fixed_centers

######################################################################
# CONSTANTS
######################################################################

MAX_INDICES_FOR_FULL_PERMUTATION = 8

RANGE_SELECTION = 1
MANUAL_SELECTION = 2
AUTOMATIC_SELECTION = 3
PSEUDOSMART_SELECTION = 4

OPTIONS = [RANGE_SELECTION, MANUAL_SELECTION, AUTOMATIC_SELECTION, PSEUDOSMART_SELECTION]


######################################################################
# HELPER FUNCTIONS
######################################################################

def getKey(item):
    return item[1]
'''
This helper function given a model with the K Virtual Networks construct a vector with tuples (K, total_demand),
ordered for decreasing values of total_demand
'''
def build_VNs_ordered_vector(instance):
    v = []

    for k in instance.K:
        sum = 0
        for l in instance.VN[k]:
            sum = sum + instance.qin[k,l] + instance.qout[k,l]
        v.append((k,sum))

    v = sorted(v, key=getKey, reverse=True)

    return v

'''
This helper function update the amount of used capacity durning the first phase of the heuristic
'''
def calculate_used_capacity(k, used_c, inst):

    used_c_param = {}

    for l in inst.L:
        val = sum(inst.qin[kk, im] * inst.zin[kk, im, l].value + inst.qout[kk, im] * inst.zout[kk, im, l].value for kk in
                                inst.K for im in inst.VNl[kk])

        used_c_param[l] = used_c[l] + val

    return used_c_param
'''
This helper function keeps updated the sum of the defaut capacity plus the one related to the expansion of the network
'''
def calculate_current_capacity(inst):

    a_param = {}

    for l in inst.L:
        a_param[l] = inst.a[l] + inst.b[l].value

    return a_param
'''
This helper function save the decisons related to where to put the center of a VN k established in the first phase of the heuristic.
This parameter will be passed in phase 2
'''
def save_y_assignment_VNk(k, y_param, inst):

    for l in inst.Vfc[k]:
        if inst.y[k,l].value == 1:
            y_param[k,l] = 1
    return y_param
'''
This helper function just initialize the parameter Y where to save the decision that will be taken in phase 1
'''
def init_y_assign_param():
    y = {}
    for i in instance.Y:
        y[i] = 0
    return y
'''
This helper function just initialize the parameter related the used capacity durning the first phase
'''
def init_used_capacity(L):

    used_c = {}

    for l in L:
        used_c[l] = 0

    return used_c
'''
The following 3 definitions are needed to create the set of permutations for the index to be shuffle (In the randomized part)
'''
class unique_element:
    def __init__(self,value,occurrences):
        self.value = value
        self.occurrences = occurrences

def perm_unique(elements):
    eset=set(elements)
    listunique = [unique_element(i,elements.count(i)) for i in eset]
    u=len(elements)
    return perm_unique_helper(listunique,[0]*u,u-1)

def perm_unique_helper(listunique,result_list,d):
    if d < 0:
        yield tuple(result_list)
    else:
        for i in listunique:
            if i.occurrences > 0:
                result_list[d]=i.value
                i.occurrences-=1
                for g in  perm_unique_helper(listunique,result_list,d-1):
                    yield g
                i.occurrences+=1

'''
This helper function check that the selected indices are available
'''
def check_list_indices(size, list):
    for i in list:
        if i <= 0 or i > size:
            return False
    return True

'''
This helper function retrives the tuples from the ordered vector according to the choosen indices
'''
def get_selected_tuples(vector, indices):
    array = []
    for i in indices:
        array.append(vector[i - 1])
    return array
'''
This helper function print in a nice way the ordered vector (K VN)
'''
def print_ordered_vector(ordered_VNs_vector):
    print 'Ordered Vector:'
    for ii in range(0, len(ordered_VNs_vector)):
        print (ii + 1),
        print ordered_VNs_vector[ii],
    print '\nVector length: %d' % len(ordered_VNs_vector)

'''
This helper function update the ordered vector with the current permutation to explore
'''
def perform_index_permutation(ordered_VNs_vector, list_indices, permutation):
    count = 0
    for i in list_indices:
        ordered_VNs_vector[i - 1] = permutation[count]
        count = count + 1

    return ordered_VNs_vector

'''
This helper function prints the execution's options for the randomized part
'''
def print_options():
    choice = input("Choose one the available selection mode:\n1) Range Selection \n2) Manual Selection\n3) Automatic Pure Random Selection specifying the size\n4) Automatic Pseudosmart Selection \n")

    while choice not in OPTIONS:
        choice = input("\nWARNING: WRONG SELECTION!\nChoose one the available selection mode:\n1) Range Selection \n2) Manual Selection\n3) Automatic Pure Random Selection specifying the size\n4) Automatic Pseudosmart Selection \n")

    return choice

'''
This helper function retrives range decided by the user
'''
def init_range_selection():
    print 'Maximum range [1,Vector_length]'
    start_range = input("Enter the beginning of the range: \n") - 1
    end_range = input("Enter the end of the range: \n")

    while start_range < 0 or end_range > len(ordered_VNs_vector):
        print '\nWARNING!: Please, insert a valid range\n'
        print 'Maximum range [1,Vector_length]'
        start_range = input("Enter the beginning of the range: \n") - 1
        end_range = input("Enter the end of the range: \n")

    print 'Range: [%d,%d]' % (start_range + 1, end_range)

    return range(start_range + 1, end_range + 1)

'''
This helper function retrives indices manually decided by the user
'''
def init_manual_selection():
    print '\nSet of available indices is the one between: [1,Vector_length]'
    list_indices = [int(x) for x in raw_input('Enter the list of indices that you want to permutate\n').split()]

    list_indices = list(set(list_indices))
    print list_indices
    list_ok = check_list_indices(len(ordered_VNs_vector), list_indices)

    while not (list_ok):
        print '\nWARNING!: Please, insert a valid list of indices\n'
        print_ordered_vector(ordered_VNs_vector)
        print '\nSet of available indices is the one between: [1,Vector_length]'
        list_indices = [int(x) for x in raw_input('Enter the list of indices that you want to permutate\n').split()]
        list_indices = list(set(list_indices))
        list_ok = check_list_indices(len(ordered_VNs_vector), list_indices)

    print 'Indices list:',
    print list_indices
    return list_indices

'''
This helper function retrives number of indices decided by the user (indices that will be randomized)
'''
def get_number_of_selections(max_size):
    choice = input(
        "Please, Enter how may k would you like to permutate without exceding the maximum size: \n")

    while choice <= 0 or choice > max_size:
        choice = input(
            "\nWARNING: WRONG SELECTION!\nPlease, Enter how may k would you like to permutate without exceding the maximum size: \n")
    return choice

'''
This helper function, starting from the number of indices decided by the user, retrives n random indices
'''
def init_automatic_selection(ordered_VNs_vector):

    max_size = len(ordered_VNs_vector)

    number_of_selections = get_number_of_selections(max_size)
    list_indices = random.sample(range(1, max_size + 1), number_of_selections)

    print list_indices

    return list_indices

'''
This helper function represents the core of the pseudosmart selection.
Here are defined all the step to chose the best set indices to be randomized
'''
def init_automatic_pseudosmart_selection():

    list_arches = []

    for l in instance_final_pure_heuristic.L:
        if instance_final_pure_heuristic.b[l].value > 0:
            list_arches.append(l)

    #List of k that are related to the expansion of the network
    list_k_overflow = []

    for a in list_arches:
        for k in instance_final_pure_heuristic.K:
            for f in instance_final_pure_heuristic.VNl[k]:
                if instance_final_pure_heuristic.zin[k, f, a].value > 0 or instance_final_pure_heuristic.zout[
                    k, f, a].value > 0:
                    list_k_overflow.append(k)

    list_k_overflow = list(set(list_k_overflow))
    list_k_overflow = random.sample(range(1, len(list_k_overflow) + 1), int(MAX_INDICES_FOR_FULL_PERMUTATION/2))

    print 'list_k_overflow'
    print list_k_overflow

    list_k_no_overflow = []
    #list of k that are not related to the expansion of the network
    for k in instance.K:
        if k not in list_k_overflow:
            list_k_no_overflow.append(k)
    list_k_no_overflow = random.sample(range(1, len(list_k_no_overflow) + 1), int(MAX_INDICES_FOR_FULL_PERMUTATION/2))
    print 'list_k_no_overflow'
    print list_k_no_overflow

    k_work_set = list(set().union(list_k_overflow, list_k_no_overflow))

    work_set = []

    for ii in range(0, len(ordered_VNs_vector)):
        if ordered_VNs_vector[ii][0] in k_work_set:
            if ii + 1 != 1:
                work_set.append(ii + 1)

    print 'The pseudo smart selection will work on the following indieces'
    print work_set

    return work_set


'''
This helper function save the best solution found so far durning the heuristic's execution
'''
def check_if_best_solution_so_far(best, instance_final):
    if best == 0:
        best = instance_final
    else:
        if best.Price() > instance_final.Price():
            best = instance_final

    return best

def get_permutation_list(selected_elements, n_ite):
    copy = list(selected_elements)

    #If branch when is possible to calculate all the permutations
    if len(selected_elements) <= MAX_INDICES_FOR_FULL_PERMUTATION:
        return list(perm_unique(selected_elements))
    else:
        list_random_permutations = []
        for i in range(0, n_ite):
            random.shuffle(copy)
            list_random_permutations.append(tuple(copy))
        list_random_permutations.append(tuple(selected_elements))
        return list_random_permutations



# ----------------------------------------------------Main---------------------------------------------------- #

# Import dei file di dati che poi vengono utilizzati nel file principale per creare i modelli concreti #
#------- .dat Files (Di Ampl) -------#
#dat_file_name = 'CodiciTelecom/IstanzeAbilene/abilene_s_t_8.dat'
dat_file_name = 'CodiciTelecom/IstanzeFrance/france_s_t_30.dat'

# -----Solver----- #
opt = SolverFactory("gurobi")
opt.options["mipgap"] = 0.01
opt.options["timelimit"] = 150


print "# #######################################################################################################################################"
print "# Solving the exact model to find the optimal solution. We need this information for the Gap estimation"
print "# #######################################################################################################################################\n"

start_time_exact = time.time()

abstract_model = generateModel_milp_3index()

instance = abstract_model.create_instance(dat_file_name)

#results = opt.solve(instance)

print 'Exact Optimal solution: '
print instance.Price()



end_time_exact = float(time.time() - start_time_exact)
print("Time optimal solution: %s \n" % end_time_exact)

#####################################################################
# Parameters
#####################################################################

n = instance.n.value
a = instance.a
c = instance.c
L = instance.L
vn = instance.vn
vl = instance.vl
Vf = instance.Vf
loc = instance.loc
qin = instance.qin
qout = instance.qout

opt = SolverFactory("gurobi")
opt.options["mipgap"] = 0.03

print "# #######################################################################################################################################"
print "# Pure Heuristic No Random: The idea is to fix the center in a greedy way and then, using the phase 2 of the milp, find a solution"
print "# #######################################################################################################################################\n"

start_pure_heuristic_time = time.time()

start_time_phase1_pure_heuristic = time.time()

print "# Phase 1: Now fore each k in K choose the substrate node onto which the substrate center of Gk has to be mapped"

ordered_VNs_vector = build_VNs_ordered_vector(instance)
y = init_y_assign_param()
used_capacity = init_used_capacity(L)
a = instance.a

r = instance.a

while len(ordered_VNs_vector) > 0:

    k = ordered_VNs_vector[0][0]

    model = generateModel_VNoM_3index(n, {k}, a, used_capacity, c, L, vn, vl, Vf, instance.Vfc, loc, qin, qout)
    instance_center = model.create_instance()
    results = opt.solve(instance_center)

    used_capacity = calculate_used_capacity(k, used_capacity, instance_center)

    a = calculate_current_capacity(instance_center)

    y = save_y_assignment_VNk(k, y, instance_center)

    ordered_VNs_vector.remove(ordered_VNs_vector[0])

end_time_phase1_pure_heuristic = float(time.time() - start_time_phase1_pure_heuristic)

print "# Phase 2: Solve Partial linear relaxation with NON splittable flows "

start_time_phase2_pure_heuristic = time.time()

abstract_model_phase_2 = generateModel_milp_3index_fixed_centers(y)
instance_final_pure_heuristic = abstract_model_phase_2.create_instance(dat_file_name)

results = opt.solve(instance_final_pure_heuristic)
print instance_final_pure_heuristic.Price()

end_time_phase2_pure_heuristic = float(time.time() - start_time_phase2_pure_heuristic)

end_time_heuristic_full_greedy = float(time.time() - start_time_phase1_pure_heuristic)

print 'Iteration time: %f ' % end_time_heuristic_full_greedy

print "\n# #######################################################################################################################################"
print 'Beginning of the randomized Part'
print "# #######################################################################################################################################\n"

i = 0

#Number Of Iteration Grasp
n_ite = 5

ordered_VNs_vector = build_VNs_ordered_vector(instance)

best = 0

#In case the phase 1 gives us the same y choice is useless to procede with phase 2
y_pure_heuristic = y

################# RANDOM PRE-SELECTION ################

choice = print_options()


print_ordered_vector(ordered_VNs_vector)

if choice == RANGE_SELECTION:
    list_indices = init_range_selection()
elif choice == MANUAL_SELECTION:
    list_indices = init_manual_selection()
elif choice == AUTOMATIC_SELECTION:
    list_indices = init_automatic_selection(ordered_VNs_vector)
elif choice == PSEUDOSMART_SELECTION:
    list_indices = init_automatic_pseudosmart_selection()
else:
    print 'No other options are Available!'
    sys.exit()


#Randomize The part of the vector selected [9,7,6,5,{1,2,3,4},0] for exmple i shuffle the ones within {}
selected_elements = get_selected_tuples(ordered_VNs_vector, list_indices)

print 'selected_elements'
print selected_elements

permutations_list = get_permutation_list(selected_elements, n_ite)
#To remove the standard case with all decreasing demands
permutations_list.remove(tuple(selected_elements))
random.shuffle(permutations_list)

start_time_randomized_heuristic = time.time()

while i < n_ite:

    print "\n# -------------------------------------------------------------------------"
    print "# Iteration: %d" % (i + 1)
    print "# -------------------------------------------------------------------------\n"

    start_time = time.time()

    start_time_phase1 = time.time()

    print "# Phase 1: Now fore each k in K choose the substrate node onto which the substrate center of Gk has to be mapped"

    opt = SolverFactory("gurobi")
    opt.options["mipgap"] = 0.10

    ordered_VNs_vector = build_VNs_ordered_vector(instance)
    y = init_y_assign_param()
    used_capacity = init_used_capacity(L)
    a = instance.a

    r = instance.a

    if len(permutations_list) == 0:
        print 'No more available permutations. The Algorithm has to stop!'
        break

    tmp = permutations_list[0]

    permutations_list.remove(tmp)

    # overwrite the original
    ordered_VNs_vector = perform_index_permutation(ordered_VNs_vector, list_indices, tmp)


    while len(ordered_VNs_vector) > 0:

        k = ordered_VNs_vector[0][0]

        model = generateModel_VNoM_3index(n, {k}, a, used_capacity, c, L, vn, vl, Vf, instance.Vfc, loc, qin, qout)
        instance_center = model.create_instance()
        results = opt.solve(instance_center)

        used_capacity = calculate_used_capacity(k, used_capacity, instance_center)

        a = calculate_current_capacity(instance_center)

        y = save_y_assignment_VNk(k, y, instance_center)

        ordered_VNs_vector.remove(ordered_VNs_vector[0])

    end_time_phase1 = float(time.time() - start_time_phase1)

    #If the phase 1 y assignment is equal to the phase 1 of the pure heuristic go on
    if y == y_pure_heuristic:
        print 'WARNING!:The phase 1 is the same of the pure heuristic, so I can skip to the next permutation. Going on here would be useless'
        i = (i + 1)
        n_ite = n_ite + 1
        continue

    print "# Phase 2: Solve Partial linear relaxation with NON splittable flows "

    opt = SolverFactory("gurobi")
    opt.options["mipgap"] = 0.03

    start_time_phase2 = time.time()

    abstract_model_phase_2 = generateModel_milp_3index_fixed_centers(y)
    instance_final = abstract_model_phase_2.create_instance(dat_file_name)

    results = opt.solve(instance_final)
    print "\n# Objective Iteration: %d" % (i + 1)
    print instance_final.Price()

    best = check_if_best_solution_so_far(best, instance_final)

    end_time_phase2 = float(time.time() - start_time_phase2)

    end_time_heuristic = float(time.time() - start_time)

    print '\nIteration time: %f ' % end_time_heuristic

    i = (i + 1)

    ############## END ITERATION ################################

end_time_randomized_heuristic = float(time.time() - start_time_randomized_heuristic)

print '\n\n############## RESULTS ##############\n'

#exact_optimum = instance.Price()
exact_optimum = 695.55

print 'Exact optimal solution'
print instance.Price()
print 'Exact execution time'
print end_time_exact

print '\nSolution found with the Pure heuristic'
print instance_final_pure_heuristic
print '\nPure Heuristic execution time'
print instance_final_pure_heuristic.Price()
print 'Phase1 Time: '
print end_time_phase1_pure_heuristic
print 'Phase2 Time: '
print end_time_phase2_pure_heuristic
print 'Phase1 + Phase2 Time: '
print end_time_heuristic_full_greedy
print '\nGap%'
print ((instance_final_pure_heuristic.Price() * 100) / exact_optimum) - 100


print '\nBest Solution found with the Randomized heuristic'
print best.Price()
print '\nRandomized Heuristic total execution time'
print end_time_randomized_heuristic

print '\nGap%'
if best != 0:
    print ((best.Price() * 100) / exact_optimum) - 100
else:
    print 'No alternative solution in the randomized version was found'


